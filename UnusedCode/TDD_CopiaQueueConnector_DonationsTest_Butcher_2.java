//package com.foodcloud.integration.copia.loaddriver.test;
//
//import java.io.*;
//import java.time.ZonedDateTime;
//import java.time.format.DateTimeFormatter;
//import java.util.*;
//import java.util.concurrent.*;
//import java.util.function.BiFunction;
//import java.util.stream.IntStream;
//import java.util.stream.Stream;
//import com.google.common.collect.Maps;
//import com.google.common.collect.Sets;
//import org.json.JSONObject;
//import org.testng.Assert;
//import org.testng.annotations.Test;
//import com.foodcloud.integration.copia.loaddriver.CopiaQueueConnector;
//import com.foodcloud.test.server.FCTestJsonUtils;
//import com.foodcloud.test.server.FCTestUtils;
//import com.rabbitmq.client.*;
//import com.rabbitmq.client.AMQP.BasicProperties;
//
//public class TDD_CopiaQueueConnector_DonationsTest_Butcher_2 {
//
//	/**
//	 * For a given test, we can use these to build our results
//	 * We should probably have a way to isolate these per test though.
//	 */
////	private ConcurrentHashMap<String, JSONObject> input = new ConcurrentHashMap<>();
////	private ConcurrentHashMap<String, JSONObject> output = new ConcurrentHashMap<>();
//
//
//	/**
//	 * Wraps a parallel execution and performs some logging + resource allocation
//	 *
//	 * @param segmentName 	Name for this particular segment.  Useful for logging.
//	 * @param segmentRunner A function containing the execution you would like to run.  You can assume that the
//	 *                      connection object which the lambda is given is initialized and disposed of safely.
//	 *
//	 * @return Each Callable will return the final result of the lambda.
//	 */
//	private Callable<Optional<ConcurrentHashMap<String, JSONObject>>>
//		inParallel(
//				String segmentName,
//				BiFunction<Connection, Integer,
//						BiFunction<Timeout, String, Optional<ConcurrentHashMap<String, JSONObject>>>> segmentRunner,
//				String uri,
//				int numIterations,
//				Timeout waiter) {
//
//		return () -> {
//			try (Connection connection = CopiaQueueConnector.createAMPConnection(uri)) {
//				System.out.println(segmentName + " ~ Process >>" + " EXECUTION STARTING.");
//				Optional<ConcurrentHashMap<String, JSONObject>> segmentResult =
//						segmentRunner.apply(connection, numIterations).apply(waiter, segmentName);
//				System.out.println(segmentName + " ~ Process >>" + " EXECUTION FINISHED.");
//				return segmentResult;
//			}
//		};
//	}
//
//    /**
//     * Runs a list of async executions and assumes control of a semaphore permit for each async op.
//     * When each execution is finished, the semaphore permits are returned.  This effectively blocks
//	 * until all of our async ops are run.
//     *
//     * @param testSegments The test operations which are required to be run parallel.
//     */
//	private void parallelTest(List<Callable<Optional<ConcurrentHashMap<String, JSONObject>>>> testSegments,
//							  Timeout withinTime,
//							  ExecutorService onExecutor) throws Exception {
//
//		Semaphore synchronizeOn = new Semaphore(testSegments.size());
//
//		try { onExecutor.invokeAll(testSegments).stream().forEach(
//			task -> {
//				try {
//					synchronizeOn.tryAcquire(1, withinTime.amount, withinTime.unit);
//					task.get();
//				} catch (InterruptedException | ExecutionException e) {
//					e.printStackTrace();
//				} finally {
//					synchronizeOn.release();
//				}
//			});
//		} catch (Exception e){ e.printStackTrace(); }
//
//		synchronizeOn.tryAcquire(testSegments.size(), withinTime.amount, withinTime.unit);
//
//	}
//
//
//	synchronized private void notifyComplete(Semaphore sync) { sync.release(); }
//
//    /**
//     * Function which will do the job of reading from the queue.
//	 * To prevent program termination before all expected messages are consumed,
//	 * an isolate semaphore is initialized to the size of the input.
//	 *
//	 * Make sure to perform bounded waiting, otherwise a deadlock might occur in the test.
//     *
//	 * @param con Connection to a live rabbitmq instance.
//     * @return A map of donation_id's to the responses which came out.
//     */
//	private BiFunction<Timeout, String, Optional<ConcurrentHashMap<String, JSONObject>>> reader(Connection con, int numIterations) {
//		return (Timeout withinTime, String segmentName) -> {
//
//			Semaphore amountToRun = new Semaphore(numIterations);
//
//			try {
//				amountToRun.tryAcquire(numIterations, withinTime.amount, withinTime.unit);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//			try {
//
//				Channel testChannel = con.createChannel();
//				testChannel.exchangeDeclare("event-bus", "topic", true);
//				testChannel.queueDeclare("donations-update", true, false, false, new HashMap<>());
//				testChannel.basicConsume("donations-update", new DefaultConsumer(testChannel) {
//
//					@Override
//					public void handleDelivery(String consumerTag,
//											   Envelope envelope,
//											   AMQP.BasicProperties properties,
//											   byte[] body) throws IOException {
//
//						long deliveryTag = envelope.getDeliveryTag();
//						String message = new String(body, "UTF-8");
//						JSONObject obj = new JSONObject(message);
//						TDD_CopiaQueueConnector_DonationsTest_Butcher_2.this.output.put(obj.getString("donationId"), obj);
//						System.out.println(
//							segmentName + ": Reading -> " +
//									TDD_CopiaQueueConnector_DonationsTest_Butcher_2.this.output.size() + "/" + numIterations
//						);
//						this.getChannel().basicAck(deliveryTag, false);
//						notifyComplete(amountToRun);
//					}
//
//				});
//
//				/* Wait for test completion */
//				try {
//					amountToRun.tryAcquire(numIterations, withinTime.amount, withinTime.unit);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			return Optional.empty();
//		};
//	}
//
//	private JSONObject generateTestEvent() {
//		Map<String, String> donationData = new HashMap<>();
//		donationData.put("type", "OfferedDonation");
//		donationData.put("donationId", FCTestJsonUtils.generateUUID().toString());
//		donationData.put("org_id", "13574");
//		donationData.put("donorName", "Ankara Spice Store 1");
//		ZonedDateTime currentDateTime = FCTestUtils.convertToTimeZonedDate(FCTestUtils.getUTCDate(),
//				FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));
//		donationData.put("formattedDateTime", currentDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
//		donationData.put("donorGroupCode", "FC");
//		donationData.put("donorGroupNumber", "90001");
//		donationData.put("foodCategory", FCTestJsonUtils.getRandomFCFoodCategory());
//		donationData.put("foodQuantity", Integer.toString(FCTestJsonUtils.getRandomInt(1, 19)));
//		return FCTestJsonUtils.createJsonDonationForPlaza(donationData);
//	}
//
//
//    /**
//     * Process to do the task of writing to the queue.
//     *
//     * @param con Live rabbitmq connection
//     */
//	private BiFunction<Timeout, String, Optional<ConcurrentHashMap<String, JSONObject>>> writer(
//			Connection con,
//			int numIterations
//	) {
//		return (Timeout withDelay, String segmentName) -> {
//			try {
//				IntStream.range(0, numIterations).forEach(n -> {
//
//					try {
//						BasicProperties.Builder props = new BasicProperties.Builder();
//						props = props.type("org.foodcloud.domain.share.OfferedDonation");
//						props = props.contentType("application/json");
//						props = props.timestamp(new Date());
//						Channel testChannel = con.createChannel();
//						testChannel.exchangeDeclare("event-bus", "topic", true);
//						testChannel.queueDeclare("donations-post", true, false, false, new HashMap<>());
//						testChannel.queueBind("donations-post", "event-bus", "events");
//						JSONObject generatedInput = generateTestEvent();
//						this.input.put(generatedInput.getString("donationId"), generatedInput);
//						System.out.println(segmentName + ": Writing " + (n + 1) + "/" + numIterations);
//
//						testChannel.basicPublish(
//								"event-bus",
//								"events",
//								props.build(),
//								generatedInput.toString().getBytes()
//						);
//
//						withDelay.hangBack();
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//
//				});
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			return Optional.empty();
//		};
//	}
//
////	/**
////	 * Utility class for controlling some timing related ops.
////	 */
////	private static abstract class Timeout {
////
////		int amount;
////		TimeUnit unit;
////
////		/** Call to execute a wait **/
////		public abstract void hangBack() throws InterruptedException;
////
////		static class Constant extends Timeout {
////			Constant(final int amount, final TimeUnit unit) {
////				this.amount = amount;
////				this.unit = unit;
////			}
////			@Override
////			public void hangBack() throws InterruptedException {
////				this.unit.sleep(this.amount);
////			}
////		}
////
////		static class Jittery extends Timeout {
////
////			/** factor by which to blur the wait **/
////			int jitterFactor;
////
////			Random randomSeed = new Random();
////
////			Jittery(final int amount, final TimeUnit unit, int jitterFactor) {
////				this.jitterFactor = jitterFactor;
////				this.amount = amount;
////				this.unit = unit;
////			}
////
////			@Override
////			public void hangBack() throws InterruptedException {
////				int maxWait = this.amount + this.jitterFactor;
////				int minWait = this.amount - this.jitterFactor;
////				int randomWait = randomSeed.nextInt(maxWait - minWait) + minWait;
////				this.unit.sleep(randomWait);
////			}
////		}
////	}
//
//	@Test(priority = 1)
//	public void writeReadLoop() throws Exception {
//
//		/* Params */
//		String uri = "amqp://guest:guest@localhost:5672";
//		int tps = 5; // Transactions per second (approx)
//		int numDonations = 1000;
//		int runWithinTimeMins = 10;
//		/* End Params */
//
//		int numIterationsPerWriter = numDonations / tps;
//		ExecutorService ex = Executors.newFixedThreadPool(tps + 1); // + 1 for the reader thread
//
//		Timeout maximumWait = new Timeout.Constant(runWithinTimeMins, TimeUnit.MINUTES);
//		Timeout betweenWrites = new Timeout.Jittery(1000, TimeUnit.MILLISECONDS, 500);
//
//		List<Callable<Optional<ConcurrentHashMap<String, JSONObject>>>> writerThreads = new ArrayList<>();
//
//		IntStream.range(0, tps).forEach( n -> {
//			writerThreads.add(
//				inParallel("Queue Writer " + n, this::writer, uri, numIterationsPerWriter, betweenWrites)
//			);
//		});
//
//		ArrayList<Callable<Optional<ConcurrentHashMap<String, JSONObject>>>> testSegments = new ArrayList<>();
//
//		testSegments.add(
//			inParallel("Queue Read", this::reader, uri, numDonations, maximumWait)
//		);
//
//		testSegments.addAll(writerThreads);
//
//		try {
//			parallelTest(testSegments, maximumWait, ex);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		} finally {
//			System.out.println("Key diff between input/output -> " + Sets.difference(input.keySet(), output.keySet()));
//			System.out.println("Map diff between input/output -> " + Maps.difference(input, output));
//			System.out.println("Input was -> " + input);
//			System.out.println("Output was -> " + output);
//		}
//
//		Assert.assertTrue(
//			Stream.of(
//				this.input.size() == this.output.size(),
//				Collections.list(this.input.keys()).stream().allMatch( id -> this.output.containsKey(id))
//			).allMatch(asExpected -> asExpected)
//		);
//	}
//
//	
//	
//}
