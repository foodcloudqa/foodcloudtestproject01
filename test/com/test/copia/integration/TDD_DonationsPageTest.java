package com.test.copia.integration;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.FileNotFoundException;
import com.foodcloud.model.pages.DashboardPage;
import com.foodcloud.model.pages.DonationsPage;
import com.foodcloud.model.pages.LoginPage;
import com.foodcloud.model.pages.OptionMenu.MENU_ITEMS;
import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;

/**
 * 
 * Trial page :) 
 * Need a way to extract data from file (custom file) 
 * Need a way to  :  data-driven testing (with @DataProvider and/or XML configuration).
 */
public class TDD_DonationsPageTest {

	FCTestServer server;
	private String URL = "http://copia-c.herokuapp.com/";
	private FCTestNavigator nav; 
	
	@BeforeClass
	public void setupBeforeClass() throws FileNotFoundException {
		
	nav = new FCTestNavigator();
	nav.setup();	
	server = nav.getServer();
		
	server.openURL(URL);

	}
	
	/**
	 */
	@Test ( priority = 1 )
	public void DonationPage_LoginAsAdmin() {

	String username = "im.admin@foodcloud.ie"; 
	String password = "copiate2015";
	String adminTitleLocator = "//div[@class='copia-top row']";
	
	performLogin(username, password, adminTitleLocator);
				
	DashboardPage dashPage = new DashboardPage(nav);
	dashPage.getMenu().clickItem(MENU_ITEMS.DONATIONS);
		
	DonationsPage donationsPage = new DonationsPage(nav);
	server.waitForActionToComplete();
	donationsPage.waitUntilLoaded();
	
	boolean isTitlePresent = server.isElementPresent("//h2/span[text()='Donations']");
	
	donationsPage.getMenu().clickItem(MENU_ITEMS.ADMIN_LOGOUT);
	//	getmenu= give all menus 
	// Dashboard Support Tickets Donations Collections Charities Donors Schedules Users Actions
	// PageType= admin 
	// number of rows (enough)
	// normal user = 1 row 			
	// 	donationsPage.getMenu().
	
	Assert.assertTrue(isTitlePresent, "Error: Donations title missing in Donations page Admin");
}	

	@Test ( priority = 2 )
	public void DonationPage_LoginAsStoreUser() {

	String username = "4045@uk.tesco.com";
	String password = "12345";
	String userTitleLocator = "//div[@class='span12 org-header']";

	performLogin(username, password, userTitleLocator);
				
	DashboardPage dashPage = new DashboardPage(nav);
	dashPage.getMenu().clickItem(MENU_ITEMS.DONATIONS);
		
	DonationsPage donationsPage = new DonationsPage(nav);
	server.waitForActionToComplete();
	donationsPage.waitUntilLoaded();
	
	boolean isStoreUserTitlePresent = server.isElementPresent("//div[@ng-show='showDonations']/descendant::div[contains(@class, 'store-header')]/a");
	
	donationsPage.getMenu().clickItem(MENU_ITEMS.ADMIN_LOGOUT);

	// normal user = 1 row 			
	
	Assert.assertTrue(isStoreUserTitlePresent, "Error: Donations title missing in Donations page Admin");
}	
	
	
	private void performLogin(String username, String password, String titleLocator) {
		LoginPage loginPage = new LoginPage(nav);
		loginPage.setLoginField(username);
		loginPage.setPasswordField(password);
		loginPage.clickSubmit();
		
		server.waitForElement(titleLocator);			

	}


		
	@AfterClass	
	public void tearDown(){
		server.getDriver().close();
	}
	
}
