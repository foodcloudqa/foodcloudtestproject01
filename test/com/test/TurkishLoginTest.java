package com.test;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.*;
import org.testng.Assert;
import java.io.FileNotFoundException;

import com.foodcloud.model.pages.CharitiesPage;
import com.foodcloud.model.pages.CollectionsPage;
import com.foodcloud.model.pages.DashboardPage;
import com.foodcloud.model.pages.DonationsPage;
import com.foodcloud.model.pages.DonorsPage;
import com.foodcloud.model.pages.LoginPage;
import com.foodcloud.model.pages.OptionMenu.MENU_ITEMS;
import com.foodcloud.model.pages.SchedulesPage;
import com.foodcloud.model.pages.SearchMenu;
import com.foodcloud.model.pages.SupportTicketsPage;
import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;

/**
 * 
 * Trial page :) 
 * Need a way to extract data from file (custom file) 
 * Need a way to  :  data-driven testing (with @DataProvider and/or XML configuration).
 */
public class TurkishLoginTest {

	FCTestServer server;
//	private String Url = "http://copiarms.herokuapp.com/";
	private String Url = "http://copia-c.herokuapp.com/";
	private FCTestNavigator nav; 

	@BeforeClass
	public void setupBeforeClass() throws FileNotFoundException {
		
	nav = new FCTestNavigator();
	
	nav.setup();		

	server = nav.getServer();

	server.openURL(Url);
	
	} 
	
	
	/**
	 * User Story:
	 * Given I am  a Turkish user with correct username and password, When I login in Copia, (then) I want to change the default into my language 
	 */
	@Test ( priority = 1 )
	public void loginAndChangeLanguage_OK() {
		
		String username = "tr.test.admin@foodcloud.ie"; 
		String password = "12345";

		LoginPage loginPage = new LoginPage(nav);
		loginPage.setLoginField(username);
		loginPage.setPasswordField(password);
		loginPage.clickSubmit();
		
		String titleLocator = "//div[@class='copia-top row']";
		server.waitForElement(titleLocator);	

		boolean isPagePresent =  server.isElementPresent(titleLocator);
		
		
		DashboardPage dashPage = new DashboardPage(nav);
		dashPage.getMenu().changeLanguage("Turkish");

		String actualWord = dashPage.getMenu().getItemName(MENU_ITEMS.ACTIONS);
		
		// to add in a dataprovider
		String expectedWord = "Eylemler";
		
		Assert.assertTrue(isPagePresent, "Error: Dashboard Page not displayed as expected");
		Assert.assertEquals(actualWord, expectedWord, "Error: Translated Item not matching");
	}

	
	/**
	 * User Story:
	 * Given a Turkish user logged in the system, When I enable my language, (then) each item on admin bar is translated 
	 */
	@Test (priority = 2)
	public void verifyLanguageChangedOnAdminBar_Ok() {
		
 		DashboardPage dashPage = new DashboardPage(nav);
		String actualDashboard = dashPage.getMenu().getItemName(MENU_ITEMS.DASHBOARD);
		String expectedDashboard = "Gösterge Paneli";
		
		String actualSupport = 	dashPage.getMenu().getItemName(MENU_ITEMS.SUPPORT_TICKETS);	
		String expectedSupport  = "Destek Biletleri";
		
		String actualDonations = 	dashPage.getMenu().getItemName(MENU_ITEMS.DONATIONS);	
		String expectedDonations  = "Bağışlar";

		String actualCollections    = dashPage.getMenu().getItemName(MENU_ITEMS.COLLECTIONS);	
		String expectedCollections  = "Teslimler";

		String actualCharities    = dashPage.getMenu().getItemName(MENU_ITEMS.CHARITIES);	
		String expectedCharities  = "Gıda Bankaları";

		String actualDonors    = dashPage.getMenu().getItemName(MENU_ITEMS.DONORS);	
		String expectedDonors  = "Bağışçılar";

		String actualSchedules    = dashPage.getMenu().getItemName(MENU_ITEMS.SCHEDULES);	
		String expectedSchedules  = "Planlar";

		String actualUsers    =  dashPage.getMenu().getItemName(MENU_ITEMS.USERS);	
		String expectedUsers  = "Kullanıcılar";
		
		
		Assert.assertEquals(actualDashboard, expectedDashboard, "Error: Item not matching ");
		Assert.assertEquals(actualSupport, expectedSupport, "Error: Item not matching ");
		Assert.assertEquals(actualDonations, expectedDonations, "Error: Item not matching ");
		Assert.assertEquals(actualCollections, expectedCollections, "Error: Item not matching ");
		Assert.assertEquals(actualCharities, expectedCharities, "Error: Item not matching ");

		Assert.assertEquals(actualDonors, expectedDonors, "Error: Item not matching ");
		Assert.assertEquals(actualSchedules, expectedSchedules, "Error: Item not matching ");
		Assert.assertEquals(actualUsers, expectedUsers, "Error: Item not matching ");
	
	}
	
	
	/**
	 * User Story:
	 * Given a Turkish user logged in the system, When I enable my language, (then) Dashboard Page and its messages are translated   
	 * Note: this test is currently available for no issues detected 
	 * TODO: Need a way to create tickets or alerts
	 */
	@Test (priority = 3)
	public void verifyLanguageChangedOnDashboard_Ok() {
		
		//No issues detected		
		String actualWord = server.getDriver().findElement(server.getLocatorType("//h3[@translate='No_issues_detected']")).getText();
		String expectedWord = "Herhangi bir sorun tespit edilmedi";
		
		Assert.assertEquals(actualWord, expectedWord, "Error: No issues detected Item not matching ");

	}

	@Test (priority = 4)
	public void verifyLanguageChangedOnSupportTicketPage_Ok() {
		
		DashboardPage dashPage = new DashboardPage(nav);
		dashPage.getMenu().clickItem(MENU_ITEMS.SUPPORT_TICKETS);	
			
	//server.waitForElement(server.getLocatorType(titleLocator));		
	SupportTicketsPage supportPage = new SupportTicketsPage(nav);	
	server.waitForActionToComplete();

	String actualWord = supportPage.getTitle();
	String expectedWord = "Destek Biletleri";
	
	SearchMenu sMenu = supportPage.getSearchMenu().open();
	server.waitForActionToComplete();
	
	
	// Search placeholders section
	String[] expectedPlaceholder = new String[]{"Tarihe göre filtrele", "Kategoriye göre filtrele", "Bağışçı Kuruma göre filtre","mağaza #","Gıda Bankasına göre filtrele", "Herşey"};		

	String actualTicketDatePlaceholder = (String) sMenu.getField(SupportTicketsPage.TICKET_DATE_FIELD_NAME).getPlaceholder();
	String actualTicketCategoryPlaceholder = (String) sMenu.getField(SupportTicketsPage.TICKET_CATEGORY_FIELD_NAME).getPlaceholder();
	String actualDonorPlaceholder = (String) sMenu.getField(SupportTicketsPage.DONOR_FIELD_NAME).getPlaceholder();
	String actualStorePlaceholder = (String) sMenu.getField(SupportTicketsPage.STORE_FIELD_NAME).getPlaceholder();
	String actualCharityPlaceholder = (String) sMenu.getField(SupportTicketsPage.CHARITY_FIELD_NAME).getPlaceholder();
	String actualStatusPlaceholder = (String) sMenu.getField(SupportTicketsPage.STATUS_FIELD_NAME).getPlaceholder();

	// Headers section
	String[] expectedHeaders = new String[]{"Bilet Tarihi","Açıldı","Bilet Kategorisi","Bağışçı","Mağaza numarası","Gıda Bankası","Durum"};		
	String actualTicketDateHeader = sMenu.getHeader(SupportTicketsPage.TICKET_DATE_HEADER);
	String actualOpenedHeader = sMenu.getHeader(SupportTicketsPage.OPENED_HEADER);
	String actualCategoryHeader = sMenu.getHeader(SupportTicketsPage.TICKET_CAT_HEADER);
	String actualDonorHeader = sMenu.getHeader(SupportTicketsPage.DONOR_HEADER);
	String actualStoreNumHeader = sMenu.getHeader(SupportTicketsPage.STORE_HEADER);
	String actualCharityHeader = sMenu.getHeader(SupportTicketsPage.CHARITY_HEADER);
	String actualStatusHeader = sMenu.getHeader(SupportTicketsPage.STATUS_HEADER);

	// setup for next test
		supportPage.getMenu().clickItem(MENU_ITEMS.DASHBOARD);	

	//	placeholders check 
		Assert.assertEquals(actualWord, expectedWord, "Error: donation title Item not matching ");

		Assert.assertEquals( actualTicketDatePlaceholder, expectedPlaceholder[0], "Error: placeholder not matching ");
		Assert.assertEquals(actualTicketCategoryPlaceholder, expectedPlaceholder[1], "Error: placeholder not matching ");
		Assert.assertEquals(actualDonorPlaceholder, expectedPlaceholder[2], "Error: placeholder not matching ");
		Assert.assertEquals(actualStorePlaceholder, expectedPlaceholder[3], "Error: placeholder not matching ");		
		Assert.assertEquals(actualCharityPlaceholder, expectedPlaceholder[4], "Error: placeholder not matching ");
		Assert.assertEquals(actualStatusPlaceholder, expectedPlaceholder[5], "Error: placeholder not matching ");
		
		Assert.assertEquals(actualTicketDateHeader,expectedHeaders[0], "Error: header not matching ");
		Assert.assertEquals(actualOpenedHeader,expectedHeaders[1], "Error: header not matching ");
		Assert.assertEquals(actualCategoryHeader,expectedHeaders[2], "Error: header not matching ");
		Assert.assertEquals(actualDonorHeader,expectedHeaders[3], "Error: header not matching ");		
		Assert.assertEquals(actualStoreNumHeader,expectedHeaders[4], "Error: header not matching ");
		Assert.assertEquals(actualCharityHeader,expectedHeaders[5], "Error: header not matching ");
		Assert.assertEquals(actualStatusHeader,expectedHeaders[6], "Error: header not matching ");
	
		
	}
	
	
	@Test(priority = 5)
	public void verifyLanguageChangedOnDonationsPage_Ok() {

		DashboardPage dashPage = new DashboardPage(nav);
		dashPage.getMenu().clickItem(MENU_ITEMS.DONATIONS);
		
		//server.waitForElement(server.getLocatorType(titleLocator));		
		DonationsPage donationsPage = new DonationsPage(nav);
		server.waitForActionToComplete();
		
		//No issues detected		
		String actualWord = donationsPage.getTitle();
		String expectedWord = "Bağışlar";
		
		SearchMenu sMenu = donationsPage.getSearchMenu().open();
		server.waitForActionToComplete();

		// Search placeholders section
		String[] expectedPlaceholder = new String[]{"Tarihe göre filtrele", "Bağışçı Kuruma göre filtre","mağaza #","Gıda Bankasına göre filtrele", "Herşey"};		

		String actualCollectionPlaceholder = (String) sMenu.getField(DonationsPage.COLLECTION_DATE_FIELD_NAME).getPlaceholder();
		String actualDonorPlaceholder = (String) sMenu.getField(DonationsPage.DONOR_FIELD_NAME).getPlaceholder();
		
		String actualStoreNumPlaceholder = (String) sMenu.getField(DonationsPage.STORE_FIELD_NAME).getPlaceholder();
		String actualCharityPlaceholder = (String) sMenu.getField(DonationsPage.CHARITY_FIELD_NAME).getPlaceholder();
		String actualStatusPlaceholder = (String) sMenu.getField(DonationsPage.STATUS_FIELD_NAME).getPlaceholder();

				
		// Headers section
		String[] expectedHeaders = new String[]{"Teslim Tarihi", "Gönderilen","Bilgi","Bağışçı","Mağaza numarası","Gıda Bankası","Teslim Penceresi","Durum"};		
		String actualDateHeader = sMenu.getHeader(DonationsPage.DATE_HEADER);
		String actualPostedHeader = sMenu.getHeader(DonationsPage.POSTED_HEADER);
		String actualInfoHeader = sMenu.getHeader(DonationsPage.INFO_HEADER);
		String actualDonorHeader = sMenu.getHeader(DonationsPage.DONOR_HEADER);
		String actualStoreNumHeader = sMenu.getHeader(DonationsPage.STORE_HEADER);
		String actualCharityHeader = sMenu.getHeader(DonationsPage.CHARITY_HEADER);
		String actualCollectionHeader = sMenu.getHeader(DonationsPage.COL_WIN_HEADER);
		String actualStatusHeader = sMenu.getHeader(DonationsPage.STATUS_HEADER);

		// setup for next test
		donationsPage.getMenu().clickItem(MENU_ITEMS.DASHBOARD);	
		
		Assert.assertEquals(actualWord, expectedWord, "Error: donation title Item not matching ");
		Assert.assertEquals(actualCollectionPlaceholder, expectedPlaceholder[0], "Error: placeholder not matching ");
		Assert.assertEquals(actualDonorPlaceholder, expectedPlaceholder[1], "Error: placeholder not matching ");
		Assert.assertEquals(actualStoreNumPlaceholder, expectedPlaceholder[2], "Error: placeholder not matching ");
		Assert.assertEquals(actualCharityPlaceholder, expectedPlaceholder[3], "Error: placeholder not matching ");
		Assert.assertEquals(actualStatusPlaceholder, expectedPlaceholder[4], "Error: placeholder not matching ");
		
		Assert.assertEquals(actualDateHeader,expectedHeaders[0], "Error: header not matching ");
		Assert.assertEquals(actualPostedHeader,expectedHeaders[1], "Error: header not matching ");
		Assert.assertEquals(actualInfoHeader,expectedHeaders[2], "Error: header not matching ");
		Assert.assertEquals(actualDonorHeader,expectedHeaders[3], "Error: header not matching ");		
		Assert.assertEquals(actualStoreNumHeader,expectedHeaders[4], "Error: header not matching ");
		Assert.assertEquals(actualCharityHeader,expectedHeaders[5], "Error: header not matching ");
		Assert.assertEquals(actualCollectionHeader,expectedHeaders[6], "Error: header not matching ");
		Assert.assertEquals(actualStatusHeader,expectedHeaders[7], "Error: header not matching ");

	}

	
	@Test(priority = 6)
	public void verifyLanguageChangedOnCollectionsPage_Ok() {

		DashboardPage dashPage = new DashboardPage(nav);
		dashPage.getMenu().clickItem(MENU_ITEMS.COLLECTIONS);
		
		//server.waitForElement(server.getLocatorType(titleLocator));		
		CollectionsPage collectionsPage = new CollectionsPage(nav);
		server.waitForActionToComplete();
		
		//No issues detected		
		String actualWord = collectionsPage.getTitle();
		String expectedWord = "Teslimler";
		
		SearchMenu sMenu = collectionsPage.getSearchMenu().open();
		server.waitForActionToComplete();

		// Search placeholders section
		String[] expectedPlaceholder = new String[]{"Tarihe göre filtrele", "Bağışçı Kuruma göre filtre","mağaza #","Gıda Bankasına göre filtrele", "Teslim alan kuruma göre filtrelemek"};		
		String actualCollectionDatePlaceholder = (String) sMenu.getField(CollectionsPage.DATE_FIELD_NAME).getPlaceholder();
		String actualDonorPlaceholder = (String) sMenu.getField(CollectionsPage.DONOR_FIELD_NAME).getPlaceholder();		
		String actualStoreNumPlaceholder = (String) sMenu.getField(CollectionsPage.STORE_FIELD_NAME).getPlaceholder();
		String actualCharityPlaceholder = (String) sMenu.getField(CollectionsPage.CHARITY_FIELD_NAME).getPlaceholder();
		String actualCollectedByPlaceholder = (String) sMenu.getField(CollectionsPage.COLLECTED_BY_FIELD_NAME).getPlaceholder();
				
		// Headers section
		String[] expectedHeaders = new String[]{"Tarih", "Zaman","Bağışçı","Mağaza numarası","Gıda Bankası","Teslim Alan","Toplam değer","Toplam ağırlık"};		
		String actualDateHeader = sMenu.getHeader(CollectionsPage.DATE_HEADER);
		String actualTimeHeader = sMenu.getHeader(CollectionsPage.TIME_HEADER);
		String actualDonorHeader = sMenu.getHeader(CollectionsPage.DONOR_HEADER);
		String actualStoreHeader = sMenu.getHeader(CollectionsPage.STORE_HEADER);
		String actualCharityHeader = sMenu.getHeader(CollectionsPage.CHARITY_HEADER);
		String actualCollectedHeader = sMenu.getHeader(CollectionsPage.COLLECTED_HEADER);
		String actualTotalValueHeader = sMenu.getHeader(CollectionsPage.TOTAL_VALUE_HEADER);
		String actualTotalWeightHeader = sMenu.getHeader(CollectionsPage.TOTAL_WEIGHT_HEADER);

		// setup for next test
		collectionsPage.getMenu().clickItem(MENU_ITEMS.DASHBOARD);	
		
		Assert.assertEquals(actualWord, expectedWord, "Error: donation title Item not matching ");
		Assert.assertEquals(actualCollectionDatePlaceholder, expectedPlaceholder[0], "Error: placeholder not matching ");
		Assert.assertEquals(actualDonorPlaceholder, expectedPlaceholder[1], "Error: placeholder not matching ");
		Assert.assertEquals(actualStoreNumPlaceholder, expectedPlaceholder[2], "Error: placeholder not matching ");
		Assert.assertEquals(actualCharityPlaceholder, expectedPlaceholder[3], "Error: placeholder not matching ");
		Assert.assertEquals(actualCollectedByPlaceholder, expectedPlaceholder[4], "Error: placeholder not matching ");
								
		///
		Assert.assertEquals(actualDateHeader,expectedHeaders[0], "Error: header not matching ");
		Assert.assertEquals(actualTimeHeader,expectedHeaders[1], "Error: header not matching ");
		Assert.assertEquals(actualDonorHeader,expectedHeaders[2], "Error: header not matching ");		
		Assert.assertEquals(actualStoreHeader,expectedHeaders[3], "Error: header not matching ");
		Assert.assertEquals(actualCharityHeader,expectedHeaders[4], "Error: header not matching ");
		Assert.assertEquals(actualCollectedHeader,expectedHeaders[5], "Error: header not matching ");
		Assert.assertEquals(actualTotalValueHeader,expectedHeaders[6], "Error: header not matching ");
		Assert.assertEquals(actualTotalWeightHeader,expectedHeaders[7], "Error: header not matching ");	
	}
	
	
	@Test(priority = 7)
	public void verifyLanguageChangedOnCharityPage_Ok() {

		DashboardPage dashPage = new DashboardPage(nav);
		dashPage.getMenu().clickItem(MENU_ITEMS.CHARITIES);
		
		//server.waitForElement(server.getLocatorType(titleLocator));		
		CharitiesPage charitiesPage = new CharitiesPage(nav);
		server.waitForActionToComplete();
		
		//No issues detected		
		String actualWord = charitiesPage.getTitle();
		String expectedWord = "Gıda Bankaları";
		
		SearchMenu sMenu = charitiesPage.getSearchMenu().open();
		server.waitForActionToComplete();

		// Search placeholders section             show_all Charity Name location region phone email
		String[] expectedPlaceholder = new String[]{"Herşey", "Gıda Bankası Adı", "Yer", "Bölgeler" , "Telefonu" , "E-posta"};		

		String actualSupportPlaceholder = (String) sMenu.getField(CharitiesPage.SUPPORT_FIELD_NAME).getPlaceholder();
		String actualNamePlaceholder = (String) sMenu.getField(CharitiesPage.CHARITY_NAME_FIELD_NAME).getPlaceholder();
		String actualLocationPlaceholder = (String) sMenu.getField(CharitiesPage.LOCATION_FIELD_NAME).getPlaceholder();
		String actualRegionPlaceholder = (String) sMenu.getField(CharitiesPage.REGION_FIELD_NAME).getPlaceholder();
		String actualPhonePlaceholder = (String) sMenu.getField(CharitiesPage.CONTACT_PHONE_FIELD_NAME).getPlaceholder();
		String actualEmailPlaceholder = (String) sMenu.getField(CharitiesPage.EMAIL_FIELD_NAME).getPlaceholder();
		
		
		// Headers section
		String[] expectedHeaders = new String[]{"Support","Değerlendirme","İsim","Yer","Bölge","İlgili Kişi Telefonu","E-posta"};		
		String actualSupportHeader = sMenu.getHeader(CharitiesPage.SUPPORT_HEADER);
		String actualRatingHeader = sMenu.getHeader(CharitiesPage.RATING_HEADER);
		String actualNameHeader = sMenu.getHeader(CharitiesPage.NAME_HEADER);
		String actualLocationHeader = sMenu.getHeader(CharitiesPage.LOCATION_HEADER);
		String actualRegionHeader = sMenu.getHeader(CharitiesPage.REGION_HEADER);
		String actualPhoneHeader = sMenu.getHeader(CharitiesPage.PHONE_HEADER);
		String actualEmailHeader = sMenu.getHeader(CharitiesPage.EMAIL_HEADER);
		
		
		// setup for next test
		charitiesPage.getMenu().clickItem(MENU_ITEMS.DASHBOARD);	
		
		Assert.assertEquals(actualWord, expectedWord, "Error: Charity title Item not matching ");
		Assert.assertEquals(actualSupportPlaceholder, expectedPlaceholder[0], "Error: placeholder not matching ");
		Assert.assertEquals(actualNamePlaceholder, expectedPlaceholder[1], "Error: placeholder not matching ");
		Assert.assertEquals(actualLocationPlaceholder, expectedPlaceholder[2], "Error: placeholder not matching ");
		Assert.assertEquals(actualRegionPlaceholder, expectedPlaceholder[3], "Error: placeholder not matching ");
		Assert.assertEquals(actualPhonePlaceholder, expectedPlaceholder[4], "Error: placeholder not matching ");
		Assert.assertEquals(actualEmailPlaceholder, expectedPlaceholder[5], "Error: placeholder not matching ");
								
		///
		Assert.assertEquals(actualSupportHeader,expectedHeaders[0], "Error: header not matching ");
		Assert.assertEquals(actualRatingHeader,expectedHeaders[1], "Error: header not matching ");
		Assert.assertEquals(actualNameHeader ,expectedHeaders[2], "Error: header not matching ");		
		Assert.assertEquals(actualLocationHeader,expectedHeaders[3], "Error: header not matching ");
		Assert.assertEquals(actualRegionHeader,expectedHeaders[4], "Error: header not matching ");
		Assert.assertEquals(actualPhoneHeader,expectedHeaders[5], "Error: header not matching ");
		Assert.assertEquals(actualEmailHeader,expectedHeaders[6], "Error: header not matching ");
	}
	
	
	@Test(priority = 8)
	public void verifyLanguageChangedOnDonorsPage_Ok() {

		DashboardPage dashPage = new DashboardPage(nav);
		dashPage.getMenu().clickItem(MENU_ITEMS.DONORS);
		
		//server.waitForElement(server.getLocatorType(titleLocator));		
		DonorsPage donorsPage = new DonorsPage(nav);
		server.waitForActionToComplete();
		
		//No issues detected		
		String actualWord = donorsPage .getTitle();
		String expectedWord = "Bağışçı Kuruluşlar";
		
		SearchMenu sMenu = donorsPage.getSearchMenu().open();
		server.waitForActionToComplete();

		// Search placeholders section            Name Store  location region phone email
		//NOTE:  "Ağaza" is the visible string  but the translation is Ağaza numarasına göre filtrelemek
		String[] expectedPlaceholder = new String[]{"Adına göre filtrele","Ağaza numarasına göre filtrelemek","Yer","Bölge","İlgili Kişi Telefonu","E-posta"};		
		
		
		String actualNamePlaceholder = (String) sMenu.getField(DonorsPage.DONOR_FIELD_NAME).getPlaceholder();
		String actualStorePlaceholder = (String) sMenu.getField(DonorsPage.STORE_FIELD_NAME).getPlaceholder();
		String actualLocationPlaceholder = (String) sMenu.getField(DonorsPage.LOCATION_FIELD_NAME).getPlaceholder();
		String actualRegionPlaceholder = (String) sMenu.getField(DonorsPage.REGION_FIELD_NAME).getPlaceholder();
		String actualPhonePlaceholder = (String) sMenu.getField(DonorsPage.CONTACT_PHONE_FIELD_NAME).getPlaceholder();
		String actualEmailPlaceholder = (String) sMenu.getField(DonorsPage.EMAIL_FIELD_NAME).getPlaceholder();
		
		// Headers section
		String[] expectedHeaders = new String[]{"Değerlendirme","İsim","Mağaza","Yer","Bölge","İlgili Kişi Telefonu","E-posta"};		
		String actualStoreHeader = sMenu.getHeader(DonorsPage.STORE_HEADER);
		String actualRatingHeader = sMenu.getHeader(DonorsPage.RATING_HEADER);
		String actualNameHeader = sMenu.getHeader(DonorsPage.NAME_HEADER);
		String actualLocationHeader = sMenu.getHeader(DonorsPage.LOCATION_HEADER);
		String actualRegionHeader = sMenu.getHeader(DonorsPage.REGION_HEADER);
		String actualPhoneHeader = sMenu.getHeader(DonorsPage.PHONE_HEADER);
		String actualEmailHeader = sMenu.getHeader(DonorsPage.EMAIL_HEADER);
				
		// setup for next test
		donorsPage.getMenu().clickItem(MENU_ITEMS.DASHBOARD);	
		
		Assert.assertEquals(actualWord, expectedWord, "Error: Charity title Item not matching ");
		Assert.assertEquals(actualNamePlaceholder, expectedPlaceholder[0], "Error: placeholder not matching ");
		Assert.assertEquals(actualStorePlaceholder, expectedPlaceholder[1], "Error: placeholder not matching ");
		Assert.assertEquals(actualLocationPlaceholder, expectedPlaceholder[2], "Error: placeholder not matching ");
		Assert.assertEquals(actualRegionPlaceholder, expectedPlaceholder[3], "Error: placeholder not matching ");
		Assert.assertEquals(actualPhonePlaceholder, expectedPlaceholder[4], "Error: placeholder not matching ");
		Assert.assertEquals(actualEmailPlaceholder, expectedPlaceholder[5], "Error: placeholder not matching ");

		//
		Assert.assertEquals(actualRatingHeader,expectedHeaders[0], "Error: header not matching ");
		Assert.assertEquals(actualNameHeader ,expectedHeaders[1], "Error: header not matching ");	
		Assert.assertEquals(actualStoreHeader,expectedHeaders[2], "Error: header not matching ");
		Assert.assertEquals(actualLocationHeader,expectedHeaders[3], "Error: header not matching ");
		Assert.assertEquals(actualRegionHeader,expectedHeaders[4], "Error: header not matching ");
		Assert.assertEquals(actualPhoneHeader,expectedHeaders[5], "Error: header not matching ");
		Assert.assertEquals(actualEmailHeader,expectedHeaders[6], "Error: header not matching ");

	}

	@Test(priority = 9)
	public void verifyLanguageChangedOnSchedulePage_Ok() {

		DashboardPage dashPage = new DashboardPage(nav);
		dashPage.getMenu().clickItem(MENU_ITEMS.SCHEDULES);
		
		//server.waitForElement(server.getLocatorType(titleLocator));		
		SchedulesPage schedulePage = new SchedulesPage(nav);
		server.waitForActionToComplete();
		
		//No issues detected		
		String actualWord = schedulePage .getTitle();
		String expectedWord = "Planlar";
		
		SearchMenu sMenu = schedulePage.getSearchMenu().open();
		server.waitForActionToComplete();
	
		String[] expectedPlaceholder = new String[]{"Perşembe","Bağışçı Kuruma göre filtre","mağaza #","Gıda Bankasına göre filtrele","Herşey"};		
			
		String actualDayPlaceholder = (String) sMenu.getField(SchedulesPage.DAY_DATE_FIELD_NAME).getPlaceholder();
		String actualDonorPlaceholder = (String) sMenu.getField(SchedulesPage.DONOR_FIELD_NAME).getPlaceholder();
		String actualStoreNumPlaceholder = (String) sMenu.getField(SchedulesPage.STORE_NUM_FIELD_NAME).getPlaceholder();
		String actualCharityPlaceholder = (String) sMenu.getField(SchedulesPage.CHARITY_FIELD_NAME).getPlaceholder();
		String actualStatusPlaceholder = (String) sMenu.getField(SchedulesPage.STATUS_FIELD_NAME).getPlaceholder();
		
		// Headers section
		String[] expectedHeaders = new String[]{"Gün / Tarih","Bağışçı","Mağaza numarası","Gıda Bankası","Teslim Penceresi","Bağış Durumu"};		

		String actualDayHeader = sMenu.getHeader(SchedulesPage.DAY_HEADER);
		String actualDonorHeader = sMenu.getHeader(SchedulesPage.DONOR_HEADER);
		String actualStoreHeader = sMenu.getHeader(SchedulesPage.STORE_HEADER);
		String actualCharityHeader = sMenu.getHeader(SchedulesPage.CHARITY_HEADER);
		String actualCollectionHeader = sMenu.getHeader(SchedulesPage.COLL_WIN_HEADER);
		String actualStatusHeader = sMenu.getHeader(SchedulesPage.STATUS_HEADER);
				
		// setup for next test
		schedulePage.getMenu().clickItem(MENU_ITEMS.DASHBOARD);	
		
		Assert.assertEquals(actualWord, expectedWord, "Error: Charity title Item not matching ");
		Assert.assertEquals(actualDayPlaceholder, expectedPlaceholder[0], "Error: placeholder not matching ");
		Assert.assertEquals(actualDonorPlaceholder, expectedPlaceholder[1], "Error: placeholder not matching ");
		Assert.assertEquals(actualStoreNumPlaceholder, expectedPlaceholder[2], "Error: placeholder not matching ");
		Assert.assertEquals(actualCharityPlaceholder, expectedPlaceholder[3], "Error: placeholder not matching ");
		Assert.assertEquals(actualStatusPlaceholder, expectedPlaceholder[4], "Error: placeholder not matching ");
	
		//
		Assert.assertEquals(actualDayHeader,expectedHeaders[0], "Error: header not matching ");
		Assert.assertEquals(actualDonorHeader ,expectedHeaders[1], "Error: header not matching ");	
		Assert.assertEquals(actualStoreHeader ,expectedHeaders[2], "Error: header not matching ");	
		Assert.assertEquals(actualCharityHeader ,expectedHeaders[3], "Error: header not matching ");	
		Assert.assertEquals(actualCollectionHeader ,expectedHeaders[4], "Error: header not matching ");	
		Assert.assertEquals(actualStatusHeader ,expectedHeaders[5], "Error: header not matching ");	

	}
	
	
	@AfterClass	
	public void tearDown(){
		server.getDriver().close();
	}



}
