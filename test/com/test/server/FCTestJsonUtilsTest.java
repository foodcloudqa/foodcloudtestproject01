package com.test.server;

import org.testng.annotations.Test;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.DonorAPI;
import com.foodcloud.db.copiaAPI.OfferAPI;
import com.foodcloud.db.copiaAPI.OrganizationAPI;
import com.foodcloud.db.copiaAPI.ScheduleAPI;
import com.foodcloud.server.FCTestJsonUtils;
import com.foodcloud.server.FCTestUtils;

/**
 * 
USAGE EXAMPLES VS tdd tests 

 *
 */
public class FCTestJsonUtilsTest {

	@Test(priority = 12)
	public void querySchedule_unscheduledDonationsQueries() throws SQLException {
		Connection con = ConnectionToDb.getCopiaConnection();

		ResultSet unscheduledDonationList = null;
		try {
			String param_zoneId = FCTestUtils.EUROPE_DUBLIN;
			int currentDay = FCTestUtils.getCurrentDay(FCTestUtils.getZoneId(param_zoneId));

			unscheduledDonationList = ScheduleAPI.todaysUnScheduledOrganizations(con, currentDay);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (unscheduledDonationList != null) {

			StringBuffer buffer = new StringBuffer();
			buffer.append("INSERT INTO public.donation(");
			buffer.append("external_id, donor_id, description, comments, date, \"time\", status)");
			buffer.append(" VALUES ");

			while (unscheduledDonationList.next()) {
				String org_id = unscheduledDonationList.getString(1);
				String charity_id = unscheduledDonationList.getString(2);
				ZonedDateTime currentDateTime = FCTestUtils.convertToTimeZonedDate(FCTestUtils.getUTCDate(),
						FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));

				String currentDate = FCTestUtils.formatDateToCopiaUI(currentDateTime, FCTestUtils.TYPE.DATE);
				String currentTime = FCTestUtils.formatDateToCopiaUI(currentDateTime, FCTestUtils.TYPE.TIME);

				buffer.append("('" + FCTestJsonUtils.generateUUID().toString() + "','" + org_id + "', '" + charity_id
						+ "', null, '" + currentDate + "','" + currentTime + "', 'Available'),");

			}

			buffer.replace(buffer.length() - 1, buffer.length() - 1, ";");

			buffer.setLength(buffer.length() - 1);

			System.out.println("String buffer: " + buffer);

		}

	}

	@Test(priority = 20)
	public void querySchedule_Json() throws SQLException, IOException {

		Connection con = ConnectionToDb.getCopiaConnection();

		ResultSet todaySchedule = null;
		try {
			String param_zoneId = FCTestUtils.EUROPE_DUBLIN;
			int currentDay = FCTestUtils.getCurrentDay(FCTestUtils.getZoneId(param_zoneId));

			todaySchedule = ScheduleAPI.todaysScheduledOrganizations(con, currentDay);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (todaySchedule != null) {
			
			FileWriter fileWriter = new FileWriter("./JsonDonation.json");
			PrintWriter printWriter = new PrintWriter(fileWriter);

			while (todaySchedule.next()) {
				String org_id = todaySchedule.getString(1);
				String donorName = OrganizationAPI.getOrgName(con, org_id);
				String donorGroup = DonorAPI.getExternalId(con, Integer.parseInt(org_id));
				ZonedDateTime currentDateTime = FCTestUtils.convertToTimeZonedDate(FCTestUtils.getUTCDate(),
						FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));
				String formattedDateTime = currentDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);

				int columnIndex = donorGroup.indexOf(":");
				String donorGroupCode = donorGroup.substring(0, columnIndex);
				int donorGroupNumber = Integer.parseInt(donorGroup.substring(columnIndex + 1, donorGroup.length()));

				JSONObject jsonObject = FCTestJsonUtils.createJsonDonationObject(donorName, donorGroupCode, donorGroupNumber,
						formattedDateTime.toString(), "FFV", 3.5);

				printWriter.println(jsonObject.toString());
				System.out.println("JSON Object: " + jsonObject);

			}
			printWriter.close();


			
//			Assert??
			
		}

	}

	
	/**
	 * FUNCTION - more than a test 
	 * @throws SQLException
	 * @throws IOException
	 */
	@Test(priority = 21)
	public void queryUnscheduleSchedule_Json() throws SQLException, IOException {

		Connection con = ConnectionToDb.getCopiaConnection();

		ResultSet todaySchedule = null;
		try {
			String param_zoneId = FCTestUtils.EUROPE_DUBLIN;
			int currentDay = FCTestUtils.getCurrentDay(FCTestUtils.getZoneId(param_zoneId));

			todaySchedule = ScheduleAPI.todaysUnScheduledOrganizations(con, currentDay);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (todaySchedule != null) {

			FileWriter fileWriter = new FileWriter("./JsonUnschedDonation.json");
			PrintWriter printWriter = new PrintWriter(fileWriter);

			while (todaySchedule.next()) {
				String org_id = todaySchedule.getString(1);
				String donorName = OrganizationAPI.getOrgName(con, org_id);
				String donorGroup = DonorAPI.getExternalId(con, Integer.parseInt(org_id));

				ZonedDateTime currentDateTime = FCTestUtils.convertToTimeZonedDate(FCTestUtils.getUTCDate(),
						FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));
				String formattedDateTime = currentDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);

				if (!donorGroup.contains(":")) {
					System.out.println("ERROR donor group code inconsistent: " + donorGroup + " " + donorName);
				} else {
					int columnIndex = donorGroup.indexOf(":");

					String donorGroupCode = donorGroup.substring(0, columnIndex);
					int donorGroupNumber = Integer.parseInt(donorGroup.substring(columnIndex + 1, donorGroup.length()));

					JSONObject jsonObject = FCTestJsonUtils.createJsonDonationObject(donorName, donorGroupCode, donorGroupNumber,
							formattedDateTime.toString(), "FFV", 3.5);

					printWriter.println(jsonObject.toString());
					System.out.println("JSON Object: " + jsonObject);
				}
			}
			printWriter.close();

		}

	}

	//** New stuff to double check 
	@Test(priority = 22)
	public void queryUnscheduleSchedule_Json_plaza() throws SQLException, IOException {

		Connection con = ConnectionToDb.getCopiaConnection();

		ResultSet todaySchedule = null;
		try {
			String param_zoneId = FCTestUtils.EUROPE_DUBLIN;
			int currentDay = FCTestUtils.getCurrentDay(FCTestUtils.getZoneId(param_zoneId));

			todaySchedule = ScheduleAPI.todaysUnScheduledOrganizations(con, currentDay);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (todaySchedule != null) {

			FileWriter fileWriter = new FileWriter("./JsonUnschedDonation_Plaza_Int.json");
			PrintWriter printWriter = new PrintWriter(fileWriter);

			Map<String, String>  donationData = new HashMap<String, String>(); 

			while (todaySchedule.next()) {
				
				donationData.put("type", "org.foodcloud.domain.share.OfferedDonation");				
				donationData.put("donationId", FCTestJsonUtils.generateUUID().toString());
				donationData.put("org_id", ""+ todaySchedule.getString(1) );
				donationData.put("donorName", OrganizationAPI.getOrgName(con, donationData.get("org_id")));
				String donorGroup = DonorAPI.getExternalId(con, Integer.parseInt(donationData.get("org_id")));

				ZonedDateTime currentDateTime = FCTestUtils.convertToTimeZonedDate(FCTestUtils.getUTCDate(),
						FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));
				donationData.put("formattedDateTime",currentDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
				
				if (!donorGroup.contains(":")) {
					System.out.println("ERROR donor group code inconsistent: " + donorGroup + " " + donationData.get("donorName"));
				} else {
					int columnIndex = donorGroup.indexOf(":");
		
					donationData.put("donorGroupCode", donorGroup.substring(0, columnIndex));
					donationData.put("donorGroupNumber", donorGroup.substring(columnIndex + 1, donorGroup.length()) );

					donationData.put("foodCategory", FCTestJsonUtils.getRandomFCFoodCategory());
//					donationData.put("foodQuantity", Double.toString(FCTestJsonUtils.getRandomDouble()));
					donationData.put("foodQuantity", Integer.toString(FCTestJsonUtils.getRandomInt(19)));
									
					// randomCrates
					JSONObject jsonObject = FCTestJsonUtils.createJsonDonationForPlaza(donationData);
							
						//(donorName, donorGroupCode, donorGroupNumber,
						//formattedDateTime.toString(), , 3.5);

					printWriter.println(jsonObject.toString());
					System.out.println("JSON Object: " + jsonObject);
				}
			}
			printWriter.close();

		}

	}
	
	
	@Test(priority = 23)
	public void querySchedule_Json_Azure() throws SQLException, IOException {

		Connection con = ConnectionToDb.getCopiaConnection();

		ResultSet todaySchedule = null;
		try {
			String param_zoneId = FCTestUtils.EUROPE_DUBLIN;
			int currentDay = FCTestUtils.getCurrentDay(FCTestUtils.getZoneId(param_zoneId));

			todaySchedule = ScheduleAPI.todaysScheduledOrganizations(con, currentDay);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (todaySchedule != null) {
			
			FileWriter fileWriter = new FileWriter("./JsonDonationAzure_Scheduled.json");
			PrintWriter printWriter = new PrintWriter(fileWriter);

			Map<String, String>  donationData = new HashMap<String, String>(); 

			while (todaySchedule.next()) {
									
				donationData.put("MessageType", "DonationOffered");				
				donationData.put("DonationId", FCTestJsonUtils.generateUUID().toString());

				String org_id = todaySchedule.getString(1);
				String donorGroup = DonorAPI.getExternalId(con, Integer.parseInt(org_id));

				ZonedDateTime currentDateTime = FCTestUtils.convertToTimeZonedDate(FCTestUtils.getUTCDate(),
						FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));
				donationData.put("Date",currentDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
				
				if (!donorGroup.contains(":")) {
					System.out.println("ERROR donor group code inconsistent: " + donorGroup + " " + donationData.get("donorName"));
				} else {
					int columnIndex = donorGroup.indexOf(":");
		
					donationData.put("Retailer", donorGroup.substring(0, columnIndex));
					donationData.put("StoreNumber", donorGroup.substring(columnIndex + 1, donorGroup.length()) );

					Integer quantityInt = FCTestJsonUtils.getRandomInt(5);
					Double quantityDouble = Double.valueOf(quantityInt);
					
					donationData.put("Category", FCTestJsonUtils.getRandomAzureFoodCategory());
					donationData.put("Quantity", Integer.toString(quantityInt));
					donationData.put("QuantityOffered", Double.toString(quantityDouble));								
							// randomCrates
					JSONObject jsonObject = FCTestJsonUtils.createJsonDonationForAzure(donationData);

					printWriter.println(jsonObject.toString());
					System.out.println("JSON Object: " + jsonObject);
				}
			}
			printWriter.close();
			
//			Assert??
		}

	}

	
	/**
	 * problem with visualization: 
	 * 			//donationData.put("CollectionETA", "2018-01-10T14:55:03.7519264+00:00");
			//2018-01-11T16:15:41.733Z
	 * @throws SQLException
	 * @throws IOException
	 */
	@Test(priority = 30)
	public void createFCAcceptedDonationJsonObject() throws SQLException, IOException {

			FileWriter fileWriter = new FileWriter("./JsonCopiaDonationAccepted.json");
			PrintWriter printWriter = new PrintWriter(fileWriter);

			Map<String, String> donationData = new HashMap<String, String>(); 
			
			donationData.put("MessageType", "DonationAccepted");  
			donationData.put("DonationId", FCTestJsonUtils.generateUUID().toString());
			donationData.put("AcceptanceId", FCTestJsonUtils.generateUUID().toString());
			donationData.put("Charity", "Flexy Ltd");
			donationData.put("CollectionETA",FCTestUtils.getUTCDate(FCTestUtils.getLocalDateTime()).toString());
			
			Random rand = new Random(); 
			
			Map<String, String> data = new HashMap<String, String>(); 
			data.put("Bread", ""+rand.nextInt(20) );
			data.put("Fruit", ""+rand.nextInt(20) );
			
			JSONObject jsonObject = FCTestJsonUtils.createJsonDonationFCAccepted(donationData, data); 

			printWriter.println(jsonObject.toString());
			System.out.println("JSON Object: " + jsonObject);

			printWriter.close();
			
			
	}

	
	@Test(priority = 32)
	public void createFCUnallocatedDonationJsonObject() throws SQLException, IOException {

			FileWriter fileWriter = new FileWriter("./JsonCopiaDonationUnallocated.json");
			PrintWriter printWriter = new PrintWriter(fileWriter);

			Map<String, String> donationData = new HashMap<String, String>(); 
			
			donationData.put("MessageType", "DonationUnallocated");  
			donationData.put("DonationId", FCTestJsonUtils.generateUUID().toString());
						
			JSONObject jsonObject = FCTestJsonUtils.createJsonDonationFCUnallocated(donationData); 

			printWriter.println(jsonObject.toString());
			System.out.println("JSON Object Rejected: " + jsonObject);

			printWriter.close();
						
	}
	
	
	@Test(priority = 33)
	public void query_Json_Sms() throws SQLException, IOException {

			
			FileWriter fileWriter = new FileWriter("./JsonSmsCopia.json");
			PrintWriter printWriter = new PrintWriter(fileWriter);

 
			Connection con = ConnectionToDb.getCopiaConnection();
	
			String currentDay = "2018/03/23";
			
			String currentValue = OfferAPI.getToken(con, "13574" , "13571" , currentDay);
			
			Map<String, String>  donationData = new HashMap<String, String>(); 
			
				donationData.put("MessageType", "Sms");				
				donationData.put("textAnswer", "Y" + currentValue);

				donationData.put("mobileNumber","353871640017");

					JSONObject jsonObject = FCTestJsonUtils.createJsonSmsForCopia(donationData);

					printWriter.println(jsonObject.toString());
					System.out.println("JSON Object: " + jsonObject);
	
				printWriter.close();
	}


}
