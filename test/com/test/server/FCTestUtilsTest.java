package com.test.server;

import org.testng.annotations.Test;
import org.testng.Assert;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.foodcloud.db.copiaAPI.ShortcutsAPI;
import com.foodcloud.server.FCTestUtils;

/**
 * 
 * Trial page :) 
 * Need a way to extract data from file (custom file) 
 * Need a way to  :  data-driven testing (with @DataProvider and/or XML configuration).
 */
public class FCTestUtilsTest {

	@Test ( priority = 1 )
	public void verify_CurrentCopiaDay_TimeZone_UTC_MondayBeforeNoonIsSunday() {

		// Testing with fixed values and then with dynamic values 
		LocalDateTime time = LocalDateTime.of(2017, 10, 23, 11, 05);
		ZonedDateTime UTCTime = ZonedDateTime.of(time, ZoneId.of("Z"));

		int currentCopiaDay = FCTestUtils.getCurrentCopiaDay(UTCTime); 
		
		Assert.assertEquals(currentCopiaDay, 7,  "Error: Current Sunday Copia day not matching for Monday UTC.");
	} 
	
	@Test ( priority = 2 )
	public void verify_CurrentCopiaDay_TimeZone_UTC_MondayAfterNoonIsMonday() {

		//monday AFTER midday = expected monday 		
		LocalDateTime time = LocalDateTime.of(2017, 10, 23, 12, 05);
		ZonedDateTime UTCTime = ZonedDateTime.of(time, ZoneId.of("Z"));
		
		int currentCopiaDay = FCTestUtils.getCurrentCopiaDay(UTCTime); 
		
		Assert.assertEquals(currentCopiaDay, 1,  "Error: Current Monday Copia day not matching for Monday UTC.");

	} 
		
	
	@Test ( priority = 10 )
 	public void verify_CurrentCopiaDay_TimeZone_Sydney_MondayBeforeNoonIsSunday() {

		// Testing with fixed values and then with dynamic values 
		LocalDateTime time = LocalDateTime.of(2017, 10, 23, 11, 05);
        ZoneId zoneSydney = ZoneId.of("Australia/Sydney");

		ZonedDateTime sydneyTime = ZonedDateTime.of(time, zoneSydney);

		int currentCopiaDay = FCTestUtils.getCurrentCopiaDay(sydneyTime); 
		
		Assert.assertEquals(currentCopiaDay, 7,  "Error: Current Sunday Copia day not matching for Monday Sydney Timezone.");
	} 
	
	@Test ( priority = 11 )
	public void verify_CurrentCopiaDay_TimeZone_Sydney_MondayAfterNoonIsMonday() {

		//monday AFTER midday = expected monday 		
		LocalDateTime time = LocalDateTime.of(2017, 10, 23, 12, 05);
        ZoneId zoneSydney = ZoneId.of("Australia/Sydney");

		ZonedDateTime sydneyTime = ZonedDateTime.of(time, zoneSydney);
		
		int currentCopiaDay = FCTestUtils.getCurrentCopiaDay(sydneyTime); 
		
		Assert.assertEquals(currentCopiaDay, 1,  "Error: Current Monday Copia day not matching for Monday Sydney Timezone.");

	} 
		
      
	@Test ( priority = 20 )
	public void verify_CurrentCopiaDay_DublinMondayBeforeNoonIsSydneyMonday() {
		// in this case if in UTC is 11:05 , in australia is 21:00  and it is no more copia Sunday 

		// Testing with fixed values and then with dynamic values
		LocalDateTime time = LocalDateTime.of(2017, Month.OCTOBER, 23, 11, 05);
			
        ZoneId zoneSydney = ZoneId.of("Australia/Sydney");
	    ZoneId zoneDublin = ZoneId.of("Europe/Dublin"); 

	    ZonedDateTime originDublin   = ZonedDateTime.of(time, zoneDublin); 
	    System.out.println("In Dublin Time Zone:"  + originDublin);
  	    
	    ZonedDateTime destinationSydney   = originDublin.withZoneSameInstant(zoneSydney); 
	    System.out.println("In Australian Time Zone:"  + destinationSydney);
	
	    int currentCopiaDay = FCTestUtils.getCurrentCopiaDay(destinationSydney); 
				
		Assert.assertNotEquals(currentCopiaDay, 7,  "Error: Current Copia day not matching."); 
		Assert.assertEquals(currentCopiaDay, 1,  "Error: Current Copia day not matching for Monday Sydney Timezone.");
		
	} 

	@Test ( priority = 21 )
	public void verify_CurrentCopiaDay_DublinMondayAfter2IsSydneyTuesday() {	
		// in this case if in UTC is 13:05 , in australia is 00:00 +1day  and it is still copia Monday 
	
		LocalDateTime time = LocalDateTime.of(2017, Month.OCTOBER, 23, 14, 05);
			
        ZoneId zoneSydney = ZoneId.of("Australia/Sydney");
	    ZoneId zoneDublin = ZoneId.of("Europe/Dublin"); 

	    ZonedDateTime originDublin   = ZonedDateTime.of(time, zoneDublin); 
	    System.out.println("In Dublin Time Zone:"  + originDublin);
  	    
	    ZonedDateTime destinationSydney   = originDublin.withZoneSameInstant(zoneSydney); 
	    System.out.println("In Australian Time Zone:"  + destinationSydney);
	
		int currentCopiaDay = FCTestUtils.getCurrentCopiaDay(destinationSydney); 
			
		Assert.assertEquals(currentCopiaDay, 1,  "Error: Current Monday Copia day not matching for Tuesday Sydney Timezone.");		
	} 


	
	@Test ( priority = 23 )
	public void verify_convertToAustralia() {	

		LocalDateTime time = LocalDateTime.of(2017, Month.NOVEMBER, 8, 14, 25);

		OffsetDateTime utcDate = FCTestUtils.getUTCDate(time);
		
	    System.out.println("In UTC Zone:"  + utcDate);
		
		ZonedDateTime destinationSydney  = FCTestUtils.convertToTimeZonedDate(utcDate, FCTestUtils.getZoneId(FCTestUtils.AUSTRALIA_SYDNEY));
		
		System.out.println("In Sydney Time Zone:"  + destinationSydney );

	    System.out.println("In Sydney Time Zone:"  + formatMe(destinationSydney).get(0) );
	    System.out.println("In Sydney Time Zone:"  + formatMe(destinationSydney).get(1) );
	    	    		
		ZonedDateTime originDublin   = utcDate.atZoneSameInstant(FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));	

		ZonedDateTime originDublin_withUtils   = FCTestUtils.convertToTimeZonedDate(utcDate, FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));
		
		System.out.println("In Dublin Time Zone:"  + originDublin);
		System.out.println("In Dublin Time Zone with Utils:"  + originDublin_withUtils);

		List a = formatMe(originDublin);
	    System.out.println("In Dublin Time Zone format date:" + a.get(0));
 	    System.out.println("In Dublin Time Zone time:" + a.get(1));
	    	    
	}

	
	private List<String> formatMe(ZonedDateTime aDate) {

		DateTimeFormatter DateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		DateTimeFormatter TimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

		String FormatDateAsString =  aDate.format(DateFormatter);
		String utcTime = aDate.format(TimeFormatter);

		System.out.println("Date: "+ FormatDateAsString );		
		System.out.println("Time: "+ utcTime );
		List<String> date = new ArrayList<String>();
		date.add(FormatDateAsString);
		date.add(utcTime);
		
		return date;
	    
	}


/*
 * public class TestCharter_Australia546 {

	/**
	 *	 Given I am an Australian User (Australia/Sydney timezone),
	 * When I log in to copia and I am  on Donor page and I set 
	 * 	the Start time and End Time of the collection window and 
	 * defining a schedule in my local timezone,  
	 * Then the system will understand my local timezone and 
	 * consider each Copia day starting from my 12:00 (Copia day def.)  
	 * (i.e. Previous Copia Day < 12:00 Solar Day, Current Copia Day > 12:00 Solar Day  ) 
	 */
	@Test
	public void Australian_CopiaDay(){
//			 get today / is DLSavings  applied?
//			 get the difference timezone 
//			 check copia day 

//		int currentCopiaDay = FCTestUtils.getCurrentCopiaDay(destinationSydney); 
		
//		Assert.assertEquals(currentCopiaDay, 1,  "Error: Current Monday Copia day not matching for Tuesday Sydney Timezone.");		
//		Assert.fail("not impl yet ");
	}
	}	
	/**  Before 12* ( not UTC timezone, DLS defined)
	* Given I am an Australian User (Australia/Sydney timezone), 
	* When I am between June and October (on) and 
	* I log in to copia *before* my midday (10:00) on Monday (*r) ,  
	* Then my local  Copia day will still be Sunday
	*/
//	@Test
//	public void Australian_PreviousCopiaDay_Summertime(){
//		Assert.fail("not impl yet ");
//	}
	
	/** Given I am an Australian User (Australia/Sydney timezone),
	 *  When I am between June and October and 
	 *  I log in to copia *before* my midday (10:00) on Monday (*r)  
	 *  and I post a donation and I am *in the collection window*  
	 *  (i.e 9:30-11:30) and I have a scheduled charity for Copia Sunday, 
	 *   Then the system will send a donation to the charity
	*/
//	@Test
//	public void Australian_PreviousCopiaDay_Summer_OfferToScheduled_InWindow(){
//		Assert.fail("not impl yet ");
//		// expected result = offered
//
//	}


	/** Given I am an Australian User (Australia/Sydney timezone),
	 *  When I am between June and October and I log in to copia *before* my midday (11:40) on Monday (*r)  and I post a donation and I am *outside the collection window*  (i.e 9:30-11:30) and I have a scheduled charity for Copia Sunday,  
	 * Then the system will set the donation as 'Abandoned'
	 */
//	@Test
//	public void Australian_PreviousCopiaDay_Summer_OfferToScheduled_OutWindow(){
//		Assert.fail("not impl yet ");
//		// expected result = abandoned 
//	}

	
	
	/** Note: 
	* *Nothing should happen at 10:59 Australia/Sydney local time 
	* (switch to new UTC day)*
	* *Nothing should happen at 00:00 Australia/Sydney local time 
	* (12:00 UTC -  new copia day UTC)*
	* Note: *Switching Daylight Saving time on and off should not impact 
	* donations and schedules* in the timezones where it is defined 
	* and also the ones where it is *not* defined 
	* *Charity closed* 
	*/
	
	
	/** Given I am an Australian User (Australia/Sydney timezone), When I log in 
	to copia before my midday (11:00) on Monday (*) 
	and I have a *charity closed* and I post a donation 
	* in the collection window* , then the system will not send a message to the charity (x) 
	* (!) edge case scenario: for a morning store, 
	* a charity should be closed the day before the actual one (i.e closed on Copia Sunday for Monday)     		
	*/
//	@Test
//	public void Australian_PreviousCopiaDay_Summer_OfferToClosed_InWindow(){
//		Assert.fail("not impl yet ");
//		// expected result = no scheduled if no bonus donation flag
//		// expected result = available if both bonus donation flag
//
//	}
//
//}

