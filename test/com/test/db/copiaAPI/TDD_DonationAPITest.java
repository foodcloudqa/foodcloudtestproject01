package com.test.db.copiaAPI;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.DonationAPI;
import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.ShortcutsAPI;

/**
 *
 *
 */
public class TDD_DonationAPITest {

	private Connection con;
	private String donor_name = "Donor DonationAPI";
	private String donorGroupName = "Test DonorGroup";
	private String charity_name = "Charity DonationAPI";
	private String charityGroupName = "Test CharityGroup";
	private static String donationUUID = "7935297d-303b-4235-9290-dd7723fb0981";
	
	static final Logger LOG = LogManager.getLogger(TDD_DonationAPITest.class.getName());

	@BeforeTest
	public void setup() {
		con = ConnectionToDb.getCopiaConnection();

	}

	@Test(priority = 1)
	public void insertDonation_EmptyMap_Fail() {
		// Arrange
		Map<String, String> scheduleData = new HashMap<String, String>();

		// Act
		int result = 0;
		try {
			result = DonationAPI.insert(con, scheduleData);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert
		Assert.assertEquals(result, -1, "DonationAPI test - Empty map should cause an error");

	}

	@Test(priority = 2)
	public void insertDonation_OrgIdNull_Fail() {
		// Arrange
		
		Map<String, String> data = new HashMap<String, String>();

		data.put(DonationAPI.DATE, ConnectionToDb.formatDate());
		data.put(DonationAPI.TIME, "20:00:00");
		data.put(DonationAPI.STATUS, "Available");
		data.put(DonationAPI.EXTERNAL_ID, donationUUID );
		data.put(DonationAPI.COMMENTS, "");
		data.put(DonationAPI.DESCRIPTION, "");

		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = DonationAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualScheduleInserted, expectedId,
				"DonationAPI test - No control on mandatory input data (donorId) should cause an error ");
	}

	@Test(priority = 3)
	public void insertDonation_ExternalIdNotPresent_Fail() {
		// Arrange
		int orgId = arrangeDonationTestData(con)[0];
		int charity_id = arrangeDonationTestData(con)[1];
		
		Map<String, String> data = new HashMap<String, String>();

		data.put(DonationAPI.DONOR_ID, Integer.toString(orgId));
		data.put(DonationAPI.TIME, "20:00:00");
		data.put(DonationAPI.DATE, ConnectionToDb.formatDate());
		data.put(DonationAPI.STATUS, "Available");
		data.put(DonationAPI.COMMENTS, Integer.toString(charity_id));
		data.put(DonationAPI.DESCRIPTION, "");

		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = DonationAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualScheduleInserted, expectedId,
				"DonationAPI test - No control on mandatory input data (external_id) should cause an error");
	}

	@Test(priority = 10)
	public void insertDonation_FullDataMap_Pass() {
		// Arrange
		int donorOrgId = arrangeDonationTestData(con)[0];
		int charityOrgId = arrangeDonationTestData(con)[1];
		
		Map<String, String> data = new HashMap<String, String>();

		data.put(DonationAPI.DONOR_ID, Integer.toString(donorOrgId));
		data.put(DonationAPI.TIME, ConnectionToDb.formatTime()); 
		data.put(DonationAPI.DATE, ConnectionToDb.formatDate());
		data.put(DonationAPI.EXTERNAL_ID, donationUUID);
		data.put(DonationAPI.STATUS, "Available");
		data.put(DonationAPI.COMMENTS, Integer.toString(charityOrgId));
		data.put(DonationAPI.DESCRIPTION, "");

		// Act
		int actualDonationInserted = 0;
		try {
			actualDonationInserted = DonationAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 1;
		
		Assert.assertNotEquals(actualDonationInserted, -1, "Error. Found donation amount  negative. Creation error. ");
		Assert.assertNotEquals(actualDonationInserted, 0, "Error. Found donation amount  null. Creation failed. ");
		Assert.assertEquals(actualDonationInserted, expectedId, "Error. donation amount not matching. ");
	}
	
	
	
	 @Test(priority = 40, dependsOnMethods = { "insertDonation_FullDataMap_Pass" })
		public void deleteDonation_viaExternalId_Pass() {
			//Arrange -section
		 
			// Act - section
			int deletedRows = 0;
			try {
				Map<String, String> deleteCriterion = new HashMap<String, String>();
				deleteCriterion.put(DonationAPI.EXTERNAL_ID, donationUUID);

				deletedRows = DonationAPI.delete(con, deleteCriterion);
			} catch (SQLException e) {
				LOG.error(e.getLocalizedMessage());
			}

			// Assert - section
			Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
			Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
			Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
		}
	
	private Integer[] arrangeDonationTestData(Connection con2) {
		
		Integer[] results = new Integer[2];

		int donorOrgId = ShortcutsAPI.createOrganization(con2, new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;

			{
                put(ShortcutsAPI.ORG_NAME, donor_name);
                put(ShortcutsAPI.GROUP_NAME, donorGroupName);
                put(ShortcutsAPI.REGION, "Dublin South" );
                put(ShortcutsAPI.ORG_TYPE, ShortcutsAPI.ORG_TYPE_DONOR);                
            }
        });
		
		int charityOrgId = ShortcutsAPI.createOrganization(con2, new HashMap<String, String>() {
			private static final long serialVersionUID = 2L;

			{
                put(ShortcutsAPI.ORG_NAME, charity_name);
                put(ShortcutsAPI.GROUP_NAME, charityGroupName);
                put(ShortcutsAPI.ORG_TYPE, ShortcutsAPI.ORG_TYPE_CHARITY);
                put(ShortcutsAPI.REGION, "Dublin South" );
            }
        });
		results[0] = donorOrgId;
		results[1] = charityOrgId;

		return results;
	}


}
