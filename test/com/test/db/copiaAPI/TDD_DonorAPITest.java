package com.test.db.copiaAPI;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.DonorAPI;
import com.foodcloud.db.copiaAPI.OrganizationAPI;
import com.foodcloud.db.copiaAPI.RegionAPI;

/**
 * 
 *
 *
 */
public class TDD_DonorAPITest {

	private Connection con;
	private String groupName = "FoodCloud Demo Group";
	private String donor_name = "Donor Store Test";

	 static final Logger LOG = LogManager.getLogger(TDD_DonorAPITest.class.getName());

	@BeforeTest
	public void setup() {
		con = ConnectionToDb.getCopiaConnection();

	}

	
	@Test(priority = 1)	
	public void insertDonor_EmptyMap_Fail() {
		//Arrange
		Map<String, String> donorData = new HashMap<String, String>();

		//Act
		int result = 0;
		try {
			result = DonorAPI.insert(con, donorData);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		//Assert
		Assert.assertEquals(result, -1, "Empty map should cause an error ");

	}
	
	@Test(priority = 2)	
	public void insertDonor_FullDataMap_Pass() {
		//Arrange (move in prerequirements)
		int orgId = 0 ;
		try {
			orgId = OrganizationAPI.getId(con,donor_name);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}
		
		if(orgId == 0 || orgId == -1) {
			int group_id = 0;
			try {
				group_id = OrganizationAPI.getId(con, groupName);
			} catch (SQLException sqlE) {
				LOG.error(sqlE.getLocalizedMessage());
			}

			int regionCode = 0;
			try {
				regionCode = RegionAPI.getId(con, "Dublin South");
			} catch (SQLException regErr) {
				LOG.error(regErr.getLocalizedMessage());
			}
		
			//Organization data 
			Map<String, String> donorStoreData = new HashMap<String, String>();
		
			donorStoreData.put(OrganizationAPI.PARENT_ID, Integer.toString(group_id) );
			donorStoreData.put(OrganizationAPI.NAME, donor_name);
			donorStoreData.put(OrganizationAPI.REGION_ID, Integer.toString(regionCode));
			donorStoreData.put(OrganizationAPI.SECTOR, "Retail");
			donorStoreData.put(OrganizationAPI.TYPE, "DS");
			donorStoreData.put(OrganizationAPI.CREATED, ConnectionToDb.formatTimestamp());
			donorStoreData.put(OrganizationAPI.STATUS, "Active");
			donorStoreData.put(OrganizationAPI.RATING, "0");
			donorStoreData.put(OrganizationAPI.SUPPORT, "true");
			donorStoreData.put(OrganizationAPI.VERSION, "0");

			try {
				orgId = OrganizationAPI.insert(con, donorStoreData);
			} catch (SQLException e) {
				LOG.error(e.getLocalizedMessage());
			}

		}
				
		Map<String, String> donorData = new HashMap<String, String>();
			donorData.put(DonorAPI.ORG_ID, Integer.toString(orgId));
			donorData.put(DonorAPI.EXTERNAL_ID, "TEST:0001");
		
			donorData.put(DonorAPI.POST_BY, "18:00:00");
			donorData.put(DonorAPI.COLLECT_START, "19:30:00");
			donorData.put(DonorAPI.COLLECT_END, "21:30:00");
			
			donorData.put(DonorAPI.GROUP_NAME, groupName);
			donorData.put(DonorAPI.VERSION, "0");
			donorData.put(DonorAPI.RULES, "BD");

		//Act
		int actualDonorId = 0;
		try {
			actualDonorId = DonorAPI.insert(con, donorData);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId=0;
		try {
			expectedId = DonorAPI.getId(con, orgId);
		} catch (SQLException e) {
			LOG.error("GetId "+e.getLocalizedMessage());
		}

		Assert.assertNotEquals(actualDonorId, 0, "Error. Found donor id null. Creation failed. ");
		Assert.assertNotEquals(actualDonorId, -1, "Error. Found donor id negative. Creation failed. ");
		Assert.assertEquals(actualDonorId, expectedId, "Error. Donor id not matching. ");
	}
	
 //TODO: mandatory fields check 
// TODO: update 
	
	/**
	 * 
	 */
	@Test(priority = 20)	
	public void deleteDonor_OrganizationCascadeDelete_Fails() {
			int org_id = 0;
			try {
				org_id = OrganizationAPI.getId(con, donor_name);
			} catch (SQLException e1) {
				LOG.error(e1.getLocalizedMessage());
			}
			//delete organization should delete org as well - fail 
			
			// Act - section
			int orgDeleted = 0;
			String actualError = "None";
			try {
				orgDeleted = OrganizationAPI.delete(con, org_id);
			} catch (SQLException orgDelE) {
					actualError = orgDelE.getLocalizedMessage();				
					LOG.error(actualError);
			}
			
				boolean actualResult=false;
				try {
					actualResult = DonorAPI.isElementPresent(con, org_id);
				} catch (SQLException donorPresentErr) {
					LOG.error(donorPresentErr.getLocalizedMessage());
				}

			// Assert - section
			String expectedError = "ERROR: update or delete on table \"organization\" violates foreign key constraint \"donor_org_id_fkey\" on table \"donor\"";	
			
			Assert.assertTrue(actualError.contains(expectedError), "Error. Cascade delete should not work. ");
			Assert.assertEquals(orgDeleted, 0, "Error. Found delete row amount not 0. ");
			Assert.assertTrue(actualResult, "Error. Cascade delete should not work. Donor should be in the table. ");
		}


	@Test(priority = 21, dependsOnMethods = {"insertDonor_FullDataMap_Pass"})	
	public void deleteDonor_DonorBeforeOrg_Pass() {
		int org_id = 0;
		try {
			org_id = OrganizationAPI.getId(con, donor_name);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}

		// Act - section
		int deletedRows = 0;
		try {

			deletedRows = DonorAPI.delete(con, org_id);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}

	
	@Test(priority = 22, dependsOnMethods = {"deleteDonor_DonorBeforeOrg_Pass"})	
	public void deleteDonor_OrgAfterDonor_Pass() {
		int org_id = 0;
		try {
			org_id = OrganizationAPI.getId(con, donor_name);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}

		// Act - section
		int deletedRows = 0;
		try {

			deletedRows = OrganizationAPI.delete(con, org_id);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}
	
}
