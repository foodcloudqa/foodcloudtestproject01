package com.test.db.copiaAPI;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.ScheduleAPI;
import com.foodcloud.server.FCTestJsonUtils;
import com.foodcloud.server.FCTestUtils;



/**
 * 
 * TODO: Need to create data for this test 
 * (currently relying on Copia-c data)
 *
 */
public class CreateJsonDonationsTest {

	@Test(priority = 1)
		void querySchedule_forStatus_withFixedDonorId(){
		
		Connection con = ConnectionToDb.getCopiaConnection();

		Map<String, String> data = new HashMap<String, String>();
		
		data.put("day", "6");
		data.put("org_id", "11290");
		data.put("status","Available");
							
		String actualScheduleStatus = null;
		try {
			// charityId = ScheduleAPI.getScheduledCharity(con, Integer.parseInt(data.get("org_id")), Integer.parseInt(data.get("day")));

			actualScheduleStatus = ScheduleAPI.getScheduleStatus(con, data.get("org_id"), data.get("day"));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Assert.assertNotNull(actualScheduleStatus , "No Schedule status found");
		Assert.assertEquals(actualScheduleStatus, data.get("status"), "Default Schedule status found");

		
	}	

	@Test(priority = 4)
	  void querySchedule_forStartTime_withFixedDonorId(){
		
		Connection con = ConnectionToDb.getCopiaConnection();

		Map<String, String> data = new HashMap<String, String>();
		
		data.put("day", "3");
		data.put("org_id", "11290");
		data.put("start_time","11:45:00");
							
		String actualStartTime = null;
		try {
			actualStartTime = ScheduleAPI.getStartTime(con, Integer.parseInt(data.get("org_id")), Integer.parseInt(data.get("day")));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Assert.assertNotNull(actualStartTime , "No Schedule status found");
		Assert.assertEquals(actualStartTime, data.get("start_time"), "Default Schedule status found");

	}	

	@Test(priority = 5)
	  void querySchedule_forEndTime_withFixedDonorId(){
		Connection con = ConnectionToDb.getCopiaConnection();

		Map<String, String> data = new HashMap<String, String>();
		
		data.put("day", "4");
		data.put("org_id", "11290");
		data.put("end_time","11:59:00");
							
		String actualEndTime = null;
		try {
			actualEndTime = ScheduleAPI.getEndTime(con, Integer.parseInt(data.get("org_id")), Integer.parseInt(data.get("day")));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Assert.assertNotNull(actualEndTime , "No Schedule status found");
		Assert.assertEquals(actualEndTime, data.get("end_time"), "Default Schedule status found");
		
				
	}	



@Test(priority = 6)
void querySchedule_scheduledDonationsQueries() throws SQLException{
	Connection con = ConnectionToDb.getCopiaConnection();

						
	ResultSet actualEndTime = null;
	try {
		String param_zoneId = FCTestUtils.EUROPE_DUBLIN;
		int currentDay =  FCTestUtils.getCurrentDay(FCTestUtils.getZoneId(param_zoneId));

		actualEndTime = ScheduleAPI.todaysScheduledOrganizations(con, currentDay );

	} catch (SQLException e) {
		e.printStackTrace();
	}

	if(actualEndTime != null) {
		 
		StringBuffer buffer = new StringBuffer();
		buffer.append("INSERT INTO public.donation(");
		buffer.append("external_id, donor_id, description, comments, date, \"time\", status)");
		buffer.append(" VALUES ");

		while(actualEndTime.next()){
			String org_id = actualEndTime.getString(1);
			String charity_id = actualEndTime.getString(2);
			
			ZonedDateTime currentDateTime = FCTestUtils.convertToTimeZonedDate(FCTestUtils.getUTCDate(),FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));
			
			String currentDate = FCTestUtils.formatDateToCopiaUI(currentDateTime, FCTestUtils.TYPE.DATE);
			String currentTime = FCTestUtils.formatDateToCopiaUI(currentDateTime, FCTestUtils.TYPE.TIME);
			
			buffer.append("('"
					+ FCTestJsonUtils.generateUUID().toString() + "','"+org_id+"', '"+charity_id+"', null, '"+ currentDate+"','"+ currentTime+"', 'Available'),");
		 }

		buffer.replace(buffer.length()-1, buffer.length()-1, ";");
					
		buffer.setLength(buffer.length() - 1);	
		
		System.out.println("String buffer cut: " + buffer );				
		
	}
		
	
	Assert.assertNotNull(actualEndTime , "No Schedule status found");
	
			
}

@Test(priority = 7)
public void querySchedule_unscheduledDonationsQueries() throws SQLException{
	Connection con = ConnectionToDb.getCopiaConnection();

						
	ResultSet unscheduledDonationList = null;
	try {
		String param_zoneId = FCTestUtils.EUROPE_DUBLIN;
		int currentDay =  FCTestUtils.getCurrentDay(FCTestUtils.getZoneId(param_zoneId));

		unscheduledDonationList = ScheduleAPI.todaysUnScheduledOrganizations(con, currentDay );
	} catch (SQLException e) {
		e.printStackTrace();
	}

	if(unscheduledDonationList != null) {
		
		StringBuffer buffer = new StringBuffer();
		buffer.append("INSERT INTO public.donation(");
		buffer.append("external_id, donor_id, description, comments, date, \"time\", status)");
		buffer.append(" VALUES ");

		while(unscheduledDonationList.next()){
			String org_id = unscheduledDonationList.getString(1);
			String charity_id = unscheduledDonationList.getString(2);
			ZonedDateTime currentDateTime = FCTestUtils.convertToTimeZonedDate(FCTestUtils.getUTCDate(),FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));
			
			String currentDate = FCTestUtils.formatDateToCopiaUI(currentDateTime, FCTestUtils.TYPE.DATE);
			String currentTime = FCTestUtils.formatDateToCopiaUI(currentDateTime, FCTestUtils.TYPE.TIME);
			
			buffer.append("('"
					+ FCTestJsonUtils.generateUUID().toString() + "','"+org_id+"', '"+charity_id+"', null, '"+ currentDate+"','"+ currentTime+"', 'Available'),");
							
		 }
		
		buffer.replace(buffer.length()-1, buffer.length()-1, ";");
		
		buffer.setLength(buffer.length() - 1);

		System.out.println("String buffer: " + buffer );				
		
	}

}

@Test(priority = 8)
public void createDonations_JsonFile() throws SQLException{
//	{"$type":"org.foodcloud.domain.share.OfferedDonation","donationId":'$uuid,'"donorName":"TESCO","donorExternalId": {"group":"TESCO","number": 4047 },"date": {"value":'$date'},"category":"'$cat'","quantity":'$qty'}
//	 String donorName = "TESCO";
//	 String donorGroup = "TESCO";
//	 int donorNumber = 4047;  // important!!! Type = int  
//		String date = "2017-12-13T10:28:13.7440549+00:00";
	
	 String donorName = "RawChocolate Store 1";
	 String donorGroup = "FC";
	 int donorNumber = 4344;
	 String date = "2017-12-13T13:02:13.7440549+00:00";

	 
	 
	 
//	 JSONObject jsonObject = FCTestJsonUtils.createClientDonationJsonObject(donorName, donorGroup, donorNumber, date, "FFV", 3.5 );

//	 System.out.println("JSON Object: " + jsonObject  );				
	    
	 
	 
	 // Random rand = new Random(); int value = rand.nextInt(20); 
	 
}

//	private JSONObject createJsonObject(String donorName, String donorGroup, int donorNumber, String date, String cat_code, double quantity ){
//		  JSONObject MainObject = new JSONObject();
//		   
//		  MainObject.put("$type", "org.foodcloud.domain.share.OfferedDonation");
//		  MainObject.put("donationId", FCTestUtils.generateUUID().toString());
//		  MainObject.put("donorName",donorName);
//		  
//		  JSONObject externalIdElements = new JSONObject();
//		  externalIdElements.put("group",donorGroup);
//		  externalIdElements.put("number",donorNumber);
//		  
//		  MainObject.accumulate("donorExternalId", externalIdElements);
//		  
//		  JSONObject dateElements = new JSONObject();
//		  dateElements.put("value",date);
//
//		  MainObject.accumulate("date", dateElements);
//
//		  MainObject.put("category", cat_code);
//		  MainObject.put("quantity",quantity);
//
//		  return MainObject;
//	}

}
