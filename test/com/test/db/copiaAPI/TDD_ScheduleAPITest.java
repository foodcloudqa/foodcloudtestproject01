package com.test.db.copiaAPI;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.OrganizationAPI;
import com.foodcloud.db.copiaAPI.ScheduleAPI;
import com.foodcloud.db.copiaAPI.ShortcutsAPI;

/**
 *
 *
 */
public class TDD_ScheduleAPITest {

	private Connection con;
	private String donor_name 		= "Donor Store Schedule Test";
	private String charity_name 	= "Charity Schedule org";
	private String donorGroupName 	= "ScheduleApi donor group";
    private String charityGroupName = "ScheduleApi charity Group ";
	private Integer[] testData;

	
	static final Logger LOG = LogManager.getLogger(TDD_ScheduleAPITest.class.getName());

	@BeforeTest
	public void setup() {
		con = ConnectionToDb.getCopiaConnection();		

		testData = arrangeScheduleTestData(con);
		
		cleanData(con, testData);
	}

	@Test(priority = 1)
	public void insertSchedule_EmptyMap_Fail() {
		// Arrange
		Map<String, String> scheduleData = new HashMap<String, String>();

		// Act
		int result = 0;
		try {
			result = ScheduleAPI.insert(con, scheduleData);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert
		Assert.assertEquals(result, -1, "ScheduleAPI test - Empty map should cause an error");

	}

	@Test(priority = 2)
	public void insertSchedule_OrgIdNull_Fail() {

		Map<String, String> data = new HashMap<String, String>();

		data.put(ScheduleAPI.DAY, "" + 1);
		data.put(ScheduleAPI.ORG_ID, "" + 0);
		data.put(ScheduleAPI.START_TIME, "20:00:00");
		data.put(ScheduleAPI.END_TIME, "21:40:00");
		data.put(ScheduleAPI.STATUS, "Assigned");
		data.put(ScheduleAPI.VERSION, "" + 0);
		data.put(ScheduleAPI.CANCEL_AFTER, "21:40:00");

		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = ScheduleAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualScheduleInserted, expectedId,
				"ScheduleAPI test - No control on mandatory input data (orgId) should cause an error ");
	}

	@Test(priority = 3)
	public void insertSchedule_DayNotPresent_Fail() {

		Map<String, String> data = new HashMap<String, String>();

		data.put(ScheduleAPI.ORG_ID, "" + 1);
		data.put(ScheduleAPI.START_TIME, "20:00:00");
		data.put(ScheduleAPI.END_TIME, "21:40:00");
		data.put(ScheduleAPI.STATUS, "Assigned");
		data.put(ScheduleAPI.VERSION, "" + 0);
		data.put(ScheduleAPI.CANCEL_AFTER, "21:40:00");

		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = ScheduleAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualScheduleInserted, expectedId,
				"ScheduleAPI test - No control on mandatory input data (day) should cause an error");
	}

	@Test(priority = 4)
	public void insertSchedule_OrgIdNotPresent_Fail() {

		Map<String, String> data = new HashMap<String, String>();

		data.put(ScheduleAPI.DAY, "" + 1);
		data.put(ScheduleAPI.START_TIME, "20:00:00");
		data.put(ScheduleAPI.END_TIME, "21:40:00");
		data.put(ScheduleAPI.STATUS, "None");
		data.put(ScheduleAPI.VERSION, "" + 0);
		data.put(ScheduleAPI.CANCEL_AFTER, "21:40:00");

		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = ScheduleAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualScheduleInserted, expectedId,
				"ScheduleAPI test - 0 value for mandatory data (orgId) should cause an error ");
	}

	
	@Test(priority = 5)
	public void insertSchedule_NoStartTime_Fail() {
		// Arrange
		int orgId = testData[0];

		Map<String, String> data = new HashMap<String, String>();

		data.put(ScheduleAPI.DAY, "" + 1);
		data.put(ScheduleAPI.ORG_ID, Integer.toString(orgId));

		data.put(ScheduleAPI.END_TIME, "21:40:00");
		data.put(ScheduleAPI.STATUS, "Available");
		data.put(ScheduleAPI.VERSION, "" + 0);
		data.put(ScheduleAPI.CANCEL_AFTER, "21:40:00");

		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = ScheduleAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 0;

		Assert.assertEquals(actualScheduleInserted, expectedId,
				"ScheduleAPI test - null value for mandatory data (start_time) should cause an error ");
	}
	
	

	@Test(priority = 6)
	public void insertSchedule_NoEndTime_Fail() {
		// Arrange
		int orgId = testData[0];

		Map<String, String> data = new HashMap<String, String>();

		data.put(ScheduleAPI.DAY, "" + 1);
		data.put(ScheduleAPI.ORG_ID, Integer.toString(orgId));
		data.put(ScheduleAPI.START_TIME, "21:40:00");
		
		data.put(ScheduleAPI.STATUS, "Available");
		data.put(ScheduleAPI.VERSION, "" + 0);
		data.put(ScheduleAPI.CANCEL_AFTER, "21:40:00");

		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = ScheduleAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 0;

		Assert.assertEquals(actualScheduleInserted, expectedId,
				"ScheduleAPI test - null value for mandatory data (end_time) should cause an error ");
	}

	
	@Test(priority = 10)
	public void insertSchedule_FullDataMap_Pass() {
		// Arrange
		int donorOrgId =testData[0];
		int charityOrgId =testData[1];
		
		Map<String, String> data = new HashMap<String, String>();

		data.put(ScheduleAPI.DAY, "" + 1);
		data.put(ScheduleAPI.ORG_ID, Integer.toString(donorOrgId ));
		data.put(ScheduleAPI.PARTY_ID, Integer.toString(charityOrgId));
		data.put(ScheduleAPI.START_TIME, "20:00:00");
		data.put(ScheduleAPI.END_TIME, "21:40:00");
		data.put(ScheduleAPI.STATUS, "Assigned");
		data.put(ScheduleAPI.VERSION, "" + 0);
		data.put(ScheduleAPI.CANCEL_AFTER, "21:40:00");

		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = ScheduleAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 1;

		Assert.assertNotEquals(actualScheduleInserted, -1, "Error. Found schedule amount  negative. Creation error. ");
		Assert.assertNotEquals(actualScheduleInserted, 0, "Error. Found schedule amount  null. Creation failed. ");
		Assert.assertEquals(actualScheduleInserted, expectedId, "Error. schedule amount not matching. ");
	}

	@Test(priority = 11)
	public void insertSchedule_withNoPartyId_Pass() {
		// Arrange
		int orgId = testData[0];

		Map<String, String> data = new HashMap<String, String>();

		data.put(ScheduleAPI.DAY, "" + 2);
		data.put(ScheduleAPI.ORG_ID, Integer.toString(orgId));
		data.put(ScheduleAPI.START_TIME, "21:00:00");
		data.put(ScheduleAPI.END_TIME, "21:40:00");
		data.put(ScheduleAPI.STATUS, "Available");
		data.put(ScheduleAPI.VERSION, "" + 0);
		data.put(ScheduleAPI.CANCEL_AFTER, "21:45:00");

		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = ScheduleAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 1;

		Assert.assertNotEquals(actualScheduleInserted, -1, "Error. Found schedule amount  negative. Creation error. ");
		Assert.assertNotEquals(actualScheduleInserted, 0, "Error. Found schedule amount  null. Creation failed. ");
		Assert.assertEquals(actualScheduleInserted, expectedId, "Error. schedule amount not matching. ");
	}


	/**
	 * Insert schedule row without Status
	 * Requirements : No party_id
	 * Expected Result: DB inserting default Status = None
	 */
	@Test(priority = 12)
	public void insertSchedule_withNoStatus_Pass() {
		// Arrange
		int orgId = testData[0];

		Map<String, String> data = new HashMap<String, String>();

		data.put(ScheduleAPI.DAY, "" + 3);
		data.put(ScheduleAPI.ORG_ID, Integer.toString(orgId));
		data.put(ScheduleAPI.START_TIME, "21:00:00");
		data.put(ScheduleAPI.END_TIME, "21:40:00");
		data.put(ScheduleAPI.VERSION, "" + 0);
		data.put(ScheduleAPI.CANCEL_AFTER, "21:45:00");

		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = ScheduleAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 1;

		Assert.assertNotEquals(actualScheduleInserted, -1, "Error. Found schedule amount  negative. Creation error. ");
		Assert.assertNotEquals(actualScheduleInserted, 0, "Error. Found schedule amount  null. Creation failed. ");
		Assert.assertEquals(actualScheduleInserted, expectedId, "Error. schedule amount not matching. ");
	}

	
	
	@Test(priority = 13)
	public void insertSchedule_withNoVersion_Pass() {
		// Arrange
		int orgId = testData[0];
		int charityId = testData[1];

		Map<String, String> data = new HashMap<String, String>();

		data.put(ScheduleAPI.DAY, "" + 4);
		data.put(ScheduleAPI.ORG_ID, Integer.toString(orgId));
		data.put(ScheduleAPI.PARTY_ID, Integer.toString(charityId));
		data.put(ScheduleAPI.START_TIME, "21:00:00");
		data.put(ScheduleAPI.END_TIME, "21:40:00");
		data.put(ScheduleAPI.STATUS, "Assigned");
		data.put(ScheduleAPI.CANCEL_AFTER, "21:45:00");

		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = ScheduleAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 1;

		Assert.assertNotEquals(actualScheduleInserted, -1, "Error. Found schedule amount  negative. Creation error. ");
		Assert.assertNotEquals(actualScheduleInserted, 0, "Error. Found schedule amount  null. Creation failed. ");
		Assert.assertEquals(actualScheduleInserted, expectedId, "Error. schedule amount not matching. ");
	}


	@Test(priority = 14)
	public void insertSchedule_withNoCancelAfter_Pass() {
		// Arrange
		int orgId = testData[0];
		int charityId = testData[1];

		Map<String, String> data = new HashMap<String, String>();

		data.put(ScheduleAPI.DAY, "" + 5);
		data.put(ScheduleAPI.ORG_ID, Integer.toString(orgId));
		data.put(ScheduleAPI.PARTY_ID, Integer.toString(charityId));
		data.put(ScheduleAPI.START_TIME, "22:00:00");
		data.put(ScheduleAPI.END_TIME, "23:00:00");
		data.put(ScheduleAPI.STATUS, "Assigned");
		data.put(ScheduleAPI.VERSION, "" + 2);

		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = ScheduleAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 1;

		Assert.assertNotEquals(actualScheduleInserted, -1, "Error. Found schedule amount  negative. Creation error. ");
		Assert.assertNotEquals(actualScheduleInserted, 0, "Error. Found schedule amount  null. Creation failed. ");
		Assert.assertEquals(actualScheduleInserted, expectedId, "Error. schedule amount not matching. ");
	}

	
	
	// SELECT	
		@Test(priority = 20, dependsOnMethods = {"insertSchedule_withNoPartyId_Pass"} )
		public void selectSchedule_getOrgId_Pass() {
			// Arrange
			int orgId = testData[0];
	
			Map<String, String> searchData = new HashMap<String, String>();
			searchData.put(ScheduleAPI.DAY, "" + 2);
			searchData.put(ScheduleAPI.START_TIME, "21:00:00");
			searchData.put(ScheduleAPI.END_TIME, "21:40:00");
			searchData.put(ScheduleAPI.STATUS, "Available");
			searchData.put(ScheduleAPI.VERSION, "" + 0);
			searchData.put(ScheduleAPI.CANCEL_AFTER, "21:45:00");
			
			Set<String> fieldList = new HashSet<String>();
			fieldList.add(ScheduleAPI.ORG_ID);
			
			// Act		
			Map<String,String> results = null;
			try {
				results = ScheduleAPI.getField(con,fieldList, searchData);
			} catch (SQLException e) {
				LOG.error("Select: " + e.getLocalizedMessage());
			}
			
			// Assert - section			
			Assert.assertNotEquals("Null values", results.get("Error"), "Null values ");
		
			String  actualOrgId  = results.get(ScheduleAPI.ORG_ID);						
			String expectedOrgId  = Integer.toString(orgId);
			// id not null 
			Assert.assertNotNull(actualOrgId, "Error. Found select schedule field null. Selection error. ");
			Assert.assertEquals(actualOrgId, expectedOrgId, "Error. Found select schedule field different. ");		
		}

		@Test(priority = 21, dependsOnMethods = {"insertSchedule_withNoPartyId_Pass"} )
		public void selectSchedule_getDay_Pass() {
			// Arrange
			int orgId = testData[0];

			Map<String, String> searchData = new HashMap<String, String>();
			searchData.put(ScheduleAPI.ORG_ID, Integer.toString(orgId));
			searchData.put(ScheduleAPI.START_TIME, "21:00:00");
			searchData.put(ScheduleAPI.END_TIME, "21:40:00");
			searchData.put(ScheduleAPI.STATUS, "Available");
			searchData.put(ScheduleAPI.VERSION, "" + 0);
			searchData.put(ScheduleAPI.CANCEL_AFTER, "21:45:00");
			
			Set<String> fieldList = new HashSet<String>();
			fieldList.add(ScheduleAPI.DAY);
			
			// Act		
			Map<String,String> results = null;
			try {
				results = ScheduleAPI.getField(con,fieldList, searchData);
			} catch (SQLException e) {
				LOG.error("Select: " + e.getLocalizedMessage());
			}
			
			// Assert - section			
			Assert.assertNotEquals("Null values", results.get("Error"), "Null values ");
		
			String  actualDay  = results.get(ScheduleAPI.DAY);						
			String expectedDay = Integer.toString(2);

			// id not null 
			Assert.assertNotNull(actualDay, "Error. Found select schedule field null. Selection error. ");
			Assert.assertEquals(actualDay, expectedDay, "Error. Found select "+ ScheduleAPI.DAY + " schedule field different. ");		
		}


		@Test(priority = 22, dependsOnMethods = {"insertSchedule_withNoPartyId_Pass"} )
		public void selectSchedule_getStartTime_Pass() {
			// Arrange
			int orgId = testData[0];

			String expectedStartTime = "21:00:00";

			Map<String, String> searchData = new HashMap<String, String>();
			searchData.put(ScheduleAPI.ORG_ID, Integer.toString(orgId));
			searchData.put(ScheduleAPI.DAY, ""+ 2);
			searchData.put(ScheduleAPI.END_TIME, "21:40:00");
			searchData.put(ScheduleAPI.STATUS, "Available");
			searchData.put(ScheduleAPI.VERSION, "" + 0);
			searchData.put(ScheduleAPI.CANCEL_AFTER, "21:45:00");
			
			Set<String> fieldList = new HashSet<String>();
			fieldList.add(ScheduleAPI.START_TIME);
			
			// Act		
			Map<String,String> results = null;
			try {
				results = ScheduleAPI.getField(con,fieldList, searchData);
			} catch (SQLException e) {
				LOG.error("Select: " + e.getLocalizedMessage());
			}
			
			// Assert - section			
			Assert.assertNotEquals("Null values", results.get("Error"), "Null values ");
		
			String  actualStartTime  = results.get(ScheduleAPI.START_TIME);						

			// id not null 
			Assert.assertNotNull(actualStartTime, "Error. Found select schedule field null. Selection error. ");
			Assert.assertEquals(actualStartTime, expectedStartTime, "Error. Found select "+ ScheduleAPI.START_TIME + " schedule field different. ");		
		}

		@Test(priority = 23, dependsOnMethods = {"insertSchedule_withNoPartyId_Pass"} )
		public void selectSchedule_getEndTime_Pass() {
			// Arrange
			int orgId = testData[0];

			String expectedEndTime = "21:40:00";

			Map<String, String> searchData = new HashMap<String, String>();
			searchData.put(ScheduleAPI.ORG_ID, Integer.toString(orgId));
			searchData.put(ScheduleAPI.DAY, ""+ 2);
			searchData.put(ScheduleAPI.START_TIME, "21:00:00");
			searchData.put(ScheduleAPI.STATUS, "Available");
			searchData.put(ScheduleAPI.VERSION, "" + 0);
			searchData.put(ScheduleAPI.CANCEL_AFTER, "21:45:00");
			
			Set<String> fieldList = new HashSet<String>();
			fieldList.add(ScheduleAPI.END_TIME);
			
			// Act		
			Map<String,String> results = null;
			try {
				results = ScheduleAPI.getField(con,fieldList, searchData);
			} catch (SQLException e) {
				LOG.error("Select: " + e.getLocalizedMessage());
			}
			
			// Assert - section			
			Assert.assertNotEquals("Null values", results.get("Error"), "Null values ");
		
			String  actualEndTime  = results.get(ScheduleAPI.END_TIME);						

			// id not null 
			Assert.assertNotNull(actualEndTime, "Error. Found select schedule field null. Selection error. ");
			Assert.assertEquals(actualEndTime, expectedEndTime, "Error. Found select "+ ScheduleAPI.END_TIME + " schedule field different. ");		
		}
	

		@Test(priority = 24, dependsOnMethods = {"insertSchedule_withNoPartyId_Pass"} )
		public void selectSchedule_getStatus_Pass() {
			// Arrange
			int orgId = testData[0];

			String expectedStatus = "Available";

			Map<String, String> searchData = new HashMap<String, String>();
			searchData.put(ScheduleAPI.ORG_ID, Integer.toString(orgId));
			searchData.put(ScheduleAPI.DAY, ""+ 2);
			searchData.put(ScheduleAPI.START_TIME, "21:00:00");
			searchData.put(ScheduleAPI.END_TIME, "21:40:00");
			searchData.put(ScheduleAPI.VERSION, "" + 0);
			searchData.put(ScheduleAPI.CANCEL_AFTER, "21:45:00");
			
			Set<String> fieldList = new HashSet<String>();
			fieldList.add(ScheduleAPI.STATUS);
			
			// Act		
			Map<String,String> results = null;
			try {
				results = ScheduleAPI.getField(con,fieldList, searchData);
			} catch (SQLException e) {
				LOG.error("Select: " + e.getLocalizedMessage());
			}
			
			// Assert - section			
			Assert.assertNotEquals("Null values", results.get("Error"), "Null values ");
		
			String  actualStatus  = results.get(ScheduleAPI.STATUS);						

			// id not null 
			Assert.assertNotNull(actualStatus, "Error. Found select schedule field null. Selection error. ");
			Assert.assertEquals(actualStatus, expectedStatus, "Error. Found select "+ ScheduleAPI.STATUS + " schedule field different. ");		
		}


		@Test(priority = 25, dependsOnMethods = {"insertSchedule_withNoPartyId_Pass"} )
		public void selectSchedule_getVersionAndCancelAfter_Pass() {
			// Arrange
			int orgId = testData[0];

			Map<String, String> searchData = new HashMap<String, String>();
			searchData.put(ScheduleAPI.ORG_ID, Integer.toString(orgId));
			searchData.put(ScheduleAPI.DAY, ""+ 2);
			searchData.put(ScheduleAPI.START_TIME, "21:00:00");
			searchData.put(ScheduleAPI.END_TIME, "21:40:00");
						
			Set<String> fieldList = new HashSet<String>();
			fieldList.add(ScheduleAPI.VERSION);
			fieldList.add(ScheduleAPI.CANCEL_AFTER);
			
			// Act		
			Map<String,String> results = null;
			try {
				results = ScheduleAPI.getField(con,fieldList, searchData);
			} catch (SQLException e) {
				LOG.error("Select: " + e.getLocalizedMessage());
			}
			
			// Assert - section			
			Assert.assertNotEquals("Null values", results.get("Error"), "Null values ");
		
			String  actualVersion  = results.get(ScheduleAPI.VERSION);						
			String  actualCancelAfter  = results.get(ScheduleAPI.CANCEL_AFTER);						
			
			String expectedVersion = "" + 0;
			String expectedCancelAfter = "21:45:00";
			//not null 
			Assert.assertNotNull(actualVersion, "Error. Found select schedule field null. Selection error. ");
			Assert.assertEquals(actualVersion, expectedVersion, "Error. Found select "+ ScheduleAPI.VERSION + " schedule field different. ");		

			Assert.assertNotNull(actualCancelAfter, "Error. Found select schedule field null. Selection error. ");
			Assert.assertEquals(actualCancelAfter, expectedCancelAfter, "Error. Found select "+ ScheduleAPI.CANCEL_AFTER + " schedule field different. ");		

		}

	// UPDATE	
	@Test(priority = 30, dependsOnMethods = {"insertSchedule_withNoPartyId_Pass"} )
	public void updateSchedule_AddPartyId_Pass() {
		// Arrange
		int orgId = testData[0];
		int partyId = testData[1];
		String actualDay = "" + 2;
		
		Map<String, String> data = new HashMap<String, String>();
		data.put(ScheduleAPI.DAY, actualDay);
		data.put(ScheduleAPI.ORG_ID, Integer.toString(orgId));
		data.put(ScheduleAPI.START_TIME, "21:00:00");
		data.put(ScheduleAPI.END_TIME, "21:40:00");
		data.put(ScheduleAPI.STATUS, "Available");
		data.put(ScheduleAPI.VERSION, "" + 0);
		data.put(ScheduleAPI.CANCEL_AFTER, "21:45:00");

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(ScheduleAPI.PARTY_ID, Integer.toString(partyId));
		
		// Act
		int actualScheduleUpdated = 0;
		try {
			actualScheduleUpdated = ScheduleAPI.update(con,data, updateData);
		} catch (SQLException e) {
			LOG.error("Update: " + e.getLocalizedMessage());
		}

		// Assert - section

		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(ScheduleAPI.ORG_ID, Integer.toString(orgId));
		selectData.put(ScheduleAPI.DAY, actualDay);
		
		
		Set<String> fieldList = new HashSet<String>();
		fieldList.add(ScheduleAPI.PARTY_ID);
		
		// Act		
		Map<String,String> results = null;
		try {
			results = ScheduleAPI.getField(con,fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

 		String actualPartyId  = results.get(ScheduleAPI.PARTY_ID);
		String expectedPartyId = Integer.toString(partyId);
		

		Assert.assertNotEquals(actualScheduleUpdated, -1, "Error. Found schedule amount  negative. Creation error. ");
		Assert.assertNotEquals(actualScheduleUpdated, 0, "Error. Found schedule amount  null. Creation failed. ");
		Assert.assertEquals(actualPartyId, expectedPartyId, "Error. schedule amount not matching. ");
	}
	
	
	
	@Test(priority = 40, dependsOnMethods = { "insertSchedule_FullDataMap_Pass" })
	public void deleteSchedule_viaDayAndOrgId_Pass() {
		int org_id = 0;
		try {
			org_id = OrganizationAPI.getId(con, donor_name);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}

		// Act - section
		int deletedRows = 0;
		try {
			Map<String, String> deleteCriterion = new HashMap<String, String>();
			deleteCriterion.put(ScheduleAPI.ORG_ID, "" + org_id);
			deleteCriterion.put(ScheduleAPI.DAY, "" + 1);

			deletedRows = ScheduleAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}

	@Test(priority = 41, dependsOnMethods = { "insertSchedule_withNoPartyId_Pass" })
	public void deleteSchedule_viaDayAndTime_Pass() {
		int org_id = 0;
		try {
			org_id = OrganizationAPI.getId(con, donor_name);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}

		// Act - section
		int deletedRows = 0;
		try {
			Map<String, String> deleteCriterion = new HashMap<String, String>();
			deleteCriterion.put(ScheduleAPI.ORG_ID, "" + org_id);
			deleteCriterion.put(ScheduleAPI.DAY, "" + 2);
			deleteCriterion.put(ScheduleAPI.START_TIME, "21:00:00");

			
			deletedRows = ScheduleAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}
	
	
	@Test(priority = 42, dependsOnMethods = { "insertSchedule_withNoStatus_Pass" })
	public void deleteSchedule_viaDayAndEndTime_Pass() {
		int org_id = 0;
		try {
			org_id = OrganizationAPI.getId(con, donor_name);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}

		// Act - section
		int deletedRows = 0;
		try {
			Map<String, String> deleteCriterion = new HashMap<String, String>();
			deleteCriterion.put(ScheduleAPI.ORG_ID, "" + org_id);
			deleteCriterion.put(ScheduleAPI.DAY, "" + 3);
			deleteCriterion.put(ScheduleAPI.END_TIME, "21:40:00");
			
			deletedRows = ScheduleAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}


	@Test(priority = 43, dependsOnMethods = { "insertSchedule_withNoVersion_Pass" })
	public void deleteSchedule_viaDayAndPartyId_Pass() {
		int org_id = 0;
		try {
			org_id = OrganizationAPI.getId(con, donor_name);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}

		int party_id = 0;
		try {
			party_id = OrganizationAPI.getId(con, charity_name);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}

		// Act - section
		int deletedRows = 0;
		try {
			Map<String, String> deleteCriterion = new HashMap<String, String>();
			deleteCriterion.put(ScheduleAPI.ORG_ID, "" + org_id);
			deleteCriterion.put(ScheduleAPI.DAY, "" + 4);
			deleteCriterion.put(ScheduleAPI.PARTY_ID, "" + party_id);
			
			deletedRows = ScheduleAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}

	
	
	@Test(priority = 44, dependsOnMethods = { "insertSchedule_withNoCancelAfter_Pass" })
	public void deleteSchedule_viaDayAndVersion_Pass() {
		int org_id = 0;
		try {
			org_id = OrganizationAPI.getId(con, donor_name);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}

		// Act - section
		int deletedRows = 0;
		try {
			Map<String, String> deleteCriterion = new HashMap<String, String>();
			deleteCriterion.put(ScheduleAPI.ORG_ID, "" + org_id);
			deleteCriterion.put(ScheduleAPI.DAY, "" + 5);
			deleteCriterion.put(ScheduleAPI.VERSION, "" + 2);
			
			deletedRows = ScheduleAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}
	
	private Integer[] arrangeScheduleTestData(Connection con2) {
		Integer[] results = new Integer[2];

		int donorOrgId = ShortcutsAPI.createOrganization(con2, new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;

			{
                put(ShortcutsAPI.ORG_NAME, donor_name);
                put(ShortcutsAPI.GROUP_NAME, donorGroupName);
                put(ShortcutsAPI.REGION, "Dublin South" );
                put(ShortcutsAPI.ORG_TYPE, ShortcutsAPI.ORG_TYPE_DONOR);                
            }
        });
		
		int charityOrgId = ShortcutsAPI.createOrganization(con2, new HashMap<String, String>() {
			private static final long serialVersionUID = 2L;

			{
                put(ShortcutsAPI.ORG_NAME, charity_name);
                put(ShortcutsAPI.GROUP_NAME, charityGroupName);
                put(ShortcutsAPI.ORG_TYPE, ShortcutsAPI.ORG_TYPE_CHARITY);
                put(ShortcutsAPI.REGION, "Dublin South" );
            }
        });
		results[0] = donorOrgId;
		results[1] = charityOrgId;

		return results;
	}

	
	
	private void cleanData(Connection con, Integer[] data) {
		//searching for Schedule row by org_id search key
		
		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(ScheduleAPI.ORG_ID, Integer.toString(data[0]));

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(ScheduleAPI.ORG_ID);

		//if element present - delete		
		Map<String, String> result = null;
		try {
			result = ScheduleAPI.getField(con, fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		String  currentId = result.get(ScheduleAPI.ORG_ID);
		
		if(currentId != null && Integer.parseInt(currentId) != 0 ) {
			
			Map<String, String> deleteCriterion = new HashMap<String, String>();
			deleteCriterion.put(ScheduleAPI.ORG_ID, result.get(ScheduleAPI.ORG_ID)); 
			
			// Act - section
			int deletedRows = 0;
			try {
				deletedRows = ScheduleAPI.delete(con, deleteCriterion);
			} catch (SQLException e) {
				LOG.error(e.getLocalizedMessage());
			}
		}				
	}


}
