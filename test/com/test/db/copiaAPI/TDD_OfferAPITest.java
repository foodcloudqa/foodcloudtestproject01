package com.test.db.copiaAPI;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.OfferAPI;

/**
 * 
 *
 *
 */
public class TDD_OfferAPITest {

	private Connection con;
	static final Logger LOG = LogManager.getLogger(TDD_OfferAPITest.class.getName());

	@BeforeTest
	public void setup() {
		con = ConnectionToDb.getCopiaConnection();		

	//	testData = arrangeScheduleTestData(con);
		
	//	cleanData(con, testData);
	}
	
	
	@Test(priority = 1)
	public void insertSchedule_EmptyMap_Fail() {
		// Arrange
		Map<String, String> scheduleData = new HashMap<String, String>();

		// Act
		int result = 0;
		try {
			result = OfferAPI.insert(con, scheduleData);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert
		Assert.assertEquals(result, -1, "OfferAPI test - Empty map should cause an error");

	}

	
	
	@Test(priority = 2)
	public void insertOffer_DonorIdNull_Fail() {
		// Arrange
		String contact_data = "000";
		
		Map<String, String> data = new HashMap<String, String>();

		data.put(OfferAPI.DONOR_ID,  ""+0);
		data.put(OfferAPI.CREATED,  ConnectionToDb.formatTimestamp());
		data.put(OfferAPI.DATE,  ConnectionToDb.formatDate());
		data.put(OfferAPI.CONTACT_DATA, contact_data);
		data.put(OfferAPI.STATUS, "Sent");
		
		
		// Act
		int actualOfferInserted = 0;
		try {
			actualOfferInserted = OfferAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualOfferInserted, expectedId,
				"OfferAPI test - No control on mandatory input data (orgId) should cause an error ");
	}
	
	
	
}
