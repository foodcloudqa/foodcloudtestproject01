package com.test.db.copiaAPI;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.CharityAPI;
import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.ContactAPI;
import com.foodcloud.db.copiaAPI.DonorAPI;
import com.foodcloud.db.copiaAPI.LocationAPI;
import com.foodcloud.db.copiaAPI.OrganizationAPI;
import com.foodcloud.db.copiaAPI.RegionAPI;
import com.foodcloud.db.copiaAPI.ScheduleAPI;

public class ArrangeAllOrganizationDataFromFile {

	private Connection con;
	static final Logger LOG = LogManager.getLogger(ArrangeAllOrganizationDataFromFile.class.getName());
	private Map<String, String> resultData = new HashMap<String, String>();
	private String propertyFileName = "spainTestData.properties";
	
	public Map<String, String> getResultData() {
		return resultData;
	}

	@BeforeTest
	public void setup() {
		con = ConnectionToDb.getCopiaConnection();

	}

	private Properties getPropertiesFromFile() {

		URL url = ClassLoader.getSystemResource(propertyFileName);

		System.out.println("url:" + url.getPath());

		Properties appProps = new Properties();
		try {
			appProps.load(new FileInputStream(url.getPath()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return appProps;
	}

	@Test(priority = 1)
	public void insert_Group_FullDataMap_Pass() {
		// Arrange
		Map<String, String> CharityGroupData = new HashMap<String, String>();
		Properties groupProps = this.getPropertiesFromFile();

		CharityGroupData.put("REGION_FULL_NAME", groupProps.getProperty("Region_fullName"));
		CharityGroupData.put("REGION_NAME", groupProps.getProperty("Region_name"));
		CharityGroupData.put("REGION_PARENT", groupProps.getProperty("Region_parent"));
		CharityGroupData.put("REGION_LOCALE", groupProps.getProperty("Region_locale"));
		CharityGroupData.put("REGION_TZ", groupProps.getProperty("Region_tz"));

		CharityGroupData.put("DONOR_GROUP_NAME", groupProps.getProperty("Donor_Group_name"));
		CharityGroupData.put("CHARITY_GROUP_NAME", groupProps.getProperty("Charity_Group_name"));
		//
		CharityGroupData.put("DONOR_NAME", groupProps.getProperty("Donor_name"));
		CharityGroupData.put("DONOR_ADDRESS", groupProps.getProperty("Donor_address"));
		CharityGroupData.put("DONOR_LATITUDE", groupProps.getProperty("Donor_latitude"));
		CharityGroupData.put("DONOR_LONGITUDE", groupProps.getProperty("Donor_latitude"));
		CharityGroupData.put("DONOR_ADDRESS_CODE", groupProps.getProperty("Donor_address_code"));

		CharityGroupData.put("DONOR_COUNTRY", groupProps.getProperty("Donor_country"));
		CharityGroupData.put("DONOR_TIMEZONE", groupProps.getProperty("Donor_timezone"));

		CharityGroupData.put("DONOR_PHONE", groupProps.getProperty("Donor_phone"));
		CharityGroupData.put("DONOR_SMS", groupProps.getProperty("Donor_sms"));
		CharityGroupData.put("DONOR_EMAIL", groupProps.getProperty("Donor_email"));
		CharityGroupData.put("DONOR_PUSH", groupProps.getProperty("Donor_push"));
		CharityGroupData.put("DONOR_CONTACT", groupProps.getProperty("Donor_contact"));

		CharityGroupData.put("DONOR_RANK1", groupProps.getProperty("Donor_rank1"));
		CharityGroupData.put("DONOR_RANK2", groupProps.getProperty("Donor_rank2"));
		CharityGroupData.put("DONOR_RANK3", groupProps.getProperty("Donor_rank3"));
		CharityGroupData.put("DONOR_RANK4", groupProps.getProperty("Donor_rank4"));

		CharityGroupData.put("SCHEDULE_MONDAY_START", groupProps.getProperty("Schedule_Monday_start"));
		CharityGroupData.put("SCHEDULE_MONDAY_END", groupProps.getProperty("Schedule_Monday_end"));
		CharityGroupData.put("SCHEDULE_MONDAY_CHARITY", groupProps.getProperty("Schedule_Monday_charity"));
		CharityGroupData.put("SCHEDULE_MONDAY_CANCEL", groupProps.getProperty("Schedule_Monday_cancelAfter"));

		
		CharityGroupData.put("DONOR_EXTERNAL_ID", groupProps.getProperty("Donor_external_id"));
		CharityGroupData.put("DONOR_POST_BY", groupProps.getProperty("Donor_post_by"));
		CharityGroupData.put("DONOR_COLLECT_START", groupProps.getProperty("Donor_collect_start"));
		CharityGroupData.put("DONOR_COLLECT_END", groupProps.getProperty("Donor_collect_end"));
		CharityGroupData.put("DONOR_GROUP_CODE", groupProps.getProperty("Donor_group_code"));
				
		
		createRegion(CharityGroupData);
		CharityGroupData.put("REGION_ID", getResultData().get("REGION_ID"));
		createDonorGroup(CharityGroupData);
		createCharityGroup(CharityGroupData);

		CharityGroupData.put("DONOR_GROUP_ID", getResultData().get("DONOR_GROUP_ID"));
		createDonor(CharityGroupData);

		// create schedule

	}

	public void createRegion(Map<String, String> inputData) {

		String region = inputData.get("REGION_FULL_NAME");
		// Return region code
		// if the region does not exist returns 0
		int regionCode = 0;
		try {
			regionCode = RegionAPI.getId(con, region);
		} catch (SQLException regErr) {
			LOG.error(regErr.getLocalizedMessage());
		}
		if (regionCode == 0) {
			Map<String, String> regionData = new HashMap<String, String>();

			regionData.put(RegionAPI.PARENT_ID, inputData.get("REGION_PARENT"));
			regionData.put(RegionAPI.FULL_NAME, inputData.get("REGION_FULL_NAME"));
			regionData.put(RegionAPI.NAME, inputData.get("REGION_NAME"));
			regionData.put(RegionAPI.LOCALE, inputData.get("REGION_LOCALE"));
			regionData.put(RegionAPI.TZ, inputData.get("REGION_TZ"));

			int insertRegion = 0;
			try {

				insertRegion = RegionAPI.insert(con, regionData);
			} catch (SQLException e) {
				LOG.error(e.getLocalizedMessage());
			}

			if (insertRegion != 0) {
				int insertedRegionCode = 0;
				try {
					insertedRegionCode = RegionAPI.getId(con, region);
				} catch (SQLException regErr) {
					LOG.error(regErr.getLocalizedMessage());
				}
				regionCode = insertedRegionCode;
			}
			getResultData().put("REGION_ID", Integer.toString(regionCode));
		} // end if
		getResultData().put("REGION_ID", Integer.toString(regionCode));
	}

	public void createCharityGroup(Map<String, String> inputData) {
		// Parent group
		String groupName = inputData.get("CHARITY_GROUP_NAME");
		String regionCode = inputData.get("REGION_ID");

		int group_id = 0;
		try {
			group_id = OrganizationAPI.getId(con, groupName);
		} catch (SQLException sqlE) {
			LOG.error(sqlE.getLocalizedMessage());
		}

		if (group_id == 0) {
			Map<String, String> charityGroupData = new HashMap<String, String>();

			charityGroupData.put(OrganizationAPI.PARENT_ID, "1");
			charityGroupData.put(OrganizationAPI.NAME, groupName);
			charityGroupData.put(OrganizationAPI.REGION_ID, regionCode);
			charityGroupData.put(OrganizationAPI.SECTOR, "Charity");
			charityGroupData.put(OrganizationAPI.REG_TYPE, "None");
			charityGroupData.put(OrganizationAPI.TYPE, "CG");
			charityGroupData.put(OrganizationAPI.CREATED, ConnectionToDb.formatTimestamp());
			charityGroupData.put(OrganizationAPI.STATUS, "Active");
			charityGroupData.put(OrganizationAPI.RATING, "0");
			charityGroupData.put(OrganizationAPI.SUPPORT, "false");
			charityGroupData.put(OrganizationAPI.VERSION, "0");

			// Act - section
			int insertCharityGroup = 0;
			try {

				insertCharityGroup = OrganizationAPI.insert(con, charityGroupData);
			} catch (SQLException e) {
				LOG.error(e.getLocalizedMessage());
			}

			int current_group_id = 0;
			try {
				current_group_id = OrganizationAPI.getId(con, groupName);
			} catch (SQLException sqlE) {
				LOG.error(sqlE.getLocalizedMessage());
			}
			group_id = current_group_id;
		}
		getResultData().put("CHARITY_GROUP_ID", Integer.toString(group_id));

	}

	public void createDonorGroup(Map<String, String> inputData) {
		// Parent group
		String groupName = inputData.get("DONOR_GROUP_NAME");
		String regionCode = inputData.get("REGION_ID");

		int group_id = 0;
		try {
			group_id = OrganizationAPI.getId(con, groupName);
		} catch (SQLException sqlE) {
			LOG.error(sqlE.getLocalizedMessage());
		}

		if (group_id == 0) {
			Map<String, String> donorGroupData = new HashMap<String, String>();

			donorGroupData.put(OrganizationAPI.PARENT_ID, "1");
			donorGroupData.put(OrganizationAPI.NAME, groupName);
			donorGroupData.put(OrganizationAPI.REGION_ID, regionCode);
			donorGroupData.put(OrganizationAPI.SECTOR, "Retail");
			donorGroupData.put(OrganizationAPI.REG_TYPE, "None");
			donorGroupData.put(OrganizationAPI.TYPE, "DG");
			donorGroupData.put(OrganizationAPI.CREATED, ConnectionToDb.formatTimestamp());
			donorGroupData.put(OrganizationAPI.STATUS, "Active");
			donorGroupData.put(OrganizationAPI.RATING, "0");
			donorGroupData.put(OrganizationAPI.SUPPORT, "false");
			donorGroupData.put(OrganizationAPI.VERSION, "0");

			// Act - section
			int insertAmount = 0;
			try {

				insertAmount = OrganizationAPI.insert(con, donorGroupData);
			} catch (SQLException e) {
				LOG.error(e.getLocalizedMessage());
			}

			// is this a future? or the one above?
			int current_group_id = 0;
			try {
				current_group_id = OrganizationAPI.getId(con, groupName);
			} catch (SQLException sqlE) {
				LOG.error(sqlE.getLocalizedMessage());
			}
			group_id = current_group_id;
		}
		getResultData().put("DONOR_GROUP_ID", Integer.toString(group_id));
	}

	public void createDonor(Map<String, String> inputData) {
		// Parent group 
		String groupId = inputData.get("DONOR_GROUP_ID");
		String regionCode = inputData.get("REGION_ID");

		String donorName = inputData.get("DONOR_NAME");

		int donor_id = 0;
		try {
			donor_id = OrganizationAPI.getId(con, donorName);
		} catch (SQLException sqlE) {
			LOG.error(sqlE.getLocalizedMessage());
		}

		if (donor_id == 0) {
			Map<String, String> donorStoreData = new HashMap<String, String>();

			donorStoreData.put(OrganizationAPI.PARENT_ID, groupId);
			donorStoreData.put(OrganizationAPI.NAME, donorName);
			donorStoreData.put(OrganizationAPI.REGION_ID, regionCode);
			donorStoreData.put(OrganizationAPI.SECTOR, "Retail");
			donorStoreData.put(OrganizationAPI.TYPE, "DS");
			donorStoreData.put(OrganizationAPI.CREATED, ConnectionToDb.formatTimestamp());
			donorStoreData.put(OrganizationAPI.STATUS, "Active");
			donorStoreData.put(OrganizationAPI.RATING, "0");
			donorStoreData.put(OrganizationAPI.SUPPORT, "true");
			donorStoreData.put(OrganizationAPI.VERSION, "0");
		
		// Act - section
					int insertAmount = 0;
					try {

						insertAmount = OrganizationAPI.insert(con, donorStoreData);
					} catch (SQLException e) {
						LOG.error(e.getLocalizedMessage());
					}

					// is this a future? or the one above?
					int current_donor_id = 0;
					try {
						current_donor_id = OrganizationAPI.getId(con, donorName);
					} catch (SQLException sqlE) {
						LOG.error(sqlE.getLocalizedMessage());
					}
					donor_id = current_donor_id;
				}
				getResultData().put("DONOR_ID", Integer.toString(donor_id));

		//		
		inputData.put("DONOR_ID",getResultData().get("DONOR_ID"));
				
		createDonorStore(inputData);
	   createLocation(inputData);
	   createContact(inputData);
	   createSchedule(inputData);
	   
		}

	
	
	
	private void createDonorStore(Map<String, String> inputData) {
		
		Map<String, String> donorData = new HashMap<String, String>();
		donorData.put(DonorAPI.ORG_ID, inputData.get("DONOR_ID"));
		donorData.put(DonorAPI.EXTERNAL_ID, inputData.get("DONOR_EXTERNAL_ID"));
	
		donorData.put(DonorAPI.POST_BY, inputData.get("DONOR_POST_BY"));
		donorData.put(DonorAPI.COLLECT_START,  inputData.get("DONOR_COLLECT_START"));
		donorData.put(DonorAPI.COLLECT_END,  inputData.get("DONOR_COLLECT_END"));
		
		donorData.put(DonorAPI.GROUP_NAME, inputData.get("DONOR_GROUP_NAME"));
		donorData.put(DonorAPI.VERSION, "0");

	//Act
	int actualDonorId = 0;
	try {
		actualDonorId = DonorAPI.insert(con, donorData);
	} catch (SQLException e) {
		LOG.error("Insert: " + e.getLocalizedMessage());
	}

		
	}

	private void createSchedule(Map<String, String> inputData) {

		//groupProps.getProperty("Schedule_Monday_start"));
		Map<String, String> data = new HashMap<String, String>();
		
		data.put(ScheduleAPI.DAY, "" + 1);
		data.put(ScheduleAPI.ORG_ID, inputData.get("DONOR_ID"));
	//	data.put(ScheduleAPI.PARTY_ID, Integer.toString(charityOrgId));
		data.put(ScheduleAPI.START_TIME, inputData.get("SCHEDULE_MONDAY_START"));
		data.put(ScheduleAPI.END_TIME,  inputData.get("SCHEDULE_MONDAY_END"));
		data.put(ScheduleAPI.STATUS, "Available");
		data.put(ScheduleAPI.VERSION, "" + 0);
		data.put(ScheduleAPI.CANCEL_AFTER, inputData.get("SCHEDULE_MONDAY_CANCEL"));

		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = ScheduleAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}
		
	}

	// move to separate class..
	private void createContact(Map<String, String> inputData) {
		
		//TODO Refactor	
		Map<String, String> contactData_mail = new HashMap<String, String>();

		contactData_mail.put(ContactAPI.PARTY_ID, inputData.get("DONOR_ID"));
		contactData_mail.put(ContactAPI.PARTY_TYPE, "O");
		contactData_mail.put(ContactAPI.TYPE, "email");
		contactData_mail.put(ContactAPI.DATA, inputData.get("DONOR_EMAIL"));
		contactData_mail.put(ContactAPI.RANK, "2");
		contactData_mail.put(ContactAPI.STATUS, "Active");
		contactData_mail.put(ContactAPI.LABEL, inputData.get("DONOR_CONTACT"));

		// Act
		int donorContactMail = 0;
		try {
			donorContactMail = ContactAPI.insert(con, contactData_mail);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}
		// too sequential : changing data in the map
		contactData_mail.put(ContactAPI.TYPE, "phone");
		contactData_mail.put(ContactAPI.DATA, inputData.get("DONOR_PHONE"));
		contactData_mail.put(ContactAPI.RANK, "1");
		contactData_mail.put(ContactAPI.STATUS, "Active");
		contactData_mail.put(ContactAPI.LABEL, inputData.get("DONOR_CONTACT"));

		// Act
		int donorContactPhone = 0;
		try {
			donorContactPhone = ContactAPI.insert(con, contactData_mail);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}
		// too sequential : changing data in the map
		contactData_mail.put(ContactAPI.TYPE, "push");
		contactData_mail.put(ContactAPI.DATA, inputData.get("DONOR_PUSH"));
		contactData_mail.put(ContactAPI.RANK, "3");
		contactData_mail.put(ContactAPI.STATUS, "Active");
		contactData_mail.put(ContactAPI.LABEL, inputData.get("DONOR_CONTACT"));

		// Act
		int donorContactPush = 0;
		try {
			donorContactPush = ContactAPI.insert(con, contactData_mail);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}
		// too sequential : changing data in the map
		contactData_mail.put(ContactAPI.TYPE, "sms");
		contactData_mail.put(ContactAPI.DATA, inputData.get("DONOR_SMS"));
		contactData_mail.put(ContactAPI.RANK, "4");
		contactData_mail.put(ContactAPI.STATUS, "Active");
		contactData_mail.put(ContactAPI.LABEL, inputData.get("DONOR_CONTACT"));

		// Act
		int donorContactSms = 0;
		try {
			donorContactSms = ContactAPI.insert(con, contactData_mail);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		//	Donor_rank1=phone
		//			Donor_rank2=email
		//			Donor_rank3=push
		//			Donor_rank4=sms
	
	}

	private void createLocation(Map<String, String> inputData) {
			
		Map<String, String> locationData = new HashMap<String, String>();
		
		locationData.put(LocationAPI.ORG_ID, inputData.get("DONOR_ID"));
		locationData.put(LocationAPI.ADDRESS, inputData.get("DONOR_ADDRESS"));
	
		locationData.put(LocationAPI.LATITUDE, inputData.get("DONOR_LATITUDE"));
		locationData.put(LocationAPI.LONGITUDE, inputData.get("DONOR_LATITUDE"));
		locationData.put(LocationAPI.ADDRESS_CODE , inputData.get("DONOR_ADDRESS_CODE"));
		
		locationData.put(LocationAPI.COUNTRY,  inputData.get("DONOR_COUNTRY"));
		locationData.put(LocationAPI.VERSION, "0");
		locationData.put(LocationAPI.TIMEZONE, inputData.get("DONOR_TIMEZONE"));

		// Act
		int actualDonorId = 0;
		try {
			actualDonorId = LocationAPI.insert(con, locationData);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

	}
	

}
