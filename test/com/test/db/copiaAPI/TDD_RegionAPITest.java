package com.test.db.copiaAPI;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.RegionAPI;

/**
 *
 *
 */
public class TDD_RegionAPITest {

	private Connection con;
	private String regionName_Poland = "PL";
	private String regionName_Japan = "JP";
	private String regionName_Venezuela = "VE";

	private String regionFullName = "Warsaw";
	private int dialCode = 48;
	private String timezone = "Europe/Warsaw";
	private int region_Id = 0;

	static final Logger LOG = LogManager.getLogger(TDD_RegionAPITest.class.getName());

	@BeforeTest
	public void setup() {
		con = ConnectionToDb.getCopiaConnection();
		
		cleanData(con, regionName_Poland);
		cleanData(con, regionName_Venezuela);
		cleanData(con, regionName_Japan);

	}

	@Test(priority = 1)
	public void insertRegion_EmptyMap_Fail() {
		// Arrange
		Map<String, String> regionData = new HashMap<String, String>();

		// Act
		int result = 0;
		try {
			result = RegionAPI.insert(con, regionData);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert
		Assert.assertEquals(result, -1, "RegionAPI test - Empty map should cause an error");

	}

	@Test(priority = 2)
	public void insertRegion_NameNull_Fail() {
		// Arrange
		Map<String, String> data = new HashMap<String, String>();

		data.put(RegionAPI.STATUS, "Active");
		data.put(RegionAPI.VERSION, "" + 0);
		data.put(RegionAPI.LOCALE, "en");
		data.put(RegionAPI.TZ, "UTC");

		// Act
		int actualRegionInserted = 0;
		try {
			actualRegionInserted = RegionAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualRegionInserted, expectedId,
				"RegionAPI test - No control on mandatory input data (Name) should cause an error ");
	}

	@Test(priority = 3)
	public void insertRegion_FullNameNotPresent_Fail() {
		// Arrange
		Map<String, String> data = new HashMap<String, String>();

		data.put(RegionAPI.STATUS, "Active");
		data.put(RegionAPI.VERSION, "" + 0);

		// Act
		int actualRegionInserted = 0;
		try {
			actualRegionInserted = RegionAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualRegionInserted, expectedId,
				"RegionAPI test - No control on mandatory input data (FullName) should cause an error");
	}

	@Test(priority = 10)
	public void insertRegion_FullDataMap_Pass() {
		// Arrange
		Map<String, String> data = new HashMap<String, String>();

		data.put(RegionAPI.PARENT_ID, "" + 1);
		data.put(RegionAPI.NAME, regionName_Poland);
		data.put(RegionAPI.FULL_NAME, regionFullName);
		data.put(RegionAPI.IDC, Integer.toString(dialCode));
		data.put(RegionAPI.STATUS, "Active");
		data.put(RegionAPI.VERSION, "" + 0);
		data.put(RegionAPI.LOCALE, "en");
		data.put(RegionAPI.TZ, timezone);

		// Act
		int regInsertedId = 0;
		try {
			regInsertedId = RegionAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}
		region_Id = regInsertedId;
		
		// Assert - section
		int expectedId = 0;
		try {
			expectedId = RegionAPI.getId(con, regionFullName);
		} catch (SQLException e) {
			LOG.error("Insert - Get Id: " + e.getLocalizedMessage());
		}

		Assert.assertNotEquals(regInsertedId, -1, "Error. Found Region id negative. Creation error. ");
		Assert.assertNotEquals(regInsertedId, 0, "Error. Found Region id  null. Creation failed. ");
		Assert.assertEquals(regInsertedId, expectedId, "Error. Region id not matching. ");
	}

	@Test(priority = 20, dependsOnMethods = { "insertRegion_FullDataMap_Pass" })
	public void selectRegion_getRegionId_ByName_Pass() {
		// Arrange
		int lastRegionId = 0;
		try {
			lastRegionId = RegionAPI.getLastId(con);
		} catch (SQLException e1) {
			LOG.error("Select (by orgId): " + e1.getLocalizedMessage());
		}

		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(RegionAPI.NAME, regionName_Poland);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.ID);

		// Act
		Map<String, String> result = null;
		try {
			result = RegionAPI.getField(con, fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals("Null values", result.get("Error"), "Null values ");

		String actualRegionId = result.get(RegionAPI.ID);
		String expectedRegionId = Integer.toString(lastRegionId);

		// id not null
		Assert.assertNotNull(actualRegionId, "Error. Found select Region field null. Selection error. ");
		Assert.assertEquals(actualRegionId, expectedRegionId, "Error. Found select Region field different. ");
	}

	@Test(priority = 21, dependsOnMethods = { "insertRegion_FullDataMap_Pass" })
	public void selectRegion_getParentId_ByName_Pass() {
		// Arrange
		int parentId = 1;

		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(RegionAPI.NAME, regionName_Poland);
		searchData.put(RegionAPI.FULL_NAME, regionFullName);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.PARENT_ID);

		// Act
		Map<String, String> result = null;
		try {
			result = RegionAPI.getField(con, fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals("Null values", result.get("Error"), "Null values ");

		String actualRegionParentId = result.get(RegionAPI.PARENT_ID);
		String expectedParentId = Integer.toString(parentId);

		Assert.assertNotNull(actualRegionParentId, "Error. Found select Region field null. Selection error. ");
		Assert.assertEquals(actualRegionParentId, expectedParentId,
				"Error. Found select Region " + RegionAPI.PARENT_ID + " field different. ");

	}

	@Test(priority = 22, dependsOnMethods = { "insertRegion_FullDataMap_Pass" })
	public void selectRegion_getName_ByTimezone_Pass() {
		// Arrange

		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(RegionAPI.TZ, timezone);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.NAME);

		// Act
		Map<String, String> result = null;
		try {
			result = RegionAPI.getField(con, fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals("Null values", result.get("Error"), "Null values ");

		String actualName = result.get(RegionAPI.NAME);

		Assert.assertNotNull(actualName, "Error. Found select Region field null. Selection error. ");
		Assert.assertEquals(actualName, regionName_Poland,
				"Error. Found select Region " + RegionAPI.NAME + " field different. ");
	}

	@Test(priority = 23, dependsOnMethods = { "insertRegion_FullDataMap_Pass" })
	public void selectRegion_getFullName_ByTimezone_Pass() {
		// Arrange

		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(RegionAPI.TZ, timezone);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.FULL_NAME);

		// Act
		Map<String, String> result = null;
		try {
			result = RegionAPI.getField(con, fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals("Null values", result.get("Error"), "Null values ");

		String actualFullName = result.get(RegionAPI.FULL_NAME);

		Assert.assertNotNull(actualFullName, "Error. Found select Region field null. Selection error. ");
		Assert.assertEquals(actualFullName, regionFullName,
				"Error. Found select Region " + RegionAPI.FULL_NAME + " field different. ");
	}

	@Test(priority = 24, dependsOnMethods = { "insertRegion_FullDataMap_Pass" })
	public void selectRegion_getIDC_ByTimezone_Pass() {
		// Arrange

		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(RegionAPI.TZ, timezone);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.IDC);

		// Act
		Map<String, String> result = null;
		try {
			result = RegionAPI.getField(con, fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals("Null values", result.get("Error"), "Null values ");

		String actualName = result.get(RegionAPI.IDC);

		Assert.assertNotNull(actualName, "Error. Found select Region field null. Selection error. ");
		Assert.assertNotEquals(actualName, regionName_Poland,
				"Error. Found select Region " + RegionAPI.IDC + " field different. ");
	}

	@Test(priority = 25, dependsOnMethods = { "insertRegion_FullDataMap_Pass" })
	public void selectRegion_getStatus_ByTimezone_Pass() {
		// Arrange
		String expectedStatus = "Active";

		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(RegionAPI.TZ, timezone);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.STATUS);

		// Act
		Map<String, String> result = null;
		try {
			result = RegionAPI.getField(con, fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals("Null values", result.get("Error"), "Null values ");

		String actualStatus = result.get(RegionAPI.STATUS);

		Assert.assertNotNull(actualStatus, "Error. Found select Region field null. Selection error. ");
		Assert.assertEquals(actualStatus, expectedStatus,
				"Error. Found select Region " + RegionAPI.STATUS + " field different. ");
	}

	@Test(priority = 26, dependsOnMethods = { "insertRegion_FullDataMap_Pass" })
	public void selectRegion_getVersion_ByTimezone_Pass() {
		// Arrange
		String expectedVersion = Integer.toString(0);

		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(RegionAPI.TZ, timezone);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.VERSION);

		// Act
		Map<String, String> result = null;
		try {
			result = RegionAPI.getField(con, fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals("Null values", result.get("Error"), "Null values ");

		String actualVersion = result.get(RegionAPI.VERSION);

		Assert.assertNotNull(actualVersion, "Error. Found select Region field null. Selection error. ");
		Assert.assertEquals(actualVersion, expectedVersion,
				"Error. Found select Region " + RegionAPI.VERSION + " field different. ");
	}

	@Test(priority = 27, dependsOnMethods = { "insertRegion_FullDataMap_Pass" })
	public void selectRegion_getLocale_ByTimezone_Pass() {
		// Arrange
		String expectedLocale = "en";

		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(RegionAPI.TZ, timezone);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.LOCALE);

		// Act
		Map<String, String> result = null;
		try {
			result = RegionAPI.getField(con, fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals("Null values", result.get("Error"), "Null values ");

		String actualLocale = result.get(RegionAPI.LOCALE);

		Assert.assertNotNull(actualLocale, "Error. Found select Region field null. Selection error. ");
		Assert.assertEquals(actualLocale, expectedLocale,
				"Error. Found select Region " + RegionAPI.LOCALE + " field different. ");
	}

	@Test(priority = 28, dependsOnMethods = { "insertRegion_FullDataMap_Pass" })
	public void selectRegion_getTimezone_byName_Pass() {
		// Arrange

		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(RegionAPI.NAME, regionName_Poland);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.TZ);

		// Act
		Map<String, String> result = null;
		try {
			result = RegionAPI.getField(con, fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals("Null values", result.get("Error"), "Null values ");

		String actualTimezone = result.get(RegionAPI.TZ);

		Assert.assertNotNull(actualTimezone, "Error. Found select Region field null. Selection error. ");
		Assert.assertEquals(actualTimezone, timezone,
				"Error. Found select Region " + RegionAPI.TZ + " field different. ");
	}

	@Test(priority = 30, dependsOnMethods = { "insertRegion_FullDataMap_Pass" })
	public void updateRegion_setName_Pass() {

		String expectedRegionName = "VE";

		// Arrange
		Map<String, String> criteria = new HashMap<String, String>();
		criteria.put(RegionAPI.PARENT_ID, "" + 1);
		criteria.put(RegionAPI.NAME, regionName_Poland);
		criteria.put(RegionAPI.FULL_NAME, regionFullName);
		criteria.put(RegionAPI.IDC, Integer.toString(dialCode));
		criteria.put(RegionAPI.STATUS, "Active");
		criteria.put(RegionAPI.VERSION, "" + 0);
		criteria.put(RegionAPI.LOCALE, "en");
		criteria.put(RegionAPI.TZ, timezone);

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(RegionAPI.NAME, expectedRegionName);

		// Act
		int actualRegionUpdate = 0;
		try {
			actualRegionUpdate = RegionAPI.update(con, criteria, updateData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Expected
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(RegionAPI.FULL_NAME, regionFullName);
		selectData.put(RegionAPI.TZ, timezone);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.NAME);

		Map<String, String> results = null;
		try {
			results = RegionAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert
		String actualRegionName = results.get(RegionAPI.NAME);

		Assert.assertNotEquals(actualRegionUpdate, -1, "Error. Found region amount  negative. Creation error. ");
		Assert.assertNotEquals(actualRegionUpdate, 0, "Error. Found region amount  null. Creation failed. ");
		Assert.assertEquals(actualRegionName, expectedRegionName,
				"Error. Region updated name from " + regionName_Poland + " to " + expectedRegionName + " not matching. ");
	}

	@Test(priority = 31, dependsOnMethods = { "insertRegion_FullDataMap_Pass" })
	public void updateRegion_setFullName_Pass() {

		String modifiedRegionName = regionName_Venezuela;
		String expectedRegionFullName = "Venezuela";

		// Arrange
		Map<String, String> criteria = new HashMap<String, String>();
		criteria.put(RegionAPI.PARENT_ID, "" + 1);
		criteria.put(RegionAPI.NAME, modifiedRegionName);
		// criteria.put(RegionAPI.FULL_NAME, regionFullName);
		criteria.put(RegionAPI.IDC, Integer.toString(dialCode));
		criteria.put(RegionAPI.STATUS, "Active");
		criteria.put(RegionAPI.VERSION, "" + 0);
		criteria.put(RegionAPI.LOCALE, "en");
		criteria.put(RegionAPI.TZ, timezone);

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(RegionAPI.FULL_NAME, expectedRegionFullName);

		// Act
		int actualRegionUpdate = 0;
		try {
			actualRegionUpdate = RegionAPI.update(con, criteria, updateData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Expected
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(RegionAPI.NAME, modifiedRegionName);
		selectData.put(RegionAPI.TZ, timezone);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.FULL_NAME);

		Map<String, String> results = null;
		try {
			results = RegionAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert
		String actualFullRegionName = results.get(RegionAPI.FULL_NAME);

		Assert.assertNotEquals(actualRegionUpdate, -1, "Error. Found region amount  negative. Creation error. ");
		Assert.assertNotEquals(actualRegionUpdate, 0, "Error. Found region amount  null. Creation failed. ");
		Assert.assertEquals(actualFullRegionName, expectedRegionFullName, "Error. Region updated name from "
				+ regionFullName + " to " + expectedRegionFullName + " not matching. ");
	}

	@Test(priority = 32, dependsOnMethods = { "updateRegion_setFullName_Pass" })
	public void updateRegion_setDialCode_Pass() {

		String modifiedRegionName = "VE";
		String modifiedRegionFullName = "Venezuela";
		int expectedDialCode = 58;

		// Arrange
		Map<String, String> criteria = new HashMap<String, String>();
		criteria.put(RegionAPI.PARENT_ID, "" + 1);
		criteria.put(RegionAPI.NAME, modifiedRegionName);
		criteria.put(RegionAPI.FULL_NAME, modifiedRegionFullName);
		criteria.put(RegionAPI.STATUS, "Active");
		criteria.put(RegionAPI.VERSION, "" + 0);
		criteria.put(RegionAPI.LOCALE, "en");
		criteria.put(RegionAPI.TZ, timezone);

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(RegionAPI.IDC, Integer.toString(expectedDialCode));

		// Act
		int actualIdcUpdate = 0;
		try {
			actualIdcUpdate = RegionAPI.update(con, criteria, updateData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Expected
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(RegionAPI.NAME, modifiedRegionName);
		selectData.put(RegionAPI.TZ, timezone);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.IDC);

		Map<String, String> results = null;
		try {
			results = RegionAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert
		String actualDialCode = results.get(RegionAPI.IDC);

		Assert.assertNotEquals(actualIdcUpdate, -1, "Error. Found region amount  negative. Creation error. ");
		Assert.assertNotEquals(actualIdcUpdate, 0, "Error. Found region amount  null. Creation failed. ");
		Assert.assertEquals(actualDialCode, Integer.toString(expectedDialCode), "Error. Region updated "+RegionAPI.IDC+" from "
				+ dialCode + " to " + expectedDialCode + " not matching. ");
	}

	@Test(priority = 33, dependsOnMethods = { "updateRegion_setFullName_Pass" })
	public void updateRegion_setStatus_Pass() {

		String modifiedRegionName = "VE";
		String modifiedRegionFullName = "Venezuela";
		String previousStatus = "Active";
		String expectedStatus = "Inactive";
		

		// Arrange
		Map<String, String> criteria = new HashMap<String, String>();
		criteria.put(RegionAPI.PARENT_ID, "" + 1);
		criteria.put(RegionAPI.NAME, modifiedRegionName);
		criteria.put(RegionAPI.FULL_NAME, modifiedRegionFullName);
		criteria.put(RegionAPI.STATUS, previousStatus);
		criteria.put(RegionAPI.VERSION, "" + 0);
		criteria.put(RegionAPI.LOCALE, "en");
		criteria.put(RegionAPI.TZ, timezone);

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(RegionAPI.STATUS, expectedStatus);

		// Act
		int actualStatusUpdate = 0;
		try {
			actualStatusUpdate = RegionAPI.update(con, criteria, updateData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Expected
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(RegionAPI.NAME, modifiedRegionName);
		selectData.put(RegionAPI.TZ, timezone);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.STATUS);

		Map<String, String> results = null;
		try {
			results = RegionAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert
		String actualStatus = results.get(RegionAPI.STATUS);

		Assert.assertNotEquals(actualStatusUpdate, -1, "Error. Found region amount  negative. Creation error. ");
		Assert.assertNotEquals(actualStatusUpdate, 0, "Error. Found region amount  null. Creation failed. ");
		Assert.assertEquals(actualStatus, expectedStatus, "Error. Region updated "+RegionAPI.STATUS+" from "
				+ previousStatus + " to " + actualStatus + " not matching. ");
	}

	@Test(priority = 34, dependsOnMethods = { "updateRegion_setFullName_Pass" })
	public void updateRegion_setVersion_Pass() {

		String modifiedRegionName = "VE";
		String modifiedRegionFullName = "Venezuela";
		String modifiedStatus = "Inactive";
		int previousVersion = 0;
		int expectedVersion = 1;		

		// Arrange
		Map<String, String> criteria = new HashMap<String, String>();
		criteria.put(RegionAPI.PARENT_ID, "" + 1);
		criteria.put(RegionAPI.NAME, modifiedRegionName);
		criteria.put(RegionAPI.FULL_NAME, modifiedRegionFullName);
		criteria.put(RegionAPI.STATUS, modifiedStatus);
		criteria.put(RegionAPI.VERSION, Integer.toString(previousVersion));
		criteria.put(RegionAPI.LOCALE, "en");
		criteria.put(RegionAPI.TZ, timezone);

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(RegionAPI.VERSION, Integer.toString(expectedVersion));

		// Act
		int actualVersionUpdate = 0;
		try {
			actualVersionUpdate = RegionAPI.update(con, criteria, updateData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Expected
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(RegionAPI.NAME, modifiedRegionName);
		selectData.put(RegionAPI.TZ, timezone);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.VERSION);

		Map<String, String> results = null;
		try {
			results = RegionAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert
		String actualVersion = results.get(RegionAPI.VERSION);

		Assert.assertNotEquals(actualVersionUpdate, -1, "Error. Found region amount  negative. Creation error. ");
		Assert.assertNotEquals(actualVersionUpdate, 0, "Error. Found region amount  null. Creation failed. ");
		Assert.assertEquals(actualVersionUpdate, expectedVersion, "Error. Region updated "+RegionAPI.VERSION+" from "
				+ previousVersion + " to " + actualVersion + " not matching. ");
	}

	
	@Test(priority = 35, dependsOnMethods = { "updateRegion_setStatus_Pass" })
	public void updateRegion_setLocale_Pass() {

		String modifiedRegionName = "VE";
		String modifiedRegionFullName = "Venezuela";
		String modifiedStatus = "Inactive";
		int modifiedVersion = 1;	
		String previousLocale = "en";
		String expectedLocale = "es";

		// Arrange
		Map<String, String> criteria = new HashMap<String, String>();
		criteria.put(RegionAPI.PARENT_ID, "" + 1);
		criteria.put(RegionAPI.NAME, modifiedRegionName);
		criteria.put(RegionAPI.FULL_NAME, modifiedRegionFullName);
		criteria.put(RegionAPI.STATUS, modifiedStatus);
		criteria.put(RegionAPI.VERSION, Integer.toString(modifiedVersion));
		criteria.put(RegionAPI.LOCALE, previousLocale);
		criteria.put(RegionAPI.TZ, timezone);

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(RegionAPI.LOCALE, expectedLocale );

		// Act
		int actualLocaleUpdate = 0;
		try {
			actualLocaleUpdate = RegionAPI.update(con, criteria, updateData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Expected
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(RegionAPI.NAME, modifiedRegionName);
		selectData.put(RegionAPI.FULL_NAME,  modifiedRegionFullName);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.LOCALE);

		Map<String, String> results = null;
		try {
			results = RegionAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert
		String actualLocale = results.get(RegionAPI.LOCALE);

		Assert.assertNotEquals(actualLocaleUpdate, -1, "Error. Found region amount  negative. Creation error. ");
		Assert.assertNotEquals(actualLocaleUpdate, 0, "Error. Found region amount  null. Creation failed. ");
		Assert.assertEquals(actualLocale, expectedLocale, "Error. Region updated "+RegionAPI.LOCALE +" from "
				+ previousLocale + " to " + expectedLocale + " not matching. ");
	}

	@Test(priority = 36, dependsOnMethods = { "updateRegion_setLocale_Pass" })
	public void updateRegion_setTimezone_Pass() {

		String modifiedRegionName = "VE";
		String modifiedRegionFullName = "Venezuela";
		String modifiedStatus = "Inactive";
		int modifiedVersion = 1;	
		String modifiedLocale = "es";		

		String expectedTimezone = "America/Caracas";

		// Arrange
		Map<String, String> criteria = new HashMap<String, String>();
		criteria.put(RegionAPI.PARENT_ID, "" + 1);
		criteria.put(RegionAPI.NAME, modifiedRegionName);
		criteria.put(RegionAPI.FULL_NAME, modifiedRegionFullName);
		criteria.put(RegionAPI.STATUS, modifiedStatus);
		criteria.put(RegionAPI.VERSION, Integer.toString(modifiedVersion));
		criteria.put(RegionAPI.LOCALE, modifiedLocale);
		criteria.put(RegionAPI.TZ, timezone);

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(RegionAPI.TZ, expectedTimezone );

		// Act
		int actualTimeZoneUpdate = 0;
		try {
			actualTimeZoneUpdate = RegionAPI.update(con, criteria, updateData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Expected
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(RegionAPI.NAME, modifiedRegionName);
		selectData.put(RegionAPI.FULL_NAME,  modifiedRegionFullName);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.TZ);

		Map<String, String> results = null;
		try {
			results = RegionAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert
		String actualTimezone = results.get(RegionAPI.TZ);

		Assert.assertNotEquals(actualTimeZoneUpdate, -1, "Error. Found region amount  negative. Creation error. ");
		Assert.assertNotEquals(actualTimeZoneUpdate, 0, "Error. Found region amount  null. Creation failed. ");
		Assert.assertEquals(actualTimezone, expectedTimezone, "Error. Region updated "+RegionAPI.TZ +" from "
				+ timezone + " to " + expectedTimezone + " not matching. ");
	}
	
	@Test(priority = 40, dependsOnMethods = { "insertRegion_FullDataMap_Pass", "updateRegion_setFullName_Pass" })
	public void updateRegion_FullDataMap_Pass() {

		// Arrange
		String modifiedRegionName = regionName_Venezuela;
		
		String[] expectedValues = {"1", "JP", "Japan", Integer.toString(81), "Active", "1", "jp", "Asia/Tokyo" };
		
		Map<String, String> criteria = new HashMap<String, String>();
		criteria.put(RegionAPI.PARENT_ID, "" + 1);
		criteria.put(RegionAPI.NAME, modifiedRegionName);

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(RegionAPI.PARENT_ID, expectedValues[0]);
		updateData.put(RegionAPI.NAME, expectedValues[1]);
		updateData.put(RegionAPI.FULL_NAME, expectedValues[2]);
		updateData.put(RegionAPI.IDC, expectedValues[3]);
		updateData.put(RegionAPI.STATUS, expectedValues[4]);
		updateData.put(RegionAPI.VERSION, expectedValues[5]);
		updateData.put(RegionAPI.LOCALE, expectedValues[6]);
		updateData.put(RegionAPI.TZ, expectedValues[7]);


		// Act
		int actualRegionUpdate = 0;
		try {
			actualRegionUpdate  = RegionAPI.update(con, criteria, updateData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Expected
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(RegionAPI.NAME, expectedValues[1]);
		selectData.put(RegionAPI.FULL_NAME,  expectedValues[2]);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.ID);
		fieldList.add(RegionAPI.PARENT_ID);
		fieldList.add(RegionAPI.NAME);
		fieldList.add(RegionAPI.FULL_NAME);
		fieldList.add(RegionAPI.IDC);
		fieldList.add(RegionAPI.STATUS);
		fieldList.add(RegionAPI.VERSION);
		fieldList.add(RegionAPI.LOCALE);
		fieldList.add(RegionAPI.TZ);


		Map<String, String> results = null;
		try {
			results = RegionAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert
		String actualID = results.get(RegionAPI.ID);
		String actualParentId = results.get(RegionAPI.PARENT_ID);
		String actualName = results.get(RegionAPI.NAME);
		String actualFullName = results.get(RegionAPI.FULL_NAME);
		String actualIdc = results.get(RegionAPI.IDC);
		String actualStatus = results.get(RegionAPI.STATUS);
		String actualVersion = results.get(RegionAPI.VERSION);
		String actualLocale = results.get(RegionAPI.LOCALE);
		String actualTimezone = results.get(RegionAPI.TZ);

		Assert.assertNotEquals(actualRegionUpdate, -1, "Error. Found region amount  negative. Creation error. ");
		Assert.assertNotEquals(actualRegionUpdate, 0, "Error. Found region amount  null. Creation failed. ");
		Assert.assertEquals(actualID, Integer.toString(region_Id), "Error. Region updated "+RegionAPI.ID +" from "
				+ region_Id + " to " + region_Id + " not matching. ");

		Assert.assertEquals(actualParentId, expectedValues[0] , "Error. Region updated "+RegionAPI.PARENT_ID +" from "
				+ expectedValues[0] + " to " + actualParentId + " not matching. ");

		Assert.assertEquals(actualName, expectedValues[1] , "Error. Region updated "+RegionAPI.NAME +" from "
				+ regionName_Poland + " to " + expectedValues[1] + " not matching. ");

		Assert.assertEquals(actualFullName, expectedValues[2] , "Error. Region updated "+RegionAPI.FULL_NAME +" from "
				+ regionFullName + " to " + expectedValues[2] + " not matching. ");

		Assert.assertEquals(actualIdc, expectedValues[3] , "Error. Region updated "+RegionAPI.IDC +" from "
				+ dialCode + " to " + expectedValues[3] + " not matching. ");

		Assert.assertEquals(actualStatus, expectedValues[4] , "Error. Region updated "+RegionAPI.STATUS +" from Active"
				+ " to " + expectedValues[4] + " not matching. ");

		Assert.assertEquals(actualVersion, expectedValues[5] , "Error. Region updated "+RegionAPI.VERSION +" from 0 "
				+ " to " + expectedValues[5] + " not matching. ");

		Assert.assertEquals(actualLocale, expectedValues[6] , "Error. Region updated "+RegionAPI.LOCALE +" from en "
				+ " to " + expectedValues[6] + " not matching. ");

		Assert.assertEquals(actualTimezone, expectedValues[7] , "Error. Region updated "+RegionAPI.TZ +" from " + timezone
				+ " to " + expectedValues[7] + " not matching. ");

	}

	@Test(priority = 50, dependsOnMethods = { "insertRegion_FullDataMap_Pass", "updateRegion_FullDataMap_Pass" })
	public void deleteRegion_viaNameAndParentId_Pass() {
		// Arrange - section
		String regionName = regionName_Japan;

		Map<String, String> deleteCriterion = new HashMap<String, String>();
		deleteCriterion.put(RegionAPI.PARENT_ID, "" + 1);
		deleteCriterion.put(RegionAPI.NAME, regionName);

		// Act - section
		int deletedRows = 0;
		try {
			deletedRows = RegionAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}

	
	private void cleanData(Connection con, String regionName) {
		
		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(RegionAPI.NAME, regionName);

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.ID);

		//if element present - delete		
		Map<String, String> result = null;
		try {
			result = RegionAPI.getField(con, fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		String  currentId = result.get(RegionAPI.ID);
		
		if(currentId != null && Integer.parseInt(currentId) != 0 ) {
			
			Map<String, String> deleteCriterion = new HashMap<String, String>();
			deleteCriterion.put(RegionAPI.ID, result.get(RegionAPI.ID)); 
			
			// Act - section
			int deletedRows = 0;
			try {
				deletedRows = RegionAPI.delete(con, deleteCriterion);
			} catch (SQLException e) {
				LOG.error(e.getLocalizedMessage());
			}
		}				
	}
	
}
