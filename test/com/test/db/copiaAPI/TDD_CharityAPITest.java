package com.test.db.copiaAPI;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.CharityAPI;
import com.foodcloud.db.copiaAPI.ShortcutsAPI;

/**
 *
 *
 */
public class TDD_CharityAPITest {

	private Connection con;
	private String org_name = "CharityAPI Test";
	private String charityGroupName = "CharityApi group";

	static final Logger LOG = LogManager.getLogger(TDD_CharityAPITest.class.getName());
	private Integer testData;

	@BeforeTest
	public void setup() {
		con = ConnectionToDb.getCopiaConnection();

		testData = arrangeCharityTestData(con);

		// cleanData(con, testData);

	}

	@Test(priority = 1)
	public void insertCharity_EmptyMap_Fail() {
		// Arrange
		Map<String, String> CharityData = new HashMap<String, String>();

		// Act
		int result = 0;
		try {
			result = CharityAPI.insert(con, CharityData);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert
		Assert.assertEquals(result, -1, "CharityAPI test - Empty map should cause an error ");

	}

	@Test(priority = 2)
	public void insertCharity_OrgIdNull_Fail() {
		// Arrange
		Map<String, String> CharityData_mail = new HashMap<String, String>();

		CharityData_mail.put(CharityAPI.RADIUS, "25");
		CharityData_mail.put(CharityAPI.VEHICLE, "False");
		CharityData_mail.put(CharityAPI.ACCEPT_AMBIENT, "False");
		CharityData_mail.put(CharityAPI.ACCEPT_CHILLED, "False");

		// Act
		int actualCharityId = 0;
		try {
			actualCharityId = CharityAPI.insert(con, CharityData_mail);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualCharityId, expectedId,
				"actualCharityIdAPI test - No control on mandatory input data (orgId) should cause an error ");
	}

	@Test(priority = 3)
	public void insertCharity_OrgIdZero_Fail() {
		// Arrange
		Map<String, String> CharityData_mail = new HashMap<String, String>();

		CharityData_mail.put(CharityAPI.ORG_ID, "0");
		CharityData_mail.put(CharityAPI.RADIUS, "25");
		CharityData_mail.put(CharityAPI.VEHICLE, "False");
		CharityData_mail.put(CharityAPI.ACCEPT_AMBIENT, "False");
		CharityData_mail.put(CharityAPI.ACCEPT_CHILLED, "False");

		// Act
		int actualCharityId = 0;
		try {
			actualCharityId = CharityAPI.insert(con, CharityData_mail);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualCharityId, expectedId,
				"actualCharityIdAPI test - No control on mandatory input data (orgId 0) should cause an error ");
	}

	@Test(priority = 10)
	public void insertCharity_FullDataMap_Pass() {
		// Arrange
		int orgId = testData;

		Map<String, String> CharityData_mail = new HashMap<String, String>();
		// default data
		CharityData_mail.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		CharityData_mail.put(CharityAPI.RADIUS, "5");
		CharityData_mail.put(CharityAPI.VEHICLE, "false");
		CharityData_mail.put(CharityAPI.ACCEPT_AMBIENT, "false");
		CharityData_mail.put(CharityAPI.ACCEPT_CHILLED, "false");
		CharityData_mail.put(CharityAPI.VERSION, "0");
		CharityData_mail.put(CharityAPI.MULTI_DONATION, "false");
		CharityData_mail.put(CharityAPI.ACCEPT_FROZEN, "false");

		// Act
		int actualInsert = 0;
		try {
			actualInsert = CharityAPI.insert(con, CharityData_mail);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}
		// Assert - section
		int expectedInsert = 1;

		int dbId = 0;

		try {
			dbId = CharityAPI.getId(con, orgId);
		} catch (SQLException e) {
			LOG.error("GetId " + e.getLocalizedMessage());
		}

		Assert.assertNotEquals(actualInsert, 0, "Error. Found Charity insert value null. Creation failed. ");
		Assert.assertNotEquals(expectedInsert, -1, "Error. Found Charity insert value negative. Creation failed. ");
		Assert.assertEquals(expectedInsert, expectedInsert, "Error. Charity insert value not matching. ");
		Assert.assertNotEquals(dbId, 0, "Error. Found Charity id null.	 Creation failed. ");

	}

	@Test(priority = 11)
	public void insertCharity_ExistingOrgId_Fail() {
			// Arrange

			Map<String, String> CharityData_mail = new HashMap<String, String>();

			CharityData_mail.put(CharityAPI.ORG_ID, "0");
			CharityData_mail.put(CharityAPI.RADIUS, "25");
			CharityData_mail.put(CharityAPI.VEHICLE, "False");
			CharityData_mail.put(CharityAPI.ACCEPT_AMBIENT, "False");
			CharityData_mail.put(CharityAPI.ACCEPT_CHILLED, "False");

			// Act
			int actualCharityId = 0;
			try {
				actualCharityId = CharityAPI.insert(con, CharityData_mail);
			} catch (SQLException e) {
				LOG.error("Insert: " + e.getLocalizedMessage());
			}

			// Assert - section
			int expectedId = -1;

			Assert.assertEquals(actualCharityId, expectedId,
					"actualCharityIdAPI test - No control on mandatory input data (orgId 0) should cause an error ");
		}

	@Test(priority = 20, dependsOnMethods = { "insertCharity_FullDataMap_Pass" })
	public void selectCharity_getCharityId_ByOrgId_Pass() {
		// Arrange
		int orgId = testData;

		int lastCharityId = 0;
		try {
			lastCharityId = CharityAPI.getLastId(con);
		} catch (SQLException e1) {
			LOG.error("Select (by last Id): " + e1.getLocalizedMessage());
		}

		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(CharityAPI.ORG_ID, Integer.toString(orgId));

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(CharityAPI.ID);

		// Act
		Map<String, String> result = null;
		try {
			result = CharityAPI.getField(con, fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals("Null values", result.get("Error"), "Null values ");

		String actualCharityId = result.get(CharityAPI.ID);
		String expectedCharityId = Integer.toString(lastCharityId);

		// id not null
		Assert.assertNotNull(actualCharityId, "Error. Found select Charity field null. Selection error. ");
		Assert.assertEquals(actualCharityId, expectedCharityId, "Error. Found select Charity field different. ");

	}

	@Test(priority = 30, dependsOnMethods = { "insertCharity_FullDataMap_Pass" })
	public void updateCharity_ModifyRadius_Pass() {
		// Arrange
		int orgId = testData;

		int radiusAsInt = 55;

		Map<String, String> data = new HashMap<String, String>();
		data.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		data.put(CharityAPI.RADIUS, "5");
		data.put(CharityAPI.VEHICLE, "false");
		data.put(CharityAPI.ACCEPT_AMBIENT, "false");
		data.put(CharityAPI.VERSION, "" + 0);
		data.put(CharityAPI.MULTI_DONATION, "false");

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(CharityAPI.RADIUS, Integer.toString(radiusAsInt));

		// Act
		int actualCharityUpdated = 0;
		try {
			actualCharityUpdated = CharityAPI.update(con, data, updateData);
		} catch (SQLException e) {
			LOG.error("Update: " + e.getLocalizedMessage());
		}

		// Assert - section

		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		selectData.put(CharityAPI.VEHICLE, "false");

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(CharityAPI.RADIUS);

		// Act
		Map<String, String> results = null;
		try {
			results = CharityAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		String actualRadius = results.get(CharityAPI.RADIUS);
		String expectedRadius = Integer.toString(radiusAsInt);

		Assert.assertNotEquals(actualCharityUpdated, -1, "Error. Found Charity amount  negative. Creation error. ");
		Assert.assertNotEquals(actualCharityUpdated, 0, "Error. Found Charity amount  null. Creation failed. ");
		Assert.assertEquals(actualRadius, expectedRadius, "Error. Charity field updated (radius) not matching. ");
	}

	@Test(priority = 31, dependsOnMethods = { "updateCharity_ModifyRadius_Pass" })
	public void updateCharity_ModifyHasVehicle_Pass() {
		// Arrange
		int orgId = testData;

		Boolean hasVehicle = true;
		String expectedVehicle = Boolean.toString(hasVehicle);

		Map<String, String> data = new HashMap<String, String>();
		data.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		data.put(CharityAPI.RADIUS, "55");
		data.put(CharityAPI.VEHICLE, "false");
		data.put(CharityAPI.ACCEPT_AMBIENT, "false");
		data.put(CharityAPI.VERSION, "" + 0);
		data.put(CharityAPI.MULTI_DONATION, "false");

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(CharityAPI.VEHICLE, expectedVehicle);

		// Act
		int actualCharityUpdated = 0;
		try {
			actualCharityUpdated = CharityAPI.update(con, data, updateData);
		} catch (SQLException e) {
			LOG.error("Update: " + e.getLocalizedMessage());
		}

		// Assert - section
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		selectData.put(CharityAPI.VERSION, "0");

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(CharityAPI.VEHICLE);

		Map<String, String> results = null;
		try {
			results = CharityAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		String actualVehicle = results.get(CharityAPI.VEHICLE);

		Assert.assertNotEquals(actualCharityUpdated, -1, "Error. Found Charity amount  negative. Creation error. ");
		Assert.assertNotEquals(actualCharityUpdated, 0, "Error. Found Charity amount  null. Creation failed. ");
		Assert.assertEquals(actualVehicle, expectedVehicle, "Error. Charity field updated (radius) not matching. ");
	}

	@Test(priority = 32, dependsOnMethods = { "updateCharity_ModifyHasVehicle_Pass" })
	public void updateCharity_ModifyAcceptAmbient_Pass() {
		// Arrange
		int orgId = testData;

		Boolean acceptAmbient = true;
		String expectedAmbient = Boolean.toString(acceptAmbient);

		Map<String, String> data = new HashMap<String, String>();
		data.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		data.put(CharityAPI.RADIUS, "55");
		data.put(CharityAPI.VEHICLE, "true");
		data.put(CharityAPI.ACCEPT_AMBIENT, "false");
		data.put(CharityAPI.VERSION, "" + 0);
		data.put(CharityAPI.MULTI_DONATION, "false");

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(CharityAPI.ACCEPT_AMBIENT, expectedAmbient);

		// Act
		int actualCharityUpdated = 0;
		try {
			actualCharityUpdated = CharityAPI.update(con, data, updateData);
		} catch (SQLException e) {
			LOG.error("Update: " + e.getLocalizedMessage());
		}

		// Assert - section
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		selectData.put(CharityAPI.VERSION, "0");

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(CharityAPI.ACCEPT_AMBIENT);

		Map<String, String> results = null;
		try {
			results = CharityAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		String actualAmbient = results.get(CharityAPI.ACCEPT_AMBIENT);

		Assert.assertNotEquals(actualCharityUpdated, -1, "Error. Found Charity amount  negative. Creation error. ");
		Assert.assertNotEquals(actualCharityUpdated, 0, "Error. Found Charity amount  null. Creation failed. ");
		Assert.assertEquals(actualAmbient, expectedAmbient,
				"Error. Charity field updated (accept ambient) not matching. ");
	}

	@Test(priority = 33, dependsOnMethods = { "updateCharity_ModifyAcceptAmbient_Pass" })
	public void updateCharity_AddAcceptChilled_Pass() {
		// Arrange
		int orgId = testData;

		Boolean acceptChilled = true;
		String expectedChilled = Boolean.toString(acceptChilled);

		Map<String, String> data = new HashMap<String, String>();
		data.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		data.put(CharityAPI.RADIUS, "55");
		data.put(CharityAPI.VEHICLE, "true");
		data.put(CharityAPI.ACCEPT_AMBIENT, "true");
		data.put(CharityAPI.VERSION, "" + 0);
		data.put(CharityAPI.MULTI_DONATION, "false");

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(CharityAPI.ACCEPT_CHILLED, expectedChilled);

		// Act
		int actualCharityUpdated = 0;
		try {
			actualCharityUpdated = CharityAPI.update(con, data, updateData);
		} catch (SQLException e) {
			LOG.error("Update: " + e.getLocalizedMessage());
		}

		// Assert - section
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		selectData.put(CharityAPI.VERSION, "0");

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(CharityAPI.ACCEPT_CHILLED);

		Map<String, String> results = null;
		try {
			results = CharityAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		String actualChilled = results.get(CharityAPI.ACCEPT_CHILLED);

		Assert.assertNotEquals(actualCharityUpdated, -1, "Error. Found Charity amount  negative. Creation error. ");
		Assert.assertNotEquals(actualCharityUpdated, 0, "Error. Found Charity amount  null. Creation failed. ");
		Assert.assertEquals(actualChilled, expectedChilled,
				"Error. Charity field updated (accept ambient) not matching. ");
	}

	@Test(priority = 34, dependsOnMethods = { "updateCharity_ModifyAcceptAmbient_Pass" })
	public void updateCharity_ModifyVersion_Pass() {
		// Arrange - section
		int orgId = testData;

		int version = 1;
		String expectedVersion = Integer.toString(version);

		Map<String, String> data = new HashMap<String, String>();
		data.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		data.put(CharityAPI.RADIUS, "55");
		data.put(CharityAPI.VEHICLE, "true");
		data.put(CharityAPI.ACCEPT_AMBIENT, "true");
		data.put(CharityAPI.VERSION, "" + 0);
		data.put(CharityAPI.MULTI_DONATION, "false");

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(CharityAPI.VERSION, expectedVersion);

		// Act
		int actualCharityUpdated = 0;
		try {
			actualCharityUpdated = CharityAPI.update(con, data, updateData);
		} catch (SQLException e) {
			LOG.error("Update: " + e.getLocalizedMessage());
		}

		// Assert - section
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		selectData.put(CharityAPI.MULTI_DONATION, "false");

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(CharityAPI.VERSION);

		Map<String, String> results = null;
		try {
			results = CharityAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		String actualVersion = results.get(CharityAPI.VERSION);

		Assert.assertNotEquals(actualCharityUpdated, -1, "Error. Found Charity amount  negative. Creation error. ");
		Assert.assertNotEquals(actualCharityUpdated, 0, "Error. Found Charity amount  null. Creation failed. ");
		Assert.assertEquals(actualVersion, expectedVersion,
				"Error. Charity field updated (accept ambient) not matching. ");
	}
	//////

	@Test(priority = 35, dependsOnMethods = { "updateCharity_ModifyVersion_Pass" })
	public void updateCharity_ModifyAcceptMultiDonation_Pass() {
		// Arrange - section
		int orgId = testData;

		Boolean acceptMulti = true;
		String expectedMulti = Boolean.toString(acceptMulti);

		Map<String, String> data = new HashMap<String, String>();
		data.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		data.put(CharityAPI.RADIUS, "55");
		data.put(CharityAPI.VEHICLE, "true");
		data.put(CharityAPI.ACCEPT_AMBIENT, "true");
		data.put(CharityAPI.VERSION, "" + 1);
		data.put(CharityAPI.MULTI_DONATION, "false");

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(CharityAPI.MULTI_DONATION, expectedMulti);

		// Act
		int actualCharityUpdated = 0;
		try {
			actualCharityUpdated = CharityAPI.update(con, data, updateData);
		} catch (SQLException e) {
			LOG.error("Update: " + e.getLocalizedMessage());
		}

		// Assert - section
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		selectData.put(CharityAPI.VEHICLE, "true");

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(CharityAPI.MULTI_DONATION);

		Map<String, String> results = null;
		try {
			results = CharityAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		String actualMulti = results.get(CharityAPI.MULTI_DONATION);

		Assert.assertNotEquals(actualCharityUpdated, -1, "Error. Found Charity amount  negative. Creation error. ");
		Assert.assertNotEquals(actualCharityUpdated, 0, "Error. Found Charity amount  null. Creation failed. ");
		Assert.assertEquals(actualMulti, expectedMulti, "Error. Charity field updated (Multi donation) not matching. ");
	}

	@Test(priority = 36, dependsOnMethods = { "updateCharity_ModifyAcceptMultiDonation_Pass" })
	public void updateCharity_AddExternalId_Pass() {
		// Arrange
		int orgId = testData;

		String expectedExtId = "TDD_API_TEST:00010";

		Map<String, String> data = new HashMap<String, String>();
		data.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		data.put(CharityAPI.RADIUS, "55");
		data.put(CharityAPI.VEHICLE, "true");
		data.put(CharityAPI.ACCEPT_AMBIENT, "true");
		data.put(CharityAPI.VERSION, "" + 1);
		data.put(CharityAPI.MULTI_DONATION, "true");

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(CharityAPI.EXTERNAL_ID, expectedExtId);

		// Act
		int actualCharityUpdated = 0;
		try {
			actualCharityUpdated = CharityAPI.update(con, data, updateData);
		} catch (SQLException e) {
			LOG.error("Update: " + e.getLocalizedMessage());
		}

		// Assert - section
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		selectData.put(CharityAPI.VERSION, "1");

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(CharityAPI.EXTERNAL_ID);

		Map<String, String> results = null;
		try {
			results = CharityAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		String actualExtId = results.get(CharityAPI.EXTERNAL_ID);

		Assert.assertNotEquals(actualCharityUpdated, -1, "Error. Found Charity amount  negative. Creation error. ");
		Assert.assertNotEquals(actualCharityUpdated, 0, "Error. Found Charity amount  null. Creation failed. ");
		Assert.assertEquals(actualExtId, expectedExtId, "Error. Charity field updated (External id) not matching. ");
	}

	@Test(priority = 37, dependsOnMethods = { "updateCharity_ModifyVersion_Pass" })
	public void updateCharity_AddAcceptFrozen_Pass() {
		// Arrange - section
		int orgId = testData;

		Boolean acceptFrozen = true;
		String expectedFrozen = Boolean.toString(acceptFrozen);

		Map<String, String> data = new HashMap<String, String>();
		data.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		data.put(CharityAPI.RADIUS, "55");
		data.put(CharityAPI.VEHICLE, "true");
		data.put(CharityAPI.ACCEPT_AMBIENT, "true");
		data.put(CharityAPI.VERSION, "" + 1);
		data.put(CharityAPI.MULTI_DONATION, "true");

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(CharityAPI.ACCEPT_FROZEN, expectedFrozen);

		// Act
		int actualCharityUpdated = 0;
		try {
			actualCharityUpdated = CharityAPI.update(con, data, updateData);
		} catch (SQLException e) {
			LOG.error("Update: " + e.getLocalizedMessage());
		}

		// Assert - section
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		selectData.put(CharityAPI.VEHICLE, "true");

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(CharityAPI.ACCEPT_FROZEN);

		Map<String, String> results = null;
		try {
			results = CharityAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		String actualFrozen = results.get(CharityAPI.ACCEPT_FROZEN);

		Assert.assertNotEquals(actualCharityUpdated, -1, "Error. Found Charity amount  negative. Creation error. ");
		Assert.assertNotEquals(actualCharityUpdated, 0, "Error. Found Charity amount  null. Creation failed. ");
		Assert.assertEquals(actualFrozen, expectedFrozen,
				"Error. Charity field updated (Accept Frozen) not matching. ");
	}

	@Test(priority = 38, dependsOnMethods = { "updateCharity_AddAcceptFrozen_Pass" })
	public void updateCharity_AddRules_Pass() {
		// Arrange
		int orgId = testData;

		String expectedRules = "BD;AA;PO;";

		Map<String, String> data = new HashMap<String, String>();
		data.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		data.put(CharityAPI.RADIUS, "55");
		data.put(CharityAPI.VEHICLE, "true");
		data.put(CharityAPI.ACCEPT_AMBIENT, "true");
		data.put(CharityAPI.VERSION, "" + 1);
		data.put(CharityAPI.MULTI_DONATION, "true");

		Map<String, String> updateData = new HashMap<String, String>();
		updateData.put(CharityAPI.RULES, expectedRules);

		// Act
		int actualCharityUpdated = 0;
		try {
			actualCharityUpdated = CharityAPI.update(con, data, updateData);
		} catch (SQLException e) {
			LOG.error("Update: " + e.getLocalizedMessage());
		}

		// Assert - section
		Map<String, String> selectData = new HashMap<String, String>();
		selectData.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		selectData.put(CharityAPI.VERSION, "1");

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(CharityAPI.RULES);

		Map<String, String> results = null;
		try {
			results = CharityAPI.getField(con, fieldList, selectData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		String actualRules = results.get(CharityAPI.RULES);

		Assert.assertNotEquals(actualCharityUpdated, -1, "Error. Found Charity amount  negative. Creation error. ");
		Assert.assertNotEquals(actualCharityUpdated, 0, "Error. Found Charity amount  null. Creation failed. ");
		Assert.assertEquals(actualRules, expectedRules, "Error. Charity field updated (Rules) not matching. ");
	}
	
	@Test(priority = 39) 
	public void updateCharity_EmptyMaps_Fail() {
		// Arrange

		Map<String, String> data = new HashMap<String, String>();

		Map<String, String> updateData = new HashMap<String, String>();

		// Act
		int actualCharityUpdated = 0;
		try {
			actualCharityUpdated = CharityAPI.update(con, data, updateData);
		} catch (SQLException e) {
			LOG.error("Update: " + e.getLocalizedMessage());
		}
		
		
		// Assert - section
		int expectedCharityUpdated = -1;

		Assert.assertEquals(actualCharityUpdated, expectedCharityUpdated,
				"CharityAPI test - No control on mandatory input maps (empty) should cause an error ");

	}

	@Test(priority = 40, dependsOnMethods = { "insertCharity_FullDataMap_Pass" })
	public void deleteCharity_viaOrgId_Pass() {
		// Arrange
		int orgId = testData;

		Map<String, String> deleteCriterion = new HashMap<String, String>();
		deleteCriterion.put(CharityAPI.ORG_ID, "" + orgId);

		// Act - section
		int deletedRows = 0;
		try {
			deletedRows = CharityAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}

	@Test(priority = 41)
	public void deleteCharity_NoOrgId_Fail() {
		// Arrange
		Map<String, String> deleteCriterion = new HashMap<String, String>();
		deleteCriterion.put(CharityAPI.ID, "" + 1111);

		int deletedRows = 0;
		try {
			deletedRows = CharityAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		int expectedDeletedRows = -1;

		Assert.assertEquals(deletedRows, expectedDeletedRows,
				"CharityAPI test - No control on mandatory input data (orgId 0) should cause an error ");
	}

	
	
	private int arrangeCharityTestData(Connection dbConnection) {

		int donorOrgId = ShortcutsAPI.createOrganization(dbConnection, new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;

			{
				put(ShortcutsAPI.ORG_NAME, org_name);
				put(ShortcutsAPI.GROUP_NAME, charityGroupName);
				put(ShortcutsAPI.REGION, "Dublin South");
				put(ShortcutsAPI.ORG_TYPE, ShortcutsAPI.ORG_TYPE_CHARITY);
			}
		});
		return donorOrgId;
	}

}
