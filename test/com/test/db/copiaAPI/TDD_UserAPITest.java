package com.test.db.copiaAPI;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.OrganizationAPI;
import com.foodcloud.db.copiaAPI.UserAPI;

/**
 *
 *String UserName ="Spain Admin";

		
		Map<String, String> data = new HashMap<String, String>();

		data.put(UserAPI.REGION_ID, ""+ 70);
		data.put(UserAPI.EMAIL, "es.admin@foodcloud.ie");
		data.put(UserAPI.NAME, UserName);
		data.put(UserAPI.ROLE, "Admin");
		data.put(UserAPI.HASHWORD, "764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ");
		data.put(UserAPI.ORG_ID, ""+1);		
		data.put(UserAPI.CREATED, ConnectionToDb.formatTimestamp());			
		data.put(UserAPI.LOCALE, "en");
		data.put(UserAPI.STATUS, "Active");
		data.put(UserAPI.VERSION, "" + 0);
		data.put(UserAPI.COMMENT, "Spain");

		data.put(UserAPI.REGION_ID, ""+ 70);
		data.put(UserAPI.EMAIL, "charity1@spain.es");
		data.put(UserAPI.NAME, UserName);
		data.put(UserAPI.ROLE, "Charity");
		data.put(UserAPI.HASHWORD, "764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ");
		data.put(UserAPI.ORG_ID, Integer.toString(org_id));		
		data.put(UserAPI.CREATED, ConnectionToDb.formatTimestamp());			
		data.put(UserAPI.LOCALE, "en");
		data.put(UserAPI.STATUS, "Active");
		data.put(UserAPI.VERSION, "" + 0);
		data.put(UserAPI.COMMENT, "Spain");

(japanese user)
 * move to integration test package (create a new package with mocks)
 */
public class TDD_UserAPITest {

	private Connection con;
	
	static final Logger LOG = LogManager.getLogger(TDD_UserAPITest.class.getName());

	@BeforeTest
	public void setup() {
		con = ConnectionToDb.getCopiaConnection();

	}

	@Test(priority = 1)
	public void insertUser_EmptyMap_Fail() {
		// Arrange
		Map<String, String> UserData = new HashMap<String, String>();

		// Act
		int result = 0;
		try {
			result = UserAPI.insert(con, UserData);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert
		Assert.assertEquals(result, -1, "UserAPI test - Empty map should cause an error");

	}

	@Test(priority = 2)
	public void insertUser_NameNull_Fail() {

		Map<String, String> data = new HashMap<String, String>();

		data.put(UserAPI.REGION_ID, "33");
		data.put(UserAPI.VERSION, "" + 0);
		data.put(UserAPI.EMAIL, "test@test.com");
		data.put(UserAPI.LOCALE, "en");

		// Act
		int actualUserInserted = 0;
		try {
			actualUserInserted = UserAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualUserInserted, expectedId,
				"UserAPI test - No control on mandatory input data (Name) should cause an error ");
	}

	
	@Test(priority = 3)
	public void insertUser_EmailNotPresent_Fail() {

		String UserName ="UserAPI test";
		
		Map<String, String> data = new HashMap<String, String>();

		data.put(UserAPI.NAME, UserName);
		data.put(UserAPI.STATUS, "Active");
		data.put(UserAPI.VERSION, "" + 0);

		// Act
		int actualUserInserted = 0;
		try {
			actualUserInserted = UserAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualUserInserted, expectedId,
				"UserAPI test - No control on mandatory input data (email) should cause an error");
	}



	@Test(priority = 10)
	public void insertUser_FullDataMap_Pass() {
		// Arrange
		String UserName ="Spain Cr Donor 1";
		String org_name = "Spain Crate Donor Store 1";
		
		int org_id = 0;
		try {
			org_id = OrganizationAPI.getId(con, org_name);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}
		
		
		Map<String, String> data = new HashMap<String, String>();

		data.put(UserAPI.REGION_ID, ""+ 70);
		data.put(UserAPI.EMAIL, "cr_donor1@spain.es");
		data.put(UserAPI.NAME, UserName);
		data.put(UserAPI.ROLE, "Donor");
		data.put(UserAPI.HASHWORD, "764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ");
		data.put(UserAPI.ORG_ID, Integer.toString(org_id));		
		data.put(UserAPI.CREATED, ConnectionToDb.formatTimestamp());			
		data.put(UserAPI.LOCALE, "en");
		data.put(UserAPI.STATUS, "Active");
		data.put(UserAPI.VERSION, "" + 0);
		data.put(UserAPI.COMMENT, "Spain");

		// Act
		int regInsertedAmount = 0;
		try {
			regInsertedAmount = UserAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 1;

		Assert.assertNotEquals(regInsertedAmount, -1, "Error. Found User amount  negative. Creation error. ");
		Assert.assertNotEquals(regInsertedAmount, 0, "Error. Found User amount  null. Creation failed. ");
		Assert.assertEquals(regInsertedAmount, expectedId, "Error. User amount not matching. ");
	}



	
	@Test(priority = 40, dependsOnMethods = { "insertUser_FullDataMap_Pass" })
	public void deleteUser_viaNameAndParentId_Pass() {
		// Arrange - section
		String UserName ="Spain Admin";
		String email = "es.admin@foodcloud.ie";

		Map<String, String> deleteCriterion = new HashMap<String, String>();
		deleteCriterion.put(UserAPI.EMAIL, email);
		deleteCriterion.put(UserAPI.NAME, UserName);

		// Act - section
		int deletedRows = 0;
//		try {
//	//		deletedRows = UserAPI.delete(con, deleteCriterion);
//		} catch (SQLException e) {
//			LOG.error(e.getLocalizedMessage());
//		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}

	
	


}
