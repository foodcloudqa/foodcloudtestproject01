package com.test.db.copiaAPI;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.LocationAPI;
import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.ContactAPI;
import com.foodcloud.db.copiaAPI.LocationAPI;
import com.foodcloud.db.copiaAPI.OrganizationAPI;
import com.foodcloud.db.copiaAPI.RegionAPI;
import com.foodcloud.db.copiaAPI.ShortcutsAPI;

/**
 *
 *
 */
public class TDD_LocationAPITest {

	private Connection con;
	private String groupName = "FoodCloud Demo Group";
	private String org_name = "Donor Store Loc Test";
	private int testData;

	static final Logger LOG = LogManager.getLogger(TDD_LocationAPITest.class.getName());

	@BeforeTest
	public void setup() {
		con = ConnectionToDb.getCopiaConnection();
		testData = arrangeLocationTestData(con);

	}

	@Test(priority = 1)
	public void insertLocation_EmptyMap_Fail() {
		// Arrange
		Map<String, String> locationData = new HashMap<String, String>();

		// Act
		int result = 0;
		try {
			result = LocationAPI.insert(con, locationData);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert
		Assert.assertEquals(result, -1, "Empty map should cause an error ");

	}

	@Test(priority = 2)
	public void insertLocation_NoOrgId_Fail() {
		// Arrange
		Map<String, String> locationData = new HashMap<String, String>();
		locationData.put(LocationAPI.ADDRESS, "5, Gallery Quay, Pearse St, Grand Canal Dock, Dublin 2");

		// Act
		int result = 0;
		try {
			result = LocationAPI.insert(con, locationData);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert
		Assert.assertEquals(result, -1, "Location: Map with No orgId should cause an error ");

	}

	@Test(priority = 3)
	public void insertLocation_OrgIdZero_Fail() {
		// Arrange
		Map<String, String> locationData = new HashMap<String, String>();
		locationData.put(LocationAPI.ORG_ID, Integer.toString(0));
		locationData.put(LocationAPI.ADDRESS, "5, Gallery Quay, Pearse St, Grand Canal Dock, Dublin 2");

		// Act
		int result = 0;
		try {
			result = LocationAPI.insert(con, locationData);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert
		Assert.assertEquals(result, -1, "Location: Map with No orgId should cause an error ");

	}

	@Test(priority = 4)
	public void insertLocation_FullDataMap_Pass() {
		// Arrange
		int orgId = testData;

		Map<String, String> locationData = new HashMap<String, String>();

		locationData.put(LocationAPI.ORG_ID, Integer.toString(orgId));
		locationData.put(LocationAPI.ADDRESS, "5, Gallery Quay, Pearse St, Grand Canal Dock, Dublin 2");

		locationData.put(LocationAPI.LATITUDE, "53.3426655");
		locationData.put(LocationAPI.LONGITUDE, "-6.2412509");
		locationData.put(LocationAPI.ADDRESS_CODE, "Dublin 2");

		locationData.put(LocationAPI.COUNTRY, "Ireland");
		locationData.put(LocationAPI.VERSION, "0");
		locationData.put(LocationAPI.TIMEZONE, LocationAPI.TIMEZONE_DUBLIN);

		// Act
		int actualLocationId = 0;
		try {
			actualLocationId = LocationAPI.insert(con, locationData);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(actualLocationId, 0, "Error. Found location id null. Creation failed. ");
		Assert.assertNotEquals(actualLocationId, -1, "Error. Found location id negative. Creation failed. ");
		Assert.assertNotEquals(actualLocationId, 0, "Error. Location id not matching. ");
	}

	// TODO: mandatory fields check
	// TODO: update

	
	
	@Test(priority = 10)
	public void selectLocation_EmptyMap_Fail() {
		// Arrange
		Map<String, String> searchData = new HashMap<String, String>();
	
		Set<String> fieldList = new HashSet<String>();
	
		// Act
		Map<String, String> result = null;
		try {
			result = LocationAPI.getField(con, fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert
		Assert.assertEquals(result, -1, "Empty map should cause an error ");
	}
	
	
	@Test(priority = 11, dependsOnMethods = { "insertLocation_FullDataMap_Pass" })
	public void selectLocation_getLocationId_ByOrgId_Pass() {
		// Arrange
		int orgId = testData;

		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(LocationAPI.ORG_ID, Integer.toString(orgId));

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(LocationAPI.ID);

		// Act
		Map<String, String> result = null;
		try {
			result = LocationAPI.getField(con, fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals("Null values", result.get("Error"), "Null values ");

		String actualLocationId = result.get(LocationAPI.ID);
//		String expectedLocationId = Integer.toString(lastLocationId);

		// id not null
		Assert.assertNotNull(actualLocationId, "Error. Found select Charity field null. Selection error. ");
//		Assert.assertEquals(actualLocationyId, expectedLocationId, "Error. Found select Charity field different. ");

	}


	
	
	
	/**
	 * 
	 */
	@Test(priority = 20)
	public void deleteLocation_OrganizationCascadeDelete_Fails() {
		// Arrange
		int org_id = testData;

		// Act - section
		int orgDeleted = 0;
		String actualError = "None";
		try {
			orgDeleted = OrganizationAPI.delete(con, org_id);
		} catch (SQLException orgDelE) {
			actualError = orgDelE.getLocalizedMessage();
			LOG.error(actualError);
		}

		boolean actualResult = false;
		try {
			actualResult = LocationAPI.isElementPresent(con, org_id);
		} catch (SQLException locationPresentErr) {
			LOG.error(locationPresentErr.getLocalizedMessage());
		}

		// Assert - section
		String expectedError = "ERROR: update or delete on table \"organization\" violates foreign key constraint \"location_org_id_fkey\" on table \"location\"";

		Assert.assertTrue(actualError.contains(expectedError), "Error message not matching expected. ");
		Assert.assertEquals(orgDeleted, 0, "Error. Found delete row amount not 0. ");
		Assert.assertTrue(actualResult, "Error. Cascade delete should not work. Donor should be in the table. ");
	}
	
	

	@Test(priority = 21, dependsOnMethods = { "insertLocation_FullDataMap_Pass" })
	public void deleteLocation_EmptyMap_Fail() {
		// Arrange
		Map<String, String> deleteCriterion = new HashMap();

		// Act - section
		int deletedRows = 0;
		try {
			deletedRows = LocationAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertEquals(deletedRows, -1, "LocationAPI test - Empty map should cause an error ");
	}

	@Test(priority = 22, dependsOnMethods = { "insertLocation_FullDataMap_Pass" })
	public void deleteLocation_NoOrgId_Fail() {
		// Arrange
		
		// add map?
		Map<String, String> deleteCriterion = new HashMap();
		deleteCriterion.put(LocationAPI.LATITUDE, "53.3426655");

		// Act - section
		int deletedRows = 0;
		try {
			deletedRows = LocationAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertEquals(deletedRows, -1, "LocationAPI test - No Org Id should cause an error ");
	}



	@Test(priority = 22, dependsOnMethods = { "insertLocation_FullDataMap_Pass" })
	public void deleteLocation_LocationBeforeOrg_Pass() {
		// Arrange
		int org_id = testData;

		// add map?
		Map<String, String> deleteCriterion = new HashMap();
		deleteCriterion.put(LocationAPI.ORG_ID, Integer.toString(org_id));

		// Act - section
		int deletedRows = 0;
		try {
			deletedRows = LocationAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}

	@Test(priority = 23, dependsOnMethods = { "deleteLocation_LocationBeforeOrg_Pass" })
	public void deleteOrganization_OrgAfterLocation_Pass() {
		// Arrange
		int org_id = testData;

		// Act - section
		int deletedRows = 0;
		try {

			deletedRows = OrganizationAPI.delete(con, org_id);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}

	private int arrangeLocationTestData(Connection con2) {

		int donorOrgId = ShortcutsAPI.createOrganization(con2, new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;

			{
				put(ShortcutsAPI.ORG_NAME, org_name);
				put(ShortcutsAPI.GROUP_NAME, groupName);
				put(ShortcutsAPI.REGION, "Dublin South");
				put(ShortcutsAPI.ORG_TYPE, ShortcutsAPI.ORG_TYPE_DONOR);
			}
		});
		return donorOrgId;
	}

}
