package com.test.db.copiaAPI;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.LocationAPI;
import com.foodcloud.db.copiaAPI.ShortcutsAPI;
import com.foodcloud.server.FCTestUtils;

/**
 *
 *
 */
public class TDD_ShortcutsAPITest {

	private Connection con;
	
	static final Logger LOG = LogManager.getLogger(TDD_ShortcutsAPITest.class.getName());

	@BeforeTest
	public void setup() {
		con = ConnectionToDb.getCopiaConnection();

	}

	@Test(priority = 1)
	public void modifyCriteriaClause_EmptySet_Fail() {
		// Arrange
		Set<String> setKeys = new HashSet<String>();

		// Act
		String result = "";
			result = ShortcutsAPI.modifyCriteriaClause(setKeys);

		LOG.info("ShortcutAPI: string" + result);

		// Assert
		Assert.assertEquals(result, "", "ShortcutAPI test - Empty set causes an empty string");
	}

	
//	Map<String, String> data = new HashMap<String, String>();
//	data.put("DonorName", donorName);
//	data.put("DonorGroupName", donorGroupName);
//	data.put("Region", "Dublin South" );
//	data.put("DonorType", ShortcutsAPI.ORG_TYPE_DONOR);                		
//	data.put("DonorTimezone", "");                		

//	data.put("CharityName", charityName);
//	data.put("CharityGroupName", donorGroupName);
//	data.put("Region", "Dublin South" );
//	data.put("CharityType", ShortcutsAPI.ORG_TYPE_DONOR);                		
	@Test ( priority = 29 )
	public void verify_getLastId() {
		Assert.fail("Not implemented yet");
	}
	
	@Test ( priority = 30 )
	public void verify_createExpectedResults() {
				
		int copiaDayAsInt = FCTestUtils.getCurrentCopiaDay();
	
		
	Map<String, String> data = new HashMap<String, String>();
	data.put(ShortcutsAPI.DONOR_NAME, "RawChocolate Store 2");
	data.put(ShortcutsAPI.DONORGROUP_NAME, "FoodCloud Demo Group");
	data.put(ShortcutsAPI.REGION, "IM" );
	data.put(ShortcutsAPI.DONOR_TYPE, ShortcutsAPI.ORG_TYPE_DONOR);     
	data.put(ShortcutsAPI.COPIA_DAY, ""+copiaDayAsInt);     
	data.put(ShortcutsAPI.DONOR_TIMEZONE, LocationAPI.TIMEZONE_MANISLAND);     
		
		String[] result = null;
		try {
			result =  ShortcutsAPI.createExpectedResults(data);
		} catch (SQLException e) {
			LOG.error("ShortcutAPI Test: string array" + result);
			LOG.error(e.getLocalizedMessage());
		}
		 
		Assert.assertNotNull(result, "ShortcutAPI test - Returned array found null");
		Assert.assertEquals(data.get(ShortcutsAPI.DONOR_NAME), result[2], "Donor name should match " ) ; 

//		Assert.assertEquals(date,expectedResults[0], "Error: Donations Page date not displayed");
//		Assert.assertEquals(time,expectedResults[1], "Error: Donations Page time not displayed");
//		Assert.assertEquals(donor, expectedResults[2], "Error: Donations Page donor not displayed");
//		Assert.assertEquals(donorCode,expectedResults[3], "Error: Donations Page donorCode not displayed");
//		Assert.assertEquals(charity,expectedResults[4], "Error: Donations Page charity not displayed");
//		Assert.assertEquals(collection,expectedResults[5], "Error: Donations Page collection not displayed");
//		Assert.assertEquals(status,expectedResults[6], "Error: Donations Page status not displayed");

	
	}

	

	
//	Map<String, String> data = new HashMap<String, String>();
//	data.put("DonorName", "Util Test Donor");
//	data.put("DonorGroupName", "Util Test Group");
//	data.put("Region", "Dublin South" );
//	data.put("DonorType", ShortcutsAPI.ORG_TYPE_DONOR);                		
//	data.put("CharityName", charityName);
//	data.put("CharityGroupName", donorGroupName);
//	data.put("Region", "Dublin South" );
//	data.put("CharityType", ShortcutsAPI.ORG_TYPE_DONOR);                		


}
