package com.test.db.copiaAPI;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.DonationEventAPI;
import com.foodcloud.db.copiaAPI.ShortcutsAPI;

/**
 *
 *
 */
public class TDD_DonationEventAPITest {

	private Connection con;
	private String donor_name = "Donor DonationEventAPI";
	private String donorGroupName = "Test DonorGroup";
	private String charity_name = "Charity DonationEventAPI";
	private String charityGroupName = "Test CharityGroup";
	
	
	static final Logger LOG = LogManager.getLogger(TDD_DonationEventAPITest.class.getName());

	@BeforeTest
	public void setup() {
		con = ConnectionToDb.getCopiaConnection();

	}

	@Test(priority = 1)
	public void insertDonationEvent_EmptyMap_Fail() {
		// Arrange
		Map<String, String> eventData = new HashMap<String, String>();

		// Act
		int result = 0;
		try {
			result = DonationEventAPI.insert(con, eventData);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert
		Assert.assertEquals(result, -1, "DonationEventAPI test - Empty map should cause an error");

	}

	@Test(priority = 2)
	public void insertDonationEvent_DonationIdNull_Fail() {
		// Arrange
		
		Map<String, String> data = new HashMap<String, String>();

		data.put(DonationEventAPI.TYPE, "Available");
		data.put(DonationEventAPI.DATA, "");
		data.put(DonationEventAPI.TIMESTAMP, ConnectionToDb.formatTimestamp()); 


		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = DonationEventAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualScheduleInserted, expectedId,
				"DonationEventAPI test - No control on mandatory input data (donorId) should cause an error ");
	}

	@Test(priority = 3)
	public void insertDonationEvent_ExternalIdNotPresent_Fail() {
		// Arrange
		int orgId = arrangeDonationEventTestData(con)[0];
		int charity_id = arrangeDonationEventTestData(con)[1];
		
		Map<String, String> data = new HashMap<String, String>();

	//	data.put(DonationEventAPI.DONATION_ID, Integer.toString(donorOrgId));
	//	data.put(DonationEventAPI.AGENT_ID, );
		data.put(DonationEventAPI.TYPE, "Available");
		data.put(DonationEventAPI.DATA, "");
		data.put(DonationEventAPI.TIMESTAMP, ConnectionToDb.formatTimestamp()); 

		// Act
		int actualScheduleInserted = 0;
		try {
			actualScheduleInserted = DonationEventAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualScheduleInserted, expectedId,
				"DonationEventAPI test - No control on mandatory input data (external_id) should cause an error");
	}


	@Test(priority = 10)
	public void insertDonation_FullDataMap_Pass() {
		// Arrange
		int donorOrgId = arrangeDonationEventTestData(con)[0];
		int charityOrgId = arrangeDonationEventTestData(con)[1];
		
		Map<String, String> data = new HashMap<String, String>();

	//	data.put(DonationEventAPI.DONATION_ID, Integer.toString(donorOrgId));
	//	data.put(DonationEventAPI.AGENT_ID, );
		data.put(DonationEventAPI.TYPE, "Available");
		data.put(DonationEventAPI.DATA, "");
		data.put(DonationEventAPI.TIMESTAMP, ConnectionToDb.formatTimestamp()); 

		// Act
		int actualDonationInserted = 0;
		try {
			actualDonationInserted = DonationEventAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 1;
		
		Assert.assertNotEquals(actualDonationInserted, -1, "Error. Found donation amount  negative. Creation error. ");
		Assert.assertNotEquals(actualDonationInserted, 0, "Error. Found donation amount  null. Creation failed. ");
		Assert.assertEquals(actualDonationInserted, expectedId, "Error. donation amount not matching. ");
	}
//
//	@Test(priority = 11)
//	public void insertSchedule_withNoPartyId_Pass() {
//		// Arrange
//		int orgId = arrangeScheduleTestData(con)[0];
//
//		Map<String, String> data = new HashMap<String, String>();
//
//		data.put(DonationEventAPI.DAY, "" + 2);
//		data.put(DonationEventAPI.ORG_ID, Integer.toString(orgId));
//		data.put(DonationEventAPI.START_TIME, "21:00:00");
//		data.put(DonationEventAPI.END_TIME, "21:40:00");
//		data.put(DonationEventAPI.STATUS, "Available");
//		data.put(DonationEventAPI.VERSION, "" + 0);
//		data.put(DonationEventAPI.CANCEL_AFTER, "21:45:00");
//
//		// Act
//		int actualScheduleInserted = 0;
//		try {
//			actualScheduleInserted = DonationEventAPI.insert(con, data);
//		} catch (SQLException e) {
//			LOG.error("Insert: " + e.getLocalizedMessage());
//		}
//
//		// Assert - section
//		int expectedId = 1;
//
//		Assert.assertNotEquals(actualScheduleInserted, -1, "Error. Found schedule amount  negative. Creation error. ");
//		Assert.assertNotEquals(actualScheduleInserted, 0, "Error. Found schedule amount  null. Creation failed. ");
//		Assert.assertEquals(actualScheduleInserted, expectedId, "Error. schedule amount not matching. ");
//	}
//
//
//	/**
//	 * Insert schedule row without Status
//	 * Requirements : No party_id
//	 * Expected Result: DB inserting default Status = None
//	 */
//	@Test(priority = 12)
//	public void insertSchedule_withNoStatus_Pass() {
//		// Arrange
//		int orgId = arrangeScheduleTestData(con)[0];
//
//		Map<String, String> data = new HashMap<String, String>();
//
//		data.put(DonationEventAPI.DAY, "" + 3);
//		data.put(DonationEventAPI.ORG_ID, Integer.toString(orgId));
//		data.put(DonationEventAPI.START_TIME, "21:00:00");
//		data.put(DonationEventAPI.END_TIME, "21:40:00");
//		data.put(DonationEventAPI.VERSION, "" + 0);
//		data.put(DonationEventAPI.CANCEL_AFTER, "21:45:00");
//
//		// Act
//		int actualScheduleInserted = 0;
//		try {
//			actualScheduleInserted = DonationEventAPI.insert(con, data);
//		} catch (SQLException e) {
//			LOG.error("Insert: " + e.getLocalizedMessage());
//		}
//
//		// Assert - section
//		int expectedId = 1;
//
//		Assert.assertNotEquals(actualScheduleInserted, -1, "Error. Found schedule amount  negative. Creation error. ");
//		Assert.assertNotEquals(actualScheduleInserted, 0, "Error. Found schedule amount  null. Creation failed. ");
//		Assert.assertEquals(actualScheduleInserted, expectedId, "Error. schedule amount not matching. ");
//	}
//
//	
//	
//	@Test(priority = 13)
//	public void insertSchedule_withNoVersion_Pass() {
//		// Arrange
//		int orgId = arrangeScheduleTestData(con)[0];
//		int charityId = arrangeScheduleTestData(con)[1];
//
//		Map<String, String> data = new HashMap<String, String>();
//
//		data.put(DonationEventAPI.DAY, "" + 4);
//		data.put(DonationEventAPI.ORG_ID, Integer.toString(orgId));
//		data.put(DonationEventAPI.PARTY_ID, Integer.toString(charityId));
//		data.put(DonationEventAPI.START_TIME, "21:00:00");
//		data.put(DonationEventAPI.END_TIME, "21:40:00");
//		data.put(DonationEventAPI.STATUS, "Assigned");
//		data.put(DonationEventAPI.CANCEL_AFTER, "21:45:00");
//
//		// Act
//		int actualScheduleInserted = 0;
//		try {
//			actualScheduleInserted = DonationEventAPI.insert(con, data);
//		} catch (SQLException e) {
//			LOG.error("Insert: " + e.getLocalizedMessage());
//		}
//
//		// Assert - section
//		int expectedId = 1;
//
//		Assert.assertNotEquals(actualScheduleInserted, -1, "Error. Found schedule amount  negative. Creation error. ");
//		Assert.assertNotEquals(actualScheduleInserted, 0, "Error. Found schedule amount  null. Creation failed. ");
//		Assert.assertEquals(actualScheduleInserted, expectedId, "Error. schedule amount not matching. ");
//	}
//
//
//	@Test(priority = 14)
//	public void insertSchedule_withNoCancelAfter_Pass() {
//		// Arrange
//		int orgId = arrangeScheduleTestData(con)[0];
//		int charityId = arrangeScheduleTestData(con)[1];
//
//		Map<String, String> data = new HashMap<String, String>();
//
//		data.put(DonationEventAPI.DAY, "" + 5);
//		data.put(DonationEventAPI.ORG_ID, Integer.toString(orgId));
//		data.put(DonationEventAPI.PARTY_ID, Integer.toString(charityId));
//		data.put(DonationEventAPI.START_TIME, "22:00:00");
//		data.put(DonationEventAPI.END_TIME, "23:00:00");
//		data.put(DonationEventAPI.STATUS, "Assigned");
//		data.put(DonationEventAPI.VERSION, "" + 2);
//
//		// Act
//		int actualScheduleInserted = 0;
//		try {
//			actualScheduleInserted = DonationEventAPI.insert(con, data);
//		} catch (SQLException e) {
//			LOG.error("Insert: " + e.getLocalizedMessage());
//		}
//
//		// Assert - section
//		int expectedId = 1;
//
//		Assert.assertNotEquals(actualScheduleInserted, -1, "Error. Found schedule amount  negative. Creation error. ");
//		Assert.assertNotEquals(actualScheduleInserted, 0, "Error. Found schedule amount  null. Creation failed. ");
//		Assert.assertEquals(actualScheduleInserted, expectedId, "Error. schedule amount not matching. ");
//	}
//
//	
//	
//	// SELECT	
//		@Test(priority = 20, dependsOnMethods = {"insertSchedule_withNoPartyId_Pass"} )
//		public void selectSchedule_getOrgId_Pass() {
//			// Arrange
//			int orgId = arrangeScheduleTestData(con)[0];
//	
//			Map<String, String> searchData = new HashMap<String, String>();
//			searchData.put(DonationEventAPI.DAY, "" + 2);
//			searchData.put(DonationEventAPI.START_TIME, "21:00:00");
//			searchData.put(DonationEventAPI.END_TIME, "21:40:00");
//			searchData.put(DonationEventAPI.STATUS, "Available");
//			searchData.put(DonationEventAPI.VERSION, "" + 0);
//			searchData.put(DonationEventAPI.CANCEL_AFTER, "21:45:00");
//			
//			Set<String> fieldList = new HashSet<String>();
//			fieldList.add(DonationEventAPI.ORG_ID);
//			
//			// Act		
//			Map<String,String> results = null;
//			try {
//				results = DonationEventAPI.getField(con,fieldList, searchData);
//			} catch (SQLException e) {
//				LOG.error("Select: " + e.getLocalizedMessage());
//			}
//			
//			// Assert - section			
//			Assert.assertNotEquals("Null values", results.get("Error"), "Null values ");
//		
//			String  actualOrgId  = results.get(DonationEventAPI.ORG_ID);						
//			String expectedOrgId  = Integer.toString(orgId);
//			// id not null 
//			Assert.assertNotNull(actualOrgId, "Error. Found select schedule field null. Selection error. ");
//			Assert.assertEquals(actualOrgId, expectedOrgId, "Error. Found select schedule field different. ");		
//		}
//
//		@Test(priority = 21, dependsOnMethods = {"insertSchedule_withNoPartyId_Pass"} )
//		public void selectSchedule_getDay_Pass() {
//			// Arrange
//			int orgId = arrangeScheduleTestData(con)[0];
//
//			Map<String, String> searchData = new HashMap<String, String>();
//			searchData.put(DonationEventAPI.ORG_ID, Integer.toString(orgId));
//			searchData.put(DonationEventAPI.START_TIME, "21:00:00");
//			searchData.put(DonationEventAPI.END_TIME, "21:40:00");
//			searchData.put(DonationEventAPI.STATUS, "Available");
//			searchData.put(DonationEventAPI.VERSION, "" + 0);
//			searchData.put(DonationEventAPI.CANCEL_AFTER, "21:45:00");
//			
//			Set<String> fieldList = new HashSet<String>();
//			fieldList.add(DonationEventAPI.DAY);
//			
//			// Act		
//			Map<String,String> results = null;
//			try {
//				results = DonationEventAPI.getField(con,fieldList, searchData);
//			} catch (SQLException e) {
//				LOG.error("Select: " + e.getLocalizedMessage());
//			}
//			
//			// Assert - section			
//			Assert.assertNotEquals("Null values", results.get("Error"), "Null values ");
//		
//			String  actualDay  = results.get(DonationEventAPI.DAY);						
//			String expectedDay = Integer.toString(2);
//
//			// id not null 
//			Assert.assertNotNull(actualDay, "Error. Found select schedule field null. Selection error. ");
//			Assert.assertEquals(actualDay, expectedDay, "Error. Found select "+ DonationEventAPI.DAY + " schedule field different. ");		
//		}
//
//
//		@Test(priority = 22, dependsOnMethods = {"insertSchedule_withNoPartyId_Pass"} )
//		public void selectSchedule_getStartTime_Pass() {
//			// Arrange
//			int orgId = arrangeScheduleTestData(con)[0];
//
//			String expectedStartTime = "21:00:00";
//
//			Map<String, String> searchData = new HashMap<String, String>();
//			searchData.put(DonationEventAPI.ORG_ID, Integer.toString(orgId));
//			searchData.put(DonationEventAPI.DAY, ""+ 2);
//			searchData.put(DonationEventAPI.END_TIME, "21:40:00");
//			searchData.put(DonationEventAPI.STATUS, "Available");
//			searchData.put(DonationEventAPI.VERSION, "" + 0);
//			searchData.put(DonationEventAPI.CANCEL_AFTER, "21:45:00");
//			
//			Set<String> fieldList = new HashSet<String>();
//			fieldList.add(DonationEventAPI.START_TIME);
//			
//			// Act		
//			Map<String,String> results = null;
//			try {
//				results = DonationEventAPI.getField(con,fieldList, searchData);
//			} catch (SQLException e) {
//				LOG.error("Select: " + e.getLocalizedMessage());
//			}
//			
//			// Assert - section			
//			Assert.assertNotEquals("Null values", results.get("Error"), "Null values ");
//		
//			String  actualStartTime  = results.get(DonationEventAPI.START_TIME);						
//
//			// id not null 
//			Assert.assertNotNull(actualStartTime, "Error. Found select schedule field null. Selection error. ");
//			Assert.assertEquals(actualStartTime, expectedStartTime, "Error. Found select "+ DonationEventAPI.START_TIME + " schedule field different. ");		
//		}
//
//		@Test(priority = 23, dependsOnMethods = {"insertSchedule_withNoPartyId_Pass"} )
//		public void selectSchedule_getEndTime_Pass() {
//			// Arrange
//			int orgId = arrangeScheduleTestData(con)[0];
//
//			String expectedEndTime = "21:40:00";
//
//			Map<String, String> searchData = new HashMap<String, String>();
//			searchData.put(DonationEventAPI.ORG_ID, Integer.toString(orgId));
//			searchData.put(DonationEventAPI.DAY, ""+ 2);
//			searchData.put(DonationEventAPI.START_TIME, "21:00:00");
//			searchData.put(DonationEventAPI.STATUS, "Available");
//			searchData.put(DonationEventAPI.VERSION, "" + 0);
//			searchData.put(DonationEventAPI.CANCEL_AFTER, "21:45:00");
//			
//			Set<String> fieldList = new HashSet<String>();
//			fieldList.add(DonationEventAPI.END_TIME);
//			
//			// Act		
//			Map<String,String> results = null;
//			try {
//				results = DonationEventAPI.getField(con,fieldList, searchData);
//			} catch (SQLException e) {
//				LOG.error("Select: " + e.getLocalizedMessage());
//			}
//			
//			// Assert - section			
//			Assert.assertNotEquals("Null values", results.get("Error"), "Null values ");
//		
//			String  actualEndTime  = results.get(DonationEventAPI.END_TIME);						
//
//			// id not null 
//			Assert.assertNotNull(actualEndTime, "Error. Found select schedule field null. Selection error. ");
//			Assert.assertEquals(actualEndTime, expectedEndTime, "Error. Found select "+ DonationEventAPI.END_TIME + " schedule field different. ");		
//		}
//	
//
//		@Test(priority = 24, dependsOnMethods = {"insertSchedule_withNoPartyId_Pass"} )
//		public void selectSchedule_getStatus_Pass() {
//			// Arrange
//			int orgId = arrangeScheduleTestData(con)[0];
//
//			String expectedStatus = "Available";
//
//			Map<String, String> searchData = new HashMap<String, String>();
//			searchData.put(DonationEventAPI.ORG_ID, Integer.toString(orgId));
//			searchData.put(DonationEventAPI.DAY, ""+ 2);
//			searchData.put(DonationEventAPI.START_TIME, "21:00:00");
//			searchData.put(DonationEventAPI.END_TIME, "21:40:00");
//			searchData.put(DonationEventAPI.VERSION, "" + 0);
//			searchData.put(DonationEventAPI.CANCEL_AFTER, "21:45:00");
//			
//			Set<String> fieldList = new HashSet<String>();
//			fieldList.add(DonationEventAPI.STATUS);
//			
//			// Act		
//			Map<String,String> results = null;
//			try {
//				results = DonationEventAPI.getField(con,fieldList, searchData);
//			} catch (SQLException e) {
//				LOG.error("Select: " + e.getLocalizedMessage());
//			}
//			
//			// Assert - section			
//			Assert.assertNotEquals("Null values", results.get("Error"), "Null values ");
//		
//			String  actualStatus  = results.get(DonationEventAPI.STATUS);						
//
//			// id not null 
//			Assert.assertNotNull(actualStatus, "Error. Found select schedule field null. Selection error. ");
//			Assert.assertEquals(actualStatus, expectedStatus, "Error. Found select "+ DonationEventAPI.STATUS + " schedule field different. ");		
//		}
//
//
//		@Test(priority = 25, dependsOnMethods = {"insertSchedule_withNoPartyId_Pass"} )
//		public void selectSchedule_getVersionAndCancelAfter_Pass() {
//			// Arrange
//			int orgId = arrangeScheduleTestData(con)[0];
//
//			Map<String, String> searchData = new HashMap<String, String>();
//			searchData.put(DonationEventAPI.ORG_ID, Integer.toString(orgId));
//			searchData.put(DonationEventAPI.DAY, ""+ 2);
//			searchData.put(DonationEventAPI.START_TIME, "21:00:00");
//			searchData.put(DonationEventAPI.END_TIME, "21:40:00");
//						
//			Set<String> fieldList = new HashSet<String>();
//			fieldList.add(DonationEventAPI.VERSION);
//			fieldList.add(DonationEventAPI.CANCEL_AFTER);
//			
//			// Act		
//			Map<String,String> results = null;
//			try {
//				results = DonationEventAPI.getField(con,fieldList, searchData);
//			} catch (SQLException e) {
//				LOG.error("Select: " + e.getLocalizedMessage());
//			}
//			
//			// Assert - section			
//			Assert.assertNotEquals("Null values", results.get("Error"), "Null values ");
//		
//			String  actualVersion  = results.get(DonationEventAPI.VERSION);						
//			String  actualCancelAfter  = results.get(DonationEventAPI.CANCEL_AFTER);						
//			
//			String expectedVersion = "" + 0;
//			String expectedCancelAfter = "21:45:00";
//			//not null 
//			Assert.assertNotNull(actualVersion, "Error. Found select schedule field null. Selection error. ");
//			Assert.assertEquals(actualVersion, expectedVersion, "Error. Found select "+ DonationEventAPI.VERSION + " schedule field different. ");		
//
//			Assert.assertNotNull(actualCancelAfter, "Error. Found select schedule field null. Selection error. ");
//			Assert.assertEquals(actualCancelAfter, expectedCancelAfter, "Error. Found select "+ DonationEventAPI.CANCEL_AFTER + " schedule field different. ");		
//
//		}
//
//	// UPDATE	
//	@Test(priority = 30, dependsOnMethods = {"insertSchedule_withNoPartyId_Pass"} )
//	public void updateSchedule_AddPartyId_Pass() {
//		// Arrange
//		int orgId = arrangeScheduleTestData(con)[0];
//		int partyId = arrangeScheduleTestData(con)[1];
//		String actualDay = "" + 2;
//		
//		Map<String, String> data = new HashMap<String, String>();
//		data.put(DonationEventAPI.DAY, actualDay);
//		data.put(DonationEventAPI.ORG_ID, Integer.toString(orgId));
//		data.put(DonationEventAPI.START_TIME, "21:00:00");
//		data.put(DonationEventAPI.END_TIME, "21:40:00");
//		data.put(DonationEventAPI.STATUS, "Available");
//		data.put(DonationEventAPI.VERSION, "" + 0);
//		data.put(DonationEventAPI.CANCEL_AFTER, "21:45:00");
//
//		Map<String, String> updateData = new HashMap<String, String>();
//		updateData.put(DonationEventAPI.PARTY_ID, Integer.toString(partyId));
//		
//		// Act
//		int actualScheduleUpdated = 0;
//		try {
//			actualScheduleUpdated = DonationEventAPI.update(con,data, updateData);
//		} catch (SQLException e) {
//			LOG.error("Update: " + e.getLocalizedMessage());
//		}
//
//		// Assert - section
//
//		Map<String, String> selectData = new HashMap<String, String>();
//		selectData.put(DonationEventAPI.ORG_ID, Integer.toString(orgId));
//		selectData.put(DonationEventAPI.DAY, actualDay);
//		
//		
//		Set<String> fieldList = new HashSet<String>();
//		fieldList.add(DonationEventAPI.PARTY_ID);
//		
//		// Act		
//		Map<String,String> results = null;
//		try {
//			results = DonationEventAPI.getField(con,fieldList, selectData);
//		} catch (SQLException e) {
//			LOG.error("Select: " + e.getLocalizedMessage());
//		}
//
// 		String actualPartyId  = results.get(DonationEventAPI.PARTY_ID);
//		String expectedPartyId = Integer.toString(partyId);
//		
//
//		Assert.assertNotEquals(actualScheduleUpdated, -1, "Error. Found schedule amount  negative. Creation error. ");
//		Assert.assertNotEquals(actualScheduleUpdated, 0, "Error. Found schedule amount  null. Creation failed. ");
//		Assert.assertEquals(actualPartyId, expectedPartyId, "Error. schedule amount not matching. ");
//	}
//
//	
//	
//	
//	
//	
//	@Test(priority = 40, dependsOnMethods = { "insertSchedule_FullDataMap_Pass" })
//	public void deleteSchedule_viaDayAndOrgId_Pass() {
//		int org_id = 0;
//		try {
//			org_id = OrganizationAPI.getId(con, donor_name);
//		} catch (SQLException e1) {
//			LOG.error(e1.getLocalizedMessage());
//		}
//
//		// Act - section
//		int deletedRows = 0;
//		try {
//			Map<String, String> deleteCriterion = new HashMap<String, String>();
//			deleteCriterion.put(DonationEventAPI.ORG_ID, "" + org_id);
//			deleteCriterion.put(DonationEventAPI.DAY, "" + 1);
//
//			deletedRows = DonationEventAPI.delete(con, deleteCriterion);
//		} catch (SQLException e) {
//			LOG.error(e.getLocalizedMessage());
//		}
//
//		// Assert - section
//		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
//		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
//		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
//	}
//
//	@Test(priority = 41, dependsOnMethods = { "insertSchedule_withNoPartyId_Pass" })
//	public void deleteSchedule_viaDayAndTime_Pass() {
//		int org_id = 0;
//		try {
//			org_id = OrganizationAPI.getId(con, donor_name);
//		} catch (SQLException e1) {
//			LOG.error(e1.getLocalizedMessage());
//		}
//
//		// Act - section
//		int deletedRows = 0;
//		try {
//			Map<String, String> deleteCriterion = new HashMap<String, String>();
//			deleteCriterion.put(DonationEventAPI.ORG_ID, "" + org_id);
//			deleteCriterion.put(DonationEventAPI.DAY, "" + 2);
//			deleteCriterion.put(DonationEventAPI.START_TIME, "21:00:00");
//
//			
//			deletedRows = DonationEventAPI.delete(con, deleteCriterion);
//		} catch (SQLException e) {
//			LOG.error(e.getLocalizedMessage());
//		}
//
//		// Assert - section
//		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
//		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
//		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
//	}
//	
//	
//	@Test(priority = 42, dependsOnMethods = { "insertSchedule_withNoStatus_Pass" })
//	public void deleteSchedule_viaDayAndEndTime_Pass() {
//		int org_id = 0;
//		try {
//			org_id = OrganizationAPI.getId(con, donor_name);
//		} catch (SQLException e1) {
//			LOG.error(e1.getLocalizedMessage());
//		}
//
//		// Act - section
//		int deletedRows = 0;
//		try {
//			Map<String, String> deleteCriterion = new HashMap<String, String>();
//			deleteCriterion.put(DonationEventAPI.ORG_ID, "" + org_id);
//			deleteCriterion.put(DonationEventAPI.DAY, "" + 3);
//			deleteCriterion.put(DonationEventAPI.END_TIME, "21:40:00");
//			
//			deletedRows = DonationEventAPI.delete(con, deleteCriterion);
//		} catch (SQLException e) {
//			LOG.error(e.getLocalizedMessage());
//		}
//
//		// Assert - section
//		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
//		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
//		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
//	}
//
//
//	@Test(priority = 43, dependsOnMethods = { "insertSchedule_withNoVersion_Pass" })
//	public void deleteSchedule_viaDayAndPartyId_Pass() {
//		int org_id = 0;
//		try {
//			org_id = OrganizationAPI.getId(con, donor_name);
//		} catch (SQLException e1) {
//			LOG.error(e1.getLocalizedMessage());
//		}
//
//		int party_id = 0;
//		try {
//			party_id = OrganizationAPI.getId(con, charity_name);
//		} catch (SQLException e1) {
//			LOG.error(e1.getLocalizedMessage());
//		}
//
//		// Act - section
//		int deletedRows = 0;
//		try {
//			Map<String, String> deleteCriterion = new HashMap<String, String>();
//			deleteCriterion.put(DonationEventAPI.ORG_ID, "" + org_id);
//			deleteCriterion.put(DonationEventAPI.DAY, "" + 4);
//			deleteCriterion.put(DonationEventAPI.PARTY_ID, "" + party_id);
//			
//			deletedRows = DonationEventAPI.delete(con, deleteCriterion);
//		} catch (SQLException e) {
//			LOG.error(e.getLocalizedMessage());
//		}
//
//		// Assert - section
//		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
//		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
//		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
//	}
//
	
	
//	@Test(priority = 44, dependsOnMethods = { "insertSchedule_withNoCancelAfter_Pass" })
//	public void deleteSchedule_viaDayAndVersion_Pass() {
//		int org_id = 0;
//		try {
//			org_id = OrganizationAPI.getId(con, donor_name);
//		} catch (SQLException e1) {
//			LOG.error(e1.getLocalizedMessage());
//		}
//
//		// Act - section
//		int deletedRows = 0;
//		try {
//			Map<String, String> deleteCriterion = new HashMap<String, String>();
//			deleteCriterion.put(DonationEventAPI.ORG_ID, "" + org_id);
//			deleteCriterion.put(DonationEventAPI.DAY, "" + 5);
//			deleteCriterion.put(DonationEventAPI.VERSION, "" + 2);
//			
//			deletedRows = DonationEventAPI.delete(con, deleteCriterion);
//		} catch (SQLException e) {
//			LOG.error(e.getLocalizedMessage());
//		}
//
//		// Assert - section
//		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
//		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
//		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
//	}
	
	private Integer[] arrangeDonationEventTestData(Connection con2) {
		
		Integer[] results = new Integer[2];

		int donorOrgId = ShortcutsAPI.createOrganization(con2, new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;

			{
                put(ShortcutsAPI.ORG_NAME, donor_name);
                put(ShortcutsAPI.GROUP_NAME, donorGroupName);
                put(ShortcutsAPI.REGION, "Dublin South" );
                put(ShortcutsAPI.ORG_TYPE, ShortcutsAPI.ORG_TYPE_DONOR);                
            }
        });
		
		int charityOrgId = ShortcutsAPI.createOrganization(con2, new HashMap<String, String>() {
			private static final long serialVersionUID = 2L;

			{
                put(ShortcutsAPI.ORG_NAME, charity_name);
                put(ShortcutsAPI.GROUP_NAME, charityGroupName);
                put(ShortcutsAPI.ORG_TYPE, ShortcutsAPI.ORG_TYPE_CHARITY);
                put(ShortcutsAPI.REGION, "Dublin South" );
            }
        });
		results[0] = donorOrgId;
		results[1] = charityOrgId;

		return results;
	}


}
