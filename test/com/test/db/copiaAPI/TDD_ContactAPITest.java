package com.test.db.copiaAPI;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.ContactAPI;
import com.foodcloud.db.copiaAPI.OrganizationAPI;
import com.foodcloud.db.copiaAPI.ShortcutsAPI;

/**
 * Class that Maps Contact API  on Copia DB
 *
 *	// TODO: mandatory fields check
 *	// TODO: wrong data pair??
 */
public class TDD_ContactAPITest {

	private Connection con;
	private String org_name = "Donor Store Contact Test";
	private String donorGroupName = "ContactApi group";
	private int testData;

	static final Logger LOG = LogManager.getLogger(TDD_ContactAPITest.class.getName());

	@BeforeTest
	public void setup() {
		con = ConnectionToDb.getCopiaConnection();

		testData = arrangeContactTestData(con);

		//delete data!!
	}

	@Test(priority = 1)
	public void insertContact_EmptyMap_Fail() {
		// Arrange
		Map<String, String> contactData = new HashMap<String, String>();

		// Act
		int result = 0;
		try {
			result = ContactAPI.insert(con, contactData);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert
		Assert.assertEquals(result, -1, "ContactAPI test - Empty map should cause an error ");

	}

	@Test(priority = 2)
	public void insertContact_FullDataMap_Mail_Pass() {
		// Arrange
		int orgId = testData;

		Map<String, String> contactData_mail = new HashMap<String, String>();

		contactData_mail.put(ContactAPI.PARTY_ID, Integer.toString(orgId));
		contactData_mail.put(ContactAPI.PARTY_TYPE, "O");
		contactData_mail.put(ContactAPI.TYPE, "email");
		contactData_mail.put(ContactAPI.DATA, "dublinContactAPI@donor.ie");
		contactData_mail.put(ContactAPI.RANK, "2");
		contactData_mail.put(ContactAPI.STATUS, "Active");
		contactData_mail.put(ContactAPI.LABEL, "ContactAPI admin");

		// Act
		int donorContactAmount = 0;
		try {
			donorContactAmount = ContactAPI.insert(con, contactData_mail);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 0;
		try {
			expectedId = ContactAPI.getId(con, orgId);
		} catch (SQLException e) {
			LOG.error("GetId " + e.getLocalizedMessage());
		}

		Assert.assertNotEquals(donorContactAmount, 0, "Error. Found contact amount null. Creation failed. ");
		Assert.assertNotEquals(donorContactAmount, -1, "Error. Found contact amount negative. Creation failed. ");
		Assert.assertNotEquals(expectedId, 0, "Error. Contact id not matching. ");
	}

	@Test(priority = 3)
	public void insertContact_FullDataMap_Phone_Pass() {
		// Arrange
		int orgId = testData;

		Map<String, String> contactData_phone = new HashMap<String, String>();

		contactData_phone.put(ContactAPI.PARTY_ID, Integer.toString(orgId));
		contactData_phone.put(ContactAPI.PARTY_TYPE, "O");
		contactData_phone.put(ContactAPI.TYPE, "phone");
		contactData_phone.put(ContactAPI.DATA, "+441234567899");
		contactData_phone.put(ContactAPI.RANK, "1");
		contactData_phone.put(ContactAPI.STATUS, "Active");
		contactData_phone.put(ContactAPI.LABEL, "ContactAPI admin");

		int donorPhoneAmount = 0;
		try {
			donorPhoneAmount  = ContactAPI.insert(con, contactData_phone);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 0;
		try {
			expectedId = ContactAPI.getId(con, orgId);
		} catch (SQLException e) {
			LOG.error("GetId " + e.getLocalizedMessage());
		}
		
		Assert.assertNotEquals(donorPhoneAmount, 0, "Error. Found contact phone id null. Creation failed. ");
		Assert.assertNotEquals(donorPhoneAmount, -1, "Error. Found contact phone  id negative. Creation failed. ");
		Assert.assertNotEquals(expectedId, 0, "Error. Contact id not matching. ");
	}

	@Test(priority = 4)
	public void insertContact_FullDataMap_Push_Pass() {
		// Arrange
		int orgId = testData;

		Map<String, String> contactData_push = new HashMap<String, String>();

		contactData_push.put(ContactAPI.PARTY_ID, Integer.toString(orgId));
		contactData_push.put(ContactAPI.PARTY_TYPE, "O");
		contactData_push.put(ContactAPI.TYPE, "push");
		contactData_push.put(ContactAPI.DATA, "240b42c9-0613-4d09-89cb-7d9f03039f40");
		contactData_push.put(ContactAPI.RANK, "3");
		contactData_push.put(ContactAPI.STATUS, "Active");
		contactData_push.put(ContactAPI.LABEL, "ContactAPI admin");

		// Act
		int donorPushAmount = 0;
		try {
			donorPushAmount = ContactAPI.insert(con, contactData_push);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 0;
		try {
			expectedId = ContactAPI.getId(con, orgId);
		} catch (SQLException e) {
			LOG.error("GetId " + e.getLocalizedMessage());
		}
		
		Assert.assertNotEquals(donorPushAmount, 0, "Error. Found contact push notification null. Creation failed. ");
		Assert.assertNotEquals(donorPushAmount, -1, "Error. Found contact push notificationnegative. Creation failed. ");
		Assert.assertNotEquals(expectedId,0, "Error. Contact id push notification not matching. ");
	}

	@Test(priority = 5)
	public void insertContact_FullDataMap_Sms_Pass() {
		// Arrange
		int orgId = testData;

		Map<String, String> contactData_sms = new HashMap<String, String>();

		contactData_sms.put(ContactAPI.PARTY_ID, Integer.toString(orgId));
		contactData_sms.put(ContactAPI.PARTY_TYPE, "O");
		contactData_sms.put(ContactAPI.TYPE, "sms");
		contactData_sms.put(ContactAPI.DATA, "+4412345678");
		contactData_sms.put(ContactAPI.RANK, "4");
		contactData_sms.put(ContactAPI.STATUS, "Active");
		contactData_sms.put(ContactAPI.LABEL, "ContactAPI admin");

		int donorSmsAmount = 0;
		try {
			donorSmsAmount = ContactAPI.insert(con, contactData_sms);
		} catch (SQLException e) {
			LOG.error("Insert: " + e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 0;
		try {
			expectedId = ContactAPI.getId(con, orgId);
		} catch (SQLException e) {
			LOG.error("GetId " + e.getLocalizedMessage());
		}
		
		Assert.assertNotEquals(donorSmsAmount, 0, "Error. Found contact sms id null. Creation failed. ");
		Assert.assertNotEquals(donorSmsAmount, -1, "Error. Found contact sms id negative. Creation failed. ");
		Assert.assertNotEquals(expectedId, 0, "Error. Contact sms id not matching. ");
	}

	
	@Test(priority = 10)
	public void getId_CriterionZero_Fail() {
		// Arrange
		int orgId = 0;

		// Act
		int result = 0;
		try {
			result = ContactAPI.getId(con, orgId);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert
		Assert.assertEquals(result, -1, "ContactAPI test - Empty map should cause an error ");
	}
	
	
	@Test(priority = 20, dependsOnMethods = {"insertContact_FullDataMap_Mail_Pass"} )
	public void selectContact_getOrgId_viaMail_Pass() {
		// Arrange
		int orgId = testData;

		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(ContactAPI.PARTY_TYPE, "O");
		searchData.put(ContactAPI.TYPE, "email");
		searchData.put(ContactAPI.DATA, "dublinContactAPI@donor.ie");
		searchData.put(ContactAPI.RANK, "2");
		searchData.put(ContactAPI.STATUS, "Active");
		
		Set<String> fieldList = new HashSet<String>();
		fieldList.add(ContactAPI.PARTY_ID);
		
		// Act		
		Map<String,String> results = null;
		try {
			results = ContactAPI.getField(con,fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}
		
		// Assert - section				
		String  actualOrgId  = results.get(ContactAPI.PARTY_ID);						
		String expectedOrgId  = Integer.toString(orgId);
		
		// id not null 
		Assert.assertNotNull(actualOrgId, "Error. Found select contact field null. Selection error. ");
		Assert.assertEquals(actualOrgId, expectedOrgId, "Error. Found select contact field different. ");		
	}

	@Test(priority = 21, dependsOnMethods = {"insertContact_FullDataMap_Phone_Pass"} )
	public void selectContact_getPartyType_viaPhone_Pass() {
		// Arrange

		String selectColumn = ContactAPI.PARTY_TYPE;
		String expectedPartyType = "O";

		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(ContactAPI.TYPE, "phone");
		searchData.put(ContactAPI.DATA, "+441234567899");
		
		Set<String> fieldList = new HashSet<String>();
		fieldList.add(selectColumn);
		
		// Act		
		Map<String,String> results = null;
		try {
			results = ContactAPI.getField(con,fieldList, searchData);
		} catch (SQLException e) {
			LOG.error("Select: " + e.getLocalizedMessage());
		}
		
		// Assert - section				
		String  actualPartyType  = results.get(selectColumn);						

		Assert.assertNotNull(actualPartyType, "Error. Found select contact field null. Selection error. ");
		Assert.assertEquals(actualPartyType, expectedPartyType, "Error. Found select contact field different. ");		
	}
	
	/**
	 * 
	 */
	@Test(priority = 40)
	public void deleteContact_ContactItem_Mail_Pass() {
		// Arrange
		int org_id = testData;

		// Act - section
		int deletedRows = 0;
		try {
			// add map?
			Map<String, String> deleteCriterion = new HashMap();
			deleteCriterion.put(ContactAPI.TYPE, "email");
			deleteCriterion.put(ContactAPI.DATA, "dublinContactAPI@donor.ie");
			deleteCriterion.put(ContactAPI.PARTY_ID, "" + org_id);

			deletedRows = ContactAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}

	@Test(priority = 41)
	public void deleteContact_ContactItem_Phone_Pass() {
		int org_id = 0;
		try {
			org_id = OrganizationAPI.getId(con, org_name);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}

		// Act - section
		int deletedRows = 0;
		try {
			// add map?
			Map<String, String> deleteCriterion = new HashMap<String, String>();
			deleteCriterion.put(ContactAPI.TYPE, "phone");
			deleteCriterion.put(ContactAPI.DATA, "+4412345678");
			deleteCriterion.put(ContactAPI.PARTY_ID, "" + org_id);

			deletedRows = ContactAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}

	@Test(priority = 42)
	public void deleteContact_ContactItem_SMs_Pass() {
		int org_id = 0;
		try {
			org_id = OrganizationAPI.getId(con, org_name);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}

		// Act - section
		int deletedRows = 0;
		try {
			// add map?
			Map<String, String> deleteCriterion = new HashMap<String, String>();
			deleteCriterion.put(ContactAPI.TYPE, "sms");
			deleteCriterion.put(ContactAPI.DATA, "+4412345678");
			deleteCriterion.put(ContactAPI.PARTY_ID, "" + org_id);

			deletedRows = ContactAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}

	@Test(priority = 43)
	public void deleteContact_ContactItem_Push_Pass() {
		int org_id = 0;
		try {
			org_id = OrganizationAPI.getId(con, org_name);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}

		// Act - section
		int deletedRows = 0;
		try {
			// add map?
			Map<String, String> deleteCriterion = new HashMap<String, String>();
			deleteCriterion.put(ContactAPI.TYPE, "push");
			deleteCriterion.put(ContactAPI.DATA, "240b42c9-0613-4d09-89cb-7d9f03039f40");
			deleteCriterion.put(ContactAPI.PARTY_ID, "" + org_id);

			deletedRows = ContactAPI.delete(con, deleteCriterion);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
		Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}

	@Test(priority = 44, dependsOnMethods = { "deleteContact_ContactItem_Mail_Pass" })
	public void deleteContact_CascadeNotPresent_OrganizationDelete_Pass() {
		int org_id = 0;
		try {
			org_id = OrganizationAPI.getId(con, org_name);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}

		// Act - section
		int orgDeleted = 0;
		String actualError = "None";
		try {
			orgDeleted = OrganizationAPI.delete(con, org_id);
		} catch (SQLException orgDelE) {
			actualError = orgDelE.getLocalizedMessage();
			LOG.error(actualError);
		}

		boolean actualResult = false;
		try {
			actualResult = ContactAPI.isElementPresent(con, org_id);
		} catch (SQLException contactPresentErr) {
			LOG.error(contactPresentErr.getLocalizedMessage());
		}

		// Assert - section
		String expectedError = "None";

		Assert.assertTrue(actualError.contains(expectedError),
				"Error message should not be present. Actual: " + actualError + " Expected" + expectedError + " ");
		Assert.assertEquals(orgDeleted, 1, "Error. Found org delete row amount not 1. ");
		Assert.assertFalse(actualResult, "Error. Contact should be already deleted. ");
	}

	private int arrangeContactTestData(Connection con2) {

		int donorOrgId = ShortcutsAPI.createOrganization(con2, new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;

			{
				put(ShortcutsAPI.ORG_NAME, org_name);
				put(ShortcutsAPI.GROUP_NAME, donorGroupName);
				put(ShortcutsAPI.REGION, "Dublin South");
				put(ShortcutsAPI.ORG_TYPE, ShortcutsAPI.ORG_TYPE_DONOR);
			}
		});
		return donorOrgId;
	}

}
