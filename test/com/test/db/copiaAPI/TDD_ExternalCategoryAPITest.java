package com.test.db.copiaAPI;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.ExternalCategoryAPI;
import com.foodcloud.db.copiaAPI.ShortcutsAPI;

/**
 *
 *
 */
public class TDD_ExternalCategoryAPITest {

	private Connection con;
	
	static final Logger LOG = LogManager.getLogger(TDD_ExternalCategoryAPITest.class.getName());

	@BeforeTest
	public void setup() {
		con = ConnectionToDb.getCopiaConnection();

	}

	@Test(priority = 1)
	public void insertCategory_EmptyMap_Fail() {
		// Arrange
		Map<String, String> categoryData = new HashMap<String, String>();

		// Act
		int result = 0;
		try {
			result = ExternalCategoryAPI.insert(con, categoryData);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert
		Assert.assertEquals(result, -1, "CategoryAPI test - Empty map should cause an error");

	}

	@Test(priority = 2)
	public void insertExternalCategory_OrgIdNull_Fail() {

		Map<String, String> data = new HashMap<String, String>();

		data.put(ExternalCategoryAPI.ORG_ID, "" + 0);
		data.put(ExternalCategoryAPI.CODE, "RDY");
		data.put(ExternalCategoryAPI.EXTERNAL_CODE, "Ready Meals");
		data.put(ExternalCategoryAPI.UNITS, "Tray");
		data.put(ExternalCategoryAPI.STORAGE, "Ambient");
		 		
		// Act
		int actualExternalCategoryInserted = 0;
		try {
			actualExternalCategoryInserted = ExternalCategoryAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = -1;

		Assert.assertEquals(actualExternalCategoryInserted, expectedId,
				"ExternalCategoryAPI test - No control on mandatory input data (orgId) should cause an error ");
	}

	
	@Test(priority = 3)
	public void insertExternalCategory_FullDataMap_Pass() {

		// Arrange
		int orgId = arrangeExternalCategoryTestData(con);

		Map<String, String> data = new HashMap<String, String>();

		data.put(ExternalCategoryAPI.ORG_ID, Integer.toString(orgId));
		data.put(ExternalCategoryAPI.CODE, "RDY");
		data.put(ExternalCategoryAPI.EXTERNAL_CODE, "Ready Meals");
		data.put(ExternalCategoryAPI.UNITS, "Tray");
		data.put(ExternalCategoryAPI.STORAGE, "Ambient");

		// Act
		int actualExternalCategoryInserted = 0;
		try {
			actualExternalCategoryInserted = ExternalCategoryAPI.insert(con, data);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 1;

		Assert.assertNotEquals(actualExternalCategoryInserted, -1, "Error. Found ext category amount  negative. Creation error. ");
		Assert.assertNotEquals(actualExternalCategoryInserted, 0, "Error. Found ext category amount  null. Creation failed. ");
		Assert.assertEquals(actualExternalCategoryInserted, expectedId, "Error. ext category amount not matching. ");
	}

	
	
	 @Test(priority = 40, dependsOnMethods = { "insertExternalCategory_FullDataMap_Pass" })
		public void deleteExternalCategory_viaExternalId_Pass() {
			//Arrange -section
		 	int orgId = arrangeExternalCategoryTestData(con);
		 
			// Act - section
			int deletedRows = 0;
			try {
				Map<String, String> deleteCriterion = new HashMap<String, String>();
				deleteCriterion.put(ExternalCategoryAPI.ORG_ID, Integer.toString(orgId));

				deletedRows = ExternalCategoryAPI.delete(con, deleteCriterion);
			} catch (SQLException e) {
				LOG.error(e.getLocalizedMessage());
			}

			// Assert - section
			Assert.assertNotEquals(deletedRows, 0, "Error. Found delete row amount null. Delete failed. ");
			Assert.assertNotEquals(deletedRows, -1, "Error. Found delete row amount negative. Delete failed. ");
			Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
		}
	
	
	private int arrangeExternalCategoryTestData(Connection connection) {
		String donorGroupName = "Test Category group";
		
		int donorGroupId = ShortcutsAPI.createGroup(connection, new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;

			{
                put(ShortcutsAPI.GROUP_NAME, donorGroupName);
                put(ShortcutsAPI.REGION, "Dublin South" );
                put(ShortcutsAPI.ORG_TYPE, ShortcutsAPI.ORG_TYPE_DONOR);                
            }
        });

		return donorGroupId;
	}

}
