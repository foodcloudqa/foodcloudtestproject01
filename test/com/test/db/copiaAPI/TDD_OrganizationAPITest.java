package com.test.db.copiaAPI;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.OrganizationAPI;
import com.foodcloud.db.copiaAPI.RegionAPI;

/**
 * 
 * OrganizationAPi = System_under_test
 */
public class TDD_OrganizationAPITest {

	private Connection con;
	private String groupName_delete = "TEST DEL";
	private String charityGroupName = "TEST CHARITY IE";
	private String charityName = "TEST CHARITY";
	private String donorGroupName = "TEST DONOR IE";
	private String donorName = "TEST DONOR";

	 static final Logger LOG = LogManager.getLogger(TDD_OrganizationAPITest.class.getName());

	@BeforeTest
	public void setup() {
		con = ConnectionToDb.getCopiaConnection();

	}

	@Test(priority = 1)
	public void createOrganization_DonorGroup_WithoutDonor() {
		// Arrange
		int group_id = 0;
		try {
			group_id = OrganizationAPI.getId(con, groupName_delete);
		} catch (SQLException sqlE) {
			LOG.error(sqlE.getLocalizedMessage());
			
			//e1.printStackTrace();
		}

		int regionCode = 0;

		try {
			regionCode = RegionAPI.getId(con, "Dublin South");
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
			//			e1.printStackTrace();
		}

		if (group_id == 0) {
			Map<String, String> donorGroupData = new HashMap<String, String>();

			donorGroupData.put(OrganizationAPI.PARENT_ID, "1");
			donorGroupData.put(OrganizationAPI.NAME, groupName_delete);
			donorGroupData.put(OrganizationAPI.REGION_ID, Integer.toString(regionCode));
			donorGroupData.put(OrganizationAPI.SECTOR, "Retail");
			donorGroupData.put(OrganizationAPI.REG_TYPE, "none");
			donorGroupData.put(OrganizationAPI.TYPE, "DG");
			donorGroupData.put(OrganizationAPI.CREATED, ConnectionToDb.formatTimestamp());
			donorGroupData.put(OrganizationAPI.STATUS, "Active");
			donorGroupData.put(OrganizationAPI.RATING, "0");
			donorGroupData.put(OrganizationAPI.SUPPORT, "false");
			donorGroupData.put(OrganizationAPI.VERSION, "0");

			// Act - section
			int insertAmount= 0;
			try {

				insertAmount = OrganizationAPI.insert(con, donorGroupData);
			} catch (SQLException e) {
				LOG.error(e.getLocalizedMessage());
				//e.printStackTrace();
			}

			// Assert - section
			int expectedId = 0;
			try {
				// get new row id
				expectedId = OrganizationAPI.getId(con, groupName_delete);
			} catch (SQLException e) {
				//e.printStackTrace();
				LOG.error(e.getLocalizedMessage());
			}

			// Assert - section
			Assert.assertNotEquals(insertAmount, 0, "Error. Found org insertAmount null. Creation failed. ");
			Assert.assertNotEquals(insertAmount, -1, "Error. Found org insertAmount negative. Creation failed. ");
			Assert.assertNotEquals(expectedId,0, "Error. Found org id null. Creation failed. ");
		}
	}

	@Test(priority = 2) 
	public void deleteOrganization_DonorGroup_WithoutDonor() {
		// Arrange -section
		int group_id = 0;
		try {
			group_id = OrganizationAPI.getId(con, groupName_delete);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
			//e1.printStackTrace();
		}

		// Act - section
		int deletedRows = 0;
		try {

			deletedRows = OrganizationAPI.delete(con, group_id);
		} catch (SQLException e) {
			//e.printStackTrace();
			LOG.error(e.getLocalizedMessage());

		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found org id null. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");

	}

	
	
	@Test(priority = 3)
	public void createOrganization_CharityGroup() {
		// Arrange
		int group_id = 0;
		int regionCode = 0;
		
		try {
			group_id = OrganizationAPI.getId(con, charityGroupName);
		} catch (SQLException sqlE) {
			LOG.error(sqlE.getLocalizedMessage());
		}

		try {
			regionCode = RegionAPI.getId(con, "Dublin South");
		} catch (SQLException regErr) {
			LOG.error(regErr.getLocalizedMessage());
		}

		
		if (group_id == 0) {
			Map<String, String> charityGroupData = new HashMap<String, String>();

			charityGroupData.put(OrganizationAPI.PARENT_ID, "1");
			charityGroupData.put(OrganizationAPI.NAME, charityGroupName);
			charityGroupData.put(OrganizationAPI.REGION_ID, Integer.toString(regionCode));
			charityGroupData.put(OrganizationAPI.SECTOR, "Charity");
			charityGroupData.put(OrganizationAPI.REG_TYPE, "None");
			charityGroupData.put(OrganizationAPI.TYPE, "CG");
			charityGroupData.put(OrganizationAPI.CREATED, ConnectionToDb.formatTimestamp());
			charityGroupData.put(OrganizationAPI.STATUS, "Active");
			charityGroupData.put(OrganizationAPI.RATING, "0");
			charityGroupData.put(OrganizationAPI.SUPPORT, "false");
			charityGroupData.put(OrganizationAPI.VERSION, "0");

			// Act - section
			int insertCharityGroup= 0;
			try {

				insertCharityGroup = OrganizationAPI.insert(con, charityGroupData);
			} catch (SQLException e) {
				LOG.error(e.getLocalizedMessage());
			}

			// Assert - section
			int expectedId = 0;
			try {
				// get new row id
				expectedId = OrganizationAPI.getId(con, charityGroupName);
			} catch (SQLException e) {
				LOG.error(e.getLocalizedMessage());
			}


			Assert.assertNotEquals(insertCharityGroup, 0, "Error. Found group amount null. Creation failed. ");
			Assert.assertNotEquals(expectedId, 0, "Error. Org id not matching. ");
		}
	}

	@Test(priority = 4)
	public void createOrganization_CharityWithExistingGroup() {
		// Arrange
		int group_id = 0;
		int regionCode = 0;
		
		try {
			group_id = OrganizationAPI.getId(con, charityGroupName);
		} catch (SQLException sqlE) {
			LOG.error(sqlE.getLocalizedMessage());
		}

		try {
			regionCode = RegionAPI.getId(con, "Dublin South");
		} catch (SQLException regErr) {
			LOG.error(regErr.getLocalizedMessage());
		}
	
		Map<String, String> charityData = new HashMap<String, String>();
	
		charityData.put(OrganizationAPI.PARENT_ID, Integer.toString(group_id) );
		charityData.put(OrganizationAPI.NAME, charityName);
		charityData.put(OrganizationAPI.REGION_ID, Integer.toString(regionCode));
		charityData.put(OrganizationAPI.SECTOR, "Charity");
		charityData.put(OrganizationAPI.REG_TYPE, "CHY");
		charityData.put(OrganizationAPI.REG_ID, "1223456");
		charityData.put(OrganizationAPI.TYPE, "C");
		charityData.put(OrganizationAPI.CREATED, ConnectionToDb.formatTimestamp());
		charityData.put(OrganizationAPI.STATUS, "Active");
		charityData.put(OrganizationAPI.RATING, "0");
		charityData.put(OrganizationAPI.SUPPORT, "true");
		charityData.put(OrganizationAPI.VERSION, "0");
		
		
		// Act - section
		int charityAdded = 0;
		try {
			charityAdded = OrganizationAPI.insert(con, charityData);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 0;
		try {
			// get new row id
			expectedId = OrganizationAPI.getId(con, charityName);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		Assert.assertNotEquals(charityAdded, 0, "Error. Found org added null. Creation failed. ");
		Assert.assertNotEquals(expectedId, 0, "Error. Org id not matching. ");
	}
	
	
	@Test(priority = 5 ) 
	public void deleteOrganization_CharityBeforeExistingGroup() {
		// Arrange -section
		int group_id = 0;
		try {
			group_id = OrganizationAPI.getId(con, charityName);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
			//e1.printStackTrace();
		}

		// Act - section
		int deletedRows = 0;
		try {

			deletedRows = OrganizationAPI.delete(con, group_id);
		} catch (SQLException e) {
			//e.printStackTrace();
			LOG.error(e.getLocalizedMessage());

		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found org id null. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}

	
	/**
	 * Linking 2 tests together. 
	 * 2018-04-12: Found that deleting the GROUP before the included Charity CAUSES orphaned charity 
	 * and an error on Copia UI  
	 *  
	 */
	@Test(priority = 6, dependsOnMethods = {"deleteOrganization_CharityBeforeExistingGroup"} ) 
	public void deleteOrganization_CharityGroupAfterCharity() {
		// Arrange -section
		int group_id = 0;
		try {
			group_id = OrganizationAPI.getId(con, charityGroupName);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
			//e1.printStackTrace();
		}

		// Act - section
		int deletedRows = 0;
		try {

			deletedRows = OrganizationAPI.delete(con, group_id);
		} catch (SQLException e) {
			//e.printStackTrace();
			LOG.error(e.getLocalizedMessage());

		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found org id null. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}
	
	
	
	@Test(priority = 10)
	public void createOrganization_DonorGroup() {
		// Arrange
		int group_id = 0;
		int regionCode = 0;
		
		try {
			group_id = OrganizationAPI.getId(con, donorGroupName);
		} catch (SQLException sqlE) {
			LOG.error(sqlE.getLocalizedMessage());
		}

		try {
			regionCode = RegionAPI.getId(con, "Dublin South");
		} catch (SQLException regErr) {
			LOG.error(regErr.getLocalizedMessage());
		}
		
		if (group_id == 0) {
			Map<String, String> donorGroupData = new HashMap<String, String>();

			donorGroupData.put(OrganizationAPI.PARENT_ID, "1");
			donorGroupData.put(OrganizationAPI.NAME, donorGroupName);
			donorGroupData.put(OrganizationAPI.REGION_ID, Integer.toString(regionCode));
			donorGroupData.put(OrganizationAPI.SECTOR, "Retail");
			donorGroupData.put(OrganizationAPI.REG_TYPE, "none");
			donorGroupData.put(OrganizationAPI.TYPE, "DG");
			donorGroupData.put(OrganizationAPI.CREATED, ConnectionToDb.formatTimestamp());
			donorGroupData.put(OrganizationAPI.STATUS, "Active");
			donorGroupData.put(OrganizationAPI.RATING, "0");
			donorGroupData.put(OrganizationAPI.SUPPORT, "false");
			donorGroupData.put(OrganizationAPI.VERSION, "0");

			// Act - section
			int insertAmount = 0;
			try {

				insertAmount = OrganizationAPI.insert(con, donorGroupData);
			} catch (SQLException e) {
				LOG.error(e.getLocalizedMessage());
				// e.printStackTrace();
			}

			// Assert - section
			int expectedId = 0;
			try {
				// get new row id
				expectedId = OrganizationAPI.getId(con, donorGroupName);
			} catch (SQLException e) {
				// e.printStackTrace();
				LOG.error(e.getLocalizedMessage());
			}

			Assert.assertNotEquals(insertAmount, 0, "Error. Found org id null. Creation failed. ");
			Assert.assertNotEquals(expectedId, 0, "Error. Org id not matching. ");
		}
	}


	@Test(priority = 11,dependsOnMethods = {"createOrganization_DonorGroup"} )
	public void createOrganization_DonorWithExistingGroup() {
		// Arrange
		int group_id = 0;
		int regionCode = 0;
		
		try {
			group_id = OrganizationAPI.getId(con, donorGroupName);
		} catch (SQLException sqlE) {
			LOG.error(sqlE.getLocalizedMessage());
		}

		try {
			regionCode = RegionAPI.getId(con, "Dublin South");
		} catch (SQLException regErr) {
			LOG.error(regErr.getLocalizedMessage());
		}
	
		Map<String, String> donorStoreData = new HashMap<String, String>();
	
		donorStoreData.put(OrganizationAPI.PARENT_ID, Integer.toString(group_id) );
		donorStoreData.put(OrganizationAPI.NAME, donorName);
		donorStoreData.put(OrganizationAPI.REGION_ID, Integer.toString(regionCode));
		donorStoreData.put(OrganizationAPI.SECTOR, "Retail");
		donorStoreData.put(OrganizationAPI.TYPE, "DS");
		donorStoreData.put(OrganizationAPI.CREATED, ConnectionToDb.formatTimestamp());
		donorStoreData.put(OrganizationAPI.STATUS, "Active");
		donorStoreData.put(OrganizationAPI.RATING, "0");
		donorStoreData.put(OrganizationAPI.SUPPORT, "true");
		donorStoreData.put(OrganizationAPI.VERSION, "0");
		
		
		// Act - section
		int insertAmount= 0;
		try {

			insertAmount = OrganizationAPI.insert(con, donorStoreData);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		int expectedId = 0;
		try {
			// get new row id
			expectedId = OrganizationAPI.getId(con, donorName);
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage());
		}

		// Assert - section
		Assert.assertNotEquals(insertAmount, 0, "Error. Found org id null. Creation failed. ");
		Assert.assertNotEquals(expectedId, 0,"Error. Org id not matching. ");		
	}

	
	
	@Test(priority = 12, dependsOnMethods = { "createOrganization_DonorWithExistingGroup" }  ) 
	public void deleteOrganization_DonorBeforeExistingGroup() {
		// Arrange -section
		int org_id = 0;
		try {
			org_id = OrganizationAPI.getId(con, donorName);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
			//e1.printStackTrace();
		}

		// Act - section
		int deletedRows = 0;
		try {

			deletedRows = OrganizationAPI.delete(con, org_id);
		} catch (SQLException e) {
			//e.printStackTrace();
			LOG.error(e.getLocalizedMessage());

		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found org id null. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");
	}

	
	@Test(priority = 13, dependsOnMethods = {"deleteOrganization_DonorBeforeExistingGroup"} ) 
	public void deleteOrganization_DonorGroupAfterDonor() {
		// Arrange -section
		int group_id = 0;
		try {
			group_id = OrganizationAPI.getId(con, donorGroupName);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
			//e1.printStackTrace();
		}

		// Act - section
		int deletedRows = 0;
		try {

			deletedRows = OrganizationAPI.delete(con, group_id);
		} catch (SQLException e) {
			//e.printStackTrace();
			LOG.error(e.getLocalizedMessage());

		}

		// Assert - section
		Assert.assertNotEquals(deletedRows, 0, "Error. Found org id null. Delete failed. ");
		Assert.assertEquals(deletedRows, 1, "Error. Deleted rows  Amount not matching. ");

	}

}
