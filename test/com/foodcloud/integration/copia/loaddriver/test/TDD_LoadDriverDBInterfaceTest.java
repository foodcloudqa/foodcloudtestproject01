package com.foodcloud.integration.copia.loaddriver.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.foodcloud.integration.copia.loaddriver.LoadDriverDBInterface;

@Test
public class TDD_LoadDriverDBInterfaceTest {

	public void test_CreateConnection() throws Exception {

		java.sql.Connection conn = null;
		try {
			conn = LoadDriverDBInterface.createConnection();
		} catch (Exception e2) {
			System.out.println(e2);
			e2.printStackTrace();
		}
		
		boolean isConnectionClosed = conn.isClosed();
		
		Assert.assertFalse(isConnectionClosed, "Connection closed or not established ");
	}

	public void test_CreateConnection_B() throws Exception {

		String inputDonation="1234556667";
	
		java.sql.Connection conn = LoadDriverDBInterface.createConnection();
		LoadDriverDBInterface.createIdDiffTable(conn);
		
		boolean isIdWrittenOk =  LoadDriverDBInterface.writeDonationIds("input", inputDonation);
				
		Assert.assertTrue(isIdWrittenOk, "Problem in writing ids ");

	}	
}
