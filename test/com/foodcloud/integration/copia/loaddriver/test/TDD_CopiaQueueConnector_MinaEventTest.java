package com.foodcloud.integration.copia.loaddriver.test;

import java.io.*;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.*;

import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.foodcloud.integration.copia.loaddriver.CopiaQueueConnector;
import com.foodcloud.integration.copia.loaddriver.CopiaQueueConnector.QUEUE_TYPE;
import com.rabbitmq.client.*;


public class TDD_CopiaQueueConnector_MinaEventTest {

	/**
	 * This assumes (so far) that mina-c is down (sleeping)
	 * 
	 * */
	@Test(priority = 1)
	public void readMinaQueueTest()
			throws IOException, KeyManagementException, NoSuchAlgorithmException, URISyntaxException, TimeoutException {

		// String busUri="amqp://guest:guest@localhost:5672";

		String busUri = "amqp://nafpgjjx:4Zg26SY6ra9dUYtyDYAg3Ldh93Uq15va@squirrel.rmq.cloudamqp.com/nafpgjjx";

		String minaExchange = "event-bus";
		final Channel channel = CopiaQueueConnector.createChannel(QUEUE_TYPE.EVENT, busUri, minaExchange);
		final String minaQueue = CopiaQueueConnector.getQueueName();

		long messageBeforeReading = CopiaQueueConnector.getMessagesAmount(QUEUE_TYPE.EVENT, busUri,minaExchange);

		boolean autoAck = false;

		long consumerAmount = channel.consumerCount(minaQueue);

		System.out.println(" Queue: " + minaQueue);
		System.out.println(" Consumer amount: " + consumerAmount);

		System.out.println(" [*] Waiting for messages.");
				
		Consumer consumer = createConsumerExchange(channel, autoAck, 500);
		
// needs a semaphore 		
//		long readableMessages = messageBeforeReading;
//		while (readableMessages != 0) {
			channel.basicConsume(minaQueue, autoAck, "minaTestConsumerTag", consumer);

//			readableMessages = readableMessages -1;	
//		}
		

		long messageAfterReading = CopiaQueueConnector.getMessagesAmount(QUEUE_TYPE.EVENT, busUri, minaExchange);
		channel.close();

		Assert.assertEquals(messageAfterReading, messageBeforeReading - 1, "Message not found in the queue");

	}

	
	
	
	private Consumer createConsumerExchange(Channel channel, boolean autoAck, int timeOut) {

		Consumer currentConsumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
								
				String routingKey = envelope.getRoutingKey();
				String contentType = properties.getContentType();
				long deliveryTag = envelope.getDeliveryTag();
				// (process the message components here ...)

				String message = new String(body, "UTF-8");
				JSONObject obj = new JSONObject(message);

				createLog("./ReceivedMina.json", obj);

				System.out.println(" [x] Received '" + message + "'");
				//channel.basicAck(deliveryTag, false);

				
				try {
					Thread.sleep(timeOut);
				} catch (InterruptedException e) {
				}
			}
		};
		return currentConsumer;
	}
	
	
	
	private void createLog(String fileName, Object obj) throws IOException {
	    
		FileWriter fileWriter = new FileWriter(fileName);
		PrintWriter printWriter = new PrintWriter(fileWriter);
		printWriter.print(obj.toString());
		printWriter.close();
		fileWriter.close();

	}

}
