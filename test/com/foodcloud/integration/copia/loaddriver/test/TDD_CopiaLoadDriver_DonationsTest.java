package com.foodcloud.integration.copia.loaddriver.test;




/**
 * 
Caused by: com.rabbitmq.client.ShutdownSignalException: connection error; protocol method: #method<connection.close>(reply-code=530, reply-text=NOT_ALLOWED - access to vhost 'virtualHost' refused for user 'guest', class-id=10, method-id=40)
	at com.rabbitmq.utility.ValueOrException.getValue(ValueOrException.java:66)
	at com.rabbitmq.utility.BlockingValueOrException.uninterruptibleGetValue(BlockingValueOrException.java:36)
	at com.rabbitmq.client.impl.AMQChannel$BlockingRpcContinuation.getReply(AMQChannel.java:494)
	at com.rabbitmq.client.impl.AMQChannel.privateRpc(AMQChannel.java:288)
	at com.rabbitmq.client.impl.AMQChannel.exnWrappingRpc(AMQChannel.java:138)
 */

import org.testng.Assert;
import org.testng.annotations.Test;

import com.foodcloud.integration.copia.loaddriver.CopiaLoadDriver;


public class TDD_CopiaLoadDriver_DonationsTest {

	//more related to command line interface 
	@Test (priority=1)
	public void missingMandatoryTypeValue_showErrorAndQuit() {
		
		//copiaLoadDriver --type
		String[] testArgs = {"--type"};
				
		String message = "supposed to fail";
		try {
			CopiaLoadDriver.main(testArgs);
		} catch (Exception e) {

			message = e.getLocalizedMessage();
		}
	
		String expectedMessage = "Missing type (i.e. Donation, Sms)";
		//expecting error: Missing type (i.e. Donation, Sms)
		Assert.assertEquals(message, expectedMessage , "User Message not matching");

	}

	@Test (priority=2)
	public void missingMandatoryBusValue_showErrorAndQuit() {
		
		//	copiaLoadDriver --bus
		
	
		//expecting error: Missing bus uri
		Assert.fail("Not implemented yet");

	}
	
	
	@Test (priority=3)
	public void missingMandatoryFields_showErrorAndQuit() {
		
		//	copiaLoadDriver
			
		//expecting error: Missing mandatory fields type or bus uri
		Assert.fail("Not implemented yet");

	}

	
	@Test (priority=4)
	public void wrongConnectionUrl_showErrorAndQuit() {
		
	//	copiaLoadDriver --type  Donation  --bus http://blu 
		
		//expecting error: Invalid uri: Cannot establish a connection with uri: http://blu
		Assert.fail("Not implemented yet");
	}

	
	@Test (priority=5)
	public void wrongInput_showHelpAndQuit() {
		
		//	copiaLoadDriver --m 

		//expecting error: Unexpected Exception: Unrecognized option: --m
		Assert.fail("Not implemented yet");

	}

	
	@Test (priority=6)
	public void wrongType_showErrorAndQuit() {
		
		//	copiaLoadDriver --type Emoji  --bus http://blu 
		
		//expecting error: Type not available: use Donation instead of: Emoji
		Assert.fail("Not implemented yet");

	}
	
	@Test(priority = 7)
	public void wrongCountFormat_showErrorAndQuit() {

		// copiaLoadDriver --count one-hundred --type Donation --bus http://blu

		//expecting error:Value for count not parsable as number. Try 10 instead of: one-hundred
		Assert.fail("Not implemented yet");
	}	
		
	@Test(priority = 8)
	public void missingTpsValue_showErrorAndQuit() {

		// copiaLoadDriver --count 10 --type Donation --tps --bus http://blu

		//expecting error: Missing argument for option: tps
		Assert.fail("Not implemented yet");

	}
	
	@Test(priority = 9)
	public void wrongTpsFormat_showErrorAndQuit() {

		// copiaLoadDriver --count 10 --type Donation --tps five --bus amqp://guest:guest@localhost:5672

		//expecting error:  Value for timeout not parsable as number. Try 5 instead of: five
		Assert.fail("Not implemented yet");
	}	


	
	@Test(priority = 10)
	public void missingTimeoutValue_showErrorAndQuit() {

		// copiaLoadDriver --count 10 --type Donation --timeout --bus http://blu

		//expecting error: Missing argument for option: timeout
		Assert.fail("Not implemented yet");

	}
	
	@Test(priority = 11)
	public void wrongTimeoutFormat_showErrorAndQuit() {

		// copiaLoadDriver --count 10 --type Donation --timeout five --bus amqp://guest:guest@localhost:5672

		//expecting error: Value for timeout not parsable as number. Try 10 instead of: five
		Assert.fail("Not implemented yet");
	}	
	
	
	
	@Test(priority = 12)
	public void missingTpsFormat_showErrorAndQuit() {

		// copiaLoadDriver --count 10 --type Donation --timeout 5 --bus amqp://guest:guest@localhost:5672

		//expecting warning: Missing tps: Using default value = 5
	//		uri: amqp://guest:guest@localhost:5672
	//		tps: 5
	//		numDonations: 10
	//		runWithinTimeMins: 5
	//		type: Donation
		Assert.fail("Not implemented yet");
	}

	
	@Test(priority = 13)
	public void runOnCopiaC_showResultsAndQuit() {

		// copiaLoadDriver --count 2 --type Donation --timeout 3 --tps 2 --bus amqp://nafpgjjx:4Zg26SY6ra9dUYtyDYAg3Ldh93Uq15va@squirrel.rmq.cloudamqp.com/nafpgjjx

		Assert.fail("Not implemented yet");
	}

	@Test
	public void wrongJsonFile() {
		Assert.fail("Not implemented yet");

	}

	@Test
	public void createJsonFileWithTestData() {
		Assert.fail("Not implemented yet");
	}

	// if the message is in the exchange : get an ok -  Ack?
	// if the json is wrong?? 
	
	// Read the json file (using a parser) 
	//% copDriver --type Donation --count 10000 --bus "amqp://guest:guest@localhost:5672/"

	
}
