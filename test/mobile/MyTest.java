package mobile;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;

/**
 * First Test from Saucelab talk 
 * @author Viviana
 *
 */
public class MyTest {

	private AndroidDriver driver;
	
	@BeforeClass
	public void setup() throws MalformedURLException, InterruptedException{
		
		//emulator -avd Nexus6_26 -netdelay none -netspeed full
		// https://developer.android.com/studio/command-line/logcat.html
		DesiredCapabilities emulatorCaps  = new DesiredCapabilities() ;
		emulatorCaps.setCapability("deviceName", "Nexus6_26");
		emulatorCaps.setCapability("platformName", "Android");
		emulatorCaps.setCapability("platformVersion", "8.0");
		emulatorCaps.setCapability( "emulator", "emulator");
		emulatorCaps.setCapability("appPackage", "cloud.food");
		emulatorCaps.setCapability("appActivity", "host.exp.exponent.experience.ShellAppActivity");


		DesiredCapabilities sonyCapabilities  = new DesiredCapabilities() ;

		sonyCapabilities.setCapability("deviceName", "9014e052");
		sonyCapabilities.setCapability("platformName", "Android");
		sonyCapabilities.setCapability("platformVersion", "4.4.2");
		//sonyCapabilities.setCapability("browserName", "Chrome"); // for the browser 
		sonyCapabilities.setCapability("appPackage", "cloud.food");
		sonyCapabilities.setCapability("appActivity", "host.exp.exponent.experience.ShellAppActivity");

		// not working String url1= "http://127.0.0.1:4723/wd/hub/sessions";
		String url2= "http://127.0.0.1:4723/wd/hub";
		String url= "http://localhost:4723/wd/hub";

		driver = new AndroidDriver(new URL(url), sonyCapabilities ) ; 
	
		driver.wait();
	//	driver. wait for element to appear
	}

	
	@Test
	public void testVigilAppRunning(){
		
		
		//Need ids!!
		String locator= "//td[text()='Copia Status']";
		boolean  result = driver.findElement(By.xpath(locator)).isDisplayed();
		
		Assert.assertEquals(result, true, "Element not found on Copia Status Page");
		
	}
	
	@AfterClass
	public void tearDown(){
		driver.quit();
	}

}
