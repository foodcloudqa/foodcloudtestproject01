package com.foodcloud.copia.ui.checks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.TestNG;
import org.testng.collections.Lists;

import java.io.File;
import java.util.*;

/**
 * Main Class : This invokes all UI Copia Test stored in src/resources.testng.xml
 * It simple invoke testng xml and run it.
 * 
 * USAGE: java -jar foodcloudtestproject01-1.0.5-SNAPSHOT.jar
 * @author Viviana  
 *
 */
public class InvokeCopiaUITests {
	
	 private static final Logger LOG = LogManager.getLogger(InvokeCopiaUITests.class);


	public static void main(String[] args) throws Exception {
	    LOG.info("Tests Started!");
	    TestNG testng = new TestNG();
	    List<String> suites = Lists.newArrayList();
	    suites.add("src"+File.separator+"resources"+File.separator+"testng.xml");
	  //  suites.add(".."+File.separator+"resources"+File.separator+"testng.xml");
	    testng.setTestSuites(suites);
	    testng.run();
	}



}
