package com.foodcloud.copia.ui.checks;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.*;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;


import com.foodcloud.model.dialogs.DonationDialog;
import com.foodcloud.model.dialogs.DropDownDialog;
import com.foodcloud.model.dialogs.OrgInfoDialog;
import com.foodcloud.model.dialogs.ScheduleTab;
import com.foodcloud.model.dialogs.OrgInfoDialog.TAB_ITEMS;
import com.foodcloud.model.pages.DashboardPage;
import com.foodcloud.model.pages.DonationsPage;
import com.foodcloud.model.pages.LoginPage;
import com.foodcloud.model.pages.OptionMenu.MENU_ITEMS;
import com.foodcloud.model.pages.SearchMenu;
import com.foodcloud.model.tables.DonationResultTable;
import com.foodcloud.model.tables.DonationRow;
import com.foodcloud.model.tables.TableRow;
import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;
import com.foodcloud.server.FCTestUtils;
import com.foodcloud.db.copiaAPI.DonationAPI;
import com.foodcloud.db.copiaAPI.DonorAPI;
import com.foodcloud.db.copiaAPI.LocationAPI;
import com.foodcloud.db.copiaAPI.OrganizationAPI;
import com.foodcloud.db.copiaAPI.RegionAPI;
import com.foodcloud.db.copiaAPI.ScheduleAPI;
import com.foodcloud.db.copiaAPI.ShortcutsAPI;
import com.foodcloud.db.copiaAPI.ConnectionToDb;
import java.sql.Connection;
import java.sql.SQLException;
import java.lang.StringBuffer;

/**
 * 
 * Trial page :) 
 * Need a way to extract data from file (custom file) 
 * Need a way to  :  data-driven testing (with @DataProvider and/or XML configuration).
 */
public class AustraliaDonationTest {

	static final Logger LOG = LogManager.getLogger(AustraliaDonationTest.class.getName());

	FCTestServer server;
	private String URL = "http://copia-c.herokuapp.com/";
	private FCTestNavigator nav; 
	

	
	@BeforeClass
	public void setupBeforeClass() throws FileNotFoundException {
		
	nav = new FCTestNavigator();
	nav.setup();	
	server = nav.getServer();
		
	server.openURL(URL);

	String username = "au.admin@foodcloud.ie"; 
	String password = "12345";
	String adminTitleLocator = "//div[@class='copia-top row']";
		
	LoginPage loginPage = new LoginPage(nav);
	loginPage.setLoginField(username);
	loginPage.setPasswordField(password);
	loginPage.clickSubmit();
	
	String titleLocator = adminTitleLocator;	
	
	server.waitForElement(titleLocator);				
	} 
	
	

	/**
	 * Jira: 
	 * User Story:
	 * Given I am logged into Copia, 
	 * When I click on Donations, 
	 * (then) I want to land on Donations page 
	 */
	@Test ( priority = 1 )
	public void verifyClickingOnDonations_OpensDonationsPage() {
		String adminTitleLocator = "//h2/span[@translate='Donations']";
		
		String titleLocator =  adminTitleLocator;
		
 		DashboardPage dashPage = new DashboardPage(nav);
		dashPage.getMenu().clickItem(MENU_ITEMS.DONATIONS);
		
		server.waitForElement(titleLocator);
		
		DonationsPage donationsPage = new DonationsPage(nav);
		server.waitForActionToComplete();

		boolean isEmpty = donationsPage.getResultTable().load().isEmpty();	
		
		Assert.assertFalse(isEmpty, "Error: Donations Page with data not displayed.");

		} 
	
	/**
	 * Jira: 
	 * User Story:
	 * Given I am logged into Copia as Search User, 
	 * When I type an incorrect search criteria, 
	 * (then) I want the Donations page to display an empty table.
	 * 
	 * Note: Search criteria can vary from user to user depending on Role / Organization / Region they have access to 
	 */
	@Test(priority = 2)
	public void verifyDonationsSearchCriterion_ReturningEmptyTable() {
		DonationsPage donationsPage = new DonationsPage(nav);
		
		server.waitForActionToComplete();
		
		SearchMenu searchMenu = donationsPage.getSearchMenu().open();	
		//not working for all pages 
		searchMenu.setField(DonationsPage.DONOR_FIELD_NAME,"Thursoup");

		server.waitForActionToComplete();

		boolean isEmpty = donationsPage.getResultTable().load().isEmpty();	
		
		Assert.assertTrue(isEmpty, "Error: Donations Page no results not displayed.");
	}

	@Test(priority = 3, enabled = false)
	public void verifyDonationsSearchCriterion_ReturningFullTableSinglePage() {
		DonationsPage donationsPage = new DonationsPage(nav);		

		SearchMenu searchMenu = donationsPage.getSearchMenu().clear();
		server.waitForActionToComplete();
		
		DonationResultTable table = donationsPage.getResultTable().load().read();
		int elementListSize = table.getRowList().size(); 
		
		//Database?
		
		//array size vary according to browser size: 8 rows in full screen, 7 in resized ones 
		Assert.assertEquals(elementListSize, 1, "Error: Donations Page number of results not matching.");
		
	}

	
	
	/**
	 * Note: This test needs at least one row on the UI 
	 * */
	@Test(priority = 5) 
	public void verifyDonationsSearchCriterion_ReadingSingleRow() {
		
		String expectedDonor = "Sydney Tamarama Deli 1";
		int copiaDayAsInt = FCTestUtils.getCurrentCopiaDay();

		String[] expectedResults =	createExpectedResults(expectedDonor, copiaDayAsInt);
						
		DonationsPage donationsPage = new DonationsPage(nav);		
		SearchMenu searchMenu = donationsPage.getSearchMenu().clear();
		
		server.waitForActionToComplete();
		
		DonationResultTable table = donationsPage.getResultTable().load().read();	

		String locator =table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);
				
		String date = row.getDate();
		String time = row.getTime();
		String donor = row.getDonor();
		String donorCode = row.getDonorCode();
		String charity = row.getCharity();
		String collection = row.getCollectionWindow();
		String status = row.getStatus();
		
		System.out.println("_ReadingSingleRow: ");		
		System.out.println("date: "+  date);
		System.out.println("time: "+ time );
		System.out.println("donor: "+ donor );
		System.out.println("donorCode: "+ donorCode );
		System.out.println("charity: "+ charity );
		System.out.println("collection: "+ collection );
		System.out.println("status: "+ status );

		//delete data 		
		deleteExpectedResults(expectedResults);
		
		// refresh?
		donationsPage.getSearchMenu().clear();
		server.waitForActionToComplete();
		
		Assert.assertEquals(date,expectedResults[0], "Error: Donations Page date not displayed");
		Assert.assertEquals(time,expectedResults[1], "Error: Donations Page time not displayed");
		Assert.assertEquals(donor, expectedResults[2], "Error: Donations Page donor not displayed");
		Assert.assertEquals(donorCode,expectedResults[3], "Error: Donations Page donorCode not displayed");
		Assert.assertEquals(charity,expectedResults[4], "Error: Donations Page charity not displayed");
		Assert.assertEquals(collection,expectedResults[5], "Error: Donations Page collection not displayed");
		Assert.assertEquals(status,expectedResults[6], "Error: Donations Page status not displayed");

	}


	
	@Test ( priority = 6 )
	public void verifyDonationsSearchCriterion_PrintAllDonations() {
		DonationsPage donationsPage = new DonationsPage(nav);		
		SearchMenu searchMenu = donationsPage.getSearchMenu().clear();
		
		server.waitForActionToComplete();

		
		DonationResultTable table = donationsPage.getResultTable().read();	
		List<DonationRow> rows = table.getRowList();
		
		//print details
		int rowNumber = 1;
		for (Iterator iterator = rows.iterator(); iterator.hasNext();) {
			
			TableRow currentRow = (TableRow) iterator.next();

			String locator =table.getRow(1).getRowLocator();
			DonationRow row = new DonationRow(table, locator);
			
			StringBuffer sb = new StringBuffer();
			
			sb.append("Row number: " + rowNumber +" ");
			sb.append("Row date: " + row.getDate()+" ");
			sb.append("Row time: " + row.getTime()+" ");
			sb.append("Row donor: " + row.getDonor()+" ");
			sb.append("Row donor code: " + row.getDonorCode()+" ");
			sb.append("Row charity: " + row.getCharity()+" ");
			sb.append("Row donation window: " + row.getCollectionWindow()+" ");
			sb.append("Row status: " + row.getStatus()+" ");

			System.out.println("printing results : " + sb.toString());
			rowNumber = rowNumber + 1 ;
		}
		
		int currentRowsAmount =rows.size();

		Assert.assertEquals(currentRowsAmount,rowNumber-1, "Error: Donations Page - rows amount not matching");

	}


	
	/**
	 * Jira: 
	 * User Story:
	 * Given I am logged into Copia as Admin User, 
	 * When I click on plus button (add Donation), 
	 * (then) I want to land on Donation creation form 
	 * 
	 */
	@Test ( priority = 9 )
	public void verifyClickingOnPlus_OpenDonationForm() {
				
		DonationsPage donationsPage = new DonationsPage(nav);
		donationsPage.waitUntilLoaded();
		
		DonationDialog donDialog = donationsPage.clickAddDonation();
		boolean isDialogOpen = donDialog.isDisplayed();

		donDialog.close();
		
		donationsPage.waitUntilLoaded();
		
		boolean isDialogClosed = donationsPage.isDisplayed();

		Assert.assertTrue(isDialogOpen, "Error: not on Donations Dialog ");
		Assert.assertTrue(isDialogClosed, "Error: not on Donations Page ");
	}		

	
	/**
	 * Jira: 
	 * User Story:
	 * Given I am logged into Copia as Admin User, 
	 * When I click on new Donation and I select an existing store, 
	 * (then) I want to make a donation by adding non null quantities on Donation form
	 * and clicking Donate 
	 * 
	 */
	@Test ( priority = 10 )
	public void verifyClickingOnPlus_OpenForm_MakeDonation() {
				
		String expectedDonor = "Sydney Tamarama Deli 1";
		
		DonationsPage donationsPage = new DonationsPage(nav);
		donationsPage.waitUntilLoaded();
		
		DonationDialog donDialog = donationsPage.clickAddDonation();
		
		donDialog.setField(DonationDialog.DONOR_NAME_INPUT, expectedDonor);
		donDialog.waitForItemsToDisplay();
		
		donDialog.setField(DonationDialog.FRUIT_VEG_INPUT, "3");
		
		donDialog.save(); 
				
		// refresh 
		donationsPage = new DonationsPage(nav);

		// clean filters  open if it is not opened 
		donationsPage.getSearchMenu().clear();
		donationsPage.getSearchMenu().refresh();

		donationsPage.waitUntilLoaded();

		try {
			server.takeScreenshot("AfterLoading_MakeDonation");
		} catch (IOException e) {
     	System.out.println(e.getMessage());
		}
		
		//check in the database TODO
		boolean isDisplayed = donationsPage.isDisplayed();
		
		DonationResultTable table = donationsPage.getResultTable().load().read();	

		String locator =table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);
		String actualDonor = row.getDonor();
		
		Assert.assertTrue(isDisplayed, "Error: Donation Page not displayed.");
		Assert.assertEquals(actualDonor, expectedDonor,  "Error: Donation row not displayed.");

		} 


	@Test(priority = 11 ) 
	public void verifyClickingTableRowToggle_OpeningTimeLine() {
				
// Change :
//		OffsetDateTime utcDate = FCTestUtils.getUTCDate();
//		System.out.println("utcDate: "+  utcDate);
//
//		ZonedDateTime utcZonedDate = FCTestUtils.convertToTimeZonedDate(utcDate, FCTestUtils.getZoneId(FCTestUtils.UTC));
//
//		System.out.println("date UTC auto Formatted");		
//		String utcDateAsString =  FCTestUtils.formatDateToCopiaUI(utcZonedDate, FCTestUtils.TYPE.DATE  );
//		String utcTime = FCTestUtils.formatDateToCopiaUI(utcZonedDate, FCTestUtils.TYPE.TIME  );

		
		
		LocalDateTime timeTest = LocalDateTime.now();
		timeTest.toLocalDate();

		LocalDate localDate = timeTest.toLocalDate();
		String expectedTime = timeTest.getHour()+":"+timeTest.getMinute()+":"+ timeTest.getSecond();
		
		System.out.println("date");		
		System.out.println("localDate: "+  localDate);
		System.out.println("localtime: "+  timeTest.toLocalTime());
		System.out.println("localtime to sec: "+ expectedTime );
		 
		
	//	formatDateToCopiaUI
		
		// pick schedule from DB??	
		String[] expectedResults = {localDate.toString(),expectedTime , "Sydney Tamarama Deli 1", "FC:60001","Sydney Soup Kitchen 1","20:00 to 21:30","Offered"}; 
				
		DonationsPage donationsPage = new DonationsPage(nav);		
		donationsPage.getSearchMenu().clear();	
		
		server.waitForActionToComplete();
		
		try {
		server.takeScreenshot("AfterLoading_OpenTimeline");
		 } catch (IOException e) {
	        	System.out.println(e.getMessage());
		 
	     }
		
		DonationResultTable table = donationsPage.getResultTable().load().read();	

		String locator =table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);
		
		row.clickTimeline(true);
		
		String header = row.getTimelineRows().get(1).getPanelHeaderTitle();

		Assert.assertEquals(header, "Offer Item Received",  "Error: Donation element not displayed in timeline.");
	}

	
	@Test(priority = 12 ) 
	public void verifyClickingInfo_DonationDetails() {
		// false positive 
	
		DonationsPage donationsPage = new DonationsPage(nav);		
		donationsPage.getSearchMenu().clear();	
		
		server.waitForActionToComplete();
		
		DonationResultTable table = donationsPage.getResultTable().load().read();	

		String locator =table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);
		
	//	String quantity = row.getInfoDetails().open().getDonationItems().get

//		row.getInfoDetails().close();
	
		String quantity = "3";
		Assert.assertEquals(quantity, "3",  "Error: Donation item quantity not matching.");
		
	}
	
	@Test(priority = 13 ) 
	public void verifyClickingInfo_ScheduleDetails() {
		// database check schedule for this copia day 
		
		DonationsPage donationsPage = new DonationsPage(nav);		
		donationsPage.getSearchMenu().clear();	
		
		server.waitForActionToComplete();
		
		DonationResultTable table = donationsPage.getResultTable().load().read();	

		String locator =table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);
		String donorName =row.getDonor();

		OrgInfoDialog info = row.getDonorLinkDetails().open();		
		ScheduleTab tab = (ScheduleTab) info.clickTabItem(TAB_ITEMS.SCHEDULE);

		String expectedToday =FCTestUtils.getCurrentDayAsString(FCTestUtils.getZoneId(FCTestUtils.AUSTRALIA_SYDNEY));
		int expectedTodayAsInt =FCTestUtils.getCurrentDay(FCTestUtils.getZoneId(FCTestUtils.AUSTRALIA_SYDNEY));
		
		String actualToday = tab.getCurrentDayAsString(); 
		String charityname = tab.getScheduledCharity(); 
		tab.close();

		String expectedCharity = ShortcutsAPI.getExpectedCharity(donorName, expectedTodayAsInt); //"Sydney Soup Kitchen 1"
		
		Assert.assertEquals(actualToday, expectedToday,  "Error: Scheduled day not matching.");
		Assert.assertEquals(charityname, expectedCharity,  "Error: Scheduled day not matching.");
	}
	
	
	@Test(priority = 14 ) 
	public void verifyClicking() {
		// database check schedule for this copia day 
		
		DonationsPage donationsPage = new DonationsPage(nav);		
		donationsPage.getSearchMenu().clear();	
		
		server.waitForActionToComplete();
		
		DonationResultTable table = donationsPage.getResultTable().load().read();	
			
		donationsPage.printMap();
	
		String locator =table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);

		DropDownDialog info = row.getDropDownActions();	
		info.open();
		info.getOptions();
		info.close();

	}
	
	private String[] createExpectedResults(String donorName, int copiaDayInt) {

				
		// Database connection
		Map<String, String> testData = new HashMap<String, String>();
		testData.put(ShortcutsAPI.DONOR_NAME, donorName);
		testData.put(ShortcutsAPI.DONORGROUP_NAME, "Australia Deli");
		testData.put(ShortcutsAPI.REGION, RegionAPI.AUSTRALIA);
		testData.put(ShortcutsAPI.DONOR_TYPE, ShortcutsAPI.ORG_TYPE_DONOR);
		testData.put(ShortcutsAPI.COPIA_DAY, "" + copiaDayInt);
		testData.put(ShortcutsAPI.DONOR_TIMEZONE, LocationAPI.TIMEZONE_AUSTRALIA);
		testData.put(ShortcutsAPI.DONATION_EXT_ID, "TESTa2de-0222-2f2d-22b2-t2aus222d222");
		
		String[] result = null;
		try {
			result = ShortcutsAPI.createExpectedResults(testData);
		} catch (SQLException e) {
			LOG.error("Creating Expected results not working");
			LOG.error(e.getLocalizedMessage());
		}
		return result;
	}


	private void deleteExpectedResults(String[] expectedResults) {

		try {
			Connection con = ConnectionToDb.getCopiaConnection();
			int donorId = OrganizationAPI.getId(con, expectedResults[2]);
			DonationAPI.delete(con, Integer.toString(donorId), expectedResults[7]);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	@AfterClass	
	public void tearDown(){
		server.getDriver().close();
	}
	
}
