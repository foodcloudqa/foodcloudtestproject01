package com.foodcloud.copia.ui.checks;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.sql.Connection;
import java.sql.SQLException;


import com.foodcloud.model.dialogs.DonationDialog;
import com.foodcloud.model.dialogs.DropDownDialog;
import com.foodcloud.model.dialogs.OrgInfoDialog;
import com.foodcloud.model.dialogs.ScheduleTab;
import com.foodcloud.model.dialogs.OrgInfoDialog.TAB_ITEMS;
import com.foodcloud.model.pages.DashboardPage;
import com.foodcloud.model.pages.DonationsPage;
import com.foodcloud.model.pages.LoginPage;
import com.foodcloud.model.pages.OptionMenu.MENU_ITEMS;
import com.foodcloud.model.pages.SearchMenu;
import com.foodcloud.model.tables.DonationRow;
import com.foodcloud.model.tables.DonationResultTable;
import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;
import com.foodcloud.server.FCTestUtils;
import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.DonationAPI;
import com.foodcloud.db.copiaAPI.LocationAPI;
import com.foodcloud.db.copiaAPI.OrganizationAPI;
import com.foodcloud.db.copiaAPI.RegionAPI;
import com.foodcloud.db.copiaAPI.ShortcutsAPI;

/**
 * 
 * Trial page :) 
 * Need a way to extract data from file (custom file) 
 * Need a way to  :  data-driven testing (with @DataProvider and/or XML configuration).
 * 
 * Runtime.getRuntime().exec("cmd /C date " + strDateToSet + "& time " + strTimeToSet);
 */
public class DonationTest {

	FCTestServer server;
	private String URL = "http://copia-c.herokuapp.com/";
	private FCTestNavigator nav; 
	
	static final Logger DONATION_LOG = LogManager.getLogger(DonationTest.class.getName());

	@BeforeClass
	public void setupBeforeClass() throws FileNotFoundException {

	nav = new FCTestNavigator();
	nav.setup();	
	server = nav.getServer();
		
	server.openURL(URL);

	String username = "im.admin@foodcloud.ie"; 
	String password = "copiate2015";
	String adminTitleLocator = "//div[@class='copia-top row']";
	
//	String username = "4045@uk.tesco.com";
//	String password = "12345";
//	String userTitleLocator = "//div[@class='span12 org-header']";

	
	LoginPage loginPage = new LoginPage(nav);
	loginPage.setLoginField(username);
	loginPage.setPasswordField(password);
	loginPage.clickSubmit();
	
	String titleLocator = adminTitleLocator;	
	//String titleLocator = userTitleLocator;
	
	server.waitForElement(titleLocator);				
	} 
	
	

	/**
	 * Jira: 
	 * User Story:
	 * Given I am logged into Copia, When I click on Donations, (then) I want to land on Donations page 
	 */
	@Test ( priority = 1 )
	public void verifyClickingOnDonations_OpensDonationsPage() {
		String normalUserTitleLocator = "id=donation-table-container";
		String adminTitleLocator = "//h2/span[@translate='Donations']";
		
		String titleLocator = normalUserTitleLocator; // adminTitleLocator;
		
 		DashboardPage dashPage = new DashboardPage(nav);
		dashPage.getMenu().clickItem(MENU_ITEMS.DONATIONS);
		
		server.waitForElement(titleLocator);
		
		DonationsPage donationsPage = new DonationsPage(nav);
		server.waitForActionToComplete();

		boolean isEmpty = donationsPage.getResultTable().load().isEmpty();	
		
		Assert.assertFalse(isEmpty, "Error: Donations Page with data not displayed.");

		} 
	
	/**
	 * Jira: 
	 * User Story:
	 * Given I am logged into Copia as Search User, When I type an incorrect search criteria, (then) I want the Donations page to display an empty table.
	 * Note: Search criteria can vary from user to user depending on Role / Organization / Region they have access to 
	 */
	@Test(priority = 2)
	public void verifyDonationsSearchCriterion_ReturningEmptyTable() {
		DonationsPage donationsPage = new DonationsPage(nav);
		
		server.waitForActionToComplete();
		
		SearchMenu searchMenu = donationsPage.getSearchMenu().open();	
		//not working for all pages 
		searchMenu.setField(DonationsPage.DONOR_FIELD_NAME,"Thursoup");

		server.waitForActionToComplete();

		boolean isEmpty = donationsPage.getResultTable().load().isEmpty();	
		
		Assert.assertTrue(isEmpty, "Error: Donations Page no results not displayed.");
	}

	@Test(priority = 3)
	public void verifyDonationsSearchCriterion_ReturningFullTableSinglePage() {
		DonationsPage donationsPage = new DonationsPage(nav);		
		SearchMenu searchMenu = donationsPage.getSearchMenu().clear();

		server.waitForActionToComplete();
		
		DonationResultTable table = donationsPage.getResultTable().load().read();
		int elementListSize = table.getRowList().size(); 
		
		// read the dom ?? window pane size?
		//array size vary according to browser size: 8 rows in full screen, 7 in resized ones 
		Assert.assertEquals(elementListSize, 8, "Error: Donations Page nunmber of results not matching.");
		
	}

	
	@Test ( priority = 4 , enabled = false)
	public void verifyDonationsSearchCriterion_ReturningFullTableMultiplePages() {

		//how can I know how many pages according to the screen resolution?
		
		Assert.fail("Not impl yet");		
	}
	
	
	
	@Test(priority = 5) 
	public void verifyDonationsSearchCriterion_ReadingSingleRow() {
		
		String expectedDonor = "RawChocolate Store 2";
		int copiaDayAsInt = FCTestUtils.getCurrentCopiaDay();

		//expected result failing with DST (daylight saving)
		String[] expectedResults =	createExpectedResults(expectedDonor, copiaDayAsInt );
						
		DonationsPage donationsPage = new DonationsPage(nav);		
		donationsPage.getSearchMenu().clear();
		
		server.waitForActionToComplete();
		
		DonationResultTable table = donationsPage.getResultTable().load().read();	
		String locator =table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);

				
		String date = row.getDate();
		String time = row.getTime();
		String donor = row.getDonor();
		String donorCode = row.getDonorCode();
		String charity = row.getCharity();
		String collection = row.getCollectionWindow();
		String status = row.getStatus();
		
		System.out.println("_ReadingSingleRow: ");		
		System.out.println("date: "+  date);
		System.out.println("time: "+ time );
		System.out.println("donor: "+ donor );
		System.out.println("donorCode: "+ donorCode );
		System.out.println("charity: "+ charity );
		System.out.println("collection: "+ collection );
		System.out.println("status: "+ status );

		//delete data 		
		deleteExpectedResults(expectedResults);
		
		// refresh?
		donationsPage.getSearchMenu().clear();
		server.waitForActionToComplete();
		
		Assert.assertEquals(date,expectedResults[0], "Error: Donations Page date not displayed");
		Assert.assertEquals(time,expectedResults[1], "Error: Donations Page time not displayed");
		Assert.assertEquals(donor, expectedResults[2], "Error: Donations Page donor not displayed");
		Assert.assertEquals(donorCode,expectedResults[3], "Error: Donations Page donorCode not displayed");
		Assert.assertEquals(charity,expectedResults[4], "Error: Donations Page charity not displayed");
		Assert.assertEquals(collection,expectedResults[5], "Error: Donations Page collection not displayed");
		Assert.assertEquals(status,expectedResults[6], "Error: Donations Page status not displayed");

	}

	
	/**
	 * Jira: 
	 * User Story:
	 * Given I am logged into Copia as Admin User, When I click on plus button (add Donation), (then) I want to land on Donation creation form 
	 * 
	 * driver.navigate().refresh();
	 */
	@Test ( priority = 9 )
	public void verifyClickingOnPlus_OpenDonationForm() {
				
		DonationsPage donationsPage = new DonationsPage(nav);
		donationsPage.waitUntilLoaded();
		
		DonationDialog donDialog = donationsPage.clickAddDonation();
		boolean isDialogOpen = donDialog.isDisplayed();

		donDialog.close();
		
		donationsPage.waitUntilLoaded();
		
		boolean isDialogClosed = donationsPage.isDisplayed();

		Assert.assertTrue(isDialogOpen, "Error: not on Donations Dialog ");
		Assert.assertTrue(isDialogClosed, "Error: not on Donations Page ");
	}		

	
	/**
	 * Jira: 
	 * User Story:
	 * Given I am logged into Copia as Admin User, When I click on plus button (add Donation), (then) I want to land on Donation creation form 
	 * 
	 * driver.navigate().refresh();
	 */
	@Test ( priority = 10 )
	public void verifyClickingOnPlus_OpenForm_MakeDonation() {
				
		String expectedDonor = "RawChocolate Store 2";
		
		DonationsPage donationsPage = new DonationsPage(nav);
		donationsPage.waitUntilLoaded();
		
		DonationDialog donDialog = donationsPage.clickAddDonation();
		
		donDialog.setField(DonationDialog.DONOR_NAME_INPUT, expectedDonor);
		donDialog.waitForItemsToDisplay();
		
		donDialog.setField(DonationDialog.BAKERY_INPUT, "5");
		donDialog.setField(DonationDialog.FRUIT_VEG_INPUT, "10");
		donDialog.setField(DonationDialog.MEAT_INPUT, "2");
		donDialog.setField(DonationDialog.GROCERIES_INPUT, "1");
		donDialog.setField(DonationDialog.FISH_INPUT, "1");
		donDialog.setField(DonationDialog.CHILLED_PROD_INPUT, "1");
		donDialog.setField(DonationDialog.PREP_FOOD_INPUT, "3");
		
		donDialog.save(); 
				
		// refresh 
		//donationsPage = new DonationsPage(nav);
		donationsPage.waitForAlert();

		//expected - alert message 
		String isSuccessBox = donationsPage.getAlert();
		
		// clean filters  open if it is not opened 
		donationsPage.getSearchMenu().clear();
		donationsPage.getSearchMenu().refresh();

		donationsPage.waitUntilLoaded();
		
		//check in the database TODO
		boolean isDisplayed = donationsPage.isDisplayed();

		DonationResultTable table = donationsPage.getResultTable().load().read();	
		
		try {
			server.takeScreenshot("DonationTest_AfterLoading_MakeDonation");
		} catch (IOException e) {
     	System.out.println(e.getMessage());
		}
				
		String locator =table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);
		String actualDonor = row.getDonor();

		
		Assert.assertTrue(isDisplayed, "Error: Donation Page not displayed.");
		Assert.assertEquals(isSuccessBox, "Success", "Error: Success Alert not displayed.");
		Assert.assertEquals(actualDonor, expectedDonor,  "Error: Donation row not displayed.");

		} 


	@Test(priority = 11 ) 
	public void verifyClickingTableRowToggle_OpeningTimeLine() {
		
		DonationsPage donationsPage = new DonationsPage(nav);		
		donationsPage.getSearchMenu().clear();	
		
		server.waitForActionToComplete();
		
		DonationResultTable table = donationsPage.getResultTable().load().read();	
		String locator =table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);
		
		row.clickTimeline(true);
		
		String header = row.getTimelineRows().get(1).getPanelHeaderTitle();

		Assert.assertEquals(header, "Offer Item Received",  "Error: Donation element not displayed in timeline.");
	}

	
	@Test(priority = 12 ) 
	public void verifyClickingInfo_DonationDetails() {

		DonationsPage donationsPage = new DonationsPage(nav);		
		donationsPage.getSearchMenu().clear();	
		
		server.waitForActionToComplete();
		
		DonationResultTable table = donationsPage.getResultTable().load().read();	
		String locator =table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);
		
		String quantity = row.getInfoDetails().open().getDonationItems().get("Fish");

		row.getInfoDetails().close();
		
		Assert.assertEquals(quantity, "1",  "Error: Donation item quantity not matching.");
		
	}
	
	
	@Test(priority = 13 ) 
	public void verifyClickingInfo_ScheduleDetails() throws SQLException {
		DonationsPage donationsPage = new DonationsPage(nav);		
		donationsPage.getSearchMenu().clear();	
		
		server.waitForActionToComplete();
		
		DonationResultTable table = donationsPage.getResultTable().load().read();	

		String locator =table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);
		String donorName =row.getDonor();

		OrgInfoDialog info = row.getDonorLinkDetails().open();		
		ScheduleTab tab = (ScheduleTab) info.clickTabItem(TAB_ITEMS.SCHEDULE);

		String expectedToday =FCTestUtils.getCurrentDayAsString(FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));
		int expectedTodayAsInt =FCTestUtils.getCurrentDay(FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));
		
		String actualToday = tab.getCurrentDayAsString(); 
		String charityname = tab.getScheduledCharity(); 
		tab.close();

		String expectedCharity = ShortcutsAPI.getExpectedCharity(donorName, expectedTodayAsInt); //"Sydney Soup Kitchen 1"
		
		Assert.assertEquals(actualToday, expectedToday,  "Error: Scheduled day not matching.");
		Assert.assertEquals(charityname, expectedCharity,  "Error: Scheduled day not matching.");

	}


	@Test(priority = 14 ) 
	public void verifyClicking() {
		// database check schedule for this copia day 
		
		DonationsPage donationsPage = new DonationsPage(nav);		
		donationsPage.getSearchMenu().clear();	
		
		server.waitForActionToComplete();
		
		DonationResultTable table = donationsPage.getResultTable().load().read();	
				
		donationsPage.printMap();

		server.waitForActionToComplete();

		donationsPage.isSuccessfulReload();
		
		String locator =table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);
		DropDownDialog info = row.getDropDownActions();	
		info.open();

		info.getOptions();

		info.close();
				
	}

	
	
	private String[] createExpectedResults(String donorName, int copiaDayInt) {

		// Database connection
		Map<String, String> testData = new HashMap<String, String>();
		testData.put(ShortcutsAPI.DONOR_NAME, donorName);
		testData.put(ShortcutsAPI.DONORGROUP_NAME, "FoodCloud Demo Group");
		testData.put(ShortcutsAPI.REGION, RegionAPI.ISLE_OF_MAN);
		testData.put(ShortcutsAPI.DONOR_TYPE, ShortcutsAPI.ORG_TYPE_DONOR);
		testData.put(ShortcutsAPI.COPIA_DAY, "" + copiaDayInt);
		testData.put(ShortcutsAPI.DONOR_TIMEZONE, LocationAPI.TIMEZONE_MANISLAND);
		testData.put(ShortcutsAPI.DONATION_EXT_ID, "TESTa1de-0111-1f1d-11b1-t1iom111d111");

		String[] result = null;
		try {
			result = ShortcutsAPI.createExpectedResults(testData);
		} catch (SQLException e) {
			DONATION_LOG.error("Creating Expected results not working");
			DONATION_LOG.error(e.getLocalizedMessage());
		}
		return result;
	}	
	

	private void deleteExpectedResults(String[] expectedResults) {

		try {
			Connection con = ConnectionToDb.getCopiaConnection();
			int donorId = OrganizationAPI.getId(con, expectedResults[2]);
//			DonationEventAPI.
			
			DonationAPI.delete(con, Integer.toString(donorId), expectedResults[7]);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	@AfterClass	
	public void tearDown(){
		server.getDriver().close();
	}
	
}
