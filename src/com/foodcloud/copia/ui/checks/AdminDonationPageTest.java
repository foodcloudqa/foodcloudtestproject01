package com.foodcloud.copia.ui.checks;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.FileNotFoundException;
import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.SQLException;

import com.foodcloud.model.dialogs.DonationDialog;
import com.foodcloud.model.dialogs.DropDownDialog;
import com.foodcloud.model.dialogs.OrgInfoDialog;
import com.foodcloud.model.dialogs.ScheduleTab;
import com.foodcloud.model.dialogs.OrgInfoDialog.TAB_ITEMS;
import com.foodcloud.model.pages.DashboardPage;
import com.foodcloud.model.pages.DonationsPage;
import com.foodcloud.model.pages.LoginPage;
import com.foodcloud.model.pages.OptionMenu.MENU_ITEMS;
import com.foodcloud.model.pages.SearchMenu;
import com.foodcloud.model.tables.DonationRow;
import com.foodcloud.model.tables.DonationResultTable;
import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;
import com.foodcloud.server.FCTestUtils;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.DonationAPI;
import com.foodcloud.db.copiaAPI.DonorAPI;
import com.foodcloud.db.copiaAPI.OrganizationAPI;
import com.foodcloud.db.copiaAPI.ScheduleAPI;

/**
 * 
 * Trial page :) Need a way to extract data from file (custom file) Need a way
 * to : data-driven testing (with @DataProvider and/or XML configuration).
 */
public class AdminDonationPageTest {

	FCTestServer server;
	private String URL = "http://copia-c.herokuapp.com/";
	private FCTestNavigator nav;

	static final Logger LOG = LogManager.getLogger(AdminDonationPageTest.class.getName());

	@BeforeClass
	public void setupBeforeClass() throws FileNotFoundException {

		nav = new FCTestNavigator();
		nav.setup();
		server = nav.getServer();

		server.openURL(URL);

		String username = "im.admin@foodcloud.ie";
		String password = "copiate2015";
		String adminTitleLocator = "//div[@class='copia-top row']";

		LoginPage loginPage = new LoginPage(nav);
		loginPage.setLoginField(username);
		loginPage.setPasswordField(password);
		loginPage.clickSubmit();

		String titleLocator = adminTitleLocator;

		server.waitForElement(titleLocator);
	}

	/**
	 * User Story: Given I am logged into Copia, When I click on Donations,
	 * (then) I want to land on Donations page
	 */
	@Test(priority = 1)
	public void verifyClickingOnDonations_OpensDonationsPage() {
		String normalUserTitleLocator = "id=donation-table-container";
		// String adminTitleLocator = "//h2/span[@translate='Donations']";

		String titleLocator = normalUserTitleLocator; // adminTitleLocator;

		DashboardPage dashPage = new DashboardPage(nav);
		dashPage.getMenu().clickItem(MENU_ITEMS.DONATIONS);

		server.waitForElement(titleLocator);

		DonationsPage donationsPage = new DonationsPage(nav);
		server.waitForActionToComplete();

		boolean isEmpty = donationsPage.getResultTable().load().isEmpty();

		Assert.assertFalse(isEmpty, "Error: Donations Page with data not displayed.");

	}

	/**
	 * User Story: Given I am logged into Copia as Admin User, When I want to
	 * Search and I type an incorrect search criteria, (then) I want the
	 * Donations page to display an empty table. Note: Search criteria can vary
	 * from user to user depending on Role / Organization / Region they have
	 * access to
	 */
	@Test(priority = 2)
	public void verifyDonationsSearchCriterion_ReturningEmptyTable() {
		DonationsPage donationsPage = new DonationsPage(nav);

		server.waitForActionToComplete();

		SearchMenu searchMenu = donationsPage.getSearchMenu().open();
		// not working for all pages
		searchMenu.setField(DonationsPage.DONOR_FIELD_NAME, "Thursoup");

		server.waitForActionToComplete();

		boolean isEmpty = donationsPage.getResultTable().load().isEmpty();

		Assert.assertTrue(isEmpty, "Error: Donations Page no results not displayed.");
	}

	@Test(priority = 3)
	public void verifyDonationsSearchCriterion_ReturningFullTableSinglePage() {
		DonationsPage donationsPage = new DonationsPage(nav);
		SearchMenu searchMenu = donationsPage.getSearchMenu().clear();

		server.waitForActionToComplete();

		DonationResultTable table = donationsPage.getResultTable().load().read();
		int elementListSize = table.getRowList().size();

		// array size vary according to browser size: 8 rows in full screen, 7
		// in resized ones
		Assert.assertEquals(elementListSize, 8, "Error: Donations Page nunmber of results not matching.");

	}

	@Test(priority = 4, enabled = false)
	public void verifyDonationsSearchCriterion_ReturningFullTableMultiplePages() {
		Assert.fail("Not impl yet");
	}

	@Test(priority = 5)
	public void verifyDonationsSearchCriterion_ReadingSingleRow() {

		String expectedDonor = "RawChocolate Store 2";
		int copiaDayAsInt = FCTestUtils.getCurrentCopiaDay();

		String[] expectedResults = createExpectedResults(expectedDonor, copiaDayAsInt);

		DonationsPage donationsPage = new DonationsPage(nav);
		SearchMenu searchMenu = donationsPage.getSearchMenu().clear();

		server.waitForActionToComplete();

		DonationResultTable table = donationsPage.getResultTable().load().read();
		String locator = table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);

		String date = row.getDate();
		String time = row.getTime();
		String donor = row.getDonor();
		String donorCode = row.getDonorCode();
		String charity = row.getCharity();
		String collection = row.getCollectionWindow();
		String status = row.getStatus();

		LOG.warn("_ReadingSingleRow: ");
		LOG.warn("date: " + date);
		LOG.warn("time: " + time);
		LOG.warn("donor: " + donor);
		LOG.warn("donorCode: " + donorCode);
		LOG.warn("charity: " + charity);
		LOG.warn("collection: " + collection);
		LOG.warn("status: " + status);

		// delete data
		deleteExpectedResults(expectedResults);

		// refresh?
		donationsPage.getSearchMenu().clear();
		server.waitForActionToComplete();

		Assert.assertEquals(date, expectedResults[0], "Error: Donations Page date not displayed");
		Assert.assertEquals(time, expectedResults[1], "Error: Donations Page time not displayed");
		Assert.assertEquals(donor, expectedResults[2], "Error: Donations Page donor not displayed");
		Assert.assertEquals(donorCode, expectedResults[3], "Error: Donations Page donorCode not displayed");
		Assert.assertEquals(charity, expectedResults[4], "Error: Donations Page charity not displayed");
		Assert.assertEquals(collection, expectedResults[5], "Error: Donations Page collection not displayed");
		Assert.assertEquals(status, expectedResults[6], "Error: Donations Page status not displayed");

	}

	/**
	 * Jira: User Story: Given I am logged into Copia as Admin User, When I
	 * click on plus button (add Donation), (then) I want to land on Donation
	 * creation form
	 * 
	 * driver.navigate().refresh();
	 */
	@Test(priority = 9)
	public void verifyClickingOnPlus_OpenDonationForm() {

		DonationsPage donationsPage = new DonationsPage(nav);
		donationsPage.waitUntilLoaded();

		DonationDialog donDialog = donationsPage.clickAddDonation();
		boolean isDialogOpen = donDialog.isDisplayed();

		donDialog.close();

		donationsPage.waitUntilLoaded();

		boolean isDialogClosed = donationsPage.isDisplayed();

		Assert.assertTrue(isDialogOpen, "Error: not on Donations Dialog ");
		Assert.assertTrue(isDialogClosed, "Error: not on Donations Page ");
	}

	@Test(priority = 13)
	public void verifyClickingInfo_ScheduleDetails() {

		String expectedDonor = "RawChocolate Store 2";
		int copiaDayAsInt = FCTestUtils.getCurrentCopiaDay();

		// database check schedule for this copia day
		String expectedCharityName = getScheduledCharity(expectedDonor, copiaDayAsInt);

		DonationsPage donationsPage = new DonationsPage(nav);
		donationsPage.getSearchMenu().clear();
		server.waitForActionToComplete();

		DonationResultTable table = donationsPage.getResultTable().load().read();

		String locator = table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);

		OrgInfoDialog info = row.getDonorLinkDetails().open();
		ScheduleTab tab = (ScheduleTab) info.clickTabItem(TAB_ITEMS.SCHEDULE);

		String expectedToday = FCTestUtils.getCurrentDayAsString(FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));

		String today = tab.getCurrentDayAsString();
		String charityname = tab.getScheduledCharity();

		// to test!!! copia day charity
		tab.getAllRows();

		tab.close();
		
		Assert.assertEquals(today, expectedToday, "Error: Scheduled day not matching.");
		Assert.assertEquals(charityname, expectedCharityName, "Error: Scheduled charity name not matching.");
	}

	@Test(priority = 14)
	public void verifyClicking() {
		// database check schedule for this copia day

		DonationsPage donationsPage = new DonationsPage(nav);
		donationsPage.getSearchMenu().clear();

		server.waitForActionToComplete();

		DonationResultTable table = donationsPage.getResultTable().load().read();

		donationsPage.printMap();

		server.waitForActionToComplete();

		donationsPage.isSuccessfulReload();

		String locator = table.getRow(1).getRowLocator();
		DonationRow row = new DonationRow(table, locator);
		DropDownDialog info = row.getDropDownActions();
		info.open();

		List<String> actualOptions = info.getOptions();

		info.close();

		ArrayList expectedOptions = new ArrayList(){};
		
	//	Raise lert, 
		
		//getOtions
		Assert.assertEquals(actualOptions, expectedOptions, "Error: Scheduled charity name not matching.");

	}

	private String[] createExpectedResults(String donorName, int copiaDayInt) {

		OffsetDateTime utcDate = FCTestUtils.getUTCDate();
		System.out.println("utcDate: " + utcDate);

		ZonedDateTime utcZonedDate = FCTestUtils.convertToTimeZonedDate(utcDate,
				FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));

		System.out.println("date UTC auto Formatted");
		String expectedDate = FCTestUtils.formatDateToCopiaUI(utcZonedDate, FCTestUtils.TYPE.DATE);
		String expectedTime = FCTestUtils.formatDateToCopiaUI(utcZonedDate, FCTestUtils.TYPE.TIME);

		System.out.println("utcDate: " + expectedDate);
		System.out.println("utcTime: " + expectedTime);
		// Database connection
		String donationExternalId = "TESTa1de-0111-1f1d-11b1-t1aus111c111";
		String expectedStatus = "Collected";

		// create donation
		try {
			Connection con = ConnectionToDb.getCopiaConnection();
			int donorId = OrganizationAPI.getId(con, donorName);
		//	DonationAPI.create(con, Integer.toString(donorId), donationExternalId, expectedDate, expectedTime,
		//			expectedStatus);
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// get schedule
		String charityName = "";
		String startTime = "";
		String endTime = "";
		String donorCode = "";
		try {
			Connection con = ConnectionToDb.getCopiaConnection();
			int donorId = OrganizationAPI.getId(con, donorName);
			donorCode = DonorAPI.getExternalId(con, donorId);

			String charityId = ScheduleAPI.getScheduledCharity(con, donorId, copiaDayInt);
			charityName = OrganizationAPI.getOrgName(con, charityId);

			startTime = ScheduleAPI.getStartTime(con, donorId, copiaDayInt);

			endTime = ScheduleAPI.getEndTime(con, donorId, copiaDayInt);
			con.close();

			System.out.println("CharityName: " + charityName);
			System.out.println("startTime: " + startTime);
			System.out.println("endTime: " + endTime);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		String startTimeforUI = ((startTime.length() > 0) ? startTime.substring(0, 5) : startTime);
		String endTimeforUI = ((endTime.length() > 0) ? endTime.substring(0, 5) : endTime);
		String collectionWindow = startTimeforUI + " to " + endTimeforUI;

		String[] expectedResults = { expectedDate, expectedTime, donorName, donorCode, charityName, collectionWindow,
				expectedStatus, donationExternalId };

		return expectedResults;
	}

	private void deleteExpectedResults(String[] expectedResults) {

		try {
			Connection con = ConnectionToDb.getCopiaConnection();
			int donorId = OrganizationAPI.getId(con, expectedResults[2]);
			DonationAPI.delete(con, Integer.toString(donorId), expectedResults[7]);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private String getScheduledCharity(String donorName, int copiaDayInt) {

		String expectedCharityName = "No charity assigned";

		try {
			Connection con = ConnectionToDb.getCopiaConnection();
			int donorId = OrganizationAPI.getId(con, donorName);

			String charityId = ScheduleAPI.getScheduledCharity(con, donorId, copiaDayInt);
			String charityName = OrganizationAPI.getOrgName(con, charityId);

			if (!charityName.isEmpty()) {
				expectedCharityName = charityName;
			}
		} catch (Exception connection) {
			LOG.error(connection.getMessage());
		}

		return expectedCharityName;

	}

	@AfterClass
	public void tearDown() {
		server.getDriver().close();
	}

}
