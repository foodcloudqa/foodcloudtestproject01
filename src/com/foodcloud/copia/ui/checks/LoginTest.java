package com.foodcloud.copia.ui.checks;

import org.testng.annotations.*;
import org.testng.Assert;
import java.io.FileNotFoundException;
import com.foodcloud.model.pages.DashboardPage;
import com.foodcloud.model.pages.LoginPage;
import com.foodcloud.model.pages.OptionMenu.MENU_ITEMS;
import com.foodcloud.model.pages.SchedulePage;
import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;

/**
 * 
 * Trial page :) 
 * Need a way to extract data from file (custom file) 
 * Need a way to  :  data-driven testing (with @DataProvider and/or XML configuration).
 */
public class LoginTest {

	FCTestServer server;
//	private String Url = "http://copiarms.herokuapp.com/";
	private String Url = "http://copia-c.herokuapp.com/";
	private FCTestNavigator nav; 

	@BeforeClass
	public void setupBeforeClass() throws FileNotFoundException {
		
	nav = new FCTestNavigator();
	
	nav.setup();		

	server = nav.getServer();

	server.openURL(Url);
	
	} 
	
	
	/**
	 * User Story:
	 * Given correct username and password, When I login in Copia, (then) I want to land Dashboard page 
	 */
	@Test ( priority = 1 )
	public void verifyLoginSuccessful() {
		
//		String username = "im.admin@foodcloud.ie"; 
//		String password = "copiate2015";
		// user type= admin

		String username = "4045@uk.tesco.com"; 
		String password = "12345";
//		// user type= store
		
		LoginPage loginPage = new LoginPage(nav); //(nav, user)
		loginPage.setLoginField(username);
		loginPage.setPasswordField(password);
		loginPage.clickSubmit();
		
		//Landing Page
//		DashboardPage dashPage = new DashboardPage(nav);
//		dashPage.waitUntilLoaded();
//		boolean isPagePresent = dashPage.isDisplayed();
		
		SchedulePage schedulePage = new SchedulePage(nav);
		schedulePage.waitUntilLoaded();
		boolean isPagePresent = schedulePage.isDisplayed();
		
//		// generic locator: //div[@class='org-header-buttons'] | //div[@id='copia-option-menu']
//		// class="org-header-buttons"
//		String titleLocator = "//div[@class='copia-top row']";  // Admin locator (move to login page admin)
//		//loginpage / LandingPage admin type .isOnLandingPage();
//		server.waitForElement(server.getLocatorType(titleLocator));	
//
//		boolean isPagePresent =  server.isElementPresent(titleLocator);
		
		Assert.assertTrue(isPagePresent, "Error: Dashboard Page not displayed as expected");

	}

	/**
	 * User Story:
	 * Given a user logged in the system, When I click into Support Tickets menu item, (then) I want to land Support Ticket page 
	 */
	@Test ( priority = 2)  // (isAdmin)  SupportTicket
	public void verifyNavigationOK_DashboardPageDisplay() {

		//LandingPage lp = new Page / DashboardPage()  dataProvider.getLandingPage().getMenu().click()
 		DashboardPage dashPage = new DashboardPage(nav);
		dashPage.getMenu().clickItem(MENU_ITEMS.SUPPORT_TICKETS);

		String titleLocator = "//h2/span[@translate='Issues']";
		server.waitForElement(titleLocator);

		boolean isPagePresent =  server.isElementPresent(titleLocator);
		
		Assert.assertTrue(isPagePresent, "Error: Support Tickets Page not displayed as expected");
	}

	@AfterClass	
	public void tearDown(){
		server.getDriver().close();
	}



}
