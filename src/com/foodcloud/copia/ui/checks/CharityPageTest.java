package com.foodcloud.copia.ui.checks;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.FileNotFoundException;

import com.foodcloud.model.dialogs.CharityDialog;
import com.foodcloud.model.pages.CharitiesPage;
import com.foodcloud.model.pages.DashboardPage;
import com.foodcloud.model.pages.LoginPage;
import com.foodcloud.model.pages.OptionMenu.MENU_ITEMS;
import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;

/**
 * Test Charity Page
 * Need a way to  :  data-driven testing (with @DataProvider and/or XML configuration).
 */
public class CharityPageTest {

	FCTestServer server;
	private String URL = "http://copia-c.herokuapp.com/";
//	private String URL = "https://foodcloud.eu.ngrok.io/dashboard";
	private FCTestNavigator nav; 
	private DashboardPage dashPage;
	
	@BeforeClass
	public void setupBeforeClass() throws FileNotFoundException {
		
	nav = new FCTestNavigator();
	nav.setup();	
	server = nav.getServer();
		
	server.openURL(URL);

	// Only admin can create charities
	String username = "im.admin@foodcloud.ie"; 
	String password = "copiate2015";
		
	LoginPage loginPage = new LoginPage(nav);
	loginPage.setLoginField(username);
	loginPage.setPasswordField(password);
	loginPage.clickSubmit();
	
	dashPage = new DashboardPage(nav);
	dashPage.waitUntilLoaded();
	
	} 
	
	
	/**
	 * Jira: 
	 * User Story:
	 * Given I am logged into Copia as Admin User, When I click on Charities, (then) I want to land on Charities page 
	 */
	@Test ( priority = 1 )
	public void verifyClickingOnCharities_OpensCharitiesPage() {

		dashPage.getMenu().clickItem(MENU_ITEMS.CHARITIES);
				
		CharitiesPage charitiesPage = new CharitiesPage(nav);
		charitiesPage.waitUntilLoaded();

		boolean isDisplayed = charitiesPage.isDisplayed();
		
		Assert.assertTrue(isDisplayed, "Error: Charity Page not displayed.");

		} 
	
	/**
	 * Jira: 
	 * User Story:
	 * Given I am logged into Copia as Admin User, When I click on plus button (add Charity), (then) I want to land on Charities creation form 
	 */
	@Test ( priority = 2 )
	public void verifyClickingOnPlus_OpensCharitiesForm() {
				
		CharitiesPage charitiesPage = new CharitiesPage(nav);
		charitiesPage.waitUntilLoaded();
		
		String[] data = new String[]{"a", "b", "c"};
		//charitiesPage.createCharity().populateDialog(data);

		CharityDialog cd = charitiesPage.createCharity();
		
		cd.setField(CharityDialog.CHARITY_NAME_INPUT, "CharityTest");
		cd.setField(CharityDialog.CONTACT_NAME_INPUT, "ContactTest");
		cd.setField(CharityDialog.CONTACT_PHONE_INPUT, "+4488888888");
		cd.setField(CharityDialog.SMS_PHONE_INPUT, "+4477777777");  //there is a maximum lenght 
		cd.setField(CharityDialog.CONTACT_EMAIL_INPUT, "contact@test.ie");
		cd.setField(CharityDialog.ADDRESS_INPUT, "TestRoad, TestRoad, TestRoad");

		cd.setField(CharityDialog.LOCATION_POSTCODE_INPUT, "IM");		
		cd.setField(CharityDialog.REGION_CODE_INPUT, "Isle of Man");
		cd.setField(CharityDialog.GROUP_INPUT, "FoodCloud IoM");
		cd.setField(CharityDialog.EXTERNAL_ID_INPUT, "FC:12344444");
		
		cd.save(); 
				
		//check in the database TODO
		boolean isDisplayed = charitiesPage.isDisplayed();
		
		Assert.assertTrue(isDisplayed, "Error: Charity Page not displayed.");

		} 
	
	@AfterClass	
	public void tearDown(){
		server.getDriver().close();
	}
	
}
