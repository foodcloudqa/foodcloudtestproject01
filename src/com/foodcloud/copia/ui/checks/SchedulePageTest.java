package com.foodcloud.copia.ui.checks;

import java.io.FileNotFoundException;

import org.testng.annotations.BeforeClass;

import com.foodcloud.model.pages.LoginPage;
import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;

public class SchedulePageTest {

		FCTestServer server;
		private String URL = "http://copia-c.herokuapp.com/";
		private FCTestNavigator nav; 
		
		@BeforeClass
		public void setupBeforeClass() throws FileNotFoundException {
			
		nav = new FCTestNavigator();
		nav.setup();	
		server = nav.getServer();
			
		server.openURL(URL);

		String username = "im.admin@foodcloud.ie"; 
		String password = "copiate2015";
		String adminTitleLocator = "//div[@class='copia-top row']";
				
		LoginPage loginPage = new LoginPage(nav);
		loginPage.setLoginField(username);
		loginPage.setPasswordField(password);
		loginPage.clickSubmit();
		
		String titleLocator = adminTitleLocator;	
		//String titleLocator = userTitleLocator;
		
		server.waitForElement(titleLocator);				
		} 
}