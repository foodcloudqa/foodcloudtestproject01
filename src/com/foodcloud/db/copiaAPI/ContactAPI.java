package com.foodcloud.db.copiaAPI;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class representing Contact table in Copia Database 
 * Currently pointing to Copia-c
 * 
 * @author Viviana
 *
 */
public class ContactAPI {


	public static String ID = "id"; 
	public static String PARTY_ID = "party_id"; 
	public static String PARTY_TYPE = "party_type";
	public static String RANK = "rank";
	public static String TYPE = "type";
	public static String DATA = "data";
	public static String STATUS = "status";
	public static String LABEL = "label";
	public static String VERSION = "version";
	
	
	 static final Logger LOG = LogManager.getLogger(ContactAPI.class.getName());


		/**
		 * creates a contact r table 
		 * @throws SQLException 
		 */	
		//public void create(Connection con, Map<String, String> Data ) {	}
		
		
		
		/**
		 * 
		 * creates a new contact in the db by INSERTing data provided
		 *
		 */
		public static int insert(Connection con, Map<String, String> contactItemData) throws SQLException {
			// need control on data : ORG_ID most important!!
			if (!contactItemData.containsKey(PARTY_ID) || !contactItemData.containsKey(TYPE)){
				LOG.error("ContactAPI: Error! Mandatory field missing");
				return -1;
			} else {
				if(Integer.parseInt(contactItemData.get(PARTY_ID)) == 0) {
					LOG.error("ContactAPI: Error! Party / Org_id 0");
					return -1;				
				}
			}

			Set<String> fieldKeys = contactItemData.keySet();

			String fieldClause = ShortcutsAPI.modifyInsertSelectClause(fieldKeys);
			String valuesClause = ShortcutsAPI.modifyInsertValuesClause(fieldKeys);
					
			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append("INSERT INTO public.contact( ");
			queryBuffer.append(fieldClause + " )");
			queryBuffer.append("VALUES ( " + valuesClause);
			queryBuffer.append(" );");

			PreparedStatement insertStatement = con.prepareStatement(queryBuffer.toString());

			int i = 0;
			i = modifyStatement(insertStatement, i, contactItemData);

			LOG.info("ContactAPI:  Before executing query: " + insertStatement.toString());
			int result = insertStatement.executeUpdate();

			if (result == 0) {
				LOG.error("ContactAPI:  new element not inserted!");
			}
			return result;

		}


		/**
		 * Get contact specific ID 
		 * @param con
		 * @param orgIdCriterion
		 * @return
		 * @throws SQLException
		 */
	public static int getId(Connection con, int orgIdCriterion) throws SQLException {

		if (orgIdCriterion == 0) {
			LOG.error("ContactAPI: Error! Criteria must be not 0 ");
			return -1;
		}

		String idColumn = ContactAPI.ID;
		Map<String, String> searchCriterion = new HashMap<String, String>();
		searchCriterion.put(ContactAPI.PARTY_ID, Integer.toString(orgIdCriterion));

		Set<String> fieldList = new HashSet<String>();
		fieldList.add(idColumn);

		Map<String, String> foundId = getField(con, fieldList, searchCriterion);
		return Integer.parseInt(foundId.get(idColumn));
	}		
		
	public static int delete(Connection con, Map<String, String> criterion) throws SQLException {

		if (!criterion.containsKey(PARTY_ID)) {
			LOG.error("ContactAPI: Error!  Missing mandatory data: party id (org id) ");
			return -1;
		}

		Set<String> deleteKeys = criterion.keySet();
		String whereClause = ShortcutsAPI.modifyCriteriaClause(deleteKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("DELETE FROM public.contact WHERE ");
		queryBuffer.append(whereClause + " ;");

		PreparedStatement deleteStatement = con.prepareStatement(queryBuffer.toString());

		int delIndex = 0;
		delIndex = modifyStatement(deleteStatement, delIndex, criterion);

		LOG.info("ContactAPI:  Before executing query: " + deleteStatement.toString());
		int deletedRows = deleteStatement.executeUpdate();

		if (deletedRows == 0) {
			LOG.error("ContactAPI: element not deleted!");
		}
		return deletedRows;
	}
		
	public static int update(Connection con, Map<String, String> criteriaValues, Map<String, String> updatedValues)
			throws SQLException {

		if (criteriaValues.isEmpty() || updatedValues.isEmpty()) {
			LOG.error("ContactAPI: Error! Update values and criteria must be not null ");
			return -1;
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		Set<String> setKeys = updatedValues.keySet();

		String updateClause = ShortcutsAPI.modifyUpdateClause(setKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("UPDATE public.contact ");
		queryBuffer.append("SET " + updateClause + " ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());

		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, updatedValues);

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("ContactAPI:  Before executing query: " + statement.toString());
		int updatedRows = statement.executeUpdate();

		if (updatedRows == 0) {
			LOG.error("ContactAPI: element not updated!");
		}
		return updatedRows;
	}		
		
		public static boolean isElementPresent(Connection con, int idCriterion) throws SQLException {

			int foundId  = getId(con, idCriterion);
					
			boolean result=false;
			if (foundId!=0) {
				result = true;
			}
			return result;
		}
		
		
		public static Map<String, String> getField(Connection con, Set<String> fieldNeeded,
				Map<String, String> criteriaValues) throws SQLException {

			if (criteriaValues.isEmpty() || fieldNeeded.isEmpty()) {
				LOG.error("ContactAPI: Error! field values and criteria must be not null ");
				Map<String, String> empty = (Map<String, String>) new HashMap().put("Error", "Null values");
				return empty;
			}

			Set<String> whereKeys = criteriaValues.keySet();

			String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

			String selectClause = ShortcutsAPI.modifyInsertSelectClause(fieldNeeded);

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT " + selectClause + " ");
			queryBuffer.append("FROM public.contact ");
			queryBuffer.append("WHERE " + criteriaClause);
			queryBuffer.append(" ;");

			PreparedStatement statement = con.prepareStatement(queryBuffer.toString());
			int questionMarkAmount = 0;

			questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

			LOG.info("ContactAPI:  Before executing query: " + statement.toString());
			ResultSet rs = statement.executeQuery();

			Map<String, String> resultMap = new HashMap<String, String>();

			while (rs.next()) {
				if (fieldNeeded.contains(ContactAPI.ID)) {
					int idFound = rs.getInt(ContactAPI.ID);
					resultMap.put(ContactAPI.ID, Integer.toString(idFound));
				}			
				if (fieldNeeded.contains(ContactAPI.PARTY_ID)) {
					int orgIdFound = rs.getInt(ContactAPI.PARTY_ID);
					resultMap.put(ContactAPI.PARTY_ID, Integer.toString(orgIdFound));
				}
				if (fieldNeeded.contains(ContactAPI.PARTY_TYPE)) {
					String partyTypeFound = rs.getString(ContactAPI.PARTY_TYPE);
					resultMap.put(ContactAPI.PARTY_TYPE, partyTypeFound);
				}				
		
				if (fieldNeeded.contains(ContactAPI.RANK)) {
					int rankFound = rs.getInt(ContactAPI.RANK);
					resultMap.put(ContactAPI.RANK, Integer.toString(rankFound));
				}
				
				if (fieldNeeded.contains(ContactAPI.TYPE)) {
					String typeFound = rs.getString(ContactAPI.TYPE);
					resultMap.put(ContactAPI.TYPE, typeFound);
				}
				
				if (fieldNeeded.contains(ContactAPI.DATA)) {
					String dataFound = rs.getString(ContactAPI.DATA);
					resultMap.put(ContactAPI.DATA, dataFound);
				}

				if (fieldNeeded.contains(ContactAPI.STATUS)) {
					String statusFound = rs.getString(ContactAPI.STATUS);
					resultMap.put(ContactAPI.STATUS, statusFound);
				}

				if (fieldNeeded.contains(ContactAPI.LABEL)) {
					String labelFound = rs.getString(ContactAPI.LABEL);
					resultMap.put(ContactAPI.LABEL, labelFound);
				}

				if (fieldNeeded.contains(ContactAPI.VERSION)) {
					int versionFound = rs.getInt(ContactAPI.VERSION);
					resultMap.put(ContactAPI.VERSION, Integer.toString(versionFound));
				}
			}

			return resultMap;
		}

		
		private static int modifyStatement(PreparedStatement statement, int amountOfVariables, Map<String, String> inputMap)
				throws SQLException {

			Set<String> currentKeys = inputMap.keySet();

			for (Iterator<String> iterator = currentKeys.iterator(); iterator.hasNext();) {
				String key = (String) iterator.next();
				if (key.equalsIgnoreCase(PARTY_TYPE) || key.equalsIgnoreCase(TYPE) || key.equalsIgnoreCase(DATA)
						|| key.equalsIgnoreCase(STATUS)|| key.equalsIgnoreCase(LABEL) ) {
					statement.setString(amountOfVariables + 1, inputMap.get(key));
				} else if (key.equalsIgnoreCase(PARTY_ID) || key.equalsIgnoreCase(ID) || key.equalsIgnoreCase(RANK)  
						|| key.equalsIgnoreCase(VERSION)) {
					statement.setInt(amountOfVariables + 1, Integer.parseInt(inputMap.get(key)));
				}
				amountOfVariables = amountOfVariables + 1;
			}
			LOG.info("Current statement " + statement.toString());
			LOG.info("Current index value: " + amountOfVariables);

			return amountOfVariables;
		}

	
}
