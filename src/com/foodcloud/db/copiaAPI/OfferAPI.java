package com.foodcloud.db.copiaAPI;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class representing Donor table in Copia Database 
 * Currently pointing to Copia-c
 * 
 * @author Viviana
 *
 */
public class OfferAPI {

	public static String CREATED = "created";
	public static String DATE = "date";
	public static String TIME = "time";
	public static String DONOR_ID = "donor_id";
	public static String DONATION_ID = "donation_id";
	public static String ACCEPTANCE_ID = "acceptance_id";
	public static String RECIPIENT_ID = "recipient_id";
	public static String CONTACT_DATA = "contact_data";
	public static String STATUS = "status";
	public static String VERSION = "version";
	public static String TOKEN = "token";
	
	
	static final Logger LOG = LogManager.getLogger(OfferAPI.class.getName());
	
	/**
	 * creates a new charity in the db, having the organization id available 
	 * @throws SQLException 
	 */	
	//public static void create(Connection con, Map<String, String> offerData ) throws SQLException{
	//}
	
	
	public static int insert(Connection con, Map<String, String> offerData) throws SQLException {

		//if (!offerData.containsKey(DONATION_ID) || 
		if(	!offerData.containsKey(DONOR_ID)) {
			LOG.error("OfferAPI: Error!  Missing mandatory data : donation_id and donor_id ");
			return -1;
		} else if (Integer.parseInt(offerData.get(DONOR_ID)) == 0) {
			LOG.error("OfferAPI: Error! Donor id =  0");
			return -1;
		} else if (offerData.get(STATUS) == null || offerData.get(CONTACT_DATA) == null) {
			LOG.error("OfferAPI: Error! null value in Status or contact data field");
			return -1;
		}
		
		Set<String> fieldKeys = offerData.keySet();

		String fieldClause = ShortcutsAPI.modifyInsertSelectClause(fieldKeys);
		String valuesClause = ShortcutsAPI.modifyInsertValuesClause(fieldKeys);
				
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("INSERT INTO public.offer( ");
		queryBuffer.append(fieldClause + " )");
		queryBuffer.append("VALUES ( " + valuesClause);
		queryBuffer.append(" );");

		PreparedStatement insertStatement = con.prepareStatement(queryBuffer.toString());

		int i = 0;
		i = modifyStatement(insertStatement, i, offerData);

		LOG.info("OfferAPI:  Before executing query: " + insertStatement.toString());
		int result = insertStatement.executeUpdate();

		if (result == 0) {
			LOG.error("OfferAPI:  new element not inserted!");
		}
		return result;
	}
	
	
	/**
	 * Delete offer row with criterion
	 */
	public static int delete(Connection con, Map<String, String> criterion) throws SQLException {

		if (!criterion.containsKey(DONOR_ID)) {
			LOG.error("OfferAPI: Error!  Missing mandatory data: donor_id ");
			return -1;
		}

		Set<String> deleteKeys = criterion.keySet();
		String whereClause = ShortcutsAPI.modifyCriteriaClause(deleteKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("DELETE FROM public.offer WHERE ");
		queryBuffer.append(whereClause + " ;");

		PreparedStatement deleteStatement = con.prepareStatement(queryBuffer.toString());

		int delIndex = 0;
		delIndex = modifyStatement(deleteStatement, delIndex, criterion);

		LOG.info("OfferAPI:  Before executing query: " + deleteStatement.toString());
		int deletedRows = deleteStatement.executeUpdate();

		if (deletedRows == 0) {
			LOG.error("OfferAPI: element not deleted!");
		}
		return deletedRows;
	}

	/**
	 * Updates offer item in the DB given the criterion and updated values as
	 * maps
	 * 
	 * @param con
	 * @param criteriaValues
	 * @param updatedValues
	 * @return
	 * @throws SQLException
	 */
	public static int update(Connection con, Map<String, String> criteriaValues, Map<String, String> updatedValues)
			throws SQLException {

		if (criteriaValues.isEmpty() || updatedValues.isEmpty()) {
			LOG.error("OfferAPI: Error! Update values and criteria must be not null ");
			return -1;
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		Set<String> setKeys = updatedValues.keySet();

		String updateClause = ShortcutsAPI.modifyUpdateClause(setKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("UPDATE public.offer ");
		queryBuffer.append("SET " + updateClause + " ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());

		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, updatedValues);

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("OfferAPI:  Before executing query: " + statement.toString());
		int updatedRows = statement.executeUpdate();

		if (updatedRows == 0) {
			LOG.error("OfferAPI: element not updated!");
		}
		return updatedRows;
	}


	
	
	public static String getToken(Connection con,  String donorIdCriterion, String charityIdCriterion, String currentDate) throws SQLException{
		
		String checkFormattedDate = currentDate;
		
		String query = "SELECT token FROM public.offer WHERE  donor_id='"+donorIdCriterion+ "' AND  recipient_id='"+ charityIdCriterion +"'"
				+ "and date ='"+ currentDate +"' ORDER BY time DESC ;";

		System.out.println("OfferAPI: Before executing query: "+query );

		ResultSet re = con.createStatement().executeQuery(query);
		re.next();
		int foundValue = re.getInt("token");
			
		return Integer.toString(foundValue);
	}


	public static String getId(Connection con, String donorIdCriterion, String createdCriterion) throws SQLException {
		String query = "SELECT id FROM public.offer WHERE  donor_id='"+donorIdCriterion+ "' and created ='"+ createdCriterion +"'";

		System.out.println("OfferAPI: Before executing query: "+query );

		ResultSet re = con.createStatement().executeQuery(query);
		re.next();
		int foundValue = re.getInt("id");
			
		return Integer.toString(foundValue);
	}
	
	
	private static int modifyStatement(PreparedStatement statement, int amountOfVariables, Map<String, String> inputMap)
			throws SQLException {

		Set<String> currentKeys = inputMap.keySet();

		for (Iterator<String> iterator = currentKeys.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if (key.equalsIgnoreCase(STATUS)
					|| key.equalsIgnoreCase(CONTACT_DATA)	
					|| key.equalsIgnoreCase(TOKEN)
					|| key.equalsIgnoreCase(ACCEPTANCE_ID) ) {
				statement.setString(amountOfVariables + 1, inputMap.get(key));
			} else if (key.equalsIgnoreCase(DONOR_ID) || key.equalsIgnoreCase(DONATION_ID) 
					|| key.equalsIgnoreCase(RECIPIENT_ID)
					|| key.equalsIgnoreCase(VERSION)) {
				statement.setInt(amountOfVariables + 1, Integer.parseInt(inputMap.get(key)));
			} else if (key.equalsIgnoreCase(TIME) ) {
				statement.setTime(amountOfVariables + 1, Time.valueOf(inputMap.get(key)));
			} else if (key.equalsIgnoreCase(DATE) ) {
				statement.setDate(amountOfVariables + 1, Date.valueOf(inputMap.get(key)));
			}	else if (key.equalsIgnoreCase(CREATED)) {
			    statement.setTimestamp(amountOfVariables + 1, Timestamp.valueOf(inputMap.get(key)));
			}
			//created date 
			
			amountOfVariables = amountOfVariables + 1;
		}
		LOG.info("Current statement " + statement.toString());
		LOG.info("Current index value: " + amountOfVariables);

		return amountOfVariables;
	}

	
}
