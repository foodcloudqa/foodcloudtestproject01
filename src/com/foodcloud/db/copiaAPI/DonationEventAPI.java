package com.foodcloud.db.copiaAPI;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class representing Schedule table in Copia Database Currently pointing to
 * Copia-c
 * 
 * @author Viviana
 *
 */
public class DonationEventAPI {

	public static String ID = "id";
	public static String DONATION_ID = "donation_id";
	public static String AGENT_ID = "agent_id";

	public static String TYPE = "type";
	public static String DATA = "data";
	public static String TIMESTAMP = "timestamp";

	static final Logger LOG = LogManager.getLogger(DonationEventAPI.class.getName());

	// public void createTable(Connection con, Map<String, String> scheduleData)
	// throws SQLException {}

	/**
	 * creates a new Donation event row in the db, by insert the data available
	 * 
	 * @throws SQLException
	 */
	public static int insert(Connection con, Map<String, String> eventData) throws SQLException {

		// need control on data : ORG_ID most important!!
		if (!eventData.containsKey(DONATION_ID) || !eventData.containsKey(TIMESTAMP)) {
			LOG.error("DonationEventAPI: Error!  Missing mandatory data : donation id or timestamp ");
			return -1;
		} else if (Integer.parseInt(eventData.get(DONATION_ID)) == 0) {
			LOG.error("DonationEventAPI: Error! Donation id 0");
			return -1;
		}

		Set<String> fieldKeys = eventData.keySet();

		String fieldClause = ShortcutsAPI.modifyInsertSelectClause(fieldKeys);
		String valuesClause = ShortcutsAPI.modifyInsertValuesClause(fieldKeys);
				
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("INSERT INTO public.donation_event( ");
		queryBuffer.append(fieldClause + " )");
		queryBuffer.append("VALUES ( " + valuesClause);
		queryBuffer.append(" );");

		PreparedStatement insertStatement = con.prepareStatement(queryBuffer.toString());

		int i = 0;
		i = modifyStatement(insertStatement, i, eventData);

		LOG.info("DonationEventAPI:  Before executing query: " + insertStatement.toString());
		int result = insertStatement.executeUpdate();

		if (result == 0) {
			LOG.error("DonationEventAPI:  new element not inserted!");
		}
		return result;
	}

	
	/**
	 * Delete Donation event row with criterion
	 */
	public static int delete(Connection con, Map<String, String> criterion) throws SQLException {

		if (!criterion.containsKey(DONATION_ID) || !criterion.containsKey(ID) ) {
			LOG.error("DonationEventAPI: Error!  Missing mandatory data: id or donation id");
			return -1;
		}

		Set<String> deleteKeys = criterion.keySet();
		String whereClause = ShortcutsAPI.modifyCriteriaClause(deleteKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("DELETE FROM public.donation_event WHERE ");
		queryBuffer.append(whereClause + " ;");

		PreparedStatement deleteStatement = con.prepareStatement(queryBuffer.toString());

		int delIndex = 0;
		delIndex = modifyStatement(deleteStatement, delIndex, criterion);

		LOG.info("DonationEventAPI:  Before executing query: " + deleteStatement.toString());
		int deletedRows = deleteStatement.executeUpdate();

		if (deletedRows == 0) {
			LOG.error("DonationEventAPI: element not deleted!");
		}
		return deletedRows;
	}

	/**
	 * Updates schedule item in the DB given the criterion and updated values as
	 * maps
	 * 
	 * @param con
	 * @param criteriaValues
	 * @param updatedValues
	 * @return
	 * @throws SQLException
	 */
	public static int update(Connection con, Map<String, String> criteriaValues, Map<String, String> updatedValues)
			throws SQLException {

		if (criteriaValues.isEmpty() || updatedValues.isEmpty()) {
			LOG.error("DonationEventAPI: Error! Update values and criteria must be not null ");
			return -1;
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		Set<String> setKeys = updatedValues.keySet();

		String updateClause = ShortcutsAPI.modifyUpdateClause(setKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("UPDATE public.donation_event ");
		queryBuffer.append("SET " + updateClause + " ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());

		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, updatedValues);

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("DonationEventAPI:  Before executing query: " + statement.toString());
		int deletedRows = statement.executeUpdate();

		if (deletedRows == 0) {
			LOG.error("DonationEventAPI: element not updated!");
		}
		return deletedRows;
	}

	/**
	 * Updates schedule item in the DB given the criterion and updated values as
	 * maps
	 * 
	 * @param con
	 * @param criteriaValues
	 * @param updatedValues
	 * @return
	 * @throws SQLException
	 */
	public static Map<String, String> getField(Connection con, Set<String> fieldNeeded,
			Map<String, String> criteriaValues) throws SQLException {

		if (criteriaValues.isEmpty() || fieldNeeded.isEmpty()) {
			LOG.error("DonationEventAPI: Error! field values and criteria must be not null ");
			return (Map<String, String>) new HashMap().put("Error", "Null values");
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		String selectClause = ShortcutsAPI.modifyInsertSelectClause(fieldNeeded);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("SELECT " + selectClause + " ");
		queryBuffer.append("FROM public.donation_event ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());
		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("DonationEventAPI:  Before executing query: " + statement.toString());
		ResultSet rs = statement.executeQuery();

		Map<String, String> resultMap = new HashMap<String, String>();

		while (rs.next()) {
			if (fieldNeeded.contains(DonationEventAPI.ID)) {
				int idFound = rs.getInt(DonationEventAPI.ID);
				resultMap.put(DonationEventAPI.ID, Integer.toString(idFound));
			}			
			if (fieldNeeded.contains(DonationEventAPI.DONATION_ID)) {
				int donationIdFound = rs.getInt(DonationEventAPI.DONATION_ID);
				resultMap.put(DonationEventAPI.DONATION_ID, Integer.toString(donationIdFound));
			}
			if (fieldNeeded.contains(DonationEventAPI.AGENT_ID)) {
				int agentIdFound = rs.getInt(DonationEventAPI.AGENT_ID);
				resultMap.put(DonationEventAPI.AGENT_ID, Integer.toString(agentIdFound));
			}
			
			if (fieldNeeded.contains(DonationEventAPI.TYPE)) {
				String typeFound = rs.getString(DonationEventAPI.TYPE);
				resultMap.put(DonationEventAPI.TYPE, typeFound);
			}

			if (fieldNeeded.contains(DonationEventAPI.DATA)) {
				String dataFound = rs.getString(DonationEventAPI.DATA);
				resultMap.put(DonationEventAPI.DATA, dataFound);
			}


			if (fieldNeeded.contains(DonationEventAPI.TIMESTAMP)) {
				Timestamp timeFound = rs.getTimestamp(DonationEventAPI.TIMESTAMP);
				resultMap.put(DonationEventAPI.TIMESTAMP, timeFound.toString());
			}
		}

		return resultMap;
	}

	private static int modifyStatement(PreparedStatement statement, int amountOfVariables, Map<String, String> inputMap)
			throws SQLException {

		Set<String> currentKeys = inputMap.keySet();

		for (Iterator<String> iterator = currentKeys.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if (key.equalsIgnoreCase(DATA) || key.equalsIgnoreCase(TYPE)) {
				statement.setString(amountOfVariables + 1, inputMap.get(key));
			} else if (key.equalsIgnoreCase(ID) || key.equalsIgnoreCase(DONATION_ID) 
					|| key.equalsIgnoreCase(AGENT_ID)) {
				statement.setInt(amountOfVariables + 1, Integer.parseInt(inputMap.get(key)));
			} else if (key.equalsIgnoreCase(TIMESTAMP)) {
				statement.setTimestamp(amountOfVariables + 1, Timestamp.valueOf(inputMap.get(key)));
			}
			amountOfVariables = amountOfVariables + 1;
		}
		LOG.info("Current statement " + statement.toString());
		LOG.info("Current index value: " + amountOfVariables);

		return amountOfVariables;
	}


}
