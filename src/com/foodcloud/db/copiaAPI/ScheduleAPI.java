package com.foodcloud.db.copiaAPI;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class representing Schedule table in Copia Database Currently pointing to
 * Copia-c
 * 
 * @author Viviana
 *
 */
public class ScheduleAPI {

	public static String ORG_ID = "org_id";
	public static String PARTY_ID = "party_id";

	public static String DAY = "day";
	public static String START_TIME = "start_time";
	public static String END_TIME = "end_time";
	public static String CANCEL_AFTER = "cancel_after";

	public static String STATUS = "status";
	public static String VERSION = "version";

	static final Logger LOG = LogManager.getLogger(ScheduleAPI.class.getName());

	// public void createTable(Connection con, Map<String, String> scheduleData)
	// throws SQLException {}

	/**
	 * creates a new Schedule row in the db, by insert the data available
	 * 
	 * @throws SQLException
	 */
	public static int insert(Connection con, Map<String, String> scheduleData) throws SQLException {

		if (!scheduleData.containsKey(DAY) || !scheduleData.containsKey(ORG_ID)) {
			LOG.error("ScheduleAPI: Error!  Missing mandatory data : day and org_id ");
			return -1;
		} else if (Integer.parseInt(scheduleData.get(ORG_ID)) == 0) {
			LOG.error("ScheduleAPI: Error! Party / Org_id 0");
			return -1;
		}

		Set<String> fieldKeys = scheduleData.keySet();

		String fieldClause = ShortcutsAPI.modifyInsertSelectClause(fieldKeys);
		String valuesClause = ShortcutsAPI.modifyInsertValuesClause(fieldKeys);
				
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("INSERT INTO public.schedule( ");
		queryBuffer.append(fieldClause + " )");
		queryBuffer.append("VALUES ( " + valuesClause);
		queryBuffer.append(" );");

		PreparedStatement insertStatement = con.prepareStatement(queryBuffer.toString());

		int i = 0;
		i = modifyStatement(insertStatement, i, scheduleData);

		LOG.info("ScheduleAPI:  Before executing query: " + insertStatement.toString());
		int result = insertStatement.executeUpdate();

		if (result == 0) {
			LOG.error("ScheduleAPI:  new element not inserted!");
		}
		return result;
	}

	// TODO: Refactor:
	public static String getScheduleStatus(Connection con, String donorId, String dayOfWeek) throws SQLException {

		String query = "SELECT status FROM public.schedule WHERE org_id='" + donorId + "' AND day='" + dayOfWeek + "';";

		System.out.println("ScheduleAPI: Before executing query: " + query);

		ResultSet re = con.createStatement().executeQuery(query);

		String foundStatus = "status";
		while (re.next()) {
			foundStatus = re.getString("status");
		}

		return foundStatus;
	}

	public static String getScheduledCharity(Connection con, int donorId, int dayOfWeek) throws SQLException {
		String query = "SELECT party_id FROM public.schedule WHERE org_id='" + donorId + "' AND day='" + dayOfWeek
				+ "';";

		System.out.println("Before executing query: " + query);

		ResultSet re = con.createStatement().executeQuery(query);

		String foundCharity = "charity";
		while (re.next()) {
			foundCharity = re.getString("party_id");
		}

		return foundCharity;
	}

	public static String getStartTime(Connection con, int donorId, int dayOfWeek) throws SQLException {
		String query = "SELECT start_time FROM public.schedule WHERE org_id='" + donorId + "' AND day='" + dayOfWeek
				+ "';";

		System.out.println("Before executing query: " + query);

		ResultSet re = con.createStatement().executeQuery(query);

		String foundStartTime = "time";
		while (re.next()) {
			foundStartTime = re.getString("start_time");
		}

		return foundStartTime;
	}

	public static String getEndTime(Connection con, int donorId, int dayOfWeek) throws SQLException {
		String query = "SELECT end_time FROM public.schedule WHERE org_id='" + donorId + "' AND day='" + dayOfWeek
				+ "';";

		System.out.println(
				"ScheduleAPI:                                                                                                                   Before executing query: "
						+ query);

		ResultSet re = con.createStatement().executeQuery(query);

		String foundEndTime = "time";
		while (re.next()) {
			foundEndTime = re.getString("end_time");
		}

		return foundEndTime;
	}

	public static ResultSet todaysScheduledOrganizations(Connection con, int day) throws SQLException {

		// String queryLeftJoin = "SELECT org_id, party_id FROM schedule WHERE
		// status='Assigned' and day='" + day + "' ORDER BY org_id;";
		String queryLeftJoin = "SELECT S.org_id, S.party_id,  D.org_id "
				+ "FROM Schedule S	 LEFT JOIN donor D ON S.org_id = D.org_id "
				+ " WHERE S.status=\'Assigned\' and S.day='" + day + "' " + " ORDER BY S.org_id;";
		// create donation with UUID (donor and charity already present in the
		// system??
		// create alerts
		ResultSet re = con.createStatement().executeQuery(queryLeftJoin);

		return re;

	}

	public static ResultSet todaysUnScheduledOrganizations(Connection con, int day) throws SQLException {

		// String query = "SELECT org_id, party_id FROM schedule WHERE party_id
		// IS NULL and day='" + day + "' and status='Available' ORDER BY
		// org_id;";
		//
		String queryLeftJoin = "SELECT S.org_id, S.party_id,  D.org_id "
				+ "FROM Schedule S	 LEFT JOIN donor D ON S.org_id = D.org_id "
				+ " WHERE S.party_id IS NULL and S.status=\'Available\' and S.day='" + day + "' "
				+ " ORDER BY S.org_id;";

		// create donation with UUID (donor and charity already present in the
		// system??
		// create alerts
		ResultSet re = con.createStatement().executeQuery(queryLeftJoin);

		return re;
	}

	/**
	 * Delete schedule row with criterion
	 */
	public static int delete(Connection con, Map<String, String> criterion) throws SQLException {

		if (!criterion.containsKey(ORG_ID)) {
			LOG.error("ScheduleAPI: Error!  Missing mandatory data: org_id ");
			return -1;
		}

		Set<String> deleteKeys = criterion.keySet();
		String whereClause = ShortcutsAPI.modifyCriteriaClause(deleteKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("DELETE FROM public.schedule WHERE ");
		queryBuffer.append(whereClause + " ;");

		PreparedStatement deleteStatement = con.prepareStatement(queryBuffer.toString());

		int delIndex = 0;
		delIndex = modifyStatement(deleteStatement, delIndex, criterion);

		LOG.info("ScheduleAPI:  Before executing query: " + deleteStatement.toString());
		int deletedRows = deleteStatement.executeUpdate();

		if (deletedRows == 0) {
			LOG.error("ScheduleAPI: element not deleted!");
		}
		return deletedRows;
	}

	/**
	 * Updates schedule item in the DB given the criterion and updated values as
	 * maps
	 * 
	 * @param con
	 * @param criteriaValues
	 * @param updatedValues
	 * @return
	 * @throws SQLException
	 */
	public static int update(Connection con, Map<String, String> criteriaValues, Map<String, String> updatedValues)
			throws SQLException {

		if (criteriaValues.isEmpty() || updatedValues.isEmpty()) {
			LOG.error("ScheduleAPI: Error! Update values and criteria must be not null ");
			return -1;
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		Set<String> setKeys = updatedValues.keySet();

		String updateClause = ShortcutsAPI.modifyUpdateClause(setKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("UPDATE public.schedule ");
		queryBuffer.append("SET " + updateClause + " ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());

		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, updatedValues);

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("ScheduleAPI:  Before executing query: " + statement.toString());
		int updatedRows = statement.executeUpdate();

		if (updatedRows == 0) {
			LOG.error("ScheduleAPI: element not updated!");
		}
		return updatedRows;
	}

	/**
	 * Updates schedule item in the DB given the criterion and updated values as
	 * maps
	 * 
	 * @param con
	 * @param criteriaValues
	 * @param updatedValues
	 * @return
	 * @throws SQLException
	 */
	public static Map<String, String> getField(Connection con, Set<String> fieldNeeded,
			Map<String, String> criteriaValues) throws SQLException {

		if (criteriaValues.isEmpty() || fieldNeeded.isEmpty()) {
			LOG.error("ScheduleAPI: Error! field values and criteria must be not null ");
			return (Map<String, String>) new HashMap().put("Error", "Null values");
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		String selectClause = ShortcutsAPI.modifyInsertSelectClause(fieldNeeded);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("SELECT " + selectClause + " ");
		queryBuffer.append("FROM public.schedule ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());
		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("ScheduleAPI:  Before executing query: " + statement.toString());
		ResultSet rs = statement.executeQuery();

		Map<String, String> resultMap = new HashMap<String, String>();

		while (rs.next()) {
			if (fieldNeeded.contains(ScheduleAPI.ORG_ID)) {
				int orgIdFound = rs.getInt(ScheduleAPI.ORG_ID);
				resultMap.put(ScheduleAPI.ORG_ID, Integer.toString(orgIdFound));
			}
			if (fieldNeeded.contains(ScheduleAPI.PARTY_ID)) {
				int partyIdFound = rs.getInt(ScheduleAPI.PARTY_ID);
				resultMap.put(ScheduleAPI.PARTY_ID, Integer.toString(partyIdFound));
			}

			if (fieldNeeded.contains(ScheduleAPI.DAY)) {
				int dayFound = rs.getInt(ScheduleAPI.DAY);
				resultMap.put(ScheduleAPI.DAY, Integer.toString(dayFound));
			}

			if (fieldNeeded.contains(ScheduleAPI.START_TIME)) {
				Time timeFound = rs.getTime(ScheduleAPI.START_TIME);
				resultMap.put(ScheduleAPI.START_TIME, timeFound.toString());
			}

			if (fieldNeeded.contains(ScheduleAPI.END_TIME)) {
				Time endTimeFound = rs.getTime(ScheduleAPI.END_TIME);
				resultMap.put(ScheduleAPI.END_TIME, endTimeFound.toString());
			}

			if (fieldNeeded.contains(ScheduleAPI.CANCEL_AFTER)) {
				Time cancelAfterFound = rs.getTime(ScheduleAPI.CANCEL_AFTER);
				resultMap.put(ScheduleAPI.CANCEL_AFTER, cancelAfterFound.toString());
			}

			if (fieldNeeded.contains(ScheduleAPI.VERSION)) {
				int versionFound = rs.getInt(ScheduleAPI.VERSION);
				resultMap.put(ScheduleAPI.VERSION, Integer.toString(versionFound));
			}

			if (fieldNeeded.contains(ScheduleAPI.STATUS)) {
				String statusFound = rs.getString(ScheduleAPI.STATUS);
				resultMap.put(ScheduleAPI.STATUS, statusFound);
			}

		}

		return resultMap;
	}

	private static int modifyStatement(PreparedStatement statement, int amountOfVariables, Map<String, String> inputMap)
			throws SQLException {

		Set<String> currentKeys = inputMap.keySet();

		for (Iterator<String> iterator = currentKeys.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if (key.equalsIgnoreCase(STATUS)) {
				statement.setString(amountOfVariables + 1, inputMap.get(key));
			} else if (key.equalsIgnoreCase(ORG_ID) || key.equalsIgnoreCase(PARTY_ID) || key.equalsIgnoreCase(DAY)
					|| key.equalsIgnoreCase(VERSION)) {
				statement.setInt(amountOfVariables + 1, Integer.parseInt(inputMap.get(key)));
			} else if (key.equalsIgnoreCase(START_TIME) || key.equalsIgnoreCase(END_TIME)
					|| key.equalsIgnoreCase(CANCEL_AFTER)) {
				statement.setTime(amountOfVariables + 1, Time.valueOf(inputMap.get(key)));
			}
			amountOfVariables = amountOfVariables + 1;
		}
		LOG.info("Current statement " + statement.toString());
		LOG.info("Current index value: " + amountOfVariables);

		return amountOfVariables;
	}


}
