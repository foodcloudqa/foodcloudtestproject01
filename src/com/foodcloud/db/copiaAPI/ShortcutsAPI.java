package com.foodcloud.db.copiaAPI;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.foodcloud.server.FCTestUtils;

public class ShortcutsAPI {

	public static String ORG_NAME = "orgName"; 
	public static String GROUP_NAME = "groupName"; 
	public static String ORG_TYPE = "orgType"; 
	public static String REGION = "region"; 

	public static String ORG_TYPE_DONOR = "donor"; 
	public static String ORG_TYPE_CHARITY = "charity"; 

	
	public static String DONOR_NAME = "donorName";
	public static String COPIA_DAY = "copiaDay";
	public static String DONOR_CODE = "donorCode";
	
	public static final String DONORGROUP_NAME = "donorGroupName";
	public static final String DONOR_TYPE = "donorType";
	public static final String DONOR_TIMEZONE = "donorTimezone";
	public static String CHARITY_NAME = "charityName";
	public static final String DONATION_STATUS = "donorTimezone";
	public static final String DONATION_EXT_ID = "donationExtId";
	public static final String EXPECTED_TIME = "expectedTime";
	public static final String EXPECTED_DATE = "expectedDate";
	public static final String COLLECTION_WINDOW = "collectionWindow";
	
	static final Logger LOG = LogManager.getLogger(ShortcutsAPI.class.getName());

	
	public static int createOrganization(Connection connection, Map<String, String> inputData) {
		if (!inputData.containsKey(ORG_NAME) || !inputData.containsKey(ORG_TYPE)) {
			LOG.error("ShortcutsAPI: Error!  Missing mandatory data : org name and org type ");
			return -1;
		} else {
			if (!inputData.containsKey(REGION) || !inputData.containsKey(GROUP_NAME)) {
				// group name needed for creating groupOrg  AND associating food_categories 
				LOG.error("ShortcutsAPI: Error!  Missing mandatory data : group name and region ");
				return -1;
			}
//			if (inputData.get(ORG_TYPE).equalsIgnoreCase(ORG_TYPE_CHARITY) && !inputData.containsKey(REG_ID)) {
//				LOG.error("ShortcutsAPI: Error!  Missing mandatory data for Charity: registration id ");
//				return -1;
//			}
		}

		
		int organizationId = 0;
		String orgName =  inputData.get(ORG_NAME); 
		String region = inputData.get(REGION); 
		String orgType =  inputData.get(ORG_TYPE); 
		
		
		try {
			organizationId = OrganizationAPI.getId(connection, orgName);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}

		if (organizationId == 0 || organizationId == -1) {
			int regionCode = 0;
			try {
				regionCode = RegionAPI.getId(connection, region);
			} catch (SQLException regErr) {
				LOG.error(regErr.getLocalizedMessage());
			}

			int group_id = createGroup(connection, inputData);

			// Organization data
			Map<String, String> organizationData = new HashMap<String, String>();

			if (orgType.equalsIgnoreCase(ORG_TYPE_CHARITY)) {

				organizationData.put(OrganizationAPI.PARENT_ID, Integer.toString(group_id));
				organizationData.put(OrganizationAPI.NAME, orgName);
				organizationData.put(OrganizationAPI.REGION_ID, Integer.toString(regionCode));
				organizationData.put(OrganizationAPI.SECTOR, "Charity");
				organizationData.put(OrganizationAPI.REG_TYPE, "CHY");
				organizationData.put(OrganizationAPI.REG_ID, "1223777");
				organizationData.put(OrganizationAPI.TYPE, "C");
				organizationData.put(OrganizationAPI.CREATED, ConnectionToDb.formatTimestamp());
				organizationData.put(OrganizationAPI.STATUS, "Active");
				organizationData.put(OrganizationAPI.RATING, "0");
				organizationData.put(OrganizationAPI.SUPPORT, "true");
				organizationData.put(OrganizationAPI.VERSION, "0");

			} else {

				organizationData.put(OrganizationAPI.PARENT_ID, Integer.toString(group_id));
				organizationData.put(OrganizationAPI.NAME, orgName);
				organizationData.put(OrganizationAPI.REGION_ID, Integer.toString(regionCode));
				organizationData.put(OrganizationAPI.SECTOR, "Retail");		
				organizationData.put(OrganizationAPI.REG_ID, " ");
				organizationData.put(OrganizationAPI.TYPE, "DS");
				organizationData.put(OrganizationAPI.CREATED, ConnectionToDb.formatTimestamp());
				organizationData.put(OrganizationAPI.STATUS, "Active");
				organizationData.put(OrganizationAPI.RATING, "0");
				organizationData.put(OrganizationAPI.SUPPORT, "true");
				organizationData.put(OrganizationAPI.VERSION, "0");
			}

			int insertAmount = 0;
			try {
				insertAmount  = OrganizationAPI.insert(connection, organizationData);
			} catch (SQLException e) {
				LOG.error(e.getLocalizedMessage());
			}

			if(	insertAmount  != 0 ) {
				Set<String> fieldList = new HashSet<String>();
				fieldList.add(OrganizationAPI.ID);				
				// Act		
				Map<String,String> results = null;
				try {
					results = OrganizationAPI.getField(connection,fieldList, organizationData);
				} catch (SQLException e) {
					LOG.error("Select: " + e.getLocalizedMessage());
				}
				organizationId = Integer.parseInt(results.get(OrganizationAPI.ID));
			}
		}
		return organizationId;
	}
	
	
	public static int createGroup(Connection dbConn, Map<String, String> inputData) {
		int group_id = 0;
		String region = inputData.get(REGION); 
		String groupname =  inputData.get(GROUP_NAME); 
		String orgType =  inputData.get(ORG_TYPE); 
		
		int regionCode = 0;
		try {
			regionCode = RegionAPI.getId(dbConn, region);
		} catch (SQLException regErr) {
			LOG.error(regErr.getLocalizedMessage());
		}
				
		try {
			group_id = OrganizationAPI.getId(dbConn, groupname);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());
		}

		if (group_id == 0 || group_id == -1) {
			Map<String, String> groupData = new HashMap<String, String>();

			if (orgType.equalsIgnoreCase(ORG_TYPE_CHARITY)) {

				groupData.put(OrganizationAPI.PARENT_ID, "1");
				groupData.put(OrganizationAPI.NAME, groupname);
				groupData.put(OrganizationAPI.REGION_ID, Integer.toString(regionCode));
				groupData.put(OrganizationAPI.SECTOR, "Charity");
				groupData.put(OrganizationAPI.REG_TYPE, "None");
				groupData.put(OrganizationAPI.TYPE, "CG");
				groupData.put(OrganizationAPI.CREATED, ConnectionToDb.formatTimestamp());
				groupData.put(OrganizationAPI.STATUS, "Active");
				groupData.put(OrganizationAPI.RATING, "0");
				groupData.put(OrganizationAPI.SUPPORT, "false");
				groupData.put(OrganizationAPI.VERSION, "0");

			} else {

				groupData.put(OrganizationAPI.PARENT_ID, "1");
				groupData.put(OrganizationAPI.NAME, groupname);
				groupData.put(OrganizationAPI.REGION_ID, Integer.toString(regionCode));
				groupData.put(OrganizationAPI.SECTOR, "Retail");
				groupData.put(OrganizationAPI.REG_TYPE, "None");
				groupData.put(OrganizationAPI.REG_ID, " ");
				groupData.put(OrganizationAPI.TYPE, "DG");
				groupData.put(OrganizationAPI.CREATED, ConnectionToDb.formatTimestamp());
				groupData.put(OrganizationAPI.STATUS, "Active");
				groupData.put(OrganizationAPI.RATING, "0");
				groupData.put(OrganizationAPI.SUPPORT, "false");
				groupData.put(OrganizationAPI.VERSION, "0");
			}
			
			int insertAmount = 0;
			try {
				insertAmount  = OrganizationAPI.insert(dbConn, groupData);
			} catch (SQLException e) {
				LOG.error(e.getLocalizedMessage());
			}

			if(	insertAmount  != 0 ) {
				Set<String> fieldList = new HashSet<String>();
				fieldList.add(OrganizationAPI.ID);				
				// Act		
				Map<String,String> results = null;
				try {
					results = OrganizationAPI.getField(dbConn,fieldList, groupData);
				} catch (SQLException e) {
					LOG.error("Select: " + e.getLocalizedMessage());
				}
				group_id = Integer.parseInt(results.get(OrganizationAPI.ID));
			}

		}
		return group_id;
	}

	
	
	public static String modifyCriteriaClause(Set<String> keys) {

		String sqlClause = "";

		int criterionAmount = keys.size();
		for (String whereElement : keys) {
			sqlClause = sqlClause + whereElement + " =? ";
			if (criterionAmount > 1) {
				sqlClause = sqlClause + " AND ";
			}
			criterionAmount = criterionAmount - 1;
		}
		return sqlClause;

	}

	public static String modifyUpdateClause(Set<String> keys) {

		String clause = "";
		int variablesAmount = keys.size();
		for (String setElement : keys) {
			clause = clause + setElement + " =? ";
			if (variablesAmount > 1) {
				clause = clause + ", ";
			}
			variablesAmount = variablesAmount - 1;
		}
		return clause;
	}


	public static String modifyInsertSelectClause(Set<String> fields) {

		String insertField ="";
		int criterionAmount = fields.size();
		for (String restrictionElement : fields) {
			insertField  = insertField  + restrictionElement;
			if (criterionAmount > 1) {
				insertField  = insertField  + " , ";
			}
			criterionAmount = criterionAmount - 1;
		}
		return insertField ;
	}


	public static String modifyInsertValuesClause(Set<String> fields) {

		String insertValues ="";
		int criterionAmount = fields.size();
		for (String restrictionElement : fields) {
			insertValues = insertValues + " ? ";
			if (criterionAmount > 1) {
				insertValues = insertValues + " , ";
			}
			criterionAmount = criterionAmount - 1;
		}
		return insertValues ;
	}


	//private String[] createExpectedResults(String donorName, int copiaDayInt, Map<String, String> data) {
	
	public static String[] createExpectedResults(Map<String, String> data) throws SQLException {
		
		if (!data.containsKey(DONOR_NAME) || !data.containsKey(COPIA_DAY)) {
			LOG.error("ScheduleAPI: Error!  Missing mandatory data : day and donorName ");
			return null;
		}
		
		Connection con = null;
		con = ConnectionToDb.getCopiaConnection();
		
		//needed at the end
		Map<String, String> outputData = new HashMap<String, String>();

		//creation Data		
		String charityName ="";
		String startTime = "";
		String endTime = "";
		String donorCode = "";
		String expectedStatus = "";
		String donorTimezone = "";
		Map<String, String> donationData = new HashMap<String, String>();

		Map<String, String> donorData = new HashMap<String, String>();
		donorData.put(ShortcutsAPI.ORG_NAME, data.get(DONOR_NAME));
		donorData.put(ShortcutsAPI.GROUP_NAME,  data.get(DONORGROUP_NAME));
		donorData.put(ShortcutsAPI.REGION, data.get(REGION));
		donorData.put(ShortcutsAPI.ORG_TYPE, data.get(DONOR_TYPE));             		
		
		int donorId = ShortcutsAPI.createOrganization(con, donorData);
		donationData.put(DonationAPI.DONOR_ID, Integer.toString(donorId));
		try {
			donorCode = DonorAPI.getExternalId(con, donorId);
		} catch (SQLException donorErr1) {
			LOG.error("ShortcutsAPI : Error getting donor code");
			LOG.error(donorErr1.getLocalizedMessage());
		}

		if(!data.containsKey(DONOR_TIMEZONE)) {
			try {
				donorTimezone = LocationAPI.getTimezone(con, donorId);
			}
			catch (SQLException donorTzn1) {
				LOG.error("ShortcutsAPI : Error getting donor timezone");
				LOG.error(donorTzn1.getLocalizedMessage());
			}
		} else {
			donorTimezone = data.get(DONOR_TIMEZONE);
		}
		
			String charityId = null;
			try {
				charityId = ScheduleAPI.getScheduledCharity(con, donorId, Integer.parseInt(data.get(COPIA_DAY)));
			} catch (NumberFormatException | SQLException skedErr) {
				LOG.error("ShortcutsAPI : Error getting schedule");
				LOG.error( skedErr.getLocalizedMessage());
			} 
			if(charityId != null) {
				charityName = OrganizationAPI.getOrgName(con, charityId); 

				expectedStatus = "Collected";
				donationData.put(DonationAPI.STATUS, expectedStatus);
				donationData.put(DonationAPI.COMMENTS, charityId);
			} else {
				expectedStatus = "Available";
				//in case the id is null means that No charity assigned
				charityName = " ";				
			}
			
			int copiaDayInt = Integer.parseInt(data.get(COPIA_DAY));
			try {
				startTime= ScheduleAPI.getStartTime(con, donorId, copiaDayInt);
				endTime= ScheduleAPI.getEndTime(con, donorId, copiaDayInt); 

			} catch (SQLException e2) {
					LOG.error(e2.getLocalizedMessage());
			} 
			
			System.out.println("CharityName: "+ charityName );
			System.out.println("startTime: "+  startTime );
			System.out.println("endTime: "+  endTime );
		
		String donationExternalId = data.get(DONATION_EXT_ID);
		OffsetDateTime utcDate = FCTestUtils.getUTCDate();
		
		donationData.put(DonationAPI.TIME, ConnectionToDb.formatTime(utcDate)); //"20:00:00");
		donationData.put(DonationAPI.DATE, ConnectionToDb.formatDate(utcDate));
		donationData.put(DonationAPI.EXTERNAL_ID, donationExternalId);
		donationData.put(DonationAPI.DESCRIPTION, "");

		try {
			int result = DonationAPI.insert(con, donationData);
		} catch (SQLException e1) {
			LOG.error(e1.getLocalizedMessage());

		}
		
		String startTimeforUI = FCTestUtils.CopiaUITime_Substring(startTime);
		String endTimeforUI = FCTestUtils.CopiaUITime_Substring(endTime);
		String collectionWindow = startTimeforUI+ " to " + endTimeforUI; 		
		

		//expected time needs to be converted in the user timezone
		ZonedDateTime donorZonedDate = FCTestUtils.convertToTimeZonedDate(utcDate, FCTestUtils.getZoneId(donorTimezone));

		System.out.println("date Timezone auto Formatted");		
		String expectedDate = FCTestUtils.formatDateToCopiaUI(donorZonedDate, FCTestUtils.TYPE.DATE  );
		String expectedTime = FCTestUtils.formatDateToCopiaUI(donorZonedDate, FCTestUtils.TYPE.TIME  );
		
		System.out.println("timezonedDate: "+ expectedDate );		
		System.out.println("timezonedTime: "+ expectedTime);

		
		outputData.put(EXPECTED_DATE, expectedDate);
		outputData.put(EXPECTED_TIME, expectedTime);
		outputData.put(DONOR_NAME, data.get(DONOR_NAME));
		outputData.put(DONOR_CODE, donorCode);
		outputData.put(DONOR_TIMEZONE, donorTimezone);
		outputData.put(CHARITY_NAME, charityName);
		outputData.put(COLLECTION_WINDOW, collectionWindow);
		outputData.put(DONATION_STATUS, expectedStatus);
		outputData.put(DONATION_EXT_ID, donationExternalId );

		
	//	String[] expectedResults = {expectedDate, expectedTime, data.get(DONOR_NAME), donorCode,
	//			charityName,collectionWindow,expectedStatus, donationExternalId}; 

		String[] expectedResults = {outputData.get(EXPECTED_DATE), outputData.get(EXPECTED_TIME), outputData.get(DONOR_NAME),
				outputData.get(DONOR_CODE),outputData.get(CHARITY_NAME),outputData.get(COLLECTION_WINDOW),
				outputData.get(DONATION_STATUS), outputData.get(DONATION_EXT_ID)};
		
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return expectedResults;
	}


	public static String getExpectedCharity(String donorName, int expectedTodayAsInt) {
		 // Arrange
		Connection con = null;
		con = ConnectionToDb.getCopiaConnection();
		
		int donorId = 0;
		try {
			donorId = OrganizationAPI.getId(con,donorName);
		} catch (SQLException e) {
			LOG.error("ShortcutsAPI : Failed getting donor id ");
			LOG.error(e.getLocalizedMessage());
		}
		
		String charityId = null;
		try {
			charityId = ScheduleAPI.getScheduledCharity(con, donorId, expectedTodayAsInt);
		} catch (NumberFormatException | SQLException skedErr) {
			LOG.error("ShortcutsAPI : Error getting schedule");
			LOG.error( skedErr.getLocalizedMessage());
		} 
		
		String charityName = "No charity assigned";
		if(charityId != null) {
			try {
				charityName = OrganizationAPI.getOrgName(con, charityId);
			} catch (SQLException charityError) {
				LOG.error("ShortcutsAPI : Failed getting charity name ");
				LOG.error(charityError.getLocalizedMessage());
			} 
		} 
		return charityName;		
	}



	
	public static int  getLastId(Connection con, String dbTableName, String tableNameId) throws SQLException {

	
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("SELECT "+ tableNameId + " FROM public."+ dbTableName +" order by id desc limit 1;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());

		LOG.info("ShortcutAPI:  Before executing query: " + statement.toString());
		ResultSet rs = statement.executeQuery();

		int lastId = 0 ;
		while ( rs.next() ) { 
			lastId = rs.getInt(tableNameId);
		}						
		return lastId;
	}


}
