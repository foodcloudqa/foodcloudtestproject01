package com.foodcloud.db.copiaAPI;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class representing Donor table in Copia Database 
 * Currently pointing to Copia-c
 * 
 * @author Viviana
 *
 */
public class CharityAPI {

	public static String TABLE_NAME = "charity";

	public static String ID = "id";
	public static String ORG_ID = "org_id";
	public static String RADIUS = "travel_radius"; 
	public static String VEHICLE = "has_vehicle"; 
	public static String ACCEPT_AMBIENT = "accept_ubd"; 
	public static String ACCEPT_CHILLED = "accept_chill"; 
	public static String VERSION = "version"; 
	public static String MULTI_DONATION = "multi_donation"; 
	public static String EXTERNAL_ID = "external_id"; 
	public static String ACCEPT_FROZEN = "accept_frozen"; 
	public static String RULES = "rules";
	
	
	static final Logger LOG = LogManager.getLogger(CharityAPI.class.getName());

	
	/**
	 * Inserts a new charity row in the db, having the organization id available 
	 * @throws SQLException 
	 */		
	public static int insert(Connection con, Map<String, String> charityData) throws SQLException {

		// need control on data : ORG_ID most important!!
		if (!charityData.containsKey(ORG_ID) ) {
			LOG.error("CharityAPI: Error!  Missing mandatory data : org id  ");
			return -1;
		} else if (Integer.parseInt(charityData.get(ORG_ID)) == 0) {
			LOG.error("CharityAPI: Error! Org id 0");
			return -1;
		}

		Set<String> fieldKeys = charityData.keySet();

		String fieldClause = ShortcutsAPI.modifyInsertSelectClause(fieldKeys);
		String valuesClause = ShortcutsAPI.modifyInsertValuesClause(fieldKeys);
				
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("INSERT INTO public.charity( ");
		queryBuffer.append(fieldClause + " )");
		queryBuffer.append("VALUES ( " + valuesClause);
		queryBuffer.append(" );");

		PreparedStatement insertStatement = con.prepareStatement(queryBuffer.toString());

		int i = 0;
		i = modifyStatement(insertStatement, i, charityData);

		LOG.info("CharityAPI:  Before executing query: " + insertStatement.toString());
		int result = insertStatement.executeUpdate();

		if (result == 0) {
			LOG.error("CharityAPI:  new element not inserted!");
		}
		return result;

	}
	
	
	
	/**
	 * Delete charity row with criterion
	 */
	public static int delete(Connection con, Map<String, String> criterion) throws SQLException {

		if (!criterion.containsKey(ORG_ID)) {
			LOG.error("CharityAPI: Error!  Missing mandatory data: org_id ");
			return -1;
		}

		Set<String> deleteKeys = criterion.keySet();
		String whereClause = ShortcutsAPI.modifyCriteriaClause(deleteKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("DELETE FROM public.charity WHERE ");
		queryBuffer.append(whereClause + " ;");

		PreparedStatement deleteStatement = con.prepareStatement(queryBuffer.toString());

		int delIndex = 0;
		delIndex = modifyStatement(deleteStatement, delIndex, criterion);

		LOG.info("CharityAPI:  Before executing query: " + deleteStatement.toString());
		int deletedRows = deleteStatement.executeUpdate();

		if (deletedRows == 0) {
			LOG.error("CharityAPI: element not deleted!");
		}
		return deletedRows;
	}


	/**
	 * Updates charity item in the DB given the criterion and updated values as
	 * maps
	 * 
	 * @param con
	 * @param criteriaValues
	 * @param updatedValues
	 * @return
	 * @throws SQLException
	 */
	public static int update(Connection con, Map<String, String> criteriaValues, Map<String, String> updatedValues)
			throws SQLException {

		if (criteriaValues.isEmpty() || updatedValues.isEmpty()) {
			LOG.error("CharityAPI: Error! Update values and criteria must be not null ");
			return -1;
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		Set<String> setKeys = updatedValues.keySet();

		String updateClause = ShortcutsAPI.modifyUpdateClause(setKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("UPDATE public.charity ");
		queryBuffer.append("SET " + updateClause + " ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());

		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, updatedValues);

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("CharityAPI:  Before executing query: " + statement.toString());
		int deletedRows = statement.executeUpdate();

		if (deletedRows == 0) {
			LOG.error("CharityAPI: element not updated!");
		}
		return deletedRows;
	}
	
	public static Map<String, String> getField(Connection con, Set<String> fieldNeeded,
			Map<String, String> criteriaValues) throws SQLException {

		if (criteriaValues.isEmpty() || fieldNeeded.isEmpty()) {
			LOG.error("CharityAPI: Error! field values and criteria must be not null ");
			Map<String, String> empty = (Map<String, String>) new HashMap().put("Error", "Null values");
			return empty;
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		String selectClause = ShortcutsAPI.modifyInsertSelectClause(fieldNeeded);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("SELECT " + selectClause + " ");
		queryBuffer.append("FROM public.charity ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());
		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("CharityAPI:  Before executing query: " + statement.toString());
		ResultSet rs = statement.executeQuery();

		Map<String, String> resultMap = new HashMap<String, String>();

		while (rs.next()) {
			if (fieldNeeded.contains(CharityAPI.ID)) {
				int idFound = rs.getInt(CharityAPI.ID);
				resultMap.put(CharityAPI.ID, Integer.toString(idFound));
			}			
			if (fieldNeeded.contains(CharityAPI.ORG_ID)) {
				int orgIdFound = rs.getInt(CharityAPI.ORG_ID);
				resultMap.put(CharityAPI.ORG_ID, Integer.toString(orgIdFound));
			}
			if (fieldNeeded.contains(CharityAPI.RADIUS)) {
				int radiusFound = rs.getInt(CharityAPI.RADIUS);
				resultMap.put(CharityAPI.RADIUS, Integer.toString(radiusFound));
			}
			
			if (fieldNeeded.contains(CharityAPI.VEHICLE)) {
				Boolean vehicleFound = rs.getBoolean(CharityAPI.VEHICLE);
				resultMap.put(CharityAPI.VEHICLE, vehicleFound.toString());
			}

			if (fieldNeeded.contains(CharityAPI.ACCEPT_AMBIENT)) {
				Boolean ambientFound = rs.getBoolean(CharityAPI.ACCEPT_AMBIENT);
				resultMap.put(CharityAPI.ACCEPT_AMBIENT, ambientFound.toString());
			}
			
			if (fieldNeeded.contains(CharityAPI.ACCEPT_CHILLED)) {
				Boolean chilledFound = rs.getBoolean(CharityAPI.ACCEPT_CHILLED);
				resultMap.put(CharityAPI.ACCEPT_CHILLED, chilledFound.toString());
			}

			if (fieldNeeded.contains(CharityAPI.VERSION)) {
				int versionFound = rs.getInt(CharityAPI.VERSION);
				resultMap.put(CharityAPI.VERSION, Integer.toString(versionFound));
			}

			if (fieldNeeded.contains(CharityAPI.MULTI_DONATION)) {
				Boolean multiDonation = rs.getBoolean(CharityAPI.MULTI_DONATION);
				resultMap.put(CharityAPI.MULTI_DONATION, multiDonation.toString());
			}

			
			if (fieldNeeded.contains(CharityAPI.EXTERNAL_ID)) {
				String externalIdFound = rs.getString(CharityAPI.EXTERNAL_ID);
				resultMap.put(CharityAPI.EXTERNAL_ID, externalIdFound);
			}
			
			if (fieldNeeded.contains(CharityAPI.ACCEPT_FROZEN)) {
				Boolean frozenFound = rs.getBoolean(CharityAPI.ACCEPT_FROZEN);
				resultMap.put(CharityAPI.ACCEPT_FROZEN, frozenFound.toString());
			}

			if (fieldNeeded.contains(CharityAPI.RULES)) {
				String rulesFound = rs.getString(CharityAPI.RULES);
				resultMap.put(CharityAPI.RULES, rulesFound);
			}
		}

		return resultMap;
	}

	
	private static int modifyStatement(PreparedStatement statement, int amountOfVariables, Map<String, String> inputMap)
			throws SQLException {

		Set<String> currentKeys = inputMap.keySet();

		for (Iterator<String> iterator = currentKeys.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if (key.equalsIgnoreCase(EXTERNAL_ID) || key.equalsIgnoreCase(RULES)) {
				statement.setString(amountOfVariables + 1, inputMap.get(key));
			} else if (key.equalsIgnoreCase(ORG_ID) || key.equalsIgnoreCase(ID) || key.equalsIgnoreCase(RADIUS)  
					|| key.equalsIgnoreCase(VERSION)) {
				statement.setInt(amountOfVariables + 1, Integer.parseInt(inputMap.get(key)));
			} else if (key.equalsIgnoreCase(VEHICLE) || key.equalsIgnoreCase(ACCEPT_AMBIENT) || key.equalsIgnoreCase(ACCEPT_FROZEN)
					|| key.equalsIgnoreCase(ACCEPT_CHILLED) || key.equalsIgnoreCase(MULTI_DONATION)) {
				statement.setBoolean(amountOfVariables + 1, Boolean.valueOf(inputMap.get(key)));
			}
			amountOfVariables = amountOfVariables + 1;
		}
		LOG.info("Current statement " + statement.toString());
		LOG.info("Current index value: " + amountOfVariables);

		return amountOfVariables;
	}



	public static int getId(Connection con, int orgId) throws SQLException {
		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(CharityAPI.ORG_ID, Integer.toString(orgId));
		
		Set<String> fieldList = new HashSet<String>();
		fieldList.add(CharityAPI.ID);
		
		Map<String,String> result = getField(con,fieldList, searchData);
		String  actualCharityId  = result.get(CharityAPI.ID);						
			
		return Integer.parseInt(actualCharityId);		
	}

	public static int getLastId(Connection con) throws SQLException {
		
		return ShortcutsAPI.getLastId(con, TABLE_NAME, ID);		
	}
	
}
