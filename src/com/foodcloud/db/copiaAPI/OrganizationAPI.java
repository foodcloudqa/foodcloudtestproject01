package com.foodcloud.db.copiaAPI;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OrganizationAPI {

	public static final String ID = "id";
	public static final String PARENT_ID = "parent_id";
	public static final String NAME = "name";
	public static final String REGION_ID = "region_id";
	public static final String SECTOR = "sector";
	public static final String REG_TYPE = "reg_type";
	public static final String REG_ID = "reg_id";
	public static final String TYPE = "type";
	public static final String CREATED = "created";
	public static final String STATUS = "status";
	public static final String RATING = "rating";
	public static final String SUPPORT = "support";
	public static final String VERSION = "version";

	// public void create(Connection con, List<String> orgData ){ refers to
	// create Table!!
	 static final Logger LOG = LogManager.getLogger(OrganizationAPI.class.getName());
	
	/**
	 * creates a new organization in the db by INSERTing data provided
	 * 
	 * @throws SQLException
	 */
	public static int insert(Connection con, Map<String, String> organizationData) throws SQLException {
		// need control on data : ORG_ID most important!!
		if (!organizationData.containsKey(NAME) || !organizationData.containsKey(REGION_ID)) {
			LOG.error("OrganizationAPI: Error! Name field missing");
			return -1;
		}

		Set<String> fieldKeys = organizationData.keySet();

		String fieldClause = ShortcutsAPI.modifyInsertSelectClause(fieldKeys);
		String valuesClause = ShortcutsAPI.modifyInsertValuesClause(fieldKeys);
				
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("INSERT INTO public.organization( ");
		queryBuffer.append(fieldClause + " )");
		queryBuffer.append("VALUES ( " + valuesClause);
		queryBuffer.append(" );");

		PreparedStatement insertStatement = con.prepareStatement(queryBuffer.toString());

		int i = 0;
		i = modifyStatement(insertStatement, i, organizationData);

		LOG.info("OrganizationAPI:  Before executing query: " + insertStatement.toString());
		int result = insertStatement.executeUpdate();

		if (result == 0) {
			LOG.error("OrganizationAPI:  new element not inserted!");
		}
		return result;

	}

	/**
	 * returns an organization id if any present in the db with the criterion data provided
	 * 
	 * @param con DB Connection
	 * @param orgNameCriterion String representing organization name
	 * @return int representing the id 
	 * @throws SQLException
	 */
	public static int getId(Connection con, String orgNameCriterion) throws SQLException {

			Map<String, String> searchData = new HashMap<String, String>();
			searchData.put(OrganizationAPI.NAME, orgNameCriterion);
			
			Set<String> fieldList = new HashSet<String>();
			fieldList.add(OrganizationAPI.ID);
			
			Map<String,String> result = getField(con,fieldList, searchData);
			
			int actualOrgId = 0 ;
			if (!result.isEmpty()) {
				String  orgId  = result.get(OrganizationAPI.ID);						
				actualOrgId = Integer.parseInt(orgId);
			}				
			return	actualOrgId;	
	}

	public static String getOrgName(Connection con, String charityOrDonorId) throws SQLException {

//		Map<String, String> searchData = new HashMap<String, String>();
//		searchData.put(OrganizationAPI.NAME, orgNameCriterion);
//		
//		Set<String> fieldList = new HashSet<String>();
//		fieldList.add(OrganizationAPI.ID);
//		
//		Map<String,String> result = getField(con,fieldList, searchData);
//		
//		int actualOrgId = 0 ;
//		if (!result.isEmpty()) {
//			String  orgId  = result.get(OrganizationAPI.ID);						
//			actualOrgId = Integer.parseInt(orgId);
//		}				
//		return	actualOrgId;	

		
		String nameFound = "";

		if (charityOrDonorId != null && !charityOrDonorId.isEmpty()) {
			String query = "SELECT name FROM public.organization WHERE id=" + charityOrDonorId + ";";

			LOG.info("Before executing name query: " + query);

			ResultSet re = con.createStatement().executeQuery(query);

			while (re.next()) {
				nameFound = re.getString("name");
				LOG.info("OrganizationAPI: name " + nameFound);
			}
		}
		return nameFound;

	}

	/**
	 * Deletes a row in the  Organization  table
	 * @param con
	 * @param org_id
	 * @return
	 * @throws SQLException
	 */
	public static int delete(Connection con, int org_id) throws SQLException {
		// need control on data : ORG_ID most important!!

		String deleteQuery = "DELETE FROM public.organization WHERE id = ?";

		PreparedStatement statement = con.prepareStatement(deleteQuery);

		statement.setInt(1, org_id);

		LOG.info("OrganizationAPI:  Before executing query: " + deleteQuery);
		int deletedRow = statement.executeUpdate();

		if (deletedRow == 0) {
			LOG.error("OrganizationAPI:  element not deleted!!");

		}
		return deletedRow;
	}

	// In progress
	//public static String getField(Connection con, String fieldName, Map<String, String> criterion) throws SQLException {

		public static Map<String, String> getField(Connection con, Set<String> fieldNeeded,	Map<String, String> criteriaValues) throws SQLException {

			if (criteriaValues.isEmpty() || fieldNeeded.isEmpty()) {
				LOG.error("OrganizationAPI: Error! field values and criteria must be not null ");
				Map<String, String> empty = (Map<String, String>) new HashMap().put("Error", "Null values");
				return empty;
			}

			Set<String> whereKeys = criteriaValues.keySet();

			String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

			String selectClause = ShortcutsAPI.modifyInsertSelectClause(fieldNeeded);

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT " + selectClause + " ");
			queryBuffer.append("FROM public.organization ");
			queryBuffer.append("WHERE " + criteriaClause);
			queryBuffer.append(" ;");

			PreparedStatement statement = con.prepareStatement(queryBuffer.toString());
			int questionMarkAmount = 0;

			questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

			LOG.info("OrganizationAPI:  Before executing query: " + statement.toString());
			ResultSet rs = statement.executeQuery();

			Map<String, String> resultMap = new HashMap<String, String>();

			while (rs.next()) {
				if (fieldNeeded.contains(OrganizationAPI.ID)) {
					int idFound = rs.getInt(OrganizationAPI.ID);
					resultMap.put(OrganizationAPI.ID, Integer.toString(idFound));
				}			
			
				if (fieldNeeded.contains(OrganizationAPI.REGION_ID)) {
					int parentIdFound = rs.getInt(OrganizationAPI.REGION_ID);
					resultMap.put(OrganizationAPI.REGION_ID, Integer.toString(parentIdFound));
				}
				
				if (fieldNeeded.contains(OrganizationAPI.NAME)) {
					String nameFound = rs.getString(OrganizationAPI.NAME);
					resultMap.put(OrganizationAPI.NAME, nameFound);
				}				

				if (fieldNeeded.contains(OrganizationAPI.PARENT_ID)) {
					int parentIdFound = rs.getInt(OrganizationAPI.PARENT_ID);
					resultMap.put(OrganizationAPI.PARENT_ID, Integer.toString(parentIdFound));
				}
				
				if (fieldNeeded.contains(OrganizationAPI.SECTOR)) {
					String sectorFound = rs.getString(OrganizationAPI.SECTOR);
					resultMap.put(OrganizationAPI.SECTOR, sectorFound);
				}				

				if (fieldNeeded.contains(OrganizationAPI.REG_TYPE)) {
					String regTypeFound = rs.getString(OrganizationAPI.REG_TYPE);
					resultMap.put(OrganizationAPI.REG_TYPE, regTypeFound);
				}				

				if (fieldNeeded.contains(OrganizationAPI.REG_ID)) {
					String regIdFound = rs.getString(OrganizationAPI.REG_ID);
					resultMap.put(OrganizationAPI.REG_ID, regIdFound);
				}				

				if (fieldNeeded.contains(OrganizationAPI.TYPE)) {
					String typeFound = rs.getString(OrganizationAPI.TYPE);
					resultMap.put(OrganizationAPI.TYPE, typeFound);
				}
								
				if (fieldNeeded.contains(OrganizationAPI.CREATED)) {
					Timestamp createdFound = rs.getTimestamp(OrganizationAPI.CREATED);
					resultMap.put(OrganizationAPI.CREATED, createdFound.toString());
				}

				if (fieldNeeded.contains(OrganizationAPI.STATUS)) {
					String statusFound = rs.getString(OrganizationAPI.STATUS);
					resultMap.put(OrganizationAPI.STATUS, statusFound);
				}


				if (fieldNeeded.contains(OrganizationAPI.RATING)) {
					int versionFound = rs.getInt(OrganizationAPI.RATING);
					resultMap.put(OrganizationAPI.RATING, Integer.toString(versionFound));
				}

				if (fieldNeeded.contains(OrganizationAPI.SUPPORT)) {
					Boolean labelFound = rs.getBoolean(OrganizationAPI.SUPPORT);
					resultMap.put(OrganizationAPI.SUPPORT, labelFound.toString());
				}

				if (fieldNeeded.contains(OrganizationAPI.VERSION)) {
					int versionFound = rs.getInt(OrganizationAPI.VERSION);
					resultMap.put(OrganizationAPI.VERSION, Integer.toString(versionFound));
				}

			}

			return resultMap;
		}
	
	
	
	
	private static int modifyStatement(PreparedStatement statement, int amountOfVariables, Map<String, String> inputMap)
			throws SQLException {

		Set<String> currentKeys = inputMap.keySet();

		for (Iterator<String> iterator = currentKeys.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if (key.equalsIgnoreCase(NAME) || key.equalsIgnoreCase(REG_TYPE) || key.equalsIgnoreCase(SECTOR)
					|| key.equalsIgnoreCase(STATUS) || key.equalsIgnoreCase(REG_ID)
					|| key.equalsIgnoreCase(TYPE)) {
				statement.setString(amountOfVariables + 1, inputMap.get(key));
			} else if (key.equalsIgnoreCase(REGION_ID) || key.equalsIgnoreCase(PARENT_ID) || key.equalsIgnoreCase(ID)
					|| key.equalsIgnoreCase(RATING) || key.equalsIgnoreCase(VERSION)) {
				statement.setInt(amountOfVariables + 1, Integer.parseInt(inputMap.get(key)));
			} else if (key.equalsIgnoreCase(CREATED)) {
				statement.setTimestamp(amountOfVariables + 1, Timestamp.valueOf(inputMap.get(key)));
			} else if (key.equalsIgnoreCase(SUPPORT)) {
				statement.setBoolean(amountOfVariables + 1, Boolean.valueOf(inputMap.get(key)));
			}
			amountOfVariables = amountOfVariables + 1;
		}
		LOG.info("Current statement " + statement.toString());
		LOG.info("Current index value: " + amountOfVariables);

		return amountOfVariables;
	}


}
