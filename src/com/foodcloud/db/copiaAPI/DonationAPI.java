package com.foodcloud.db.copiaAPI;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Donation API object 
 * before DBunit 
 * 
 * @author Viviana
 *
 */
public class DonationAPI {

	public static String ID = "id";
	public static String EXTERNAL_ID = "external_id";
	public static String DONOR_ID = "donor_id";
	public static String DESCRIPTION = "description";
	public static String COMMENTS = "comments";
	public static String DATE = "date";
	public static String TIME = "time";
	public static String STATUS = "status";

	static final Logger LOG = LogManager.getLogger(DonationAPI.class.getName());

	
//	public void create(){
		// return donation id 
		//		Statement stmt = con.createStatement(
		//                ResultSet.TYPE_SCROLL_INSENSITIVE,
		//                ResultSet.CONCUR_UPDATABLE);
		//ResultSet rs = stmt.executeQuery("SELECT a, b FROM TABLE2");

//	}

//	public static void create(Connection con, String donorId, String donationExternalId, 
//			String expectedDate, String expectedTime, String expectedStatus ) throws SQLException {
//
//		System.out.println("Before creating Donation query ");
//
//		String query = "INSERT INTO public.donation("
//				+ "external_id, donor_id, description, comments, date, \"time\", status)"
//				+ "	VALUES ('"+ donationExternalId +"', "+ donorId +", ' ',' ', '"+ expectedDate +"', '"+expectedTime+"', '"+expectedStatus+"');";
//		
//		System.out.println("Before executing create Donation query: "+ query );
//
//		con.createStatement().execute(query);
//	
//	}

	
	/**
	 * creates a new Schedule row in the db, by insert the data available
	 * 
	 * @throws SQLException
	 */
	public static int insert(Connection con, Map<String, String> donationData) throws SQLException {

		if (!donationData.containsKey(DONOR_ID) || !donationData.containsKey(EXTERNAL_ID)) {
			LOG.error("DonationAPI: Error!  Missing mandatory data : donor_id and external_id");
			return -1;
		} 
		Set<String> fieldKeys = donationData.keySet();

		String fieldClause = ShortcutsAPI.modifyInsertSelectClause(fieldKeys);
		String valuesClause = ShortcutsAPI.modifyInsertValuesClause(fieldKeys);
				
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("INSERT INTO public.donation( ");
		queryBuffer.append(fieldClause + " )");
		queryBuffer.append("VALUES ( " + valuesClause);
		queryBuffer.append(" );");

		PreparedStatement insertStatement = con.prepareStatement(queryBuffer.toString());

		int i = 0;
		i = modifyStatement(insertStatement, i, donationData);

		LOG.info("DonationAPI:  Before executing query: " + insertStatement.toString());
		int result = insertStatement.executeUpdate();

		if (result == 0) {
			LOG.error("DonationAPI:  new element not inserted!");
		}
		return result;

	}


	public static int getDonationId(Connection con, String donorId, String date, String time ) throws SQLException{

		System.out.println("Before get Donation id  query ");

		String query = "SELECT id FROM public.donation WHERE donor_id = "
				+ ""+donorId +" AND date='"+ date +"' AND  time ='"+time+"';";
		
		System.out.println("Before executing create Donation query: "+ query );

		ResultSet re = con.createStatement().executeQuery(query);
	
		int donationId = 0;
		
		while(re.next()) {
			donationId= re.getInt("id");
			}
			
		return donationId;
	}
	

	public int getDonationsAmount(Connection con, String donorId) throws SQLException{
			
			String query = "SELECT * FROM public.donation WHERE donor_id="+ donorId +" order by time DESC";
		
			System.out.println("Before executing query: "+query );

			ResultSet re = con.createStatement().executeQuery(query);
			int donationAmount = re.getFetchSize();
				
			return donationAmount;
		}

	
	public static int getDonationId(Connection con, String donorId, String donationExternalId) throws SQLException {
		System.out.println("Before get Donation id -with external id-  query ");

		String query = "SELECT id FROM public.donation WHERE donor_id = "
				+ ""+donorId +" AND external_id ='"+donationExternalId+"';";
		
		System.out.println("Before executing create Donation query: "+ query );

		ResultSet re = con.createStatement().executeQuery(query);
	
		int donationId = 0;
		
		while(re.next()) {
			donationId= re.getInt("id");
			}
			
		return donationId;
	}

	public static void delete(Connection con, String donorId, String external_id) throws SQLException {
	
		Map<String, String> deleteCriterion = new HashMap<String, String>();
		deleteCriterion.put(DonationAPI.DONOR_ID, donorId);
		deleteCriterion.put(DonationAPI.EXTERNAL_ID, external_id);

		DonationAPI.delete(con, deleteCriterion);
	}
	
	public static int delete(Connection con, Map<String, String> criterion) throws SQLException {

		if (!criterion.containsKey(EXTERNAL_ID)) {
			LOG.error("DonationAPI: Error!  Missing mandatory data: external_id ");
			return -1;
		}

		Set<String> deleteKeys = criterion.keySet();
		String whereClause = ShortcutsAPI.modifyCriteriaClause(deleteKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("DELETE FROM public.donation WHERE ");
		queryBuffer.append(whereClause + " ;");

		PreparedStatement deleteStatement = con.prepareStatement(queryBuffer.toString());

		int delIndex = 0;
		delIndex = modifyStatement(deleteStatement, delIndex, criterion);

		LOG.info("DonationAPI:  Before executing query: " + deleteStatement.toString());
		int deletedRows = deleteStatement.executeUpdate();

		if (deletedRows == 0) {
			LOG.error("DonationAPI: element not deleted!");
		}
		return deletedRows;
	}


	
	private static int modifyStatement(PreparedStatement statement, int amountOfVariables, Map<String, String> inputMap)
			throws SQLException {
		
		Set<String> currentKeys = inputMap.keySet();

		for (Iterator<String> iterator = currentKeys.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if (key.equalsIgnoreCase(STATUS) || key.equalsIgnoreCase(EXTERNAL_ID) || 
					key.equalsIgnoreCase(DESCRIPTION) || key.equalsIgnoreCase(COMMENTS)) {
				statement.setString(amountOfVariables + 1, inputMap.get(key));
			} else if (key.equalsIgnoreCase(DONOR_ID)) {
				statement.setInt(amountOfVariables + 1, Integer.parseInt(inputMap.get(key)));
			} else if (key.equalsIgnoreCase(TIME)) {
				statement.setTime(amountOfVariables + 1, Time.valueOf(inputMap.get(key)));
			} else if ( key.equalsIgnoreCase(DATE)) {
				statement.setDate(amountOfVariables + 1, Date.valueOf(inputMap.get(key)));
			}
			amountOfVariables = amountOfVariables + 1;
		}
		LOG.info("Current statement " + statement.toString());
		LOG.info("Current index value: " + amountOfVariables);

		return amountOfVariables;
	}

}
