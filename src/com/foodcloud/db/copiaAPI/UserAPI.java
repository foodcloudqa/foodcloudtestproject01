package com.foodcloud.db.copiaAPI;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserAPI {

	public static String ID = "id";
	public static String REGION_ID = "region_id";
	public static String NAME = "name";
	public static String EMAIL = "email";
	public static String ROLE = "role";
	public static String HASHWORD = "hashword";
	public static String ORG_ID = "org_id";   
	public static String CREATED = "created";
	public static String LOCALE = "locale";
	public static String STATUS = "status";
	public static String VERSION = "version";
	public static String COMMENT = "comment";
	public static String PHONE = "phone";
	public static String RULES = "rules";

	static final Logger LOG = LogManager.getLogger(UserAPI.class.getName());

	
//	public void create(Connection con, List<String> orgData ){

	/**
	 * creates a new organization in the db 
	 * @throws SQLException 
	 */	
//	public void create(Connection con, Map<String, String> orgData ) throws SQLException{
	
		public static int insert(Connection con, Map<String, String> userData) throws SQLException {

			if (!userData.containsKey(NAME) || !userData.containsKey(EMAIL) ) {
				LOG.error("UserAPI: Error!  Missing mandatory data : name or email");
				return -1;
			} 

			Set<String> fieldKeys = userData.keySet();

			String fieldClause = ShortcutsAPI.modifyInsertSelectClause(fieldKeys);
			String valuesClause = ShortcutsAPI.modifyInsertValuesClause(fieldKeys);
					
			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append("INSERT INTO public.\"user\" ( ");
			queryBuffer.append(fieldClause + " )");
			queryBuffer.append("VALUES ( " + valuesClause);
			queryBuffer.append(" );");

			PreparedStatement insertStatement = con.prepareStatement(queryBuffer.toString());

			int i = 0;
			i = modifyStatement(insertStatement, i, userData);

			LOG.info("UserAPI:  Before executing query: " + insertStatement.toString());
			int result = insertStatement.executeUpdate();

			if (result == 0) {
				LOG.error("UserAPI:  new element not inserted!");
			}
			return result;

		}

		public static Map<String, String> getField(Connection con, Set<String> fieldNeeded,
				Map<String, String> criteriaValues) throws SQLException {

			if (criteriaValues.isEmpty() || fieldNeeded.isEmpty()) {
				LOG.error("UserAPI: Error! field values and criteria must be not null ");
				return (Map<String, String>) new HashMap().put("Error", "Null values");
			}

			Set<String> whereKeys = criteriaValues.keySet();

			String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

			String selectClause = ShortcutsAPI.modifyInsertSelectClause(fieldNeeded);

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT " + selectClause + " ");
			queryBuffer.append("FROM public.\"user\" ");
			queryBuffer.append("WHERE " + criteriaClause);
			queryBuffer.append(" ;");

			PreparedStatement statement = con.prepareStatement(queryBuffer.toString());
			int questionMarkAmount = 0;

			questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

			LOG.info("UserAPI:  Before executing query: " + statement.toString());
			ResultSet rs = statement.executeQuery();

			Map<String, String> resultMap = new HashMap<String, String>();

			while (rs.next()) {
				if (fieldNeeded.contains(UserAPI.ID)) {
					int idFound = rs.getInt(UserAPI.ID);
					resultMap.put(UserAPI.ID, Integer.toString(idFound));
				}			
				if (fieldNeeded.contains(UserAPI.REGION_ID)) {
					int regionIdFound = rs.getInt(UserAPI.REGION_ID);
					resultMap.put(UserAPI.REGION_ID, Integer.toString(regionIdFound));
				}

				if (fieldNeeded.contains(UserAPI.NAME)) {
					String nameFound = rs.getString(UserAPI.NAME);
					resultMap.put(UserAPI.NAME, nameFound);
				}

				if (fieldNeeded.contains(UserAPI.EMAIL)) {
					String emailFound = rs.getString(UserAPI.EMAIL);
					resultMap.put(UserAPI.EMAIL, emailFound);
				}

				if (fieldNeeded.contains(UserAPI.ROLE)) {
					String roleFound = rs.getString(UserAPI.ROLE);
					resultMap.put(UserAPI.ROLE, roleFound);
				}

				if (fieldNeeded.contains(UserAPI.HASHWORD)) {
					String pwdFound = rs.getString(UserAPI.HASHWORD);
					resultMap.put(UserAPI.HASHWORD, pwdFound);
				}

				if (fieldNeeded.contains(UserAPI.ORG_ID)) {
					int orgIdFound = rs.getInt(UserAPI.ORG_ID);
					resultMap.put(UserAPI.ORG_ID, Integer.toString(orgIdFound));
				}			
				
				if (fieldNeeded.contains(UserAPI.CREATED)) {
					Timestamp timestampFound = rs.getTimestamp(UserAPI.CREATED);
					resultMap.put(UserAPI.CREATED, timestampFound.toString());
				}

				if (fieldNeeded.contains(UserAPI.LOCALE)) {
					String localeFound = rs.getString(UserAPI.LOCALE);
					resultMap.put(UserAPI.LOCALE, localeFound);
				}
								
				if (fieldNeeded.contains(UserAPI.STATUS)) {
					String statusFound = rs.getString(UserAPI.STATUS);
					resultMap.put(UserAPI.STATUS, statusFound);
				}

				if (fieldNeeded.contains(UserAPI.VERSION)) {
					int versionFound = rs.getInt(UserAPI.VERSION);
					resultMap.put(UserAPI.VERSION, Integer.toString(versionFound));
				}
				
				if (fieldNeeded.contains(UserAPI.PHONE)) {
					String phoneFound = rs.getString(UserAPI.PHONE);
					resultMap.put(UserAPI.PHONE, phoneFound);
				}
				
				if (fieldNeeded.contains(UserAPI.RULES)) {
					String rulesFound = rs.getString(UserAPI.RULES);
					resultMap.put(UserAPI.RULES, rulesFound);
				}
			}
			return resultMap;
		}
	
	


		public static int delete(Connection con, Map<String, String> criterion) throws SQLException {

			if (criterion.isEmpty() || criterion.isEmpty()) {
				LOG.error("UserAPI: Error! field values and criteria must be not null ");
				return -1;
			}

			Set<String> deleteKeys = criterion.keySet();
			String whereClause = ShortcutsAPI.modifyCriteriaClause(deleteKeys);

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append("DELETE FROM public.\"user\" WHERE ");
			queryBuffer.append(whereClause + " ;");

			PreparedStatement deleteStatement = con.prepareStatement(queryBuffer.toString());

			int delIndex = 0;
			delIndex = modifyStatement(deleteStatement, delIndex, criterion);

			LOG.info("UserAPI:  Before executing query: " + deleteStatement.toString());
			int deletedRows = deleteStatement.executeUpdate();

			if (deletedRows == 0) {
				LOG.error("UserAPI: element not deleted!");
			}
			return deletedRows;
		}

		/**
		 * Updates region item in the DB given the criterion and updated values as maps
		 * 
		 * @param con
		 * @param criteriaValues
		 * @param updatedValues
		 * @return
		 * @throws SQLException
		 */
		public static int update(Connection con, Map<String, String> criteriaValues, Map<String, String> updatedValues)
				throws SQLException {

			if (criteriaValues.isEmpty() || updatedValues.isEmpty()) {
				LOG.error("UserAPI: Error! Update values and criteria must be not null ");
				return -1;
			}

			Set<String> whereKeys = criteriaValues.keySet();

			String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

			Set<String> setKeys = updatedValues.keySet();

			String updateClause = ShortcutsAPI.modifyUpdateClause(setKeys);

			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append("UPDATE public.\"user\" ");
			queryBuffer.append("SET " + updateClause + " ");
			queryBuffer.append("WHERE " + criteriaClause);
			queryBuffer.append(" ;");

			PreparedStatement statement = con.prepareStatement(queryBuffer.toString());

			int questionMarkAmount = 0;

			questionMarkAmount = modifyStatement(statement, questionMarkAmount, updatedValues);

			questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

			LOG.info("UserAPI:  Before executing query: " + statement.toString());
			int deletedRows = statement.executeUpdate();

			if (deletedRows == 0) {
				LOG.error("UserAPI: element not updated!");
			}
			return deletedRows;
		}
		
		
		private static int modifyStatement(PreparedStatement statement, int amountOfVariables, Map<String, String> inputMap)
				throws SQLException {

			Set<String> currentKeys = inputMap.keySet();

			for (Iterator<String> iterator = currentKeys.iterator(); iterator.hasNext();) {
				String key = (String) iterator.next();
				if (key.equalsIgnoreCase(EMAIL) || key.equalsIgnoreCase(NAME)
						|| key.equalsIgnoreCase(HASHWORD) || key.equalsIgnoreCase(LOCALE)  
						|| key.equalsIgnoreCase(STATUS) || key.equalsIgnoreCase(COMMENT) 
						|| key.equalsIgnoreCase(ROLE)   || key.equalsIgnoreCase(RULES)    ) {
					statement.setString(amountOfVariables + 1, inputMap.get(key));
				} else if (key.equalsIgnoreCase(ID) || key.equalsIgnoreCase(REGION_ID) 
						|| key.equalsIgnoreCase(ORG_ID) 
						|| key.equalsIgnoreCase(VERSION) ) {
					statement.setInt(amountOfVariables + 1, Integer.parseInt(inputMap.get(key)));
				} 
				else if (key.equalsIgnoreCase(CREATED)) {
					statement.setTimestamp(amountOfVariables + 1, Timestamp.valueOf(inputMap.get(key)));
				} 
				amountOfVariables = amountOfVariables + 1;
			}
			LOG.info("Current statement " + statement.toString());
			LOG.info("Current index value: " + amountOfVariables);

			return amountOfVariables;
		}	
}
