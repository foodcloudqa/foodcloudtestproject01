package com.foodcloud.db.copiaAPI;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class representing Location table in Copia Database 
 * Currently pointing to Copia-c
 * 
 * @author Viviana
 *
 */
public class RegionAPI {

	public static final String TABLE_NAME = "region";
	public static String ID = "id";
	public static final String PARENT_ID = "parent_id";  
	public static final String NAME = "name";
	public static final String FULL_NAME = "full_name";
	public static final String IDC = "idc";
	public static final String STATUS = "status";
	public static final String VERSION = "version";
	public static final String LOCALE = "locale";
	public static final String TZ = "tz";

	public static String AUSTRALIA = "Australia";
	public static String ISLE_OF_MAN = "Isle of Man";
	
	public static String TIMEZONE_DUBLIN = "Europe/Dublin";
	public static String TIMEZONE_LONDON = "Europe/London";
	public static String TIMEZONE_MANISLAND = "Europe/Isle_of_Man";
	public static String TIMEZONE_AUSTRALIA = "Australia/Sydney";
	
	static final Logger LOG = LogManager.getLogger(RegionAPI.class.getName());

	
	
	/**
	 * creates a new Region in the db, having the organization id available 
	 * Default:  idc = null , status = Active, locale= en, tz=UTC
	 * @throws SQLException 
	 */	
	public static int insert(Connection con, Map<String, String> scheduleData) throws SQLException {

	if (!scheduleData.containsKey(NAME) ) {
		LOG.error("RegionAPI: Error!  Missing mandatory data : name ");
		return -1;
	} 

	Set<String> fieldKeys = scheduleData.keySet();

	String fieldClause = ShortcutsAPI.modifyInsertSelectClause(fieldKeys);
	String valuesClause = ShortcutsAPI.modifyInsertValuesClause(fieldKeys);
			
	StringBuffer queryBuffer = new StringBuffer();
	queryBuffer.append("INSERT INTO public.region( ");
	queryBuffer.append(fieldClause + " )");
	queryBuffer.append("VALUES ( " + valuesClause);
	queryBuffer.append(" );");

	PreparedStatement insertStatement = con.prepareStatement(queryBuffer.toString(), Statement.RETURN_GENERATED_KEYS);

	int i = 0;
	i = modifyStatement(insertStatement, i, scheduleData);

	LOG.info("RegionAPI:  Before executing query: " + insertStatement.toString());
	int result = insertStatement.executeUpdate();

	 ResultSet resultSet = insertStatement.getGeneratedKeys();
	 long id =  0;
     {
         while (resultSet.next())
         {
             id = resultSet.getLong(1);

         	LOG.info("RegionAPI:  new element inserted! Id: " + id);
         }
     }
	return (int) id;
}

	/**
	 * 
	 * @param con
	 * @param criterion
	 * @return
	 * @throws SQLException
	 */
	public static int delete(Connection con, Map<String, String> criterion) throws SQLException {

		if (criterion.isEmpty() || criterion.isEmpty()) {
			LOG.error("RegionAPI: Error! field values and criteria must be not null ");
			return -1;
		}

		Set<String> deleteKeys = criterion.keySet();
		String whereClause = ShortcutsAPI.modifyCriteriaClause(deleteKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("DELETE FROM public.region WHERE ");
		queryBuffer.append(whereClause + " ;");

		PreparedStatement deleteStatement = con.prepareStatement(queryBuffer.toString());

		int delIndex = 0;
		delIndex = modifyStatement(deleteStatement, delIndex, criterion);

		LOG.info("RegionAPI:  Before executing query: " + deleteStatement.toString());
		int deletedRows = deleteStatement.executeUpdate();

		if (deletedRows == 0) {
			LOG.error("RegionAPI: element not deleted!");
		}
		return deletedRows;
	}

	/**
	 * Updates region item in the DB given the criterion and updated values as maps
	 * 
	 * @param con
	 * @param criteriaValues
	 * @param updatedValues
	 * @return
	 * @throws SQLException
	 */
	public static int update(Connection con, Map<String, String> criteriaValues, Map<String, String> updatedValues)
			throws SQLException {

		if (criteriaValues.isEmpty() || updatedValues.isEmpty()) {
			LOG.error("RegionAPI: Error! Update values and criteria must be not null ");
			return -1;
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		Set<String> setKeys = updatedValues.keySet();

		String updateClause = ShortcutsAPI.modifyUpdateClause(setKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("UPDATE public.region ");
		queryBuffer.append("SET " + updateClause + " ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());

		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, updatedValues);

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("RegionAPI:  Before executing query: " + statement.toString());
		int updatedRows = statement.executeUpdate();

		if (updatedRows == 0) {
			LOG.error("RegionAPI: element not updated!");
		}
		return updatedRows;
	}
	
	/**
	 * 
	 * @param con
	 * @param fieldNeeded
	 * @param criteriaValues
	 * @return
	 * @throws SQLException
	 */
	public static Map<String, String> getField(Connection con, Set<String> fieldNeeded,
			Map<String, String> criteriaValues) throws SQLException {

		if (criteriaValues.isEmpty() || fieldNeeded.isEmpty()) {
			LOG.error("RegionAPI: Error! field values and criteria must be not null ");
			return (Map<String, String>) new HashMap().put("Error", "Null values");
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		String selectClause = ShortcutsAPI.modifyInsertSelectClause(fieldNeeded);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("SELECT " + selectClause + " ");
		queryBuffer.append("FROM public.region ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());
		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("RegionAPI:  Before executing query: " + statement.toString());
		ResultSet rs = statement.executeQuery();

		Map<String, String> resultMap = new HashMap<String, String>();

		while (rs.next()) {
			if (fieldNeeded.contains(RegionAPI.ID)) {
				int idFound = rs.getInt(RegionAPI.ID);
				resultMap.put(RegionAPI.ID, Integer.toString(idFound));
			}			
			if (fieldNeeded.contains(RegionAPI.PARENT_ID)) {
				int parentIdFound = rs.getInt(RegionAPI.PARENT_ID);
				resultMap.put(RegionAPI.PARENT_ID, Integer.toString(parentIdFound));
			}

			if (fieldNeeded.contains(RegionAPI.NAME)) {
				String nameFound = rs.getString(RegionAPI.NAME);
				resultMap.put(RegionAPI.NAME, nameFound);
			}

			if (fieldNeeded.contains(RegionAPI.FULL_NAME)) {
				String fullNameFound = rs.getString(RegionAPI.FULL_NAME);
				resultMap.put(RegionAPI.FULL_NAME, fullNameFound);
			}

			if (fieldNeeded.contains(RegionAPI.IDC)) {
				int idcFound = rs.getInt(RegionAPI.IDC);
				resultMap.put(RegionAPI.IDC, Integer.toString(idcFound));
			}

			if (fieldNeeded.contains(RegionAPI.STATUS)) {
				String statusFound = rs.getString(RegionAPI.STATUS);
				resultMap.put(RegionAPI.STATUS, statusFound);
			}

			if (fieldNeeded.contains(RegionAPI.VERSION)) {
				int versionFound = rs.getInt(RegionAPI.VERSION);
				resultMap.put(RegionAPI.VERSION, Integer.toString(versionFound));
			}

			if (fieldNeeded.contains(RegionAPI.LOCALE)) {
				String localeFound = rs.getString(RegionAPI.LOCALE);
				resultMap.put(RegionAPI.LOCALE, localeFound);
			}

			if (fieldNeeded.contains(RegionAPI.TZ)) {
				String tzFound = rs.getString(RegionAPI.TZ);
				resultMap.put(RegionAPI.TZ, tzFound);
			}
		}
		return resultMap;
	}
	
	
	/**
	 * Get Region id by looking for Full Name 
	 * @param con
	 * @param organizationId
	 * @return
	 * @throws SQLException
	 */
	public static int getId(Connection con, String fullRegionName) throws SQLException{
		
		Map<String, String> searchData = new HashMap<String, String>();
		searchData.put(RegionAPI.FULL_NAME, fullRegionName);
		
		Set<String> fieldList = new HashSet<String>();
		fieldList.add(RegionAPI.ID);
		
		Map<String,String> result = getField(con,fieldList, searchData);
		
		int actualOrgId = 0 ;
		if (!result.isEmpty()) {
			String  orgId  = result.get(RegionAPI.ID);						
			actualOrgId = Integer.parseInt(orgId);
		}				
		return	actualOrgId;	
	}

	public static int  getLastId(Connection con) throws SQLException {
		return ShortcutsAPI.getLastId(con, TABLE_NAME, ID);
	}
	
	
	private static int modifyStatement(PreparedStatement statement, int amountOfVariables, Map<String, String> inputMap)
			throws SQLException {

		Set<String> currentKeys = inputMap.keySet();

		for (Iterator<String> iterator = currentKeys.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if (key.equalsIgnoreCase(NAME) || key.equalsIgnoreCase(FULL_NAME)
					|| key.equalsIgnoreCase(STATUS) || key.equalsIgnoreCase(LOCALE) 
					|| key.equalsIgnoreCase(TZ)    ) {
				statement.setString(amountOfVariables + 1, inputMap.get(key));
			} else if (key.equalsIgnoreCase(PARENT_ID) || key.equalsIgnoreCase(IDC) 
					|| key.equalsIgnoreCase(VERSION)) {
				statement.setInt(amountOfVariables + 1, Integer.parseInt(inputMap.get(key)));
			} 
			amountOfVariables = amountOfVariables + 1;
		}
		LOG.info("Current statement " + statement.toString());
		LOG.info("Current index value: " + amountOfVariables);

		return amountOfVariables;
	}	
	
	
	
	
	

}
