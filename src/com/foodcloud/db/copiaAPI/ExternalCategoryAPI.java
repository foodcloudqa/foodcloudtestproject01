package com.foodcloud.db.copiaAPI;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class representing Schedule table in Copia Database Currently pointing to
 * Copia-c
 * 
 * @author Viviana
 *
 */
public class ExternalCategoryAPI {

	public static final String ID = "id";
	public static String ORG_ID = "org_id";
	public static final String CODE = "code";
	public static final String EXTERNAL_CODE = "external_code";
	public static final String UNITS = "units";
	public static final String STORAGE = "storage";

	static final Logger LOG = LogManager.getLogger(ExternalCategoryAPI.class.getName());
	
	// public void createTable(Connection con, Map<String, String> scheduleData)
	// throws SQLException {}

	/**
	 * Creates a new ExternalCategory row in the db, by insert the data available
	 * Mapping the organization with the food categories available 
	 * @throws SQLException
	 */
	public static int insert(Connection con, Map<String, String> categoryData) throws SQLException {

		// need control on data : ORG_ID most important!!
		if (!categoryData.containsKey(ORG_ID)) {
			LOG.error("Category: Error!  Missing mandatory data : day and org_id ");
			return -1;
		} else if (Integer.parseInt(categoryData.get(ORG_ID)) == 0) {
			LOG.error("ExternalCategoryAPI: Error! Party / Org_id 0");
			return -1;
		}

		Set<String> fieldKeys = categoryData.keySet();

		String fieldClause = ShortcutsAPI.modifyInsertSelectClause(fieldKeys);
		String valuesClause = ShortcutsAPI.modifyInsertValuesClause(fieldKeys);
				
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("INSERT INTO public.external_category( ");
		queryBuffer.append(fieldClause + " )");
		queryBuffer.append("VALUES ( " + valuesClause);
		queryBuffer.append(" );");

		PreparedStatement insertStatement = con.prepareStatement(queryBuffer.toString());

		int i = 0;
		i = modifyStatement(insertStatement, i, categoryData);

		LOG.info("ExternalCategoryAPI:  Before executing query: " + insertStatement.toString());
		int result = insertStatement.executeUpdate();

		if (result == 0) {
			LOG.error("ExternalCategoryAPI:  new element not inserted!");
		}
		return result;

	}


	/**
	 * Delete schedule row with criterion
	 */
	public static int delete(Connection con, Map<String, String> criterion) throws SQLException {

		if (!criterion.containsKey(ORG_ID)) {
			LOG.error("ExternalCategoryAPI: Error!  Missing mandatory data: org_id ");
			return -1;
		}

		Set<String> deleteKeys = criterion.keySet();
		String whereClause = ShortcutsAPI.modifyCriteriaClause(deleteKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("DELETE FROM public.external_category WHERE ");
		queryBuffer.append(whereClause + " ;");

		PreparedStatement deleteStatement = con.prepareStatement(queryBuffer.toString());

		int delIndex = 0;
		delIndex = modifyStatement(deleteStatement, delIndex, criterion);

		LOG.info("ExternalCategoryAPI:  Before executing query: " + deleteStatement.toString());
		int deletedRows = deleteStatement.executeUpdate();

		if (deletedRows == 0) {
			LOG.error("ExternalCategoryAPI: element not deleted!");
		}
		return deletedRows;
	}

	/**
	 * Updates schedule item in the DB given the criterion and updated values as
	 * maps
	 * 
	 * @param con
	 * @param criteriaValues
	 * @param updatedValues
	 * @return
	 * @throws SQLException
	 */
	public static int update(Connection con, Map<String, String> criteriaValues, Map<String, String> updatedValues)
			throws SQLException {

		if (criteriaValues.isEmpty() || updatedValues.isEmpty()) {
			LOG.error("ExternalCategoryAPI: Error! Update values and criteria must be not null ");
			return -1;
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		Set<String> setKeys = updatedValues.keySet();

		String updateClause = ShortcutsAPI.modifyUpdateClause(setKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("UPDATE public.external_category ");
		queryBuffer.append("SET " + updateClause + " ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());

		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, updatedValues);

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("ExternalCategoryAPI:  Before executing query: " + statement.toString());
		int deletedRows = statement.executeUpdate();

		if (deletedRows == 0) {
			LOG.error("ExternalCategoryAPI: element not updated!");
		}
		return deletedRows;
	}

	/**
	 * Updates schedule item in the DB given the criterion and updated values as
	 * maps
	 * 
	 * @param con
	 * @param criteriaValues
	 * @param updatedValues
	 * @return
	 * @throws SQLException
	 */
	public static Map<String, String> getField(Connection con, Set<String> fieldNeeded,
			Map<String, String> criteriaValues) throws SQLException {

		if (criteriaValues.isEmpty() || fieldNeeded.isEmpty()) {
			LOG.error("ExternalCategoryAPI: Error! field values and criteria must be not null ");
			return (Map<String, String>) new HashMap().put("Error", "Null values");
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		String selectClause = ShortcutsAPI.modifyInsertSelectClause(fieldNeeded);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("SELECT " + selectClause + " ");
		queryBuffer.append("FROM public.external_category ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());
		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("ExternalCategoryAPI:  Before executing query: " + statement.toString());
		ResultSet rs = statement.executeQuery();

		Map<String, String> resultMap = new HashMap<String, String>();

		while (rs.next()) {
			if (fieldNeeded.contains(ExternalCategoryAPI.ID)) {
				int idFound = rs.getInt(ExternalCategoryAPI.ID);
				resultMap.put(ExternalCategoryAPI.ID, Integer.toString(idFound));
			}

			if (fieldNeeded.contains(ExternalCategoryAPI.ORG_ID)) {
				int orgIdFound = rs.getInt(ExternalCategoryAPI.ORG_ID);
				resultMap.put(ExternalCategoryAPI.ORG_ID, Integer.toString(orgIdFound));
			}

			if (fieldNeeded.contains(ExternalCategoryAPI.CODE)) {
				String codeFound = rs.getString(ExternalCategoryAPI.CODE);
				resultMap.put(ExternalCategoryAPI.CODE, codeFound);
			}

			
			if (fieldNeeded.contains(ExternalCategoryAPI.EXTERNAL_CODE)) {
				String externalCodeFound = rs.getString(ExternalCategoryAPI.EXTERNAL_CODE);
				resultMap.put(ExternalCategoryAPI.EXTERNAL_CODE, externalCodeFound);
			}

			if (fieldNeeded.contains(ExternalCategoryAPI.STORAGE)) {
				String storageFound = rs.getString(ExternalCategoryAPI.STORAGE);
				resultMap.put(ExternalCategoryAPI.STORAGE, storageFound);
			}

			if (fieldNeeded.contains(ExternalCategoryAPI.UNITS)) {
				String unitsFound = rs.getString(ExternalCategoryAPI.UNITS);
				resultMap.put(ExternalCategoryAPI.UNITS, unitsFound);
			}

		}

		return resultMap;
	}

	private static int modifyStatement(PreparedStatement statement, int amountOfVariables, Map<String, String> inputMap)
			throws SQLException {

		Set<String> currentKeys = inputMap.keySet();

		for (Iterator<String> iterator = currentKeys.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if (key.equalsIgnoreCase(CODE) || key.equalsIgnoreCase(EXTERNAL_CODE) || key.equalsIgnoreCase(STORAGE) || key.equalsIgnoreCase(UNITS)) {
				statement.setString(amountOfVariables + 1, inputMap.get(key));
			} else if (key.equalsIgnoreCase(ORG_ID) || key.equalsIgnoreCase(ID) ) {
				statement.setInt(amountOfVariables + 1, Integer.parseInt(inputMap.get(key)));
			}
			amountOfVariables = amountOfVariables + 1;
		}
		LOG.info("Current statement " + statement.toString());
		LOG.info("Current index value: " + amountOfVariables);

		return amountOfVariables;
	}


}
