package com.foodcloud.db.copiaAPI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import com.foodcloud.server.FCTestUtils;

public class ConnectionToDb {

	String url = "jdbc:postgresql://localhost/test";
	Properties props = new Properties();
//	props.setProperty("user","fred");
//	props.setProperty("password","secret");
//	props.setProperty("ssl","true");
//	DriverManager driver = new Drivermanager
	
//	Connection conn = DriverManager.getConnection(url, props);

//	String url = "jdbc:postgresql://localhost/test?user=fred&password=secret&ssl=true";
//	Connection conn = DriverManager.getConnection(url);

	
	
	public static Connection getCopiaConnection() {
		Connection connection = null;
	try {
		//copia-c
		String dbUrl="ec2-52-210-102-210.eu-west-1.compute.amazonaws.com:5432";
		String dbUser="u5tqjot55fsok4";
		String dbPwd="p5li9h8io1rgfpabtkjq4bt4h1u";
		String dbName="dbp67sqiagc87v";
						
		String url = "jdbc:postgresql://"+dbUrl +"/"+dbName+"?user="+dbUser+"&password="+dbPwd+"&ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";
		// String Original_url = "jdbc:postgresql://ec2-52-210-102-210.eu-west-1.compute.amazonaws.com:5432/dbp67sqiagc87v?user=u5tqjot55fsok4&password=p5li9h8io1rgfpabtkjq4bt4h1u&ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";
		
		connection = DriverManager.getConnection(url);
		//Debug only : System.out.println(url);

	} catch (SQLException e) {

		System.out.println("Connection Failed! Check output console");
		e.printStackTrace();

	}

	if (connection != null) {
		System.out.println("You made it, take control your database now!");
			
	} else {
		System.out.println("Failed to make connection!");
	}
		
	return connection;	
}
	
	
	public static String formatDate(OffsetDateTime utcDate) {

		DateTimeFormatter db_f = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String formattedDateTime = utcDate.format(db_f);
		return formattedDateTime;
	}
	
	public static String formatDate() {

		OffsetDateTime datae = FCTestUtils.getUTCDate();
	
		return formatDate(datae) ;
	}
	
	public static String formatTime(OffsetDateTime utcDate) {
		DateTimeFormatter db_f = DateTimeFormatter.ofPattern("HH:mm:ss");
		String formattedDateTime = utcDate.format(db_f);
		return formattedDateTime;
	}

	public static String formatTime() {

		OffsetDateTime time = FCTestUtils.getUTCDate();
	
		return formatTime(time);
	}


	public static String formatTimestamp() {
		// Timestamp format must be yyyy-mm-dd hh:mm:ss[.fffffffff]
		// BUT yyyy-mm-dd hh:mm:ss.fffffffff fractional seconds are S in Datetime
		
		DateTimeFormatter db_f = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSS");
		OffsetDateTime timestamp = FCTestUtils.getUTCDate();
		String formattedDateTime = timestamp.format(db_f);

		return formattedDateTime;
	}

	
}
