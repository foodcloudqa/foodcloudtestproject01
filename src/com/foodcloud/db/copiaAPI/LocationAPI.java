package com.foodcloud.db.copiaAPI;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class representing Location table in Copia Database Currently pointing to
 * Copia-c
 * 
 * @author Viviana
 *
 */
public class LocationAPI {

	public static String ID = "id";
	public static String ORG_ID = "org_id";
	public static String LATITUDE = "lat";
	public static String LONGITUDE = "long";
	public static String ADDRESS = "address";
	public static String ADDRESS_CODE = "addr_code";
	public static String COUNTRY = "country";
	public static String VERSION = "version";
	public static String TIMEZONE = "timezone";

	public static final String TIMEZONE_DUBLIN = "Europe/Dublin";
	public static final String TIMEZONE_LONDON = "Europe/London";
	public static final String TIMEZONE_MANISLAND = "Europe/Isle_of_Man";
	public static final String TIMEZONE_AUSTRALIA = "Australia/Sydney";
	public static final String TIMEZONE_MADRID = "Europe/Madrid";

	static final Logger LOG = LogManager.getLogger(LocationAPI.class.getName());

	/**
	 * creates a new location in the db, having the organization id available
	 * 
	 * @throws SQLException
	 */
	// public void create(Connection con, Map<String, String> locationData )
	// throws SQLException{
	// }

	
	/**
	 * 
	 * creates a new location in the db by INSERTing data provided
	 *
	 */
	public static int insert(Connection con, Map<String, String> locationData) throws SQLException {

		if (!locationData.containsKey(ORG_ID)) {
			LOG.error("LocationAPI: Error! Org_id field missing");
			return -1;
		} else {
			if (Integer.parseInt(locationData.get(ORG_ID)) == 0) {
				LOG.error("LocationAPI: Error! Org_id 0");
				return -1;
			}
		}
			Set<String> fieldKeys = locationData.keySet();

			String fieldClause = ShortcutsAPI.modifyInsertSelectClause(fieldKeys);
			String valuesClause = ShortcutsAPI.modifyInsertValuesClause(fieldKeys);
					
			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append("INSERT INTO public.location( ");
			queryBuffer.append(fieldClause + " )");
			queryBuffer.append("VALUES ( " + valuesClause);
			queryBuffer.append(" );");

			PreparedStatement insertStatement = con.prepareStatement(queryBuffer.toString());

			int i = 0;
			i = modifyStatement(insertStatement, i, locationData);

			LOG.info("LocationAPI:  Before executing query: " + insertStatement.toString());
			int result = insertStatement.executeUpdate();

			if (result == 0) {
				LOG.error("LocationAPI:  new element not inserted!");
			}
			return result;

		}
	


	public static String getTimezone(Connection con, int organizationId) throws SQLException {

		String query = "SELECT timezone FROM location WHERE org_id='" + organizationId + "';";

		LOG.info("LocationAPI: Before executing query: " + query);

		ResultSet re = con.createStatement().executeQuery(query);
		String foundIt = "";
		while (re.next()) {
			foundIt = re.getString("timezone");
		}

		return foundIt;
	}

	public static int getId(Connection con, int orgIdCriterion) throws SQLException {

		String query = "SELECT id FROM public.location WHERE org_id= ?";

		PreparedStatement statement = con.prepareStatement(query);

		statement.setInt(1, orgIdCriterion);

		LOG.info("LocationAPI: Before executing query: " + query);

		ResultSet re = statement.executeQuery();
		int foundId = 0;
		while (re.next()) {
			foundId = re.getInt("id");
		}

		return foundId;

	}

	public static boolean isElementPresent(Connection con, int idCriterion) throws SQLException {

		int foundId  = getId(con, idCriterion);
				
		boolean result=false;
		if (foundId!=0) {
			result = true;
		}
		return result;
	}

	public static int delete(Connection con, Map<String, String> criterion) throws SQLException {

		if (!criterion.containsKey(ORG_ID)) {
			LOG.error("LocationAPI: Error!  Missing mandatory data: party id ( or org id) ");
			return -1;
		}

//		if (!criterion.containsKey(ID)) {
//			LOG.error("LocationAPI: Error!  Missing mandatory data: party id ( or org id) ");
//			return -1;
//		}

		Set<String> deleteKeys = criterion.keySet();
		String whereClause = ShortcutsAPI.modifyCriteriaClause(deleteKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("DELETE FROM public.location WHERE ");
		queryBuffer.append(whereClause + " ;");

		PreparedStatement deleteStatement = con.prepareStatement(queryBuffer.toString());

		int delIndex = 0;
		delIndex = modifyStatement(deleteStatement, delIndex, criterion);

		LOG.info("LocationAPI:  Before executing query: " + deleteStatement.toString());
		int deletedRows = deleteStatement.executeUpdate();

		if (deletedRows == 0) {
			LOG.error("LocationAPI: element not deleted!");
		}
		return deletedRows;
	}

	
	public static int update(Connection con, Map<String, String> criteriaValues, Map<String, String> updatedValues)
			throws SQLException {

		if (criteriaValues.isEmpty() || updatedValues.isEmpty()) {
			LOG.error("LocationAPI: Error! Update values and criteria must be not null ");
			return -1;
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		Set<String> setKeys = updatedValues.keySet();

		String updateClause = ShortcutsAPI.modifyUpdateClause(setKeys);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("UPDATE public.location ");
		queryBuffer.append("SET " + updateClause + " ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());

		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, updatedValues);

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("LocationAPI:  Before executing query: " + statement.toString());
		int updatedRows = statement.executeUpdate();

		if (updatedRows == 0) {
			LOG.error("LocationAPI: element not updated!");
		}
		return updatedRows;
	}		
	
	
	private static int modifyStatement(PreparedStatement statement, int amountOfVariables, Map<String, String> inputMap)
			throws SQLException {

		Set<String> currentKeys = inputMap.keySet();

		for (Iterator<String> iterator = currentKeys.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if (key.equalsIgnoreCase(ADDRESS) || key.equalsIgnoreCase(ADDRESS_CODE)
					|| key.equalsIgnoreCase(TIMEZONE)|| key.equalsIgnoreCase(COUNTRY) ) {
				statement.setString(amountOfVariables + 1, inputMap.get(key));
			} else if (key.equalsIgnoreCase(LATITUDE)  
					|| key.equalsIgnoreCase(LONGITUDE)) {
				statement.setDouble(amountOfVariables + 1, Double.parseDouble(inputMap.get(key)));
			}
			else if (key.equalsIgnoreCase(ORG_ID) || key.equalsIgnoreCase(ID)   
					|| key.equalsIgnoreCase(VERSION)) {
				statement.setInt(amountOfVariables + 1, Integer.parseInt(inputMap.get(key)));
			}
			amountOfVariables = amountOfVariables + 1;
		}
		LOG.info("Current statement " + statement.toString());
		LOG.info("Current index value: " + amountOfVariables);

		return amountOfVariables;
	}


	public static Map<String, String> getField(Connection con, Set<String> fieldNeeded,
			Map<String, String> criteriaValues) throws SQLException {

		if (criteriaValues.isEmpty() || fieldNeeded.isEmpty()) {
			LOG.error("LocationAPI: Error! field values and criteria must be not null ");
			Map<String, String> empty = (Map<String, String>) new HashMap().put("Error", "Null values");
			return empty;
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		String selectClause = ShortcutsAPI.modifyInsertSelectClause(fieldNeeded);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("SELECT " + selectClause + " ");
		queryBuffer.append("FROM public.location ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());
		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("LocationAPI:  Before executing query: " + statement.toString());
		ResultSet rs = statement.executeQuery();

		Map<String, String> resultMap = new HashMap<String, String>();

		while (rs.next()) {
			if (fieldNeeded.contains(LocationAPI.ID)) {
				int idFound = rs.getInt(LocationAPI.ID);
				resultMap.put(LocationAPI.ID, Integer.toString(idFound));
			}			
			if (fieldNeeded.contains(LocationAPI.ORG_ID)) {
				int orgIdFound = rs.getInt(LocationAPI.ORG_ID);
				resultMap.put(LocationAPI.ORG_ID, Integer.toString(orgIdFound));
			}
			if (fieldNeeded.contains(LocationAPI.LATITUDE)) {
				double latitudeFound = rs.getDouble(LocationAPI.LATITUDE);
				resultMap.put(LocationAPI.LATITUDE, Double.toString(latitudeFound)); 
			}
			
			if (fieldNeeded.contains(LocationAPI.LONGITUDE)) {
				double longitudeFound = rs.getDouble(LocationAPI.LONGITUDE);
				resultMap.put(LocationAPI.LONGITUDE, Double.toString(longitudeFound)); 
		}
			
			if (fieldNeeded.contains(LocationAPI.ADDRESS)) {
				String addressFound = rs.getString(LocationAPI.ADDRESS);
				resultMap.put(LocationAPI.ADDRESS, addressFound);
			}

			if (fieldNeeded.contains(LocationAPI.ADDRESS_CODE)) {
				String addressFound = rs.getString(LocationAPI.ADDRESS_CODE);
				resultMap.put(LocationAPI.ADDRESS_CODE, addressFound);
			}

			if (fieldNeeded.contains(LocationAPI.COUNTRY)) {
				String addressFound = rs.getString(LocationAPI.COUNTRY);
				resultMap.put(LocationAPI.COUNTRY, addressFound);
			}
			
			if (fieldNeeded.contains(LocationAPI.VERSION)) {
				int versionFound = rs.getInt(LocationAPI.VERSION);
				resultMap.put(LocationAPI.VERSION, Integer.toString(versionFound));
			}

			if (fieldNeeded.contains(LocationAPI.TIMEZONE)) {
				Boolean multiDonation = rs.getBoolean(LocationAPI.TIMEZONE);
				resultMap.put(LocationAPI.TIMEZONE, multiDonation.toString());
			}			

		}

		return resultMap;
	}

	
}
