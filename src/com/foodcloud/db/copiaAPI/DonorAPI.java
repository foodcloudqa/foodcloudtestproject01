package com.foodcloud.db.copiaAPI;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class representing Donor table in Copia Database 
 * Currently pointing to Copia-c
 * 
 * @author Viviana
 *
 */
public class DonorAPI {

	public static final String ID = "id" ;
	public static final String ORG_ID = "org_id" ;
	public static final String EXTERNAL_ID = "external_id";
	public static final String POST_BY = "post_by";
	public static final String COLLECT_START = "collect_start";
	public static final String COLLECT_END = "collect_end";
	public static final String VERSION = "version";
	public static final String GROUP_NAME = "group_name";
	public static final String RULES = "rules";

	 static final Logger LOG = LogManager.getLogger(DonorAPI.class.getName());


	/**
	 * creates a donor table 
	 * @throws SQLException 
	 */	
	//public void create(Connection con, Map<String, String> donorData ) {	}
	
	
	
	/**
	 * 
	 * creates a new donor in the db by INSERTing data provided
	 *
	 * @param con Connection to the DB
	 * @param donorData
	 * @return  int representing donor id 
	 * @throws SQLException
	 */
	public static int insert(Connection con, Map<String, String> donorData) throws SQLException {
		// need control on data : ORG_ID most important!!
		if (!donorData.containsKey(ORG_ID)){
			LOG.error("DonorAPI: Error! Org_id field missing");
			return -1;
		} else {
			if(Integer.parseInt(donorData.get(ORG_ID)) == 0) {
				LOG.error("DonorAPI: Error! Org_id 0");
				return -1;				
			}
		}

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("INSERT INTO public.donor( org_id, external_id, post_by, ");
		queryBuffer.append("collect_start, collect_end, version, group_name, rules) ");
		queryBuffer.append("VALUES ( ?, ?, ?, ?, ?, ?, ?, ?);");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());

		if (donorData.containsKey(ORG_ID)) {
			statement.setInt(1, Integer.parseInt(donorData.get(ORG_ID)));
		} 

		if (donorData.containsKey(EXTERNAL_ID)) {
			statement.setString(2, donorData.get(EXTERNAL_ID));
		} else {
			statement.setString(2,"TEST:00001");
		}
		
		if (donorData.containsKey(POST_BY)) {
			statement.setTime(3, Time.valueOf(donorData.get(POST_BY)));
		}

		
		if (donorData.containsKey(COLLECT_START)) {
			statement.setTime(4, Time.valueOf(donorData.get(COLLECT_START)));
		}

		if (donorData.containsKey(COLLECT_END)) {
			statement.setTime(5, Time.valueOf(donorData.get(COLLECT_END)));
		}
		
		if (donorData.containsKey(VERSION)) {
			statement.setInt(6, Integer.parseInt(donorData.get(VERSION)));
		} else {
			statement.setInt(6, 0);
		}

		if (donorData.containsKey(GROUP_NAME)) {

			statement.setString(7, donorData.get(GROUP_NAME));
		} else {
			statement.setString(7, null);
		}

		if (donorData.containsKey(RULES)) {

			statement.setString(8, donorData.get(RULES));
		} else {
			statement.setString(8, null);
		}

		
		LOG.info("DonorAPI:  Before executing query: " + statement.toString());
		int result = statement.executeUpdate();
		int donorId = result;

		if (result == 0) {
			LOG.error("DonorAPI:  new element not inserted!");

		} else {
			donorId = getId(con, Integer.parseInt(donorData.get(ORG_ID)));
		}

		return donorId;
	}

	
	public static int getId(Connection con, int donorOrgCriterion) throws SQLException {

		String query = "SELECT id FROM public.donor WHERE org_id= ?";

		PreparedStatement statement = con.prepareStatement(query);

		statement.setInt(1, donorOrgCriterion);

		LOG.info("DonorAPI: Before executing query: "+query );

		ResultSet re = statement.executeQuery();
		int foundId = 0;
		while(re.next()) {
			foundId= re.getInt("id");
			}
					
		return foundId;
			
	}	

	public static String getExternalId(Connection con, int donorId) throws SQLException {
		String query = "SELECT external_id FROM donor WHERE org_id="+donorId+";";

LOG.info("DonorAPI: Before executing externalId query: "+query );

		ResultSet re = con.createStatement().executeQuery(query);
		String foundExtId = "";
		while(re.next()) {
			foundExtId= re.getString("external_id");
			}
					
		return foundExtId;
	}


	public static boolean isElementPresent(Connection con, int orgId) throws SQLException {

		int foundId = getId(con, orgId);
			
		boolean result=false;
		if (foundId!=0) {
			result = true;
		}
		return result;
	}



	public static int delete(Connection con, int orgIdentifier) throws SQLException {

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("DELETE FROM public.donor WHERE org_id = ?;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());

		statement.setInt(1, orgIdentifier);
		
		LOG.info("DonorAPI:  Before executing query: " + statement.toString());
		int deletedRows = statement.executeUpdate();

		if (deletedRows == 0) {
			LOG.error("DonorAPI: element not deleted!");
		}
		return deletedRows;
	}
	
	
	public static Map<String, String> getField(Connection con, Set<String> fieldNeeded,
			Map<String, String> criteriaValues) throws SQLException {

		if (criteriaValues.isEmpty() || fieldNeeded.isEmpty()) {
			LOG.error("DonorAPI: Error! field values and criteria must be not null ");
			Map<String, String> empty = (Map<String, String>) new HashMap().put("Error", "Null values");
			return empty;
		}

		Set<String> whereKeys = criteriaValues.keySet();

		String criteriaClause = ShortcutsAPI.modifyCriteriaClause(whereKeys);

		String selectClause = ShortcutsAPI.modifyInsertSelectClause(fieldNeeded);

		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("SELECT " + selectClause + " ");
		queryBuffer.append("FROM public.donor ");
		queryBuffer.append("WHERE " + criteriaClause);
		queryBuffer.append(" ;");

		PreparedStatement statement = con.prepareStatement(queryBuffer.toString());
		int questionMarkAmount = 0;

		questionMarkAmount = modifyStatement(statement, questionMarkAmount, criteriaValues);

		LOG.info("DonorAPI:  Before executing query: " + statement.toString());
		ResultSet rs = statement.executeQuery();

		Map<String, String> resultMap = new HashMap<String, String>();

		while (rs.next()) {
			if (fieldNeeded.contains(DonorAPI.ID)) {
				int idFound = rs.getInt(DonorAPI.ID);
				resultMap.put(DonorAPI.ID, Integer.toString(idFound));
			}			
			if (fieldNeeded.contains(DonorAPI.ORG_ID)) {
				int orgIdFound = rs.getInt(DonorAPI.ORG_ID);
				resultMap.put(DonorAPI.ORG_ID, Integer.toString(orgIdFound));
			}

			if (fieldNeeded.contains(DonorAPI.EXTERNAL_ID)) {
				String externalIdFound = rs.getString(DonorAPI.EXTERNAL_ID);
				resultMap.put(DonorAPI.EXTERNAL_ID, externalIdFound);
			}

			if (fieldNeeded.contains(DonorAPI.POST_BY)) {
				Time createdFound = rs.getTime(DonorAPI.POST_BY);
				resultMap.put(DonorAPI.POST_BY, createdFound.toString());
			}

			if (fieldNeeded.contains(DonorAPI.COLLECT_START)) {
				Time createdFound = rs.getTime(DonorAPI.COLLECT_START);
				resultMap.put(DonorAPI.COLLECT_START, createdFound.toString());
			}

			if (fieldNeeded.contains(DonorAPI.COLLECT_END)) {
				Time createdFound = rs.getTime(DonorAPI.COLLECT_END);
				resultMap.put(DonorAPI.COLLECT_END, createdFound.toString());
			}

			if (fieldNeeded.contains(DonorAPI.VERSION)) {
				int versionFound = rs.getInt(DonorAPI.VERSION);
				resultMap.put(DonorAPI.VERSION, Integer.toString(versionFound));
			}
			
			if (fieldNeeded.contains(DonorAPI.GROUP_NAME)) {
				String externalIdFound = rs.getString(DonorAPI.GROUP_NAME);
				resultMap.put(DonorAPI.GROUP_NAME, externalIdFound);
			}
			
			if (fieldNeeded.contains(DonorAPI.RULES)) {
				String rulesFound = rs.getString(DonorAPI.RULES);
				resultMap.put(DonorAPI.RULES, rulesFound);
			}
		}

		return resultMap;
	}


	private static int modifyStatement(PreparedStatement statement, int qMarkAmount,
			Map<String, String> inputMap) throws SQLException {
		Set<String> currentKeys = inputMap.keySet();

		for (Iterator<String> iterator = currentKeys.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			if (key.equalsIgnoreCase(EXTERNAL_ID) || key.equalsIgnoreCase(RULES)) {
				statement.setString(qMarkAmount + 1, inputMap.get(key));
			} else if (key.equalsIgnoreCase(ORG_ID) || key.equalsIgnoreCase(ID)   
					|| key.equalsIgnoreCase(VERSION)) {
				statement.setInt(qMarkAmount + 1, Integer.parseInt(inputMap.get(key)));
			} else if (key.equalsIgnoreCase(POST_BY) 
					|| key.equalsIgnoreCase(COLLECT_START) || key.equalsIgnoreCase(COLLECT_END)) {
				statement.setTime(qMarkAmount + 1, Time.valueOf(inputMap.get(key)));
			}
			qMarkAmount = qMarkAmount + 1;
		}
		LOG.info("Current statement " + statement.toString());
		LOG.info("Current index value: " + qMarkAmount);

		return qMarkAmount;
	}
	
	
}

