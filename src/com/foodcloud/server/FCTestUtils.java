package com.foodcloud.server;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * FoodCloud Test Utils 
 * This class is wrapping all the utilities method - Date and time related ones especially
 * Rules: 
 *  current copia day = calendar_day -1 if < 12:00 local time
 *  current copia day = calendar_day  if > 12:00 local time
 * 
 * @author Viviana
 */
public class FCTestUtils {

	public static String UTC = "Z";
	public static String AUSTRALIA_SYDNEY = "Australia/Sydney";
	public static String EUROPE_DUBLIN = "Europe/Dublin";
	
	public static enum TYPE { DATE, TIME};
	
	static final Logger LOG = LogManager.getLogger(FCTestUtils.class.getName());
	
	public static int getCurrentCopiaDay() {
		
		LocalDateTime currentTime = LocalDateTime.now();
		
		return getCurrentCopiaDay(currentTime.atZone(ZoneId.of("Z")));
	}
	
	
	public static String getCurrentDayAsString(ZoneId zone) {

		ZonedDateTime currentDateTime = convertToTimeZonedDate(getUTCDate(), zone);
		
		String dayOfWeekForDb = getDayAsString(currentDateTime.getDayOfWeek().getValue());
				
		return dayOfWeekForDb;
	}	
	
	public static int getCurrentDay(ZoneId zone) {

		ZonedDateTime currentDateTime = convertToTimeZonedDate(getUTCDate(), zone);
		
		int dayOfWeekAsInt = currentDateTime.getDayOfWeek().getValue();		
				
		return dayOfWeekAsInt;
	}	

	
	public static String getCurrentCopiaDayAsString(ZoneId zone) {

		ZonedDateTime currentDateTime = convertToTimeZonedDate(getUTCDate(), zone);
		
		int day = getCurrentCopiaDay(currentDateTime);

		LOG.debug("getCurrentCopiaDayAsString- ZonedDateTime: " + currentDateTime);
		LOG.debug("getCurrentCopiaDayAsString- Copia Day: " + day);

		System.out.println("DEBUG - getCurrentCopiaDayAsString- ZonedDateTime: " + currentDateTime);
		System.out.println("DEBUG - getCurrentCopiaDayAsString- Copia Day: " + day);
		
		return getDayAsString(day);
	}

	
	
	public static int getCurrentCopiaDay(ZoneId zone) {

		ZonedDateTime currentDateTime = convertToTimeZonedDate(getUTCDate(), zone);
		
		int day = getCurrentCopiaDay(currentDateTime);

		System.out.println("DEBUG - getCurrentCopiaDay - ZonedDateTime: " + currentDateTime);
		System.out.println("DEBUG - getCurrentCopiaDay - Copia Day: " + day);
		
		return day;
	}

	
	
	public static int getCurrentCopiaDay(ZonedDateTime inputDateTime) {
		
		ZonedDateTime localDayAtMidday = ZonedDateTime.of(inputDateTime.getYear(), inputDateTime.getMonth().getValue(), inputDateTime.getDayOfMonth(), 12, 00, 00, 00, inputDateTime.getZone());
		
		int dayOfWeekAsInt = inputDateTime.getDayOfWeek().getValue();		
		DayOfWeek dayOfWeekAsString = inputDateTime.getDayOfWeek();

		System.out.println("DEBUG - ZonedDateTime: " + inputDateTime);
		System.out.println("DEBUG - Day of Week : " + dayOfWeekAsString.toString());
		System.out.println("DEBUG - Day of Week for DB  : " + dayOfWeekAsInt);

				
		if(inputDateTime.isBefore(localDayAtMidday)) {
			if(dayOfWeekAsInt == 1) {
				// double check for Monday / Sunday	
					dayOfWeekAsInt = 7;
					System.out.println("DEBUG - before local midday  : " + dayOfWeekAsInt);

					return dayOfWeekAsInt;
				} else {
					
					dayOfWeekAsInt = dayOfWeekAsInt - 1;
					System.out.println("DEBUG - before local midday  : " + dayOfWeekAsInt);

					return dayOfWeekAsInt;  
				}			
			} else {
				System.out.println("DEBUG - After local midday  : " + dayOfWeekAsInt);

				return dayOfWeekAsInt;
		}
	}


	public static ZonedDateTime convertToUTCDate(ZonedDateTime dateTime) {
		
		 System.out.println("Original OffsetDateTime:"  + dateTime);
	     System.out.println("Destination Time Zone:"  + ZoneId.of("Z") );
		
		 ZonedDateTime convertedUTCDateTime    = dateTime.toInstant().atZone(ZoneId.of("Z"));
		
		 return convertedUTCDateTime;
	}
	
	public static ZonedDateTime convertToTimeZonedDate(OffsetDateTime UTCDate, ZoneId zoneId ) {
//	    System.out.println("Original OffsetDateTime:  "  + UTCDate);
//	    System.out.println("Destination Time Zone:  "  + zoneId );
	
		ZonedDateTime convertedDateTime    = UTCDate.atZoneSameInstant(zoneId);	   
	
		return 	convertedDateTime;
	}

	public static ZoneId getZoneId(String zoneCode) {
		return  ZoneId.of(zoneCode);		
	}	

	public static String formatDateToCopiaUI(ZonedDateTime unformattedDate, TYPE formattingType) {

		DateTimeFormatter DateFormatter =  getCopiaUIDateFormatter();
		DateTimeFormatter TimeFormatter = getCopiaUITimeFormatter();

		String formattedDateTime ="x";
		switch(formattingType) {
		case DATE:			
			formattedDateTime =  unformattedDate.format(DateFormatter);
			System.out.println("Date: "+ formattedDateTime );		
			break;
			
		case TIME:			
			formattedDateTime = unformattedDate.format(TimeFormatter);
			
		System.out.println("Time: "+ formattedDateTime );
		break;
		}
		return formattedDateTime;
	}



	public static OffsetDateTime getUTCDate(LocalDateTime time) {

		OffsetDateTime utcDateFromInput = time.atZone(FCTestUtils.getZoneId(FCTestUtils.UTC)).toOffsetDateTime();

		return utcDateFromInput;
	}	

	
	public static OffsetDateTime getUTCDate() {

		Instant instant = Instant.now();
		OffsetDateTime utcDateFromInstant = instant.atOffset(ZoneOffset.UTC);

		return utcDateFromInstant;
	}	

	public static LocalDateTime getLocalDateTime() {

		LocalDateTime localDateTime = LocalDateTime.now(); 

		return localDateTime;
	}	

	
	public static DateTimeFormatter getCopiaUIDateFormatter() {

		return DateTimeFormatter.ofPattern("yyyy-MM-dd");
	}	


	public static DateTimeFormatter getCopiaUITimeFormatter() {

		return DateTimeFormatter.ofPattern("HH:mm:ss");
	}	

	private static String getDayAsString(int day_as_int) {

	String todayUI="";
		switch (day_as_int) {

		case 1:
			todayUI= "Monday";
			break;
		case 2:
			todayUI= "Tuesday";
			break;

		case 3:
			todayUI= "Wednesday";

			break;
		case 4:
			todayUI= "Thursday";
			break;

		case 5:
			todayUI= "Friday";
			break;

		case 6:
			todayUI= "Saturday";
			break;

		case 7:
			todayUI= "Sunday";
			break;

		}
		System.out.println("DEBUG - getDayAsString: " + todayUI);

		return todayUI;
	}


	public static String CopiaUITime_Substring(String TimeFromDB) {
		
		return ((TimeFromDB.length() > 0) ? TimeFromDB.substring(0, 5) : TimeFromDB );
	}	

	
	
	
	
}
