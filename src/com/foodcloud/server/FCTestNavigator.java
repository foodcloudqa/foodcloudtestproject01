package com.foodcloud.server;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

/**
 * move in this class all utilities (Wrapping Selenium webdriver) (wrapper class) 
 * Rename as FoodCloudServer 
 * @author Viviana
 *
 */
public class FCTestNavigator {

	private FCTestServer server;
	private static String CONFIG_FILE_PATH = "configuration.properties";
	private String webDriverType; 
	private String webDriverPath; 

	private static String OS = System.getProperty("os.name").toLowerCase();
	
//	private 
	
	public FCTestServer getServer() {
		return server;
	}

	public WebDriver getDriver() {
		return server.getDriver();
	}

	public void setup() {
 // 	readFile(CONFIG_FILE_PATH);
		String browser = "googleChrome";
//		String browser = "android";
					
		// setting webdriver
		WebDriver driver = null;
		switch (browser) {
		case "googleChrome":
			// setting properties first
			webDriverType = "webdriver.chrome.driver";
			
			String defaultWebDriverPath = "path";
			String currentDriverDir = System.getenv("CHROMEDRIVER_DIR");   

			//OS checking
			System.out.println("System or OS is:" + OS );
			
			if(OS.contains("linux")) {
				defaultWebDriverPath = "/home/rof/bin/chromedriver";
				webDriverPath = currentDriverDir + "/chromedriver";
				
			} 
			if(OS.contains("mac")) {
				defaultWebDriverPath = "/Users/vivianamarra/Documents/Webdriver/chromedriver";
				webDriverPath = currentDriverDir + "/chromedriver";
				
			} 
			if(OS.contains("windows")) {
				//windows
				webDriverPath = currentDriverDir + "\\chromedriver.exe";
				defaultWebDriverPath = "C:/WebDriver/chromedriver/chromedriver.exe";
			 } 
			
		
			if (currentDriverDir != null ) {
				System.setProperty(webDriverType, webDriverPath);
			}
			else{
				System.setProperty(webDriverType, defaultWebDriverPath);
			}
			
			ChromeOptions options = new ChromeOptions();
	//		options.addArguments("--start-maximized");
			options.addArguments("--window-size=1920,1080");
			
			
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled", false);
			options.setExperimentalOption("prefs", prefs);

			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			driver = new ChromeDriver(capabilities);
			
			break;

		case "firefox":
			driver =  new FirefoxDriver();
			break;

		case "explorer":
			webDriverType = "webdriver.ie.driver";
			webDriverPath = "C:/WebDriver/iexploredriver.exe";

			System.setProperty(webDriverType, webDriverPath);

			driver = new InternetExplorerDriver();
			break;
			
		case "android":
			  File appDir = new File("C:/Users/Viviana/Downloads/fc_app/");
		      File app = new File(appDir, "app-debug.apk");

			// real device
			DesiredCapabilities mobileCapabilities = new DesiredCapabilities();
			mobileCapabilities.setCapability("deviceName", "9014e052");
			// mandatory capabilities
			mobileCapabilities.setCapability("platformVersion", "4.4.2");
			mobileCapabilities.setCapability("platformName", "Android");

			mobileCapabilities.setCapability("app", app.getAbsolutePath());
		      try {

		    	  URL url = new URL("http://127.0.0.1:4723/wd/hub"); 
				driver = new AndroidDriver<MobileElement>(url,mobileCapabilities);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}

		default:
			break;
		}
				
		server = new FCTestServer(driver);
	}

	/**
	 * Read work in progress 
	 * @throws IOException 
	 */	
	private void readFile2(String propFileName) throws IOException {	
	Properties prop = new Properties();

	InputStream inputStream = getClass().getResourceAsStream(propFileName);

	if(inputStream != null) {
	  prop.load(inputStream);
	} else {
		throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");

	}
	Date time = new Date(System.currentTimeMillis());
}	
	
	private void readFile(String file) {
		//File file = new File(CONFIG_FILE_PATH);
		
		FileInputStream stream = null;
		try {
			stream = new FileInputStream(file);
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		
		BufferedInputStream buffer = new BufferedInputStream(stream);
		
			try {
				int temp = buffer.read();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
				
	}
		
	
}
