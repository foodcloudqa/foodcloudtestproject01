package com.foodcloud.server;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.json.JSONObject;

/**
 * FoodCloud Test  Json Utils 
 * This class is wrapping all the utilities method linked to the json file  
 * @author Viviana
 */
public class FCTestJsonUtils {

	private static String copiaSmsType ="org.foodcloud.domain.share.ShortMessage";
	private static String plazaOfferedDonation ="org.foodcloud.domain.share.OfferedDonation";
	private static String minaDonationEvent = "org.foodcloud.mina.model.DonationEvent";
	
/**
 * This method is required for Json or donation creation
 * @return UUID
 */
	public static UUID generateUUID() {

	    return UUID.randomUUID();
	}	

	/**
	 * 
	 * @param donorName
	 * @param donorGroup
	 * @param donorNumber
	 * @param date
	 * @param cat_code
	 * @param quantity
	 * @return JSON object to be included in a file
	 */
	public static JSONObject createJsonDonationObject(String donorName, String donorGroup, int donorNumber, String date,
			String cat_code, double quantity) {
		JSONObject MainObject = new JSONObject();

		MainObject.put("$type", "org.foodcloud.domain.share.OfferedDonation");
		MainObject.put("donationId", FCTestJsonUtils.generateUUID().toString());
		MainObject.put("donorName", donorName);

		JSONObject externalIdElements = new JSONObject();
		externalIdElements.put("group", donorGroup);
		externalIdElements.put("number", donorNumber);

		MainObject.accumulate("donorExternalId", externalIdElements);

		JSONObject dateElements = new JSONObject();
		dateElements.put("value", date);

		MainObject.accumulate("date", dateElements);

		MainObject.put("category", cat_code);
		MainObject.put("quantity", quantity);

		return MainObject;
	}

	public static JSONObject createJsonDonationForPlaza(Map<String, String> data) {
		JSONObject MainObject = new JSONObject();

		String plazaType = (String) data.get("type");
		switch(plazaType) {
		case "OfferedDonation":
			MainObject.put("$type", plazaOfferedDonation);
			break;

		default:
			break;	
		} 
		
		MainObject.put("donationId", data.get("donationId"));
		MainObject.put("donorName", data.get("donorName"));

		JSONObject externalIdElements = new JSONObject();
		
		externalIdElements.put("group", data.get("donorGroupCode"));
		externalIdElements.put("number", data.get("donorGroupNumber"));

		MainObject.accumulate("donorExternalId", externalIdElements);

		JSONObject dateElements = new JSONObject();
		dateElements.put("value", data.get("formattedDateTime"));

		MainObject.accumulate("date", dateElements);
		
		MainObject.put("category", data.get("foodCategory"));
		Double quantity = Double.valueOf((String) data.get("foodQuantity"));
		MainObject.put("quantity", quantity ); 

		return MainObject;
	}


	public static JSONObject createJsonDonationForAzure(Map<String, String> data) {
		JSONObject azureJsonObject = new JSONObject();
		
		azureJsonObject.put("MessageType", data.get("MessageType"));
		azureJsonObject.put("DonationId", data.get("DonationId"));
		azureJsonObject.put("Retailer", data.get("Retailer"));
		azureJsonObject.put("StoreNumber", data.get("StoreNumber"));
		azureJsonObject.put("Date", data.get("Date"));
		azureJsonObject.put("StoreNumber", data.get("StoreNumber"));
		azureJsonObject.put("Category", data.get("Category"));

		Integer quantityInt = Integer.valueOf((String) data.get("Quantity"));
		Double quantityDouble = Double.parseDouble((String) data.get("QuantityOffered"));		
		DecimalFormat df = new DecimalFormat("0.0");
	    String formattedQuantityDouble = df.format(quantityDouble);

		azureJsonObject.put("Quantity", quantityInt);
		azureJsonObject.put("QuantityOffered", formattedQuantityDouble);

		return azureJsonObject;
	}

	/**
	 * Communication Azure Bus (Tesco) - FoodCloud: special format for unallocated donations
	 * was createFCAcceptedDonationJsonObject
	 * @param externalElements
	 * @return
	 */
	public static JSONObject createJsonDonationFCUnallocated(Map<String, String> externalElements) {

		JSONObject mainUnallocatedDonationJsonObject = new JSONObject();
		
		mainUnallocatedDonationJsonObject.put("MessageType", externalElements.get("MessageType"));
		mainUnallocatedDonationJsonObject.put("DonationId",  externalElements.get("DonationId"));

		return mainUnallocatedDonationJsonObject;

	}
	

	/**
	 * Communication Azure Bus (Tesco) - FoodCloud: special format for Accepted donations
	 * was  createFCAcceptedDonationJsonObject
	 * @param externalElements
	 * @param internalElements
	 * @return
	 */
	public static JSONObject createJsonDonationFCAccepted(Map<String, String> externalElements, Map<String, String> internalElements ) {
		
			JSONObject MainObject = new JSONObject();
			
			MainObject.put("MessageType", externalElements.get("MessageType"));
			MainObject.put("DonationId",  externalElements.get("DonationId"));
			MainObject.put("AcceptanceId", externalElements.get("AcceptanceId"));
			MainObject.put("Charity", externalElements.get("Charity"));
			MainObject.put("CollectionETA", externalElements.get("CollectionETA"));
						
			
			//internalElements
			List<JSONObject> categoryElementsList = new ArrayList<JSONObject>();

			for (Iterator<String> iterator = internalElements.keySet().iterator(); iterator.hasNext();) {
				String foodCategory = (String) iterator.next();
					JSONObject categoryElement = new JSONObject();
					
					categoryElement.put("CategoryName", foodCategory);  
					categoryElement.put("Quantity", Integer.parseInt(internalElements.get(foodCategory)));
					
					categoryElementsList.add(categoryElement);
			} 
			
			for (Iterator<JSONObject> iterator = categoryElementsList.iterator(); iterator.hasNext();) {
				JSONObject jsonObject = (JSONObject) iterator.next();
				
				MainObject.accumulate("CategoryQuantity", jsonObject);			
			}
			
			return MainObject;
		}
	
	
/**
 * returns a random food category string code that can be used to communicate with Plaza 
 * @return
 */
	public static String getRandomFCFoodCategory() {
		List<String> copiaFoodCategories = getCopiaFoodCategories();
		int pickedCategory = getRandomInt(14);
		
		return copiaFoodCategories.get(pickedCategory);
	}
		

	private static List<String> getAzureFoodCategories() {
		List<String> azureFoodCategories = new ArrayList<String>();
		azureFoodCategories.add("BREAD");
		azureFoodCategories.add("BAKERY");
		azureFoodCategories.add("GROCERY");
		azureFoodCategories.add("DAIRY");
		azureFoodCategories.add("CHILLED");
		azureFoodCategories.add("EGGS");
		azureFoodCategories.add("FRUIT AND VEG");
		return  azureFoodCategories ;
	}

	private static List<String> getCopiaFoodCategories() {
		List<String> copiaFoodCategories = new ArrayList<String>();
		copiaFoodCategories.add("RDY");
		copiaFoodCategories.add("GRY");
		copiaFoodCategories.add("NOF");
		copiaFoodCategories.add("AMB");
		copiaFoodCategories.add("CHL");
		copiaFoodCategories.add("DRY");
		copiaFoodCategories.add("FFV");
		copiaFoodCategories.add("CNF");
		copiaFoodCategories.add("CRL");
		copiaFoodCategories.add("BKR");
	//	copiaFoodCategories.add("MTP");  //removing Meat - because it is causing problems
		copiaFoodCategories.add("FSH");
		copiaFoodCategories.add("EGG");
		copiaFoodCategories.add("BEV");
		copiaFoodCategories.add("PRP");

		return  copiaFoodCategories;
	}

	/**
	 * returns a random food category string code that can be used to communicate with Azure Test Bus  
	 * @return
	 */
	public static String getRandomAzureFoodCategory() {
		List<String> azureFoodCategories = getAzureFoodCategories();
			
		int pickedCategory = getRandomInt(3);
		
		return azureFoodCategories.get(pickedCategory);
	}

	public static Double getRandomDouble() {
	
		Random randomSeed = new Random(); 
		 //randomSeed.nextDouble(20d);
		// Formula: 
		// setting the minimum?? 0.5 or 1 ??
		double value = 0.5d + (50d - 0.5d) * randomSeed.nextDouble();
		DecimalFormat df = new DecimalFormat("0.0");
	    String formattedValue = df.format(value);

		return Double.parseDouble(formattedValue);
	}
	
	public static int getRandomInt(int maxValue) {
		
		Random randomSeed = new Random();
		int value = randomSeed.nextInt(maxValue);
		return value;
	}

	public static int getRandomInt(int lowerBound, int maxValue) {

		Random randomSeed = new Random();
		int value = randomSeed.nextInt(maxValue - lowerBound) + lowerBound;
		return value;
	}
	
	
	public static JSONObject createJsonSmsForCopia(Map<String, String> data) {
		JSONObject MainObject = new JSONObject();
		
//		{"$type":"org.foodcloud.domain.share.ShortMessage","text":"Y31","number":{"value":"353871640017"}}
			
		String type = (String) data.get("MessageType");
		switch(type) {
		case "Sms":
			MainObject.put("$type", copiaSmsType);
			break;

		default:
			break;	
		} 
		
		MainObject.put("text", data.get("textAnswer"));

		JSONObject numberElements = new JSONObject();
		numberElements.put("value", data.get("mobileNumber"));

		MainObject.accumulate("number", numberElements);
		

		return MainObject;
	}

	
	
}
