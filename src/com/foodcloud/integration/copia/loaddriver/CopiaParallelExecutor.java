package com.foodcloud.integration.copia.loaddriver;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import org.json.JSONObject;

import com.foodcloud.server.FCTestJsonUtils;
import com.foodcloud.server.FCTestUtils;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.AMQP.BasicProperties;


public class CopiaParallelExecutor {
	
	private ConcurrentHashMap<String, JSONObject> input = new ConcurrentHashMap<>();
	private ConcurrentHashMap<String, JSONObject> output = new ConcurrentHashMap<>();
	private ConcurrentHashMap<String, String> variablesMap;

	 static final Logger LOG = LogManager.getLogger(CopiaParallelExecutor.class.getName());
	 
	public CopiaParallelExecutor(ConcurrentHashMap<String, String> properties) {
		this.variablesMap = properties; 	
		this.variablesMap.put("propertytype", "org.foodcloud.domain.share.OfferedDonation");
		this.variablesMap.put("propertycontenttype", "application/json");
		
		this.variablesMap.put("writingQueueName", "donations-post"); 
		this.variablesMap.put("exchangeName","event-bus");
		this.variablesMap.put("routingKey","events");
		this.variablesMap.put("readingQueueName", "donations-update");

	}



	// writer
	private BiFunction<Timeout, String, Optional<ConcurrentHashMap<String, JSONObject>>> writer(
			Connection con,
			int numIterations
	) {
		return (Timeout withDelay, String segmentName) -> {
			// basic Properties from command line  variables 
			BasicProperties props = CopiaQueueConnector.createProperties(variablesMap);
			
			final String exchangeName = variablesMap.get("exchangeName");
			final String writingQueueName = variablesMap.get("writingQueueName");
			final String writingRoutingKey = variablesMap.get("routingKey");

			final Boolean dbAvailable = Boolean.valueOf(variablesMap.get("dbactive"));

			if(dbAvailable) {
				LoadDriverDBInterface.createConnection();
			}
			
			try {
				IntStream.range(0, numIterations).forEach(n -> {

					try {											
						//testChannelData
						Channel testChannel = con.createChannel();
						testChannel.exchangeDeclare(exchangeName, "topic", true);
						testChannel.queueDeclare(writingQueueName, true, false, false, new HashMap<>());
						testChannel.queueBind(writingQueueName, exchangeName, writingRoutingKey);
						JSONObject generatedInput = generateTestEvent();
						
						String inputDonation = generatedInput.getString("donationId");
						this.input.put(inputDonation, generatedInput);
						
						if (dbAvailable) {
						//writing in the database 
						LoadDriverDBInterface.writeDonationIds("input", inputDonation);
						}						
						
						String segmentMessage =segmentName + ": Writing " + (n + 1) + "/" + numIterations;

						LOG.info(segmentMessage);
						
						testChannel.basicPublish(
								exchangeName,
								writingRoutingKey,
								props,
								generatedInput.toString().getBytes()
						);

						long temp = testChannel.getNextPublishSeqNo();

						LOG.info("checking Next Publish " + temp);

						long messagesAmountBefore =  testChannel.messageCount(writingQueueName);

						long consumersAmountBefore = testChannel.consumerCount(writingQueueName);
						
						
						LOG.info("checking message amount Before " + messagesAmountBefore);

						LOG.info("checking consumers amount Before " + consumersAmountBefore);

						withDelay.hangBack();
						
						long messagesAmountAfter =  testChannel.messageCount(writingQueueName);
						
						LOG.info("checking message amount After " + messagesAmountAfter);
					} catch (Exception e) {
						
						LOG.error("(Writing) Channel Error: " + e);
						LOG.error(e.getLocalizedMessage());

					}

				});
			} catch (Exception e) {
				LOG.error("(Writing) Bifunction Error: " + e);
				LOG.error(e.getLocalizedMessage());

			}
			return Optional.empty();
		};
	}

	
	
	/**
	 * Utility class for controlling some timing related ops.
	 */
	private static abstract class Timeout {

		int amount;
		TimeUnit unit;

		/** Call to execute a wait **/
		public abstract void hangBack() throws InterruptedException;

		static class Constant extends Timeout {
			Constant(final int amount, final TimeUnit unit) {
				this.amount = amount;
				this.unit = unit;
			}
			@Override
			public void hangBack() throws InterruptedException {
				this.unit.sleep(this.amount);
			}
		}

		static class Jittery extends Timeout {

			/** factor by which to blur the wait **/
			int jitterFactor;

			Random randomSeed = new Random();

			Jittery(final int amount, final TimeUnit unit, int jitterFactor) {
				this.jitterFactor = jitterFactor;
				this.amount = amount;
				this.unit = unit;
			}

			@Override
			public void hangBack() throws InterruptedException {
				int maxWait = this.amount + this.jitterFactor;
				int minWait = this.amount - this.jitterFactor;
				int randomWait = randomSeed.nextInt(maxWait - minWait) + minWait;
				this.unit.sleep(randomWait);
			}
		}
	}

	
	
	public boolean executeLoader() {
		
		String uri = variablesMap.get("uri");
		int tps = Integer.parseInt(variablesMap.get("tps")); // Transactions per second (approx)
		int numDonations = Integer.parseInt(variablesMap.get("count"));
		int runWithinTimeMins = Integer.parseInt(variablesMap.get("timeout"));

		
		int numIterationsPerWriter = numDonations / tps;
		ExecutorService ex = Executors.newFixedThreadPool(tps + 1); // + 1 for the reader thread

		Timeout maximumWait = new Timeout.Constant(runWithinTimeMins, TimeUnit.MINUTES);
		Timeout betweenWrites = new Timeout.Jittery(1000, TimeUnit.MILLISECONDS, 500);

		List<Callable<Optional<ConcurrentHashMap<String, JSONObject>>>> writerThreads = new ArrayList<>();

		IntStream.range(0, tps).forEach( n -> {
			writerThreads.add(
				inParallel("Queue Writer " + n, this::writer, uri, numIterationsPerWriter, betweenWrites)
			);
		});

		ArrayList<Callable<Optional<ConcurrentHashMap<String, JSONObject>>>> testSegments = new ArrayList<>();

		testSegments.add(
			inParallel("Queue Read", this::reader, uri, numDonations, maximumWait)
		);

		testSegments.addAll(writerThreads);

			try {
				Instant start = Instant.now();

				parallelTest(testSegments, maximumWait, ex);
				
				Instant end = Instant.now();

				Duration interval = Duration.between(start,end);
				long executionTimeInSeconds = interval.getSeconds();
				long executionTimeInMinutes = interval.toMinutes();
				
				LOG.info("Execution time in seconds:" + executionTimeInSeconds);
				LOG.info("Execution time in minutes:" + executionTimeInMinutes);
				
			} catch (Exception e) {
			String message = "Error Occurred" +  e.getLocalizedMessage();

			LOG.error(message);


		} finally {
			String keyDiffMessage = "Key diff between input/output -> " + Sets.difference(input.keySet(), output.keySet());
			LOG.info(keyDiffMessage);

			String mapDiffMessage ="Map diff between input/output -> " + Maps.difference(input, output);
			LOG.info(mapDiffMessage);
			LOG.info("More on the left = more input or offered donations than output or processed donations ");
			LOG.info("More on the right = more output or processed donations than input or offered donations");

			String inputMessage ="Input was -> " + input;
			LOG.info(inputMessage);

			String outputMessage ="Output was -> " + output;
			LOG.info(outputMessage);


		}

		boolean comparsonResult =  	Stream.of(
				this.input.size() == this.output.size(),
				Collections.list(this.input.keys()).stream().allMatch( id -> this.output.containsKey(id))
			).allMatch(asExpected -> asExpected);

		String message = "Are the 2 sets of ids the same? " + comparsonResult;
		LOG.info(message);

		return comparsonResult;
	}


	

/**
 * Wraps a parallel execution and performs some logging + resource allocation
 *
 * @param segmentName 	Name for this particular segment.  Useful for logging.
 * @param segmentRunner A function containing the execution you would like to run.  You can assume that the
 *                      connection object which the lambda is given is initialized and disposed of safely.
 *
 * @return Each Callable will return the final result of the lambda.
 */
private Callable<Optional<ConcurrentHashMap<String, JSONObject>>>
	inParallel(
			String segmentName,
			BiFunction<Connection, Integer,
					BiFunction<Timeout, String, Optional<ConcurrentHashMap<String, JSONObject>>>> segmentRunner,
			String uri,
			int numIterations,
			Timeout waiter) {

	return () -> {
		try (Connection connection = CopiaQueueConnector.createAMPConnection(uri)) {
			LOG.info(segmentName + " ~ Process >>" + " EXECUTION STARTING.");
			Optional<ConcurrentHashMap<String, JSONObject>> segmentResult =
					segmentRunner.apply(connection, numIterations).apply(waiter, segmentName);
			LOG.info(segmentName + " ~ Process >>" + " EXECUTION FINISHED.");
			return segmentResult;
		}
	};
}

/**
 * Runs a list of async executions and assumes control of a semaphore permit for each async op.
 * When each execution is finished, the semaphore permits are returned.  This effectively blocks
 * until all of our async ops are run.
 *
 * @param testSegments The test operations which are required to be run parallel.
 */
private void parallelTest(List<Callable<Optional<ConcurrentHashMap<String, JSONObject>>>> testSegments,
						  Timeout withinTime,
						  ExecutorService onExecutor) throws Exception {

	Semaphore synchronizeOn = new Semaphore(testSegments.size());

	try { onExecutor.invokeAll(testSegments).stream().forEach(
		task -> {
			try {
				synchronizeOn.tryAcquire(1, withinTime.amount, withinTime.unit);
				task.get();
			} catch (InterruptedException | ExecutionException e) {

			LOG.error(e.getLocalizedMessage());

			} finally {
				synchronizeOn.release();
			}
		});
	} catch (Exception e){ 			
		LOG.error(e.getLocalizedMessage());
	}

	synchronizeOn.tryAcquire(testSegments.size(), withinTime.amount, withinTime.unit);

}


synchronized private void notifyComplete(Semaphore sync) { sync.release(); }


/**
 * Function which will do the job of reading from the queue.
 * To prevent program termination before all expected messages are consumed,
 * an isolate semaphore is initialized to the size of the input.
 *
 * Make sure to perform bounded waiting, otherwise a deadlock might occur in the test.
 *
 * @param con Connection to a live rabbitmq instance.
 * @return A map of donation_id's to the responses which came out.
 */
private BiFunction<Timeout, String, Optional<ConcurrentHashMap<String, JSONObject>>> reader(Connection con, int numIterations) {
	return (Timeout withinTime, String segmentName) -> {

		Semaphore amountToRun = new Semaphore(numIterations);

		try {
			amountToRun.tryAcquire(numIterations, withinTime.amount, withinTime.unit);
		} catch (Exception e) {
			LOG.error("(Reading) Bifunction Error: " + e);
			LOG.error(e.getLocalizedMessage());
		}

		final String exchangeName = variablesMap.get("exchangeName");
		final String readingQueueName = variablesMap.get("readsame").equalsIgnoreCase("true") ? variablesMap.get("writingQueueName") : variablesMap.get("readingQueueName");
		final Boolean dbAvailable = Boolean.valueOf(variablesMap.get("dbactive"));

		if(dbAvailable) {
			LoadDriverDBInterface.createConnection();
		}

		try {

			Channel testChannel = con.createChannel();
			testChannel.exchangeDeclare(exchangeName, "topic", true);
			testChannel.queueDeclare(readingQueueName, true, false, false, new HashMap<>());
			testChannel.basicConsume(readingQueueName, new DefaultConsumer(testChannel) {

				@Override
				public void handleDelivery(String consumerTag,
										   Envelope envelope,
										   AMQP.BasicProperties properties,
										   byte[] body) throws IOException {

					long deliveryTag = envelope.getDeliveryTag();
					String message = new String(body, "UTF-8");
					JSONObject obj = new JSONObject(message);
					
					String outputDonation = obj.getString("donationId");
					output.put(outputDonation, obj);

						if (dbAvailable) {
							LoadDriverDBInterface.writeDonationIds("output", outputDonation);
						}
					
					String segmentMessage = 
						segmentName + ": Reading -> " +
								output.size() + "/" + numIterations;

					LOG.info(segmentMessage);
					
					this.getChannel().basicAck(deliveryTag, false);
					notifyComplete(amountToRun);
				}

			});

			/* Wait for test completion */
			try {
				amountToRun.tryAcquire(numIterations, withinTime.amount, withinTime.unit);
			} catch (Exception e) {
				LOG.error("(Reading) Error: " + e);
				LOG.error(e.getLocalizedMessage());

			}

		} catch (Exception e) {
			LOG.error("(Reading) Exception: " + e);
			LOG.error(e.getLocalizedMessage());

		}
		return Optional.empty();
	};
}

private JSONObject generateTestEvent() {
	Map<String, String> donationData = new HashMap<>();
	donationData.put("type", "OfferedDonation");
	donationData.put("donationId", FCTestJsonUtils.generateUUID().toString());
	donationData.put("org_id", "13574");
	donationData.put("donorName", "Ankara Spice Store 1");
	ZonedDateTime currentDateTime = FCTestUtils.convertToTimeZonedDate(FCTestUtils.getUTCDate(),
			FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));
	donationData.put("formattedDateTime", currentDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
	donationData.put("donorGroupCode", "FC");
	donationData.put("donorGroupNumber", "90001");
	donationData.put("foodCategory", FCTestJsonUtils.getRandomFCFoodCategory());// minus food categories that are not accepted by the charity
	donationData.put("foodQuantity", Integer.toString(FCTestJsonUtils.getRandomInt(1, 19)));
	return FCTestJsonUtils.createJsonDonationForPlaza(donationData);
}




//private void writingOnCopiaSmsQueue(Connection con){
//	
//	this.variablesMap.put("copiaWritingQueueName", "donations-post"); 
//	this.variablesMap.put("copiaExchangeName","event-bus");
//
//	this.variablesMap.put("routingKey","events");
//	this.variablesMap.put("readingQueueName", "donations-update");
//
//	
//	try {											
//		//testChannelData
//		Channel testChannel = con.createChannel();
//		testChannel.exchangeDeclare(copiaExchangeName, "topic", true);
//		testChannel.queueDeclare(copiaWritingQueueName, true, false, false, new HashMap<>());
//		testChannel.queueBind(writingQueueName, exchangeName, writingRoutingKey);
//		JSONObject generatedInput = generateTestEvent();
//		
//		String inputDonation = generatedInput.getString("donationId");
//		this.input.put(inputDonation, generatedInput);
//		
//		if (dbAvailable) {
//		//writing in the database 
//		LoadDriverDBInterface.writeDonationIds("input", inputDonation);
//		}						
//		
//		String segmentMessage =segmentName + ": Writing " + (n + 1) + "/" + numIterations;
//
//		LOG.info(segmentMessage);
//		
//		testChannel.basicPublish(
//				exchangeName,
//				writingRoutingKey,
//				props,
//				generatedInput.toString().getBytes()
//		);
//
//		long temp = testChannel.getNextPublishSeqNo();
//
//		LOG.info("checking Next Publish " + temp);
//
//		long messagesAmountBefore =  testChannel.messageCount(writingQueueName);
//
//		long consumersAmountBefore = testChannel.consumerCount(writingQueueName);
//		
//		
//		withDelay.hangBack();
//		
//	} catch (Exception e) {
//		
//		LOG.error("(Writing) Channel Error: " + e);
//		LOG.error(e.getLocalizedMessage());
//
//	}
	
// }

	
}
