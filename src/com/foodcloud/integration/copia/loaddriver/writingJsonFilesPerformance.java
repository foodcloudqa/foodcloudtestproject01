package com.foodcloud.integration.copia.loaddriver;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.foodcloud.server.FCTestJsonUtils;
import com.foodcloud.server.FCTestUtils;



public class writingJsonFilesPerformance {

	 static final Logger LOG = LogManager.getLogger(writingJsonFilesPerformance.class.getName());

	public static void main(String[] args) throws IOException {

		//write 50, 100, 200 , 500, 1000

		createStressJson(50, "./Donations_50.json");
		
		createStressJson(100, "./Donations_100.json");
		createStressJson(200, "./Donations_200.json");
		createStressJson(500, "./Donations_500.json");
		createStressJson(1000, "./Donations_1000.json");		
		createStressJson(2000, "./Donations_2000.json");
		
	}
	
	
	private static void createStressJson(int donationAmount, String outputFileName) {
		
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(outputFileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PrintWriter printWriter = new PrintWriter(fileWriter);

		JSONObject jsonObject = null;
		for (int i = 0; i < donationAmount; i++) {
			jsonObject = generateTestEvent();
			printWriter.println(jsonObject.toString());	
		}
		
		printWriter.close();
		
	}


	private static JSONObject generateTestEvent() {
		Map<String, String> donationData = new HashMap<>();
		donationData.put("type", "OfferedDonation");
		donationData.put("donationId", FCTestJsonUtils.generateUUID().toString());
		donationData.put("org_id", "13574");
		donationData.put("donorName", "Ankara Spice Store 1");
		ZonedDateTime currentDateTime = FCTestUtils.convertToTimeZonedDate(FCTestUtils.getUTCDate(),
				FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));
		donationData.put("formattedDateTime", currentDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
		donationData.put("donorGroupCode", "FC");
		donationData.put("donorGroupNumber", "90001");
		donationData.put("foodCategory", FCTestJsonUtils.getRandomFCFoodCategory());// minus food categories that are not accepted by the charity
		donationData.put("foodQuantity", Integer.toString(FCTestJsonUtils.getRandomInt(1, 19)));
		return FCTestJsonUtils.createJsonDonationForPlaza(donationData);
	}

	
	

}
