package com.foodcloud.integration.copia.loaddriver;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.foodcloud.integration.copia.loaddriver.CopiaParallelExecutor;


/**
 * 
 * Generate input messages for Copia (e,g., donations) and publish to the
 * appropriate Metro bus queue Listen on the Metro bus queues for the expected
 * results Use persistent state (e.g., hashmap, in-memory database) to verify
 * that the messages received match expectations
 * 
 * Example:
 * 
 * % copDriver --type Donation --count 10000 --bus
 * "amqp://guest:guest@localhost:5672/"
 * 
 * // create Copia Donations Messages // create Copia Sms Messages // how to
 * compile it (Maven)
 * 
 * // example CopiaLoadDriver --type Sms ---count 1 --bus
 * "amqp://guest:guest@localhost:5672" // example json //The sms is phase II for
 * phase I concentrate on create the message just for copia "autoaccept" //
 * {"$type":"org.foodcloud.domain.share.ShortMessage","text":"Y1","number":{"value":"447825239024"}}
 * 
 * @author Viviana
 *
 */
public class CopiaLoadDriver {

	private static String count;
	private static String messageType;
	private static String validUri;
	private static String transactionsSecond;
	private static String runWithinTimeMins;
	private static String runWithReadSame;

	 private static final Logger LOG = LogManager.getLogger(CopiaLoadDriver.class);
	 
	public static void main(String args[]) throws IOException {

		// creating the command line parser
		CommandLineParser parser = new DefaultParser();

		// creating the Options and the related object
		Options options = new Options();
		options.addOption("type", "type", true, "message type. i.e. Donation, .");
		options.addOption("count", "count", true, "how many messages this tool should create");
		options.addOption("bus", "bus", true, "use the argument as AMPQ uri to connect");
		options.addOption("readsame", "readsame", true, "set to false if the tool should read from a different queue (copia-c)");
		options.addOption("tps", "tps", true, "how many transactionsSecond this tool should create");
		options.addOption("timeout", "timeout", true, "Maximum runtime in Mins");



		// automatically generate the help statement
		HelpFormatter formatter = new HelpFormatter();

		try {
			CommandLine cmd = parser.parse(options, args);

			boolean isTypeSet = cmd.hasOption("type");
			boolean isBusSet = cmd.hasOption("bus");

			// checking mandatory fields
			if (!isTypeSet || !isBusSet) {
				String message = "Missing mandatory fields type or bus uri ";

				LOG.error(message);
				formatter.printHelp("copiaDriver", options);

				System.exit(1);
			} else {

				String type = cmd.getOptionValue("type");

				if (type.equalsIgnoreCase("Donation")) {
					messageType = type;
				} else {
					String typeMessage = "Type not available: use Donation instead of: " + type;

					LOG.error(typeMessage);
					formatter.printHelp("copiaDriver", options);

					System.exit(1);
				}

				boolean isCountSet = cmd.hasOption("count");

				if (!isCountSet) {
					String defaultCount = "Missing count : Using default value = 1";
					LOG.info(defaultCount);

					count = "1";
				} else {					
					// this is mapping numDonations
					String countValue = cmd.getOptionValue("count");

					if (isValidNumber(countValue)) {
						count = countValue;
					} else {
						String messageCountValue = "Value for count not parsable as number. Try 10 instead of: "
								+ countValue;

						LOG.error(messageCountValue);

						formatter.printHelp("copiaDriver", options);

						System.exit(1);
					}
				}

				String busUri = cmd.getOptionValue("bus");

				boolean isValidUri = CopiaQueueConnector.checkConnection(busUri);
				// validate uri , no trial slashes
				if (!isValidUri) {

					String messageUri = "Invalid Uri: Cannot establish a connection with uri: " + busUri;

					LOG.error(messageUri);

					formatter.printHelp("copiaDriver", options);
					System.exit(1);
				} else {
					validUri = busUri;
				}
				//other  variables				
				boolean isTpsSet = cmd.hasOption("tps");

				if (!isTpsSet) {
					String defaultCount = "Missing tps: Using default value = 5";

					LOG.info(defaultCount);

					transactionsSecond = "5";
				} else {
					// this is mapping numDonations
					String tpsValue = cmd.getOptionValue("tps");

					if (isValidNumber(tpsValue)) {
						transactionsSecond = tpsValue;
					} else {
						String messageCountValue = "Value for tps not parsable as number. Try 5 instead of: "
								+ tpsValue;

						LOG.error(messageCountValue);

						formatter.printHelp("copiaDriver", options);

						System.exit(1);
					}

				}

				boolean isTimeoutSet = cmd.hasOption("timeout");

				if (!isTimeoutSet) {
					String defaultCount = "Missing timeout: Using default value = 10";

					LOG.info(defaultCount);

					runWithinTimeMins = "10";
				} else {
					// this is mapping numDonations
					String timeoutValue = cmd.getOptionValue("timeout");

					if (isValidNumber(timeoutValue)) {
						runWithinTimeMins = timeoutValue;
					} else {
						String messageCountValue = "Value for timeout not parsable as number. Try 10 instead of: "
								+ timeoutValue;

						LOG.error(messageCountValue);

						formatter.printHelp("copiaDriver", options);

						System.exit(1);
					}
				}
				boolean isReadsameSet = cmd.hasOption("readsame");

				if (!isReadsameSet) {
					String defaultReadSameMessage = "Missing readsame: Using default value = true";

					LOG.info(defaultReadSameMessage);

					runWithReadSame = "true";
				} else {
					// this is mapping numDonations
					String readSameValue = cmd.getOptionValue("readsame");

					if (readSameValue.equalsIgnoreCase("true") || readSameValue.equalsIgnoreCase("false")) {
						runWithReadSame = readSameValue;
					} else {
						String messageCountValue = "Value for readsame need to be true or false. Not"
								+ readSameValue;

						LOG.error(messageCountValue);

						formatter.printHelp("copiaDriver", options);

						System.exit(1);
					}
				}
				
					
				boolean isDBActive = LoadDriverDBInterface.checkConnection();

				if (!isDBActive) {

					String messageDBActive = "Connection with Database not available. Try turn it on again.";

					LOG.error(messageDBActive);

				}
				else {
					String messageDB = "Connection with Database established. Setting up tables";

					LOG.info("DatabaseConnection : " + messageDB);
					
					LoadDriverDBInterface.removeTable(isDBActive, "ProcessedDonations");
					LoadDriverDBInterface.removeTable(isDBActive, "OfferedDonations");

					LoadDriverDBInterface.createTable(isDBActive, "ProcessedDonations");
					LoadDriverDBInterface.createTable(isDBActive, "OfferedDonations");

				}
				
				// create initial channel property map
				CopiaQueueConnector.getChannelProperties().put("uri", validUri);
				CopiaQueueConnector.getChannelProperties().put("count", count);
				CopiaQueueConnector.getChannelProperties().put("type", messageType);
				CopiaQueueConnector.getChannelProperties().put("tps", transactionsSecond);
				CopiaQueueConnector.getChannelProperties().put("timeout", runWithinTimeMins);
				CopiaQueueConnector.getChannelProperties().put("readsame", runWithReadSame );
				CopiaQueueConnector.getChannelProperties().put("dbactive", Boolean.toString(isDBActive) );
	
								
				ConcurrentHashMap<String, String> properties = CopiaQueueConnector.getChannelProperties();
				
				LOG.info("uri: " + CopiaQueueConnector.getChannelProperties().get("uri") );
				LOG.info("tps: " + CopiaQueueConnector.getChannelProperties().get("tps") );
				LOG.info("numDonations: " + CopiaQueueConnector.getChannelProperties().get("count") );
				LOG.info("runWithinTimeMins: " + CopiaQueueConnector.getChannelProperties().get("timeout") );
				LOG.info("type: " + CopiaQueueConnector.getChannelProperties().get("type") );

				//readsame = true / false 
				LOG.info("readsame: " + CopiaQueueConnector.getChannelProperties().get("readsame") );

				LOG.info("dbactive: " + CopiaQueueConnector.getChannelProperties().get("dbactive") );


				CopiaParallelExecutor pe = new CopiaParallelExecutor(properties);
				boolean result = pe.executeLoader();
				
				if (result) {
					LOG.info("Id Elements are the same");

					System.exit(0);
				} else {
					LOG.info("Id Elements are not the same, check in the logs");

					if (isDBActive) {
						int processed = LoadDriverDBInterface.processedDonations();
						LOG.info("Processed donations from the database " + processed);
					}
					
					System.exit(1);					
				}
			}
		}catch(	ParseException e)
	{

		// check the main arguments
		if (e.getMessage().contains("type")) {
			String messageTypeValue = "Missing type  (i.e. Donation, Sms) ";

			System.out.println(messageTypeValue);
			LOG.error(messageTypeValue);

			formatter.printHelp("copiaDriver", options);
			return;
		}

		if (e.getMessage().contains("bus")) {
			String messageBusValue = "Missing bus uri  ";

			System.out.println(messageBusValue);
			LOG.error(messageBusValue);

			formatter.printHelp("copiaDriver", options);
			return;
		}

		if (e.getMessage().contains("count")) {
			String messageAmountValue = "Missing count value  (i.e. amount of message to generate)";

			System.out.println(messageAmountValue);
			LOG.error(messageAmountValue);

			formatter.printHelp("copiaDriver", options);
			return;
		}

		if (e.getMessage().contains("readsame")) {
			String messageAmountValue = "Missing readsame value  (i.e. true/false)";

			System.out.println(messageAmountValue);
			LOG.error(messageAmountValue);

			formatter.printHelp("copiaDriver", options);
			return;
		}

		String messageUnexpected = "Unexpected Exception: " + e.getMessage();

		System.out.println(messageUnexpected);
		LOG.error(messageUnexpected);
		formatter.printHelp("copiaDriver", options);
		return;
	}
	}


	private static boolean isValidNumber(String optionValue) {
		// check for strings (needed only integer values)

		boolean isANumber = Pattern.matches("[0-9]*", optionValue);

		if (isANumber) {
			return true;
		} else {
			return false;
		}
	}

}
