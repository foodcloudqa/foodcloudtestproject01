package com.foodcloud.integration.copia.loaddriver;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.foodcloud.db.copiaAPI.ConnectionToDb;
import com.foodcloud.db.copiaAPI.OfferAPI;
import com.foodcloud.server.FCTestJsonUtils;
import com.foodcloud.server.FCTestUtils;
//import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.AMQP.BasicProperties;


public class writingSmsCopia {

	 static final Logger LOG = LogManager.getLogger(writingSmsCopia.class.getName());

	public static void main(String[] args) throws KeyManagementException, NoSuchAlgorithmException, URISyntaxException {

		final String uri = "amqp://nafpgjjx:4Zg26SY6ra9dUYtyDYAg3Ldh93Uq15va@squirrel.rmq.cloudamqp.com/nafpgjjx";
		com.rabbitmq.client.Connection con = CopiaQueueConnector.createAMPConnection(uri);
		
		ConcurrentHashMap<String, String> variablesMap = new ConcurrentHashMap<String, String>();
		variablesMap.put("propertytype", "org.foodcloud.domain.share.ShortMessage");
		variablesMap.put("propertycontenttype", "application/json");
		variablesMap.put("copiaWritingQueueName", "sms-receive"); 
		variablesMap.put("copiaExchangeName","event-bus");
	
		variablesMap.put("copiaWritingRoutingKey","events");
	//	variablesMap.put("readingQueueName", "donations-update");
	
		BasicProperties props = CopiaQueueConnector.createProperties(variablesMap);

		
		try {						
			String copiaExchangeName = variablesMap.get("copiaExchangeName");
			String copiaWritingQueueName = variablesMap.get("copiaWritingQueueName");
			String copiaWritingRoutingKey = variablesMap.get("copiaWritingRoutingKey");

			//testChannelData
			Channel testChannel = con.createChannel();
			testChannel.exchangeDeclare(copiaExchangeName, "topic", true);
			testChannel.queueDeclare(copiaWritingQueueName, true, false, false, new HashMap<>());
			testChannel.queueBind(copiaWritingQueueName, copiaExchangeName, copiaWritingRoutingKey);
			JSONObject generatedInput = createSms();
			
//			String inputDonation = generatedInput.getString("donationId");
//			this.input.put(inputDonation, generatedInput);
						
			testChannel.basicPublish(
					copiaExchangeName,
					copiaWritingRoutingKey,
					props,
					generatedInput.toString().getBytes()
			);
	
			long temp = testChannel.getNextPublishSeqNo();
	
			LOG.info("checking Next Publish " + temp);
	
			long messagesAmountBefore =  testChannel.messageCount(copiaWritingQueueName);
	
			long consumersAmountBefore = testChannel.consumerCount(copiaWritingQueueName);
			
			
			//withDelay.hangBack();
			
			if(messagesAmountBefore == 0 ) {
				con.close();
			}
			
		} catch (Exception e) {
			
			LOG.error("(Writing) Channel Error: " + e);
			LOG.error(e.getLocalizedMessage());
		}
		
	}
	
	private static JSONObject createSms() throws IOException, SQLException {	
		FileWriter fileWriter = new FileWriter("./JsonSmsCopia.json");
		PrintWriter printWriter = new PrintWriter(fileWriter);


		Connection dbConnection = ConnectionToDb.getCopiaConnection();

		String todaysDate = FCTestUtils.getLocalDateTime().format(FCTestUtils.getCopiaUIDateFormatter());
		
		String currentValue = OfferAPI.getToken(dbConnection, "13574" , "13571", todaysDate );
		
		Map<String, String>  donationData = new HashMap<String, String>(); 
		
			donationData.put("MessageType", "Sms");				
			donationData.put("textAnswer", "Y" + currentValue);

			donationData.put("mobileNumber","353871640017");

				JSONObject jsonObject = FCTestJsonUtils.createJsonSmsForCopia(donationData);

				printWriter.println(jsonObject.toString());
				System.out.println("JSON Object: " + jsonObject);

			printWriter.close();
			dbConnection.close();
			
			return jsonObject;	

	}
	
	
	private JSONObject generateTestEvent() {
		Map<String, String> donationData = new HashMap<>();
		donationData.put("type", "OfferedDonation");
		donationData.put("donationId", FCTestJsonUtils.generateUUID().toString());
		donationData.put("org_id", "13574");
		donationData.put("donorName", "Ankara Spice Store 1");
		ZonedDateTime currentDateTime = FCTestUtils.convertToTimeZonedDate(FCTestUtils.getUTCDate(),
				FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));
		donationData.put("formattedDateTime", currentDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
		donationData.put("donorGroupCode", "FC");
		donationData.put("donorGroupNumber", "90001");
		donationData.put("foodCategory", FCTestJsonUtils.getRandomFCFoodCategory());// minus food categories that are not accepted by the charity
		donationData.put("foodQuantity", Integer.toString(FCTestJsonUtils.getRandomInt(1, 19)));
		return FCTestJsonUtils.createJsonDonationForPlaza(donationData);
	}

	
	

}
