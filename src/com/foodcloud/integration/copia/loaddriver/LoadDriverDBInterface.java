package com.foodcloud.integration.copia.loaddriver;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.foodcloud.server.FCTestJsonUtils;
import com.foodcloud.server.FCTestUtils;

/**
 * Connection with H2 database 
 * @author Viviana
 *
 */
public class LoadDriverDBInterface {

	private static Connection connection;
	
	static final Logger LOG = LogManager.getLogger(LoadDriverDBInterface.class.getName());

	private static final String dbDefaultDriver  = "org.h2.Driver";
	private static final String dbDefaultUrl  = "jdbc:h2:tcp://localhost/~/test";
	private static final String dbDefaultUsername  = "sa";
	private static final String dbDefaultPassword  = "";

			
	public static Connection createConnection() {

		Connection conn = null;
		try {
			Class.forName(dbDefaultDriver);
			conn = DriverManager.getConnection(dbDefaultUrl, dbDefaultUsername, dbDefaultPassword);

			setConnection(conn);
			return conn;
		} catch (Exception e) {
			LOG.error("Creating connection exception: " + e.getMessage());
			e.printStackTrace();
		}
		return conn;
	}

	
	private static void setConnection(Connection conn) {
		connection = conn;

	}
	
	private static Connection getConnection() {
		return connection;
	}

	public static void createIdDiffTable(Connection con) {
  //   if(enabled){
		final String tableName = "idDonationDifferences";
		
		if(!tableAlreadyExists(tableName)) {
		
		String query = "CREATE TABLE "+tableName+ "(" + "offeredDonation_id varchar(255),"
				+ "processedDonation_id varchar(255)" + ");";

		LOG.info("Before executing query: " + query);
		
		try {
			
			// false positive - change 
			boolean test = con.createStatement().execute(query);

		} catch (SQLException e) {
			LOG.error("Error in executing query:" + query);
			LOG.error("Error message: " + e.getMessage());

			e.printStackTrace();

		}
		}
	}
	
	public static boolean checkConnection() {
		
		boolean isDbActive = false; 
		
		try {
			try {
				Class.forName(dbDefaultDriver);
			} catch (ClassNotFoundException dri) {
				LOG.error("Driver problem: " + dri.getMessage());

			}
			
			DriverManager.getConnection(dbDefaultUrl, dbDefaultUsername, dbDefaultPassword);

			isDbActive = true;
			return isDbActive;
			
		} catch (SQLException e_sql) {
			LOG.error("Connection problem: " + e_sql.getMessage());
		}
		
		return isDbActive;
	}

	public static boolean writeDonationIds(String columntype, String donationId) {
		boolean connection = checkConnection();
		boolean successfulQuery = false;

		if (connection) {
			String tableName = "";
			String query = "";

			switch (columntype) {
			case "input":

				tableName = "offeredDonations".toUpperCase();
				break;
			case "output":
				tableName = "processedDonations".toUpperCase();
				break;

			}
			if (!tableAlreadyExists(tableName)) {
				createTable(connection, tableName);
			}
			String date = FCTestUtils.getLocalDateTime().format(FCTestUtils.getCopiaUIDateFormatter());
			String time = FCTestUtils.getLocalDateTime().format(FCTestUtils.getCopiaUITimeFormatter());

			query = "INSERT INTO " + tableName + " VALUES( '" + date + "', '" + time + "', '" + donationId + "');";

			try {
				successfulQuery = getConnection().prepareStatement(query).execute();
			} catch (SQLException e) {
				LOG.error("getConnection problem " + e.getLocalizedMessage());
				e.printStackTrace();
			}
		}
		return successfulQuery;
	}

	public static void createTable(boolean isConnectionEnabled, String tableName) {
		
		if (isConnectionEnabled == true) {
	
		String query = "CREATE TABLE "+tableName+" (" + "date DATE, time TIME,"
				+ "donation_id varchar(255)" + ");";

		System.out.println("Before executing query: " + query);
		LOG.info("Before executing query: " + query);

		try {
			getConnection().createStatement().execute(query);

		} catch (SQLException e) {
			LOG.error("Error in creating Table:" + e.getLocalizedMessage());
			e.printStackTrace();
		}
		}
	}

	private static boolean tableAlreadyExists(String tableName) {
		
		boolean tableExists = false;
		    DatabaseMetaData dbm = null;
			try {
				dbm = getConnection().getMetaData();
			} catch (SQLException e) {
				LOG.error("Database Metadata error:" + e.getLocalizedMessage());
				e.printStackTrace();
			}
		    ResultSet rs = null;
			try {
				rs = dbm.getTables(null, null, tableName.toUpperCase(), null);
				
			} catch (SQLException e) {
				LOG.error("Database fetching tables error:" + e.getLocalizedMessage());
				e.printStackTrace();
			}
		    try {
				if (rs.next()) {
					tableExists = true;
					LOG.info("Table "+ tableName +" exists"); 
				} else {
					tableExists = false;
					LOG.info("Table "+ tableName +" does not exist"); 
				}
			} catch (SQLException e) {
				LOG.error("Database resultset error:" + e.getLocalizedMessage());
				e.printStackTrace();
			}
		return tableExists ;
	}

	//get processed Donation amount 
	public static int processedDonations() {
		
		String query = "SELECT count(*) FROM OFFEREDDONATIONS "
				+ "INNER JOIN PROCESSEDDONATIONS "
				+ "ON OFFEREDDONATIONS.donation_id =PROCESSEDDONATIONS.donation_id;";

		System.out.println("Before executing query: " + query);
		LOG.info("Before executing query: " + query);

		
		 ResultSet rs = null;
			try {
			rs = getConnection().createStatement().executeQuery(query);

						} catch (SQLException e) {
							LOG.error("Error in creating Table:" + e.getLocalizedMessage());
							e.printStackTrace();
						}
			
			int numberOfDonations = 0;
		    try {
				if (rs.next()) {
					numberOfDonations =  rs.getInt("count(*)");
					LOG.info("Fetching all processed donations: "+ numberOfDonations ); 
				} else {
					LOG.info("Error"); 
				}
			} catch (SQLException e) {
				LOG.error("Database resultset error:" + e.getLocalizedMessage());

			}
		return numberOfDonations ;
	}	
	
	
	
	public static void removeTable(boolean isConnectionEnabled, String tableName) {
		if (isConnectionEnabled == true) {

			setConnection(createConnection());
			
			String query = "DROP TABLE IF EXISTS " + tableName.toUpperCase() + " ;";

			final String logMessage= "Before executing query: " + query;

			LOG.info(logMessage);

			try {
				 Statement stmt = getConnection().createStatement();
			      			 
			     stmt.executeUpdate(query);
				
			} catch (SQLException e) {
				LOG.error("Error in removing Table:" + e.getLocalizedMessage());

			}
		}
	}

	private JSONObject generateTestEvent_fromDB() {
		Map<String, String> donationData = new HashMap<>();
		donationData.put("type", "OfferedDonation");
		donationData.put("donationId", FCTestJsonUtils.generateUUID().toString());
		donationData.put("org_id", "13574");
		donationData.put("donorName", "Ankara Spice Store 1");
		ZonedDateTime currentDateTime = FCTestUtils.convertToTimeZonedDate(FCTestUtils.getUTCDate(),
				FCTestUtils.getZoneId(FCTestUtils.EUROPE_DUBLIN));
		donationData.put("formattedDateTime", currentDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
		donationData.put("donorGroupCode", "FC");
		donationData.put("donorGroupNumber", "90001");
		donationData.put("foodCategory", FCTestJsonUtils.getRandomFCFoodCategory());
		donationData.put("foodQuantity", Integer.toString(FCTestJsonUtils.getRandomInt(1, 19)));
			
		return FCTestJsonUtils.createJsonDonationForPlaza(donationData);
	}
	
	
	
}
