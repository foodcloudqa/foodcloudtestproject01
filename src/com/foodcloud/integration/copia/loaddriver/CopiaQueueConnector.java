package com.foodcloud.integration.copia.loaddriver;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;



/**
 * 
 * Generate input messages for Copia (e,g., donations) and publish to the
 * appropriate Metro bus queue Listen on the Metro bus queues for the expected
 * results Use persistent state (e.g., hashmap, in-memory database) to verify
 * that the messages received match expectations
 * 
 * Example:
 * 
 * % copDriver --type Donation --count 10000 --bus
 * "amqp://guest:guest@localhost:5672/"
 * 
 * // create Copia Donations Messages // create Copia Sms Messages // how to
 * compile it (Maven)
 * 
 * // example CopiaLoadDriver --type Sms ---count 1 --bus
 * "amqp://guest:guest@localhost:5672" // example json //The sms is phase II for
 * phase I concentrate on create the message just for copia "autoaccept" //
 * {"$type":"org.foodcloud.domain.share.ShortMessage","text":"Y1","number":{"value":"447825239024"}}
 * 
 * @author Viviana
 *
 */
public class CopiaQueueConnector {

	public enum QUEUE_TYPE {
		DONATION_POST, DONATION_UPDATE, SMS_SEND, EVENT
	}

	private static String exchangeName;
	private static String routingKey = null;
	private static String queueName = null;

	private static ConcurrentHashMap<String, String> channelProperties = new ConcurrentHashMap(); 
	
	 static final Logger LOG = LogManager.getLogger(CopiaQueueConnector.class.getName());

		
	public static ConcurrentHashMap<String, String> getChannelProperties() {
		return channelProperties;
	}
	
	
	public static boolean checkConnection(String uri) throws IOException{

		ConnectionFactory factory = new ConnectionFactory();
		try {
			factory.setUri(uri);
		} 
		catch (NullPointerException pointer) {
			LOG.error("Malformed url: " + uri);
			LOG.error(pointer.getLocalizedMessage());
						
			return false;
		}

		catch (IllegalArgumentException illegal) {

			LOG.error("Wrong scheme: " + uri);
			LOG.error(illegal.getLocalizedMessage());
						
			return false;
		}

		catch (KeyManagementException | NoSuchAlgorithmException | URISyntaxException e) {

			LOG.error("Error occurred : " + uri);
			LOG.error(e.getLocalizedMessage());
			
			return false;
		}
		return true;
	}	
	
	public static Connection createAMPConnection(String uri)
			throws KeyManagementException, NoSuchAlgorithmException, URISyntaxException {

		ConnectionFactory factory = new ConnectionFactory();
		factory.setUri(uri);
		Connection conn = null;
		try {
			conn = factory.newConnection();
		} catch (IOException | TimeoutException e) {

			LOG.error("Timeout occurred while creating a conection: " + uri);
			LOG.error(e.getLocalizedMessage());

		}
		
		return conn;
	}

	public static Channel createChannel(QUEUE_TYPE qType, String busUri)
			throws IOException, KeyManagementException, NoSuchAlgorithmException, URISyntaxException {
		return createChannel(qType, busUri, null);
	}

	public static Channel createChannel(QUEUE_TYPE qType, String busUri, String exchange)
			throws IOException, KeyManagementException, NoSuchAlgorithmException, URISyntaxException {

		final Connection conn = createAMPConnection(busUri);

		final Channel channel = conn.createChannel();

		switch (qType) {
		case DONATION_POST:
			if (exchange != null) {
				setExchangeName("TestExchange");
				setRoutingKey("TestKey");
			}
			setQueueName("donations-post");
			break;

		case DONATION_UPDATE:
			if (exchange != null) {
				setExchangeName("TestExchange");
				setRoutingKey("TestKey");
			}
			setQueueName("donations-update");
			break;
	
			
		case SMS_SEND:
			setQueueName("sms-send");
			break;

		case EVENT:
			if (exchange != null) {
				setExchangeName(exchange);
				setRoutingKey("event");
			}
			setQueueName("events");
			break;
			
		default:
			break;
		}

		if (exchange != null) {
			//mina example
			channel.exchangeDeclare(exchangeName, "topic", true, false, new HashMap<String,Object>());

			channel.queueBind(getQueueName(), getExchangeName(), getRoutingKey());
		} else {
			channel.queueDeclare(getQueueName(), true, false, false, null);
		}

		return channel;
	}

	public static void writeDonationUseExchange(QUEUE_TYPE queue_type, JSONObject jsonObject, String busUri)
			throws KeyManagementException, NoSuchAlgorithmException, URISyntaxException, IOException, TimeoutException {
		
		final Channel donationChannel = createChannel(queue_type, busUri);

		byte[] messageBodyBytes = jsonObject.toString().getBytes();
		
		BasicProperties properties = createPropertiesAndLog(jsonObject.getString("$type"));

		donationChannel.basicPublish(getExchangeName(), getRoutingKey(), properties, messageBodyBytes);

		donationChannel.close();
	}

	public static void writeDonationUseQueue(QUEUE_TYPE queue_type, JSONObject jsonObject, String busUri)
			throws KeyManagementException, NoSuchAlgorithmException, URISyntaxException, IOException, TimeoutException {

		final Channel donationChannel = createChannel(queue_type, busUri);

		byte[] messageBodyBytes = jsonObject.toString().getBytes();

		BasicProperties properties = createPropertiesAndLog(jsonObject.getString("$type"));
				   
		donationChannel.basicPublish("", getQueueName(), properties, messageBodyBytes);

		System.out.println("Writing data:" + jsonObject.toString());
		donationChannel.close();
	}
	
	/*
	 * Donations-post queue needs to know the message type not in the $type format 
	 */
	private static BasicProperties createPropertiesAndLog(String copiaMessageType) throws IOException {
		BasicProperties.Builder props = new BasicProperties.Builder();
		
		props = props.type(copiaMessageType);
	    props = props.contentType("application/json");
	    props = props.timestamp(new Date());
	    
	   FileOutputStream fi = new FileOutputStream("./queueconnector.properties");
	   fi.write(props.build().toString().getBytes());
	   fi.close();

	   return props.build();
	}

	public static String getExchangeName() {
		return exchangeName;
	}

	private static void setExchangeName(String exchangeName) {
		CopiaQueueConnector.exchangeName = exchangeName;
	}

	public static String getRoutingKey() {
		return routingKey;
	}

	private static void setRoutingKey(String routingKey) {
		CopiaQueueConnector.routingKey = routingKey;
	}

	public static String getQueueName() {
		return queueName;
	}

	private static void setQueueName(String queueName) {
		CopiaQueueConnector.queueName = queueName;
	}

	public static long getMessagesAmount(QUEUE_TYPE donationPost, String busUri)
			throws KeyManagementException, NoSuchAlgorithmException, IOException, URISyntaxException {

		Channel donationChannel = createChannel(donationPost, busUri);

		long messageNumber = donationChannel.messageCount(getQueueName());

		return messageNumber;

	}

	public static long getMessagesAmount(QUEUE_TYPE donationPost, String busUri, String exchange)
			throws KeyManagementException, NoSuchAlgorithmException, IOException, URISyntaxException {

		Channel donationChannel = createChannel(donationPost, busUri, exchange);

		long messageNumber = donationChannel.messageCount(getQueueName());

		return messageNumber;

	}
		
	
	/**
	 * Donations-post queue needs to know the message type not in the $type format 
	 */
	public static BasicProperties createProperties(ConcurrentHashMap<String, String> propertyMap) {

		//for each key that contains property 
		BasicProperties.Builder props = new BasicProperties.Builder();
		
			props = props.type(propertyMap.get("propertytype")); //i.e. OfferedDonation
			props = props.contentType(propertyMap.get("propertycontenttype"));
			props = props.timestamp(new Date());
	    
	   return props.build();
	}

	
}
