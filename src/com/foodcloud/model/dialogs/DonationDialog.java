package com.foodcloud.model.dialogs;

import com.foodcloud.model.pages.Menu;
import com.foodcloud.server.FCTestNavigator;

/**
 * Class that represents the DonationDialog in Copia.
 * Characteristics:
 * 				presence of Donor Search Text field
 * 				presence of (dynamic) Food Categories
 * 				Post Donation and No Donation buttons 
 * 
 * 	//input[@id='donor'] //specialInputTextField    - typeAheadField

 * NOTE: Found solution not really customizable (see list of categories )
 * @author Viviana
 *
 */
public class DonationDialog extends Menu {

	private FCTestNavigator driver;
	private String baseLocator;

	private static String ENABLED_DIALOG_LOCATOR ="//tr[@ng-show='newDonation'][not(@class='ng-hide')]";
	private static String ENABLED_SAVE_BUTTON_LOCATOR ="//button[@id='submit'][@translate='Donate']";
	private static String NO_DONATION_BUTTON_LOCATOR ="//button[@id='none'][@translate='Post_No_Donation']";
	private static String CLOSE_BUTTON_LOCATOR ="//i[@class='fa fa-times']";
	
	private static String ENABLED_TABLE_LOCATOR = "//div[@id='category-table']/table[@class='table-condensed']/tbody/tr";
	
	public static String DONOR_NAME_INPUT = "Donor";
	
	public static String BAKERY_INPUT = "Bakery";
	public static String FRUIT_VEG_INPUT = "Fruit and Veg";
	public static String MEAT_INPUT = "Meat Products";
	public static String GROCERIES_INPUT = "Groceries";
	public static String FISH_INPUT = "Fish";
	public static String CHILLED_PROD_INPUT = "Chilled Products";
	public static String PREP_FOOD_INPUT = "Prepared Food";

	public static String EGG_PROD_INPUT = "Prepared Food";
	public static String DAIRY_INPUT = "Dairy Products";
	public static String NON_FOOD_INPUT = "Non Food";
	public static String CONF_INPUT = "Confectionery";
	public static String BEVERAGES_INPUT = "Beverages";
	public static String CEREALS_INPUT = "Cereals";
	public static String READY_FOOD_INPUT = "Ready Food";
	public static String AMBIENT_INPUT = "Ambient";

	private String DONOR_NAME_LOCATOR = "//input[@id='donor']";
	
	//Temporary lables - waiting for UNIQUE identifiers on UI 
	private String BAKERY_LABEL_LOCATOR = "//tr[@ng-repeat='cat in categories'][1]/td[1]";
	private String BAKERY_LOCATOR = "//tr[@ng-repeat='cat in categories'][1]/td[2]/input";
	private String BAKERY_UNIT_LOCATOR = "//tr[@ng-repeat='cat in categories'][1]/td[3]";
	
	private String FRUIT_VEG_LABEL_LOCATOR = "//tr[@ng-repeat='cat in categories'][2]/td[1]";
	private String FRUIT_VEG_LOCATOR = "//tr[@ng-repeat='cat in categories'][2]/td[2]/input"; // /input is important!!
	private String MEAT_LABEL_LOCATOR = "//tr[@ng-repeat='cat in categories'][3]/td[1]";
	private String MEAT_LOCATOR = "//tr[@ng-repeat='cat in categories'][3]/td[2]/input";
	private String GROCERIES_LABEL_LOCATOR = "//tr[@ng-repeat='cat in categories'][4]/td[1]";
	private String GROCERIES_LOCATOR = "//tr[@ng-repeat='cat in categories'][4]/td[2]/input";
	private String FISH_LABEL_LOCATOR = "//tr[@ng-repeat='cat in categories'][5]/td[1]";
	private String FISH_LOCATOR = "//tr[@ng-repeat='cat in categories'][5]/td[2]/input";
	private String CHILLED_PROD_LABEL_LOCATOR = "//tr[@ng-repeat='cat in categories'][6]/td[1]";
	private String CHILLED_PROD_LOCATOR = "//tr[@ng-repeat='cat in categories'][6]/td[2]/input";
	
	private String PREP_FOOD_LABEL_LOCATOR = "//tr[@ng-repeat='cat in categories'][7]/td[1]";
	private String PREP_FOOD_LOCATOR = "//tr[@ng-repeat='cat in categories'][7]/td[2]/input";


	private String EGG_PROD_LOCATOR = "//tr[@ng-repeat='cat in categories'][8]/td[2]/input";
	private String DAIRY_LOCATOR = "//tr[@ng-repeat='cat in categories'][9]/td[2]/input";
	private String NON_FOOD_LOCATOR = "//tr[@ng-repeat='cat in categories'][10]/td[2]/input";
	private String CONF_LOCATOR = "//tr[@ng-repeat='cat in categories'][11]/td[2]/input";
	private String BEVERAGE_LOCATOR = "//tr[@ng-repeat='cat in categories'][12]/td[2]/input";
	private String CEREALS_LOCATOR = "//tr[@ng-repeat='cat in categories'][13]/td[2]/input";
	private String READY_FOOD_LOCATOR = "//tr[@ng-repeat='cat in categories'][14]/td[2]/input";

	private String AMBIENT_LOCATOR = "//tr[@ng-repeat='cat in categories'][15]/td[2]/input";
	
	/**
	 * constructor that uses driver and locator
	 * @param nav_driver
	 * @param formLocator
	 */
	public DonationDialog(FCTestNavigator nav_driver, String formLocator) {
		super(nav_driver);
		this.baseLocator = formLocator;
		setFields();
	}

	
	/**
	 * sets internal fields 
	 */
	private void setFields() {
		fields.put(DONOR_NAME_INPUT, new TypeAheadTextField(nav, DONOR_NAME_LOCATOR));

		fields.put(BAKERY_INPUT, new InputTextField(nav,BAKERY_LOCATOR));
		fields.put(FRUIT_VEG_INPUT, new InputTextField(nav,FRUIT_VEG_LOCATOR));
		fields.put(MEAT_INPUT, new InputTextField(nav,MEAT_LOCATOR));
		fields.put(GROCERIES_INPUT, new InputTextField(nav,GROCERIES_LOCATOR));
		fields.put(FISH_INPUT, new InputTextField(nav,FISH_LOCATOR));
		fields.put(CHILLED_PROD_INPUT, new InputTextField(nav,CHILLED_PROD_LOCATOR));
		fields.put(PREP_FOOD_INPUT, new InputTextField(nav,PREP_FOOD_LOCATOR));

		// note from here elements are configurable by database table
		fields.put(EGG_PROD_INPUT, new InputTextField(nav,EGG_PROD_LOCATOR));
		fields.put(DAIRY_INPUT, new InputTextField(nav,DAIRY_LOCATOR));
		fields.put(NON_FOOD_INPUT, new InputTextField(nav,NON_FOOD_LOCATOR));

		fields.put(CONF_INPUT, new InputTextField(nav,CONF_LOCATOR));		
		fields.put(BEVERAGES_INPUT, new InputTextField(nav,BEVERAGE_LOCATOR));	
		fields.put(CEREALS_INPUT, new InputTextField(nav,CEREALS_LOCATOR));
		fields.put(READY_FOOD_INPUT, new InputTextField(nav,READY_FOOD_LOCATOR));
		fields.put(AMBIENT_INPUT, new InputTextField(nav,AMBIENT_LOCATOR));

 	}

	/**
	 * Saves and posts the donation by clicking on the button.
	 * The button can be not enabled  
	 * 	// errors?? should it return a flag?? 
	 */
	public void save(){
		System.out.println("Waiting for the Donation button to display");
		server.waitForElement(ENABLED_SAVE_BUTTON_LOCATOR);

		System.out.println("Clicking on Donation button");
		server.click(ENABLED_SAVE_BUTTON_LOCATOR);
		
	}

	/**
	 * waits until the RHS sections appears 
	 */
	public void waitForItemsToDisplay() {
		System.out.println("Waiting for the Donation Table to display");
		
		server.waitForElement(ENABLED_TABLE_LOCATOR);	
		
	}

	/**
	 * waits until the RHS sections appears 
	 */
	public void close() {
		server.click(CLOSE_BUTTON_LOCATOR);

		server.waitForElement("//tbody[@id='don-data'][not(@class='ng-hide')]");	
		
	}


	/**
	 * verifies that the dialog is displayed
	 * returns true if the main locator is found, false otherwise 
	 */
	public boolean isDisplayed() {
			return server.isElementPresent(ENABLED_DIALOG_LOCATOR);
	}
	
	public void postNoDonation(){
		System.out.println("Waiting for the Donation button to display");
		server.waitForElement(NO_DONATION_BUTTON_LOCATOR);

		System.out.println("Clicking on Donation button");
		server.click(NO_DONATION_BUTTON_LOCATOR);
		
	}

}
