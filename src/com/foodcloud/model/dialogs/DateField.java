package com.foodcloud.model.dialogs;

import com.foodcloud.server.FCTestNavigator;

/**
 * This class represents the date field in Copia (search menu) 
 * @author Viviana
 * 2017-08-24
 */
public class DateField extends DialogField {


	/**
	 * constructor that uses driver and locator
	 * @param nav_driver
	 * @param fieldLocator  string representing the container (span) of both calendar button and text area
	 */
	public DateField(FCTestNavigator nav_driver, String fieldLocator) {
		super(nav_driver, fieldLocator);

	}

	
	@Override
	public void setField(String value) {
		String elementLocator ="./span/button";
		System.out.println("Clicking into : " + fieldLocator + " and " + elementLocator);

		server.click(fieldLocator, elementLocator);
		//Todo: deal with the pop up
	}


	/**
	 * Set search date by typing the value string in the text area  
	 * @param value
	 */
	public void setTextValue(String value) {
	
		String elementLocator ="./input";
		System.out.println("Setting text value into : " + fieldLocator + " and " + elementLocator);
		
		server.type(fieldLocator, elementLocator, value);		
	}
	
	/**
	 * returning the placeholder (Internal string describing the purpose of this search field)
	 * Needed for translation tests
	 */
	@Override
	public String getPlaceholder() {
		String elementLocator ="./input";
		System.out.println("picking placeholder from : " + fieldLocator + " and " + elementLocator);
		
		return server.findElement(fieldLocator, elementLocator)
				.getAttribute("placeholder");
	}

}
