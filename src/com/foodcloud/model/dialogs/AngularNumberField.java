package com.foodcloud.model.dialogs;

import com.foodcloud.server.FCTestNavigator;

/**
 * 
 * How to use Javascript to change values: 
 * type in Chrome console:
 document.getElementsByClassName('ng-pristine ng-valid ng-empty ng-valid-min ng-touched')[1].value = "100";
 * */
public class AngularNumberField extends DialogField {

	/**
	 * constructor that uses driver and locator
	 * @param nav_driver
	 * @param fieldLocator
	 */
	public AngularNumberField(FCTestNavigator nav_driver, String fieldLocator) {
		super(nav_driver, fieldLocator);
	}

	@Override
	public void setField(String value) {

		//Not valid:  class keeps changing :( 		
	//	((JavascriptExecutor) server.getDriver()).executeScript(
	//	"document.getElementsByClassName('ng-pristine ng-valid ng-empty ng-valid-min ng-touched')[1].value = '"+value + "';");
	//			
		String classLocator = server.getDriver().findElement(server.getLocatorType(fieldLocator)).getAttribute("class");


		server.click(fieldLocator);		

	//	
		server.getJSDriver().executeScript("document.getElementsByTagName('Input')[15].value = '"+value + "';");
		
//		((JavascriptExecutor) driver).executeScript(
//				"document.getElementsByClassName('"+ classLocator+"')[1].value = '"+value + "';");
//		
//		ng-pristine ng-untouched ng-valid ng-empty ng-valid-min
//		
//		ng-pristine ng-valid ng-empty ng-valid-min ng-touched
			//	\\
		//"alert('hello world');");
		
		
//	server.type(fieldLocator, value);
		// InputTextField.java
		
	}
	
	/**
	 * returning the selected field 
	 */
	@Override
	public String getFieldValue() {
		String currentValue = driver.findElement(server.getLocatorType(fieldLocator)).getAttribute("value");
		System.out.println("Reading attribute value from InputTextField " + fieldLocator + " value: "+ currentValue);
		 return currentValue;
	}
	
		
	/**
	 * returning the placeholder (Internal string describing the purpose of this search field)
	 */
	@Override
	public String getPlaceholder() {
		return server.getDriver().findElement(server.getLocatorType(fieldLocator)).getAttribute("placeholder");
	}

}
