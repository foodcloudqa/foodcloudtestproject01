package com.foodcloud.model.dialogs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.foodcloud.model.tables.DonationRow;
import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;

/**

 * @author Viviana
 *
 */
public class DropDownDialog  {

	private String DETAILS_BODY_LOCATOR = "//div[@class='tab-content clearfix']";
	private String baseLocator = "//div[div[@class='modal-header']]//form[not(contains(@class, 'ng-invalid'))]";
	
	private String DROP_DOWN_LOCATOR;	
	
	private String dropDownButtonLocator;	

	private String  dropdownClosedButtonLocator;	
	private String  dropdownOpenedButtonLocator;	
	
	private String raiseAlertOptionLocator; 	
	private String raiseAlertOptionSuffix = "//div[@class='dropdown open']//ul/li/a[@class='ng-scope'][@translate='Raise_Alert']";	
	
	private String donationCollectedOptionLocator;	
	private String donationCollectedOptionSuffix = "//div[@class='dropdown open']//ul/li/a[@class='ng-scope'][@translate='Donation_Collected']";	

	private String offerCharityOptionLocator;	
	private String offerCharityOptionSuffix = "//div[@class='dropdown open']//ul/li/a[@class='ng-scope'][@translate='Offer_to_Charity']";	

	private String donationAcceptedOptionLocator;	
	private String donationAcceptedOption = "//div[@class='dropdown open']//ul/li/a[@class='ng-scope'][@translate='Donation_Accepted']";	

	private String donationRejectedOptionLocator; 	
	private String donationRejectedOptionSuffix = "//div[@class='dropdown open']//ul/li/a[@class='ng-scope'][@translate='Donation_Rejected']";	

	private String tableLocator = "id=donation-table-container"; 

	private WebDriver driver;
	private FCTestNavigator navigator;
	private FCTestServer server;

	public static enum OPTION_ITEMS { RAISE_ALERT, OFFER_TO_CHARITY, DONATION_ACCEPTED, DONATION_REJECTED, DONATION_COLLECTED  };
	
	
	public DropDownDialog(FCTestNavigator nav, DonationRow donationRow) {
		navigator = nav;
		driver = navigator.getDriver();
		DROP_DOWN_LOCATOR = donationRow.getDropDown(); 
		server = navigator.getServer();
		
		setLocators();
	}

	private void setLocators() {
		dropdownClosedButtonLocator = DROP_DOWN_LOCATOR + "/div[@class='dropdown']/button[@id='actions']";
		dropdownOpenedButtonLocator = DROP_DOWN_LOCATOR + "/div[@class='dropdown open']/button[@id='actions']";
		
		raiseAlertOptionLocator = DROP_DOWN_LOCATOR + raiseAlertOptionSuffix; 
		donationCollectedOptionLocator = 	DROP_DOWN_LOCATOR + donationCollectedOptionSuffix;
		offerCharityOptionLocator= 	DROP_DOWN_LOCATOR + offerCharityOptionSuffix; 
		donationAcceptedOptionLocator = DROP_DOWN_LOCATOR +  donationAcceptedOption ;
		donationRejectedOptionLocator = DROP_DOWN_LOCATOR + donationRejectedOptionSuffix ;
				
	}

	public FCTestNavigator getNavigator() {
		return navigator;
	}


	/**
	 * closes the dialog.
	 */
	public DropDownDialog close(){
		
		if (server.isElementPresent(dropdownOpenedButtonLocator)) {

			System.out.println("Before clicking on close drop down");
			
			server.click(tableLocator);
			
			System.out.println("After clicking on close drop down");

			server.waitForElement(dropdownClosedButtonLocator);
		}
		return this;
	}
	
	
	/**
	 * opens the dialog
	 */
	public DropDownDialog open() {

		// check the status??
		if (!server.isElementPresent(dropdownOpenedButtonLocator)) {
			System.out.println("Before clicking on close drop down");

			server.click(dropdownClosedButtonLocator);
			System.out.println("After clicking on close drop down");

			server.waitForElement(dropdownOpenedButtonLocator);
	//		server.waitForElement("//div[@class='dropdown-backdrop']");
		}

		return this;
	}

	/**
	 * clicks on tab element if displayed, using locator
	 */
	public DropDownDialog clickOptionItem(OPTION_ITEMS option) {

		switch (option) {

		case RAISE_ALERT:
			if (server.isElementPresent(raiseAlertOptionLocator)) {
			driver.findElement(server.getLocatorType(raiseAlertOptionLocator)).click();
//			activeTab = new DetailsTab(detailsLocator, this);
			}
			break;

		case OFFER_TO_CHARITY:
			if (server.isElementPresent(offerCharityOptionLocator)) {
				driver.findElement(server.getLocatorType(offerCharityOptionLocator)).click();
//				activeTab = new DetailsTab(detailsLocator, this);
				}
				break;
		case DONATION_ACCEPTED:
			if (server.isElementPresent(donationAcceptedOptionLocator)) {
				driver.findElement(server.getLocatorType(donationAcceptedOptionLocator)).click();
//				activeTab = new DetailsTab(detailsLocator, this);
				}
				break;

		case DONATION_REJECTED:
			if (server.isElementPresent(donationRejectedOptionLocator)) {
				driver.findElement(server.getLocatorType(donationRejectedOptionLocator)).click();
//				activeTab = new DetailsTab(detailsLocator, this);
				}
				break;
			
		case DONATION_COLLECTED:
			if (server.isElementPresent(donationCollectedOptionLocator)) {
				driver.findElement(server.getLocatorType(donationCollectedOptionLocator)).click();
//				activeTab = new DetailsTab(detailsLocator, this);
				}
				break;
	
			
		}
		return this;
	}

	public List getOptions() {
		String dropLocator = "//div[@class='dropdown open']";
		String linkLocator = "./ul/li/a[@class='ng-scope']";
		
		List optionList = new ArrayList();
		List<WebElement> list = driver.findElement(server.getLocatorType(dropLocator)).findElements(server.getLocatorType(linkLocator));
		for (Iterator<WebElement> iterator = list.iterator(); iterator.hasNext();) {
			WebElement webElement = (WebElement) iterator.next();
			System.out.println("Option: "  + webElement.getText());
			
			optionList.add(webElement.getText());
			
		}
		
		return optionList;
		
	}
	
}
