package com.foodcloud.model.dialogs;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;

import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;

public class DetailsTab extends Tab {

	private String locator;
	private OrgInfoDialog baseDialog;
	private String activePanelLocator = "//div[@id='A']";
	private FCTestNavigator navigator;
	private FCTestServer server;
	private WebDriver driver;

	private String orgNameLocator= "//tr[td[@translate='Name']]/td[2]";
	private String orgAddressLocator="//tr[td[@translate='Address']]/td[2]";
	private String orgContactNameLocator="//tr[td[@translate='Contact_Name']]/td[2]";
	private String orgContactPhoneLocator="//tr[td[@translate='Contact_Phone']]/td[2]";
	private String orgEmailLocator="//tr[td[@translate='Email']]/td[2]";
	private String orgExternalIdLocator="//tr[td[@translate='External_Id']]/td[2]";
	private String orgLocationPostcodeLocator="//tr[td[@translate='LocationPostcode']]/td[2]";
	
	private String orgRegionCodeLocator="//tr[td[@translate='Region_Code']]/td[2]";	
	private String orgGpsCoordinatesLocator="//tr[td[@translate='GPS_Coordinates']]/td[2]";
	private String orgPostByLocator="//tr[td[@translate='Post-by_Time']]/td[2]";	
	private String orgCollectionStartLocator="//tr[td[@translate='Collection_Start']]/td[2]";	
	private String orgCollectionEndLocator="//tr[td[@translate='Collection_End']]/td[2]";
	

	private String orgSupportLocator="//tr[td[@translate='Support']]/td[2]";
	private String orgRatingLocator="//tr[td[@translate='Rating']]/td[2]";

	
	private HashMap<String, String> detailsMap;
	
	public DetailsTab(String inputLocator, OrgInfoDialog orgInfoDialog) {
		this.locator = inputLocator;
		this.baseDialog = orgInfoDialog;
		this.navigator = baseDialog.getNavigator();
		this.server = navigator.getServer();
		this.driver = navigator.getDriver();

		server.waitForElement(activePanelLocator);
	}

	//Read all data in a map?	
	public void read(){
		detailsMap = new  HashMap<String, String>();

		detailsMap.put("Name", driver.findElement(server.getLocatorType(orgNameLocator)).getText());
		detailsMap.put("Address", driver.findElement(server.getLocatorType(orgAddressLocator)).getText());
		detailsMap.put("ContactName", driver.findElement(server.getLocatorType(orgContactNameLocator)).getText());
		detailsMap.put("ContactPhone", driver.findElement(server.getLocatorType(orgContactPhoneLocator)).getText());
		detailsMap.put("Email", driver.findElement(server.getLocatorType(orgEmailLocator)).getText());	
		detailsMap.put("Location", driver.findElement(server.getLocatorType(orgLocationPostcodeLocator)).getText());	

		detailsMap.put("ExternalId", driver.findElement(server.getLocatorType(orgExternalIdLocator)).getText());	

		// note: this is the donor part: missing charity part 
		detailsMap.put("RegionCode", driver.findElement(server.getLocatorType(orgRegionCodeLocator)).getText());
		detailsMap.put("GPSCoordinates", driver.findElement(server.getLocatorType(orgGpsCoordinatesLocator)).getText());
		detailsMap.put("PostBy", driver.findElement(server.getLocatorType(orgPostByLocator)).getText());
		detailsMap.put("CollectionStart", driver.findElement(server.getLocatorType(orgCollectionStartLocator)).getText());
		detailsMap.put("CollectionEnd", driver.findElement(server.getLocatorType(orgCollectionEndLocator)).getText());

		// 
	}
	
	

}
