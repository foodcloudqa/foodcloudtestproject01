package com.foodcloud.model.dialogs;

import org.openqa.selenium.By;

import com.foodcloud.server.FCTestNavigator;

public class SelectField extends DialogField {

	/**
	 * constructor that uses driver and locator
	 * @param nav_driver
	 * @param fieldLocator
	 */
	public SelectField(FCTestNavigator nav_driver, String fieldLocator) {
		super(nav_driver, fieldLocator);
	}

	@Override
	public void setField(String value) {
		// adding a click for 
		server.click(fieldLocator);
		String valueIdentifier = fieldLocator + "//*[text()='"+value+"']";
		if (server.isElementPresent(valueIdentifier)) {
			server.selectVisible(fieldLocator, value);
		} else {
			server.selectValue(fieldLocator, value);

		}
	}
	
	@Override
	public String getFieldValue() {
		 return server.getDriver().findElement(server.getLocatorType(fieldLocator)).getText();
	}
	
	/**
	 * returning the placeholder (Internal string describing the purpose of this search field)
	 */
	@Override
	public String getPlaceholder() {
		 String placeholder = server.getDriver().findElement(server.getLocatorType(fieldLocator)).findElement(By.xpath("./option[1]")).getText();
		 return placeholder;

	}

	
}
