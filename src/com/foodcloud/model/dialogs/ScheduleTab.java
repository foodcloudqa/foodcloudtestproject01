package com.foodcloud.model.dialogs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;


//Schedule Page Weekdays locators
//tr[td[@ng-class='scheduleDow(day)'][contains(text(), 'Monday')]]

//td[@ng-class='scheduleAssignment(day)']


public class ScheduleTab extends Tab {

	private String locator;
	private OrgInfoDialog baseDialog;
	private String activePanelLocator = "//div[@id='B' and @class='tab-pane active in']";
	private String tableLocator = "//table[@st-table='schedule']";
	private FCTestNavigator navigator;
	private FCTestServer server;
	private WebDriver driver;


	public ScheduleTab(String inputLocator, OrgInfoDialog orgInfoDialog) {
		this.locator = inputLocator;
		this.baseDialog = orgInfoDialog;
		this.navigator = baseDialog.getNavigator();
		this.server = navigator.getServer();
		this.driver = navigator.getDriver();

		server.waitForElement(activePanelLocator);
		server.waitForElement(tableLocator);

	}

	public String getScheduledCharity() {

		String scheduledCharityLocator = "//table[@st-table='schedule']//td[@class='schedule-assignment-today']//span[@class='ng-binding']";    
		
		String charityName = driver.findElement(server.getLocatorType(scheduledCharityLocator)).getText();
		return charityName;
	}

	public String getCurrentDayAsString() {

		String todayLocator = "//table[@st-table='schedule']//td[@class='ng-binding schedule-dow-today']";    
		
		String today = driver.findElement(server.getLocatorType(todayLocator)).getText();
		return today.trim();

	}

	public List<ScheduleRow> getAllRows() {

		List<ScheduleRow> internalRows = new ArrayList<ScheduleRow>() ;
		
		String rowsLocator = "//table[@st-table='schedule']/tbody/tr";    
		String weekDayLocator = "./td[1]";
		String infoLocator = "./td[2]";

		List<WebElement> webElementRows = driver.findElements(server.getLocatorType(rowsLocator));
		
		for (Iterator<WebElement> iterator = webElementRows.iterator(); iterator.hasNext();) {
			WebElement webElement = (WebElement) iterator.next();
			
			String weekDay = webElement.findElement(server.getLocatorType(weekDayLocator)).getText();
			String infoDay = webElement.findElement(server.getLocatorType(infoLocator)).getText();
			
			ScheduleRow scheduleRow = new ScheduleRow(weekDay, infoDay );
			
			 internalRows.add(scheduleRow);
		}
		
		return internalRows;

	}

	
	public void close() {
		 String closeButtonLocator = "//div[@modal-render='true'][contains(@style,'display: block;')]//button[@class='close']";
		 server.click(closeButtonLocator);
	
		 server.waitForElement("//body[not(@class='modal-open')]");
	}


}
