package com.foodcloud.model.dialogs;

import org.openqa.selenium.WebDriver;

import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;

/**
 *  Donor and Charity information
 *  
 *  Schedule , Calendar etc 
 *  
 * @author Viviana
 *
 */
public class OrgInfoDialog  {

	private String DETAILS_BODY_LOCATOR = "//div[@class='tab-content clearfix']";
	private String CLOSE_BUTTON_LOCATOR = "//div[div[@class='modal-header']]/div[h4[contains(text(), 'Organization Details')]]/button";
	private String baseLocator = "//div[div[@class='modal-header']]//form[not(contains(@class, 'ng-invalid'))]";

	private String detailsLocator= "//li[not(contains(@class, 'ng-hide'))]/a/div[text()='Details']";
	private String scheduleLocator= "//li[not(contains(@class, 'ng-hide'))]/a/div[text()='Schedule']";
	private String messagesLocator= "//li[not(contains(@class, 'ng-hide'))]/a/div[text()='Notes/Messages/Calls']";
	private String calendarLocator= "//li[not(contains(@class, 'ng-hide'))]/a/div[text()='Calendar']";
	
	private String LINK_LOCATOR;	
	
	private WebDriver driver;
	private FCTestNavigator navigator;
	private FCTestServer server;

	public static enum TAB_ITEMS { DETAILS, SCHEDULE, CALENDAR, NOTES_MESSAGES };
	
	public OrgInfoDialog(FCTestNavigator nav, String linkLocator) {
		
		navigator = nav;
		driver = navigator.getDriver();
		LINK_LOCATOR = linkLocator; 
		server = navigator.getServer();

	}
	
	public FCTestNavigator getNavigator() {
		return navigator;
	}


	/**
	 * closes the dialog.
	 * @return OrgInfo dialog
	 */
	public OrgInfoDialog close(){
		System.out.println("Before clicking on close dialog");

		server.click(CLOSE_BUTTON_LOCATOR);
		System.out.println("After clicking on close dialog");

		server.waitForActionToComplete();
		
		return this;
	}
	
	
	/**
	 * opens the dialog
	 */
	public OrgInfoDialog open(){
		System.out.println("Before clicking on open dialog");
		
		server.click(LINK_LOCATOR);	
		System.out.println("After clicking on open dialog");

		server.waitForElement(baseLocator);
		server.waitForElement(DETAILS_BODY_LOCATOR);
		
		return this;
	}


	/**
	 * clicks on tab element if displayed, using locator
	 */
	public Tab clickTabItem(TAB_ITEMS tabName) {

		Tab activeTab = new Tab();
		
		switch (tabName) {
		case DETAILS:
			driver.findElement(server.getLocatorType(detailsLocator)).click();
			activeTab = new DetailsTab(detailsLocator, this);
			break;

		case SCHEDULE:
			driver.findElement(server.getLocatorType(scheduleLocator)).click();
			activeTab = new ScheduleTab(scheduleLocator, this);
			break;

		case CALENDAR:
			driver.findElement(server.getLocatorType(calendarLocator)).click();
			//activeTab = new CalendarTab(calendarLocator, this);

			break;

		case NOTES_MESSAGES:
			driver.findElement(server.getLocatorType(messagesLocator)).click();
			//activeTab = new NotesTab(messagesLocator, this);
			break;
		}
		return activeTab;
	}

}
