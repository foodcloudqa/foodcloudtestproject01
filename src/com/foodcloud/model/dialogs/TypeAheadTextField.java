package com.foodcloud.model.dialogs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import org.openqa.selenium.WebElement;

import com.foodcloud.server.FCTestNavigator;

/**
 * Class representing drop down with typeahead element on Copia interface.
 * Available for Admin users
 * 
 * @author Viviana
 *
 */
public class TypeAheadTextField extends DialogField {

	
	private String activeDropDownLocator = "//ul[@class='dropdown-menu ng-isolate-scope']"; 
	private String dropDownElementLocator= activeDropDownLocator + "/li/a"; 
	
	
	/**
	 * constructor that uses driver and locator
	 * @param nav_driver
	 * @param fieldLocator
	 */
	public TypeAheadTextField(FCTestNavigator nav_driver, String fieldLocator) {
		super(nav_driver, fieldLocator);
	}

	@Override
	public void setField(String value) {

		String firstCharacters = (String) value.subSequence(0, 2);
		server.type(fieldLocator, firstCharacters );			
		server.waitForElement(activeDropDownLocator);
		
		List<WebElement> elementList = server.getDriver().findElements(server.getLocatorType(dropDownElementLocator)); 
		//list all elements 
		//select the element with 
		List<String> menuItems = new ArrayList<String>();
		for (Iterator iterator = elementList.iterator(); iterator.hasNext();) {
			WebElement webElement = (WebElement) iterator.next();
			menuItems.add(webElement.getText());
		}
		
		int itemsAmount = menuItems.indexOf(value);
		int selectedItem = (itemsAmount >= 0 ) ?  itemsAmount + 1 : + 1;
		
		System.out.println("Attempting to click drop down item on: "+activeDropDownLocator);
		
		server.click(activeDropDownLocator+"/li["+selectedItem+"]");
		///server.click(parentelementlocator);
	}
	
	/**
	 * returning the selected field 
	 */
	@Override
	public String getFieldValue() {
		String currentValue = driver.findElement(server.getLocatorType(fieldLocator)).getAttribute("value");
		System.out.println("Reading attribute value from InputTextField " + fieldLocator + " value: "+ currentValue);
		 return currentValue;
	}
	
		
	/**
	 * returning the placeholder (Internal string describing the purpose of this search field)
	 */
	@Override
	public String getPlaceholder() {
		return server.getDriver().findElement(server.getLocatorType(fieldLocator)).getAttribute("placeholder");
	}

}
