package com.foodcloud.model.dialogs;

import com.foodcloud.server.FCTestNavigator;

public class InputTextField extends DialogField {

	/**
	 * constructor that uses driver and locator
	 * @param nav_driver
	 * @param fieldLocator
	 */
	public InputTextField(FCTestNavigator nav_driver, String fieldLocator) {
		super(nav_driver, fieldLocator);
	}

	@Override
	public void setField(String value) {
		server.type(fieldLocator, value);		
	}
	
	/**
	 * returning the selected field 
	 */
	@Override
	public String getFieldValue() {
		String currentValue = driver.findElement(server.getLocatorType(fieldLocator)).getAttribute("value");
		System.out.println("Reading attribute value from InputTextField " + fieldLocator + " value: "+ currentValue);
		 return currentValue;
	}
	
		
	/**
	 * returning the placeholder (Internal string describing the purpose of this search field)
	 */
	@Override
	public String getPlaceholder() {
		return server.getDriver().findElement(server.getLocatorType(fieldLocator)).getAttribute("placeholder");
	}

}
