package com.foodcloud.model.dialogs;


/**
 *  Pojo ??
 * @author Viviana
 *
 */
public class ScheduleRow  {

	private String weekDay;
	private String scheduledCharity;
	private String collectionWindow;
	
	
	// is closed??? chilled?? frozen???
	
	public ScheduleRow(String weekName, String infoWeekDay) {
		setWeekDay(weekName);
		// need to parse the infoWeekDay
		String charity = infoWeekDay.substring(0, infoWeekDay.indexOf('\n'));
		String collection =infoWeekDay.substring(infoWeekDay.indexOf('\n')+1, infoWeekDay.length());
				
		setScheduledCharity(charity);
		setCollectionWindow(collection);
	}

	public String getWeekDay() {
		return weekDay;
	}
	
	public void setWeekDay(String weekDay) {
		this.weekDay = weekDay;
	}
	public String getScheduledCharity() {
		return scheduledCharity;
	}
	public void setScheduledCharity(String scheduledCharity) {
		this.scheduledCharity = scheduledCharity;
	}
	public String getCollectionWindow() {
		return collectionWindow;
	}
	public void setCollectionWindow(String collectionWindow) {
		this.collectionWindow = collectionWindow;
	}
	
}
