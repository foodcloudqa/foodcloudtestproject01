package com.foodcloud.model.dialogs;

import com.foodcloud.model.pages.Menu;
import com.foodcloud.server.FCTestNavigator;

/**
 * 
 * @author Viviana
 *
 */
public class CharityDialog extends Menu {

	private FCTestNavigator driver;
	private String baseLocator;
	
	//mandatory
	public static  String CHARITY_NAME_INPUT = "Charity Name";
	public static String CONTACT_NAME_INPUT = "Contact Name";
	public static String CONTACT_PHONE_INPUT = "Contact Phone";
	public static String SMS_PHONE_INPUT = "SMS Phone Phone";
	public static String CONTACT_EMAIL_INPUT = "Contact Email";
	public static String ADDRESS_INPUT = "Address";
	public static String LOCATION_POSTCODE_INPUT = "Location/Postcode";
	public static String REGION_CODE_INPUT = "Region Code";
	public static String GROUP_INPUT = "Group";
	public static String EXTERNAL_ID_INPUT = "ExternalId";
///	Region Code** 
//	Isle of Man
	
	private String CHARITY_NAME_LOCATOR = "//input[@id='orgname']";
	private String CONTACT_NAME_LOCATOR = "//input[@id='contactname']";
	private String CONTACT_PHONE_LOCATOR = "//input[@id='phone']";
	private String SMS_PHONE_LOCATOR = "//input[@id='sms']";
	private String CONTACT_EMAIL_LOCATOR = "//input[@id='email']";
	private String ADDRESS_LOCATOR = "//input[@id='address']";
	private String LOCATION_POSTCODE_LOCATOR = "//input[@id='addressCode']";
	private String REGION_CODE_LOCATOR = "//input[@id='region']";

	private String GROUP_LOCATOR = "//select[@id='group']";
	private String EXTERNAL_ID_LOCATOR = "//input[@id='externalid']";

	
	private String DISABLED_SAVE_BUTTON_LOCATOR = "//button[@translate='Save'][@id='submit'][@disabled='disabled']"; 

	private String ENABLED_SAVE_BUTTON_LOCATOR = "//button[@translate='Save'][@id='submit']"; 
	private String ENABLED_CANCEL_BUTTON_LOCATOR = "//button[@translate='Clear'][@id='cancel']";

	/**
	 * constructor that uses driver and locator
	 * @param nav_driver
	 * @param formLocator  String representing any form identifier (id, css, xpath string)  
	 */
	public CharityDialog(FCTestNavigator nav_driver, String formLocator) {
		super(nav_driver);
		this.baseLocator = formLocator;
		setFields();
	}

	private void setFields() {
		fields.put(CHARITY_NAME_INPUT, new InputTextField(nav, CHARITY_NAME_LOCATOR));
		fields.put(CONTACT_NAME_INPUT, new InputTextField(nav,CONTACT_NAME_LOCATOR));
		fields.put(CONTACT_PHONE_INPUT, new InputTextField(nav,CONTACT_PHONE_LOCATOR));
		fields.put(SMS_PHONE_INPUT, new InputTextField(nav,SMS_PHONE_LOCATOR));

		fields.put(CONTACT_EMAIL_INPUT, new InputTextField(nav,CONTACT_EMAIL_LOCATOR));
		fields.put(ADDRESS_INPUT, new InputTextField(nav,ADDRESS_LOCATOR));
		fields.put(LOCATION_POSTCODE_INPUT, new InputTextField(nav,LOCATION_POSTCODE_LOCATOR));
		fields.put(REGION_CODE_INPUT, new TypeAheadTextField(nav,REGION_CODE_LOCATOR));

		fields.put(GROUP_INPUT, new SelectField(nav,GROUP_LOCATOR));
		fields.put(EXTERNAL_ID_INPUT, new InputTextField(nav,EXTERNAL_ID_LOCATOR));

		// searchMenu.addField
 	}

	// errors??
	public void save(){
		System.out.println("Attempting to click on save button");

		server.click(ENABLED_SAVE_BUTTON_LOCATOR);
		
	}

	public void cancel(){
		System.out.println("Attempting to click on cancel button");
		
		server.click(ENABLED_CANCEL_BUTTON_LOCATOR);
		
	}

}
