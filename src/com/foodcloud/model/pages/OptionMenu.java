package com.foodcloud.model.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;

/**
 * Potentially - Admin only menu
 * for 
 * @author Viviana
 *
 */
public class OptionMenu {

	private static String dashboardLocator = "//a[@translate='Dashboard']";
	private static String ticketsLocator = "//a[@translate='Issues']";
	private static String donationsLocator = "//a[@translate='Donations']";
	private static String collectionsLocator = "//a[@translate='Collections']";
	private static String charitiesLocator = "//a[@translate='Charities']";
	private static String donorsLocator = "//a[@translate='Donors']";
	private static String schedulesLocator = "//a[@translate='Schedules']";
	private static String usersLocator = "//a[@translate='Users']";
	private static String actionsLocator = "//a[@translate='Actions']";
	private static String usersLogoutLocator = "//a[@title='Sign-out'][@type='button']";

	private static String adminRHSButtonLocator = "//li/a[@class='dropdown-toggle name-label ng-binding']";
	
	
	private WebDriver driver;
	private FCTestNavigator nav;
	private FCTestServer server;

	public static enum MENU_ITEMS { DASHBOARD, SUPPORT_TICKETS, DONATIONS, COLLECTIONS, DONORS, CHARITIES, SCHEDULES,  USERS, ACTIONS, ADMIN_LOGOUT, IMPACT, SCHEDULE, USER_LOGOUT  };

	public OptionMenu(FCTestNavigator navigator) {
		this.nav = navigator;
		this.driver = nav.getDriver();		
		this.server = nav.getServer();
	}

	public OptionMenu clickItem(MENU_ITEMS item) {
		switch (item) {
		case DASHBOARD:
			server.click(dashboardLocator);
			break;

		case SUPPORT_TICKETS:
			server.click(ticketsLocator);
			break;

		case DONATIONS:
			server.click(donationsLocator);
			break;

		case COLLECTIONS:
			server.click(collectionsLocator);
			break;

		case SCHEDULES:
			server.click(schedulesLocator);
			break;

		case CHARITIES:
			server.click(charitiesLocator);
			break;
			
		case DONORS:
			server.click(donorsLocator);
			break;

		case USERS:
			server.click(usersLocator);
			break;

		case ACTIONS:
			server.click(actionsLocator);
//			setDropDownMenu(new ActionMenu);
			break;

		case ADMIN_LOGOUT:
			//driver.findElement(By.xpath(actionsLocator)).click(); // equivalent
			
			server.click(adminRHSButtonLocator);
			server.waitForElement("//li[@class='dropdown open']");			
			server.click("//a[@translate='Logout']");
			
			break;

		// normal User side	
		case IMPACT:
			server.click("//a[@translate='Impact']");
			break;
				
		case SCHEDULE:
			server.click("//a[@translate='Schedule']");
			break;
			
		case USER_LOGOUT:
			server.click(usersLogoutLocator);
			break;
			
			
		default:
			break;
		}
		
		return this;
	}
	
	
	
	public OptionMenu changeLanguage(String language) {
		
		String option = null;
		switch(language) {
			case "Turkish":
				option="tr";
				break;
			case "English":
				option="en";
				break;
			case "German":
				option="de";
				break;
				
			default: 
				break;
						
		}
				
		server.click("//li/a[@class='dropdown-toggle name-label ng-binding']");
		server.waitForElement("//li[@class='dropdown open']");
		
		server.click("//a[@translate='Account']");
		server.waitForElement("//h4[@translate='Account']");
		
		server.selectValue("//select[@ng-change='changeLanguage()']", option);
		server.waitForActionToComplete();
		
		server.click("//button[@value='Submit']");
		server.click("//button[@class='close']");

		//wait for it to disappear?
		
		return this;
	}

	
	
	public String getItemName(MENU_ITEMS item) {
		String name = null;
		
		switch (item) {
		case DASHBOARD:
			name = driver.findElement(By.xpath(dashboardLocator)).getText();
			break;

		case SUPPORT_TICKETS:
			name =  driver.findElement(By.xpath(ticketsLocator)).getText();
			break;

		case DONATIONS:
			name =  driver.findElement(By.xpath(donationsLocator)).getText();
			break;

		case COLLECTIONS:
			name =  driver.findElement(By.xpath(collectionsLocator)).getText();
			break;

		case SCHEDULES:
			name =  driver.findElement(By.xpath(schedulesLocator)).getText();
			break;

		case CHARITIES:
			name = driver.findElement(By.xpath(charitiesLocator)).getText();
			break;
			
		case DONORS:
			name = driver.findElement(By.xpath(donorsLocator)).getText();
			break;

		case USERS:
			name = driver.findElement(By.xpath(usersLocator)).getText();
			break;

		case ACTIONS:
			name = driver.findElement(By.xpath(actionsLocator)).getText();
			break;
	
		default:
			break;
		}
		
		return name;
	}

	
	
}
