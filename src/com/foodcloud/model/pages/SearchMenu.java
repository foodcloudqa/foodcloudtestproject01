package com.foodcloud.model.pages;

import java.util.HashMap;

import com.foodcloud.server.FCTestNavigator;

/**
 * This class represents the search menus in Copia.
 * Each search menu is enabled by a magnifier glass and presents 2 rows: 
 *  1 for sorting headers and
 *  1 for search fields
 *  
 * @author Viviana
 *
 */
public class SearchMenu extends Menu {

	private String magnifierLocator = "//div[@class='header']/descendant::i[contains(@class, 'fa-search')][not(contains(@class, 'ng-hide'))]";
	private String magnifierMinusLocator = "//div[@class='header']/descendant::i[contains(@class, 'fa-search-minus')][not(contains(@class, 'ng-hide'))]";

	private String hiddenSearchMenuLocator = "//div[div[@class='header']]/table/thead[@class='ng-hide']";
	private String visibleSearchMenuLocator = "//table[@id]/thead[not(@class='ng-hide')]";
	private String clearFilterLocator = "//thead[not(@class='ng-hide')]/descendant::div[@title='Clear filters']/descendant::i[contains(@class, 'fa-filter')]";
	
	protected HashMap<String, String> headers;

//	private FCTestServer server;
	
	public SearchMenu(FCTestNavigator navigator) {
		super(navigator);
		headers = new HashMap<String, String>();
	}

	/**
	 * Opens Search panel 
	 * @return SearchMenu object
	 */
	public SearchMenu open() {

		if(!server.isElementPresent(magnifierMinusLocator)) {
			//search panel is closed
			
			server.click(magnifierLocator);
		
			server.waitForElement(visibleSearchMenuLocator);
		}
		return this;
	}


	public SearchMenu close() {
		if(server.isElementPresent(magnifierMinusLocator)){ 
			server.click(magnifierMinusLocator);
			server.waitForElement(hiddenSearchMenuLocator);	
		}
		return this;
	}


	/**
	 * clicks on the search filter clear button.
	 * @return this object
	 */
	public SearchMenu clear() {
		if(server.isElementPresent(clearFilterLocator )) {
			
			server.click(clearFilterLocator);
				
			server.waitForElement(visibleSearchMenuLocator);
		}
	
		return this;
	}

	/**
	 * add sorting headers
	 * @param headerName String representing header name
	 * @param locator String representing header locator
	 */
	public void addHeader(String headerName, String locator) {
		headers.put(headerName, locator);
	}
	
	public String getHeader(String headerName) {
		String locator = headers.get(headerName);
		
		return server.getDriver().findElement(server.getLocatorType(locator)).getText();
	}
	
	/**
	 * Opens the search panel and clicks on refreshing icon and waits until the spinner appears 
	 * NOTE: Change for not admin user 
	 */
	public void refresh() {
		
		this.open();

		String reloadLocator = "//table[@id='donation-table']/thead/tr[1]/th[@ng-if='!isOrgHome']";
			
		System.out.println("Before clicking on refresh :" + reloadLocator);
		server.waitForElement(reloadLocator);	
		
		server.click(reloadLocator);
		
		String cssSpinner = "css=i.fa-refresh.fa-2x.ng-hide";

		System.out.println("After clicking on refresh");

		server.waitForElement(cssSpinner);	
		server.waitForElement(reloadLocator);	

	}
	
}
