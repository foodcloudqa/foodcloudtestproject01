package com.foodcloud.model.pages;

import com.foodcloud.server.FCTestNavigator;

public class OptionMenuPage extends Page {
	
	protected OptionMenu menu;
	protected String titleLocator;

	public OptionMenuPage(FCTestNavigator nav) {
		super(nav);
		this.menu = new OptionMenu(nav);
		// how can I know if the menu has all buttons or not? isElementPresent? 
	}

	public OptionMenu getMenu() {
		return menu;
	}

	// not used at the moment : need to find a way to deal with not all elements (menu items) displayed
	public void setMenu(OptionMenu menu) {
		this.menu = menu;
	}
	
	/**
	 * Returning page title
	 * NOTE: This can vary from user to user (i.e. Admin Vs charity User)
	 * @return String representing Page Title
	 */
	public String getTitle(){
		return server.getDriver().findElement(server.getLocatorType(getTitleLocator())).getText();
	}


	/**
	 * Setting the title locator
	 * @param title
	 */
	protected void setTitleLocator(String title) {
		this.titleLocator = title;
	}
	
	/**
	 * returns page title locator
	 */
	protected String getTitleLocator() {
		return titleLocator;
	} 

	/**
	 * Waits until the page loads
	 */
	public void waitUntilLoaded() {
		server.waitForElement(getTitleLocator());	
	}

	/**
	 * Checks if page is displayed
	 * @return true if title locator is present, false otherwise
	 */
	public boolean isDisplayed() {
		return server.isElementPresent(getTitleLocator());
	}

	/**
	 * Returns the type of Copia alert if present on screen  
	 * Used by all Copia pages
	 * @return String representing the alert type
	 * 	// wait and get Alert??
	// who is using it? all the pages 

	 */
	public String getAlert() {
		String hiddenAlertLocator = "//div[@role='alert'][@style='display: none;']	";
		String visibleAlertLocator = "//div[@role='alert'][@style='display: block;']";

		String alertPresent = "None";
		if(server.isElementPresent(visibleAlertLocator)) {
			String alertClass = 	driver.findElement(server.getLocatorType(visibleAlertLocator)).getAttribute("id");

			switch (alertClass) {
			case "status-box":
				alertPresent = "Status";
				break;

			case "error-box":
				alertPresent = "Error";
				break;

			case "warning-box":
				alertPresent = "Warning";
				break;

			case "success-box":
				alertPresent = "Success";
				break;
				
			default:
				break;
			}
		}

		server.waitForElement(hiddenAlertLocator);	

		return alertPresent;
	}

	
	public void waitForAlert() {
		String visibleAlertLocator = "//div[@role='alert'][@style='display: block;']";
		server.waitForElement(visibleAlertLocator);	
	}
	
	
	private void changeLanguage(String language) {
		
		String option = null;
		switch(language) {
			case "Turkish":
				//option="value='tr'";
				option="tr";
				break;
			case "English":
				option="en";
				break;
			case "German":
				option="de";
				break;
				
			default: 
				break;
						
		}
				
//Not generic 		server.click("//li/a[text()='Manx Admin']");
		
		server.click("//li/a[@class='dropdown-toggle name-label ng-binding']");
		server.waitForElement("//li[@class='dropdown open']");
		
		server.click("//a[@translate='Account']");
		server.waitForElement("//h4[@translate='Account']");
		
		server.selectValue("//select[@ng-change='changeLanguage()']", option);
		server.waitForActionToComplete();
		//server.click("//option[@translate='Turkish']");
		
		server.click("//button[@value='Submit']");
		server.click("//button[@class='close']");
	}

	
}
