package com.foodcloud.model.pages;

import com.foodcloud.model.dialogs.InputTextField;
import com.foodcloud.server.FCTestNavigator;

/**
 * This class is mapping in our test environment the Donor Organizations Page from Copia 
 * Caracteristics:
 * 					* New donor creation functionality
 * 					* Search for Donor functionality
 * 					* refresh
 * @author Viviana
 *
 */
public class DonorsPage extends SearchPage  {
	
	//public static String STORE_NAME_FIELD_NAME = "Name";	
	public static String LOCATION_FIELD_NAME = "Location";
	public static String REGION_FIELD_NAME = "Region";
	public static String CONTACT_PHONE_FIELD_NAME = "Contact Phone";
	public static String EMAIL_FIELD_NAME = "Email";

	private String STORE_NAME_FIELD_LOCATOR = "//input[@st-search='name']"; //"//input[@st-search='store name']";
	private String STORE_NUM_FIELD_LOCATOR = "//input[@st-search='code']";
	private String REGION_FIELD_LOCATOR = "//input[@st-search='region']";
	private String LOCATION_FIELD_LOCATOR = "//input[@st-search='location']";
	private String CONTACT_PHONE_FIELD_LOCATOR = "//input[@st-search='phone']";
	private String EMAIL_FIELD_LOCATOR = "//input[@st-search='email']";

	public static String RATING_HEADER = "Rating";
	public static String NAME_HEADER = "Name";
	public static String STORE_HEADER = "Store";
	public static String LOCATION_HEADER = "Location";
	public static String REGION_HEADER = "Region";
	public static String PHONE_HEADER = "Contact Phone";
	public static String EMAIL_HEADER = "Email";

	
	private String ratingHeaderLocator = "//th[@translate='Rating']";
	private String nameHeaderLocator = "//th[@translate='Name']";
	private String storeHeaderLocator = "//th[@translate='Store']";
	private String locationHeaderLocator = "//th[@translate='Location']";
	private String regionHeaderLocator = "//th[@translate='Region']";
	private String phoneHeaderLocator = "//th[@translate='Contact_Phone']";
	private String emailHeaderLocator = "//th[@translate='Email']";

	
	public DonorsPage(FCTestNavigator nav_driver) {
		super(nav_driver);
		setupMenuFields();
		
		setTitleLocator("//h2/descendant::span[@translate='Donor_Organizations']");
	}

	
	private void setupMenuFields() {
		searchMenu.addField(DONOR_FIELD_NAME, new InputTextField(nav,STORE_NAME_FIELD_LOCATOR));
		searchMenu.addField(STORE_FIELD_NAME, new InputTextField(nav,STORE_NUM_FIELD_LOCATOR));
		searchMenu.addField(LOCATION_FIELD_NAME, new InputTextField(nav,LOCATION_FIELD_LOCATOR));
		searchMenu.addField(REGION_FIELD_NAME, new InputTextField(nav,REGION_FIELD_LOCATOR));
		searchMenu.addField(CONTACT_PHONE_FIELD_NAME, new InputTextField(nav,CONTACT_PHONE_FIELD_LOCATOR));
		searchMenu.addField(EMAIL_FIELD_NAME, new InputTextField(nav,EMAIL_FIELD_LOCATOR));
		// refresh		
		// clean up 

		searchMenu.addHeader(RATING_HEADER, ratingHeaderLocator);
		searchMenu.addHeader(NAME_HEADER, nameHeaderLocator);
		searchMenu.addHeader(STORE_HEADER , storeHeaderLocator);
		searchMenu.addHeader(LOCATION_HEADER , locationHeaderLocator);
		searchMenu.addHeader(REGION_HEADER, regionHeaderLocator);
		searchMenu.addHeader(PHONE_HEADER , phoneHeaderLocator);
		searchMenu.addHeader(EMAIL_HEADER, emailHeaderLocator);		
	}
		
}