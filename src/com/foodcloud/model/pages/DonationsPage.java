package com.foodcloud.model.pages;

import java.util.Iterator;
import java.util.Map;

import com.foodcloud.model.dialogs.DateField;
import com.foodcloud.model.dialogs.DonationDialog;
import com.foodcloud.model.dialogs.InputTextField;
import com.foodcloud.model.dialogs.SelectField;
import com.foodcloud.model.tables.DonationRow;
import com.foodcloud.model.tables.DonationResultTable;
import com.foodcloud.server.FCTestNavigator;

public class DonationsPage extends SearchPage  {

	protected DonationResultTable resultTable;
	
	public static String COLLECTION_DATE_FIELD_NAME = "Collection Date";
	public static String TABLE_LOCATOR = "//table[@id='donation-table']" ; // "id=donation-table";

	// Locators that are present ONLY on this page
	private String DONATION_STORE_FIELD_LOCATOR = "//input[@st-search='storenum']";
	private String ADMIN_TITLE_LOCATOR = "//span[@translate='Donations']";
	private String ADD_DONATION_LOCATOR = "//i[@class='fa fa-plus ng-scope']";
	
	public static String DATE_HEADER = "Collection Date";
	public static String POSTED_HEADER = "Posted";
	public static String INFO_HEADER = "Info";
	public static String DONOR_HEADER = "Donor";
	public static String STORE_HEADER = "Store number";
	public static String CHARITY_HEADER = "Charity";
	public static String COL_WIN_HEADER ="Collection Window";	
	public static String STATUS_HEADER = "Status";
		
	private String dateHeaderLocator = "//th[@translate='Collection_Date']";
	private String postedHeaderLocator = "//th[@translate='Posted']";
	private String donorHeaderLocator = "//th[@translate='Donor']";
	private String infoHeaderLocator = "//th[@translate='Info']";
	private String collectionHeaderLocator  = "//th[@translate='Collection_Window']";

	private String storeHeaderLocator = "//th[@translate='Store_number']";
	private String statusHeaderLocator = "//th[@translate='Status']";
	private String charityHeaderLocator = "//th[@translate='Charity']";
	
	public DonationsPage(FCTestNavigator nav_driver) {
		super(nav_driver);
		setupMenuFields();
		
		setTitleLocator(ADMIN_TITLE_LOCATOR);
		setResultTable();
	}
	
	private void setupMenuFields() {
		searchMenu.addField(COLLECTION_DATE_FIELD_NAME, new DateField(nav,DATE_FIELD_LOCATOR));
		searchMenu.addField(STORE_FIELD_NAME, new InputTextField(nav,DONATION_STORE_FIELD_LOCATOR));

		searchMenu.addField(DONOR_FIELD_NAME, new InputTextField(nav,DONOR_FIELD_LOCATOR));
		searchMenu.addField(CHARITY_FIELD_NAME, new InputTextField(nav,CHARITY_FIELD_LOCATOR));
		searchMenu.addField(STATUS_FIELD_NAME, new SelectField(nav, STATUS_FIELD_LOCATOR));	
		
		searchMenu.addHeader(DATE_HEADER, dateHeaderLocator);
		searchMenu.addHeader(POSTED_HEADER, postedHeaderLocator);
		searchMenu.addHeader(INFO_HEADER , infoHeaderLocator);
		searchMenu.addHeader(DONOR_HEADER, donorHeaderLocator);
		searchMenu.addHeader(STORE_HEADER , storeHeaderLocator);
		searchMenu.addHeader(CHARITY_HEADER, charityHeaderLocator);		
		searchMenu.addHeader(COL_WIN_HEADER, collectionHeaderLocator);
		searchMenu.addHeader(STATUS_HEADER, statusHeaderLocator);
	}

	private void setResultTable() {
		this.resultTable =new DonationResultTable(nav, TABLE_LOCATOR, this); 
	}
	
	
	// read table again??
	public DonationResultTable getResultTable() {
			return this.resultTable;
	}


	/**
	 * Brings to DonationDialog by clicking on the plus button 
	 * @return DonationDialog Object
	 */
	public DonationDialog clickAddDonation() {

		server.waitForElement(ADD_DONATION_LOCATOR);

		server.click(ADD_DONATION_LOCATOR);

		String formLocator = "//form[@name='newOrgForm']";

		return new DonationDialog(nav, formLocator);  // add this object so that the flow can come back? 
	}

	
	public boolean isSuccessfulReload() {

		if(isSpinnerSpinning()){
			waitAfterSpinning();
		}
		
		DonationResultTable table = getResultTable().read();	
		Map<Integer, DonationRow> firstMapWithIndex = table.getMap();

		searchMenu.refresh();
		
		getResultTable().read();	
		Map<Integer, DonationRow> secondMapWithIndex = table.getMap();

		boolean a =  firstMapWithIndex.equals(secondMapWithIndex);		
		
		return a;
		
//	ResultTable table = donationsPage.getResultTable().load().read();	
//	
//	table.printMap();
//
//	DonationRow row = new DonationRow(table, table.getRow(1));
	}
	

	public boolean isSpinnerSpinning() {
		// without active parent - it changes in the background without being visible	

		String elementLocator ="//i[@class='fa fa-refresh fa-2x ng-hide']";
		return server.isElementPresent(elementLocator);
	}
	
	
	public void waitAfterSpinning() {
		// without active parent - it changes in the background without being visible	
		
		String a ="//i[@class='fa fa-spinner fa-2x fa-spin ng-hide']";

		server.waitForElement(a);
	}
	
	public void printMap() {		
		DonationResultTable table = getResultTable();	
		Map<Integer, DonationRow> mapWithIndex = table.getMap();
		
		for (Iterator<Integer> iterator = mapWithIndex.keySet().iterator(); iterator.hasNext();) {
			Integer currentIndexKey =  (Integer) iterator.next();
			DonationRow currentRow = (DonationRow) mapWithIndex.get(currentIndexKey);
			
			
			System.out.println("Printing map key: " + currentIndexKey);		
			System.out.println("Printing map row: " + currentRow.getRowLocator());
			System.out.print("Printing map row to string: " + currentRow.toString());
			System.out.print("Printing map row date: " + currentRow.getDate());
			System.out.print("Printing map row time: " + currentRow.getTime());
			System.out.print("Printing map row time: " + currentRow.getDonor());
			System.out.print("Printing map row time: " + currentRow.getStatus());
			
			
		}
		
	}
	
}