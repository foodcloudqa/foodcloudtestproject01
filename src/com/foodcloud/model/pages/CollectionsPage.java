package com.foodcloud.model.pages;

import com.foodcloud.model.dialogs.DateField;
import com.foodcloud.model.dialogs.InputTextField;
import com.foodcloud.server.FCTestNavigator;

/**
 * This class is used in the auto tests to map CollectionPage on Copia Desktop.
 * It should reflect all the possible functionalities available. 
 * Functionality list:
 * 					Search for Collection 
 * 					View List of Collections
 * 
 * TODO: Define behaviour when user CANNOT access to this page 
 * @author Viviana
 *
 */
public class CollectionsPage extends SearchPage  {

	private String ADMIN_TITLE_LOCATOR = "//h2//span[@translate='Collections']";

	public static String DATE_FIELD_NAME = "Date";
	public static String COLLECTED_BY_FIELD_NAME = "Collected By";

	// Locators that are present ONLY on this page
	private String COLLECTION_STORE_FIELD_LOCATOR = "//input[@st-search='storenum']";
	private String COLLECTED_BY_FIELD_LOCATOR = "//input[@st-search='collector']";
	
	public static String DATE_HEADER = "Date";
	public static String TIME_HEADER = "Time";
	public static String DONOR_HEADER = "Donor";
	public static String STORE_HEADER = "Store number";
	public static String CHARITY_HEADER = "Charity";
	public static String COLLECTED_HEADER ="Collected By";	
	public static String TOTAL_VALUE_HEADER = "Total Value";
	public static String TOTAL_WEIGHT_HEADER = "Total Weight";
		
	private String dateHeaderLocator = "//th[@translate='Date']";
	private String timeHeaderLocator = "//th[@translate='Time']";
	private String donorHeaderLocator = "//th[@translate='Donor']";
	private String storeHeaderLocator = "//th[@translate='Store_number']";
	private String charityHeaderLocator = "//th[@translate='Charity']";
	private String collectedHeaderLocator = "//th[@translate='Collected_By']";
	private String totalValueHeaderLocator = "//th[@translate='Total Value']";
	private String totalWeightHeaderLocator = "//th[@translate='Total Weight']";

	
	public CollectionsPage(FCTestNavigator nav_driver) {
		super(nav_driver);
		setupMenuFields();
		
		setTitleLocator(ADMIN_TITLE_LOCATOR);
	}

	private void setupMenuFields() {
		
		searchMenu.addField(DATE_FIELD_NAME, new DateField(nav,DATE_FIELD_LOCATOR));
		searchMenu.addField(DONOR_FIELD_NAME, new InputTextField(nav,DONOR_FIELD_LOCATOR));
		searchMenu.addField(STORE_FIELD_NAME, new InputTextField(nav,COLLECTION_STORE_FIELD_LOCATOR));
		searchMenu.addField(CHARITY_FIELD_NAME, new InputTextField(nav,CHARITY_FIELD_LOCATOR));
		searchMenu.addField(COLLECTED_BY_FIELD_NAME, new InputTextField(nav,COLLECTED_BY_FIELD_LOCATOR));
		// total value
		// total weight 
		// refresh
		
		searchMenu.addHeader(DATE_HEADER, dateHeaderLocator);
		searchMenu.addHeader(TIME_HEADER, timeHeaderLocator);
		searchMenu.addHeader(DONOR_HEADER, donorHeaderLocator);
		searchMenu.addHeader(STORE_HEADER , storeHeaderLocator);
		searchMenu.addHeader(CHARITY_HEADER, charityHeaderLocator);		
		searchMenu.addHeader(COLLECTED_HEADER, collectedHeaderLocator);
		searchMenu.addHeader(TOTAL_VALUE_HEADER, totalValueHeaderLocator);
		searchMenu.addHeader(TOTAL_WEIGHT_HEADER, totalWeightHeaderLocator);		
	}
		
}