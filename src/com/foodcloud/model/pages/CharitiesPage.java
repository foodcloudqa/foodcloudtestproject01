package com.foodcloud.model.pages;

import com.foodcloud.model.dialogs.CharityDialog;
import com.foodcloud.model.dialogs.InputTextField;
import com.foodcloud.model.dialogs.SelectField;
import com.foodcloud.server.FCTestNavigator;

public class CharitiesPage extends SearchPage  {

	public static String SUPPORT_FIELD_NAME = "Support Rating";
	public static String CHARITY_NAME_FIELD_NAME = "Name";
	public static String LOCATION_FIELD_NAME = "Location";
	public static String REGION_FIELD_NAME = "Region";
	public static String CONTACT_PHONE_FIELD_NAME = "Contact Phone";
	public static String EMAIL_FIELD_NAME = "Email";

	private String CHARITY_NAME_FIELD_LOCATOR = "//input[@st-search='name']";
	private String SUPPORT_FIELD_LOCATOR = "//select[@st-search='support']";
	private String REGION_FIELD_LOCATOR = "//input[@st-search='region']";
	private String LOCATION_FIELD_LOCATOR = "//input[@st-search='location']";
	private String CONTACT_PHONE_FIELD_LOCATOR = "//input[@st-search='phone']";
	private String EMAIL_FIELD_LOCATOR = "//input[@st-search='email']";

	
	public static String SUPPORT_HEADER = "Support";
	public static String RATING_HEADER = "Rating";
	public static String NAME_HEADER = "Name";
	public static String LOCATION_HEADER = "Location";
	public static String REGION_HEADER = "Region";
	public static String PHONE_HEADER = "Contact Phone";
	public static String EMAIL_HEADER = "Email";

	
	private String supportHeaderLocator = "//th[@support='Name']"; // "//th[@translate='Support']";
	private String ratingHeaderLocator = "//th[@translate='Rating']";
	private String nameHeaderLocator = "//th[@translate='Name']";
	private String locationHeaderLocator = "//th[@translate='Location']";
	private String regionHeaderLocator = "//th[@translate='Region']";
	private String phoneHeaderLocator = "//th[@translate='Contact_Phone']";
	private String emailHeaderLocator = "//th[@translate='Email']";
	
	public CharitiesPage(FCTestNavigator nav_driver) {
		super(nav_driver);
		setupMenuFields();
		
		setTitleLocator("//h2/descendant::span[@translate='Charities']");
	}

	
	private void setupMenuFields() {
		searchMenu.addField(SUPPORT_FIELD_NAME, new SelectField(nav,SUPPORT_FIELD_LOCATOR));
		searchMenu.addField(CHARITY_NAME_FIELD_NAME, new InputTextField(nav,CHARITY_NAME_FIELD_LOCATOR));
		searchMenu.addField(LOCATION_FIELD_NAME, new InputTextField(nav,LOCATION_FIELD_LOCATOR));
		searchMenu.addField(REGION_FIELD_NAME, new InputTextField(nav,REGION_FIELD_LOCATOR));
		searchMenu.addField(CONTACT_PHONE_FIELD_NAME, new InputTextField(nav,CONTACT_PHONE_FIELD_LOCATOR));
		searchMenu.addField(EMAIL_FIELD_NAME, new InputTextField(nav,EMAIL_FIELD_LOCATOR));
		// refresh		
		// clean up 
		searchMenu.addHeader(SUPPORT_HEADER, supportHeaderLocator);
		searchMenu.addHeader(RATING_HEADER, ratingHeaderLocator);
		searchMenu.addHeader(NAME_HEADER, nameHeaderLocator);
		searchMenu.addHeader(LOCATION_HEADER , locationHeaderLocator);
		searchMenu.addHeader(REGION_HEADER, regionHeaderLocator);
		searchMenu.addHeader(PHONE_HEADER , phoneHeaderLocator);
		searchMenu.addHeader(EMAIL_HEADER, emailHeaderLocator);		
	}
		
	
	/**
	 * Creates a new charity by clicking on the plus button and using CharitiesForm
	 * @return CharityDialog Object
	 */
	public CharityDialog createCharity() {
		
		String plusButtonLocator = "//i[@class='fa fa-plus ng-scope']";
		server.click(plusButtonLocator);
		
		String formLocator = "//form[@name='newOrgForm']";
		server.waitForElement(formLocator);

		return new CharityDialog(nav,formLocator );	
	}

	
}