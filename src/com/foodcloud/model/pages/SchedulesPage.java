package com.foodcloud.model.pages;

import com.foodcloud.model.dialogs.InputTextField;
import com.foodcloud.model.dialogs.SelectField;
import com.foodcloud.server.FCTestNavigator;

/**
 * This class is mapping in our test environment the Schedules Page from Copia 
 * Caracteristics:
 * 					* Progress bar functionality
 * 					* Search for schedule functionality
 * 					* refresh
 * @author Viviana
 *
 */
public class SchedulesPage extends SearchPage  {

	public static String DAY_DATE_FIELD_NAME = "Day/Date";
	public static String STORE_NUM_FIELD_NAME = "Store Number";

	private String DAY_DATE_FIELD_LOCATOR = "//select[@id='dowSelect']";
	private String STORE_NUM_FIELD_LOCATOR = "//input[@st-search='storenum']";

	//header
	public static String DAY_HEADER = "Day/Date";
	public static String DONOR_HEADER = "Donor";
	public static String STORE_HEADER = "Store";
	public static String CHARITY_HEADER = "Charity";
	public static String COLL_WIN_HEADER = "Collection Window";
	public static String STATUS_HEADER = "Status";
	
	private String dayHeaderLocator = "//th[@translate='DayDate']";  
	private String donorHeaderLocator = "//th[@translate='Donor']"; 
	private String storeHeaderLocator = "//th[@translate='Store_number']"; 
	private String charityHeaderLocator = "//th[@translate='Charity']"; 

	private String collectionHeaderLocator = "//th[@translate='Collection_Window']"; 
	private String statusHeaderLocator = "//th[@translate='Donation_Status']"; 
	
	public SchedulesPage(FCTestNavigator nav_driver) {
		super(nav_driver);
		setupMenuFields();
		
		setTitleLocator("//h2/descendant::span[@translate='Schedules']");
	}

	
	private void setupMenuFields() {
		searchMenu.addField(DAY_DATE_FIELD_NAME, new SelectField(nav,DAY_DATE_FIELD_LOCATOR));
		searchMenu.addField(DONOR_FIELD_NAME, new InputTextField(nav,DONOR_FIELD_LOCATOR));
		
		searchMenu.addField(STORE_NUM_FIELD_NAME, new InputTextField(nav,STORE_NUM_FIELD_LOCATOR));
		
		searchMenu.addField(CHARITY_FIELD_NAME, new InputTextField(nav,CHARITY_FIELD_LOCATOR));
		searchMenu.addField(STATUS_FIELD_NAME, new SelectField(nav,STATUS_FIELD_LOCATOR));
		// refresh		
		// clean up 
		searchMenu.addHeader(DAY_HEADER, dayHeaderLocator);
		searchMenu.addHeader(DONOR_HEADER, donorHeaderLocator);
		searchMenu.addHeader(STORE_HEADER, storeHeaderLocator);
		searchMenu.addHeader(CHARITY_HEADER, charityHeaderLocator);
		searchMenu.addHeader(COLL_WIN_HEADER, collectionHeaderLocator);
		searchMenu.addHeader(STATUS_HEADER, statusHeaderLocator);		
	}
		
}