package com.foodcloud.model.pages;

import com.foodcloud.server.FCTestNavigator;

/**
 * 
 * @author Viviana
 * Latest change 2017-08-23
 */
public class DashboardPage extends OptionMenuPage {

	private String titleLocator = "//div[@class='copia-top row']";  // Admin only

	public DashboardPage(FCTestNavigator nav_driver) {
		super(nav_driver);
		setTitleLocator(titleLocator);
		// populate menu item how??
		//User =  nav_driver.getUser()?
	}
	
}
