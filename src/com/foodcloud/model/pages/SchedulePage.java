package com.foodcloud.model.pages;

import com.foodcloud.server.FCTestNavigator;

/**
 * This class is mapping in our test environment the Schedule Page from Copia 
 * USED ONLY BY Store and Charity Users 
 * For Admin Schedul Page see @SchedulesPage
 * 
 * Caracteristics:
 * 					* Table with week days ans Stores / Charity associated
 * 					* refresh / Printo
 * 					* NO search
 * @author Viviana
 *
 */
public class SchedulePage extends OptionMenuPage   {

	private String titleLocator = "//div[@class='schedule']"; 
	
	public SchedulePage(FCTestNavigator nav_driver) {
		super(nav_driver);
	}

	public void waitUntilLoaded() {
		server.waitForElement(titleLocator);	
	}

	public boolean isDisplayed() {
		return server.isElementPresent(titleLocator);
	}
		
}