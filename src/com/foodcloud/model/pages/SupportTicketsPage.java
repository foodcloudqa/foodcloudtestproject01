package com.foodcloud.model.pages;

import com.foodcloud.model.dialogs.DateField;
import com.foodcloud.model.dialogs.InputTextField;
import com.foodcloud.model.dialogs.SelectField;
import com.foodcloud.server.FCTestNavigator;

public class SupportTicketsPage extends SearchPage  {

	public static String TICKET_DATE_FIELD_NAME = "Ticket Date";
	public static String TICKET_CATEGORY_FIELD_NAME = "Ticket Category";	
	
	private String TICKET_CATEGORY_FIELD_LOCATOR = "//input[@st-search='category']";	
	
	private String ADMIN_TITLE_LOCATOR ="//h2/span[@translate='Issues']";
	
	public static String TICKET_DATE_HEADER = "Ticket Date";
	public static String TICKET_CAT_HEADER ="Ticket Category";	
	public static String OPENED_HEADER = "Opened";	
	public static String DONOR_HEADER = "Donor";
	public static String STORE_HEADER = "Store number";
	public static String CHARITY_HEADER = "Charity";
	public static String STATUS_HEADER = "Status";
	
	private String dateHeaderLocator = "//th[@translate='Ticket_Date']";
	private String openedHeaderLocator = "//th[@translate='Opened']";
	private String categoryHeaderLocator = "//th[@translate='Issue_Type']";
	private String donorHeaderLocator = "//th[@translate='Donor']";
	private String storeHeaderLocator = "//th[@translate='Store_number']";
	private String statusHeaderLocator = "//th[@translate='Status']";
	private String charityHeaderLocator = "//th[@translate='Charity']";
	
	public SupportTicketsPage(FCTestNavigator nav_driver) {
		super(nav_driver);
		setupMenuFields();

		setTitleLocator(ADMIN_TITLE_LOCATOR);
	}

	private void setupMenuFields() {
		
		searchMenu.addField(TICKET_DATE_FIELD_NAME, new DateField(nav, DATE_FIELD_LOCATOR));
		searchMenu.addField(TICKET_CATEGORY_FIELD_NAME, new InputTextField(nav,TICKET_CATEGORY_FIELD_LOCATOR));		
		searchMenu.addField(DONOR_FIELD_NAME, new InputTextField(nav,DONOR_FIELD_LOCATOR));		
		searchMenu.addField(STORE_FIELD_NAME, new InputTextField(nav,STORE_FIELD_LOCATOR));				
		searchMenu.addField(CHARITY_FIELD_NAME, new InputTextField(nav,CHARITY_FIELD_LOCATOR));		
		searchMenu.addField(STATUS_FIELD_NAME, new SelectField(nav,STATUS_FIELD_LOCATOR));

		//placeholder 
		searchMenu.addHeader(TICKET_DATE_HEADER, dateHeaderLocator);
		searchMenu.addHeader(OPENED_HEADER, openedHeaderLocator);
		searchMenu.addHeader(TICKET_CAT_HEADER , categoryHeaderLocator);
		searchMenu.addHeader(DONOR_HEADER, donorHeaderLocator);
		searchMenu.addHeader(STORE_HEADER , storeHeaderLocator);
		searchMenu.addHeader(CHARITY_HEADER, charityHeaderLocator);		
		searchMenu.addHeader(STATUS_HEADER, statusHeaderLocator);
		
	}

	
	
	
	
}