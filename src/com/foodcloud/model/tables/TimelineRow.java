package com.foodcloud.model.tables;

import org.openqa.selenium.WebDriver;

import com.foodcloud.server.FCTestNavigator;

/**
 * Class that maps current Timeline row , with Panel and Badge 
 * 
 * Type:  Read only 
 * 
 * @author Viviana
 *
 */
public class TimelineRow {

	private String PANEL_LOCATOR = "//div[@class='timeline-panel']"; 

	private DonationResultTable baseTable;
	private WebDriver driver;
	private FCTestNavigator navigator;
	
	private String panelHeader;
	private String panelTimestamp;
	private String panelComment;
	private String badgeLocator; 

	private String rowLocator;

		public TimelineRow(DonationRow row, String locator) {

		navigator = row.getNavigator();
		driver = navigator.getDriver();
		setTimelineElements(locator);
		
	}

	private void setTimelineElements(String root) {
		// this.panelHeader = root +
		// "//ul[@class='timeline']/li[@ng-repeat='event in row.events']";

		this.rowLocator = root;

		this.panelHeader = PANEL_LOCATOR + "/div[@class='timeline-heading']/h4";
		this.panelTimestamp = PANEL_LOCATOR + "//small[@class='text-muted ng-binding']";
		this.panelComment = PANEL_LOCATOR + "/div[@class='timeline-panel']/div[@class='timeline-body']/p";

		this.badgeLocator = PANEL_LOCATOR + "/div[@class='timeline-badge info']";

	}
		
		public String getLocator(){
			return this.rowLocator;
		}

		
		
		/**
		 * reads the Panel Header title 
		 * @return String representing panel header title
		 */
		public String getPanelHeaderTitle() {
			return  driver.findElement(navigator.getServer().getLocatorType(panelHeader)).getText();
		}

		/**
		 * reads the Panel Header title 
		 * @return String representing panel header title
		 */
		public String getPanelTimestamp() {
			return  driver.findElement(navigator.getServer().getLocatorType(panelTimestamp)).getText();
		}


		public String getPanelComment() {
			boolean commentPresent = navigator.getServer().isElementPresent(panelComment);
			String comment = "";

			if (commentPresent) {
				comment = driver.findElement(navigator.getServer().getLocatorType(panelComment)).getText();				 
			} 
			return comment; 	
		}
		
}
