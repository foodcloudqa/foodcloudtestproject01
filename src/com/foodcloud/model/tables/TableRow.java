package com.foodcloud.model.tables;

public class TableRow {

	private DonationResultTable baseTable;
	private String rowLocator;

	public TableRow(DonationResultTable table, String locator) {
		setBaseTable(table);		
		setLocator(locator);
	}


	public String getRowLocator() {
		return rowLocator;
	}


	public DonationResultTable getBaseTable() {
		return baseTable;
	}


	private void setBaseTable(DonationResultTable tableOrigin) {
		this.baseTable= tableOrigin;
		tableOrigin.getServer();
	}

	private void setLocator(String rowLocator) {
		this.rowLocator = rowLocator;
	}

}
