package com.foodcloud.model.tables;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;


public class DonationInfo  {

	// //div[@class='modal-header']/h4[@translate='Donation_Details']

	private String CLOSE_BUTTON_LOCATOR = "//div[h4[@translate='Donation_Details']]/button[@class='close']";
	private String itemsLocator = "//table[@class='table table-condensed']/descendant::tr";
	private String BUTTON_LOCATOR;	
	
	private DonationResultTable baseTable;
	private WebDriver driver;
	private FCTestNavigator navigator;
	private FCTestServer server;

	
	public DonationInfo(FCTestNavigator nav, String info) {
		
		navigator = nav;
		driver = navigator.getDriver();
		BUTTON_LOCATOR = info; 
		server = navigator.getServer();

	}
	
	
	/**
	 * returns all the TimelineRows.
	 * Needs the timeline element to be already visible
	 *  
	 * @return
	 */
	public Map<String, String> getDonationItems() {
						
			List<WebElement> list = navigator.getDriver().findElements(
					server.getLocatorType(this.itemsLocator));
	
			HashMap<String, String> donationItems = new HashMap<String, String>();
			for (Iterator iterator = list.iterator(); iterator.hasNext();) {
				WebElement webElement = (WebElement) iterator.next();
				String itemName = webElement.findElement(server.getLocatorType("./td[1]/span/b")).getText();
				String itemQuantity = webElement.findElement(server.getLocatorType("./td[2]/span")).getText();

				donationItems.put(itemName, itemQuantity);				
			}
			return donationItems;
	}	

	/**
	 * Gets  the Donatation Id from Copia UI 
	 * @return String representing Donation Id 
	 */
	public String getDonationId(){
		System.out.println("Before picking donation id");

		//open??
		
		String idLocator ="//div[@class='modal-content']//table[2]//td[@class='ng-binding']";
		String donationId = navigator.getDriver().findElement(server.getLocatorType(idLocator)).getText();
		
		return donationId;
	}


	/**
	 * closes the dialog.
	 * @return 
	 */
	public DonationInfo close(){
		System.out.println("Before clicking on close dialog");

		navigator.getServer().click(CLOSE_BUTTON_LOCATOR);
		System.out.println("After clicking on close dialog");

		navigator.getServer().waitForActionToComplete();
		
		return this;
	}
	
	
	/**
	 * opens the dialog
	 */
	public DonationInfo open(){
		System.out.println("Before clicking on open dialog");
		
		navigator.getServer().click(BUTTON_LOCATOR);	
		System.out.println("After clicking on open dialog");

		navigator.getServer().waitForActionToComplete();

		return this;
	}

	
}
