package com.foodcloud.model.tables;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.foodcloud.model.dialogs.DropDownDialog;
import com.foodcloud.model.dialogs.OrgInfoDialog;
import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestUtils;

public class DonationRow extends TableRow implements Comparable<Object> {

	private String id;
	private String icon;
	private String date;
	private String time;
	private String info;
	private String donor;
	private String donor_codeId;
	private String charity;
	private String collectionWindow;
	private String status;

	private String dateLocator;
	private String timeLocator;
	private String donationInfoLocator;
	private String donorLocator;
	private String donorCodeLocator;
	private String collectionWindowLocator;
	private String statusLocator;
	private String timeLineLocator;
	
	private String charitySpaceLocator;
	private String charityLinkLocator;

	private String openPanelLocator;
	private String timeLinesLocator;
	
	private DonationResultTable baseTable;
	private WebDriver driver;
	private FCTestNavigator navigator;
	private String donorLinkLocator;
	private String actionsDropDownLocator;
	private String dropDown;

	public DonationRow(DonationResultTable table, String locator) {
		super(table, locator );
		
		navigator = table.getNavigator();
		driver = navigator.getDriver();
		setLocators(locator);
		parseRow();
		
	}

	
	/**
	 * Each row is different but has a number of column in common. 
	 * This method is setting the locators for each column 
	 *  	
	 * @param baseLocator
	 */
	private void setLocators(String baseLocator) {

		// $x("//div[@class='dropdown']")
		
		this.dateLocator = baseLocator +"/td[@id='don.date']";
		this.timeLocator = baseLocator +"/td[@id='don.posted']";
		this.donationInfoLocator = baseLocator +"/td[@id='don.details']";
		this.donorLocator = baseLocator +"/td[@id='don.donor']";
		this.donorLinkLocator = donorLocator + "/a[@class='ng-binding']";
		this.donorCodeLocator = baseLocator +"/td[@id='don.donorStoreId']";
		
		this.charitySpaceLocator = baseLocator +"/td/a[@class='ng-binding ng-scope']";
		this.charityLinkLocator = baseLocator +"/td/a[i[@ng-if='row.charitySupport']]";
		
		this.collectionWindowLocator  = baseLocator + "/td[@id='don.window']";
		this.statusLocator = baseLocator + "/td[@id='don.status']/span";
		
	 	this.actionsDropDownLocator = baseLocator +"/td[div[button[@id='actions']]]";
	 	this.timeLineLocator = baseLocator +"/td/div/span[@class='fa fa-clock-o']";

	 	this.openPanelLocator = baseLocator +"//div[@class='container-fluid ng-scope']";

	 	//NOTE: This locator is brittle: the UI does not link the timeline element to the belonging row but creates a new one 
	 	// Careful in opening multiple timelines (don't do it!)  
	 	this.timeLinesLocator = "//tr[@ng-if='row.expanded']/descendant::ul[@class='timeline']/li[@ng-repeat='event in row.events']";
	}	
		
	private void parseRow() {
		
		String date = driver.findElement(By.xpath(dateLocator)).getText();
		String time = driver.findElement(By.xpath(timeLocator)).getText();

		String donorName = driver.findElement(By.xpath(donorLocator)).getText();
	 	String donorNumber = driver.findElement(By.xpath(donorCodeLocator)).getText();
	 
	 	// NOTE: Charity link is optional!!! It is present only if the charity is scheduled with the donor store	 		
	    String charity = null;
	 	if(navigator.getServer().isElementPresent(charityLinkLocator)){
	 	     charity = driver.findElement(By.xpath(charityLinkLocator)).getText().trim();
	 	} else {
	 	     charity = driver.findElement(By.xpath(charitySpaceLocator)).getText().trim();
	 	}
	 		
	 	String collectionWindow = driver.findElement(By.xpath(collectionWindowLocator)).getText();
	 	String status = driver.findElement(By.xpath(statusLocator)).getText();

	 	//	setBaseTable(table);
		setIconLocator(timeLineLocator);
		setDate(date);
		setTime(time);
		setInfo(donationInfoLocator);
		setDonor(donorName);
		setDonorLinkLocator(donorLinkLocator);
		setDonorCode(donorNumber); 
		//Note: this can be null because of unscheduled charity
		setCharity(charity);
		setCollection(collectionWindow); 
		setStatus(status);
		setDropDown(actionsDropDownLocator);

	}

	private void setDropDown(String locator) {
		this.dropDown = locator;		
	}

	private void setStatus(String currentStatus) {
		this.status = currentStatus;
		
	}

	private void setCollection(String collectionTimeWindow) {
		this.collectionWindow = collectionTimeWindow;			
	}

	private void setCharity(String charityName) {
		this.charity = charityName;	
	}

	private void setDonorCode(String donorNumber) {
		this.donor_codeId = donorNumber;			
	}

	private void setDonor(String donorName) {
		this.donor = donorName;	
	}

	private void setDonorLinkLocator(String donorLocator) {
		this.donorLinkLocator = donorLocator;	
	}

	private void setInfo(String infoLocator) {
		this.info = infoLocator;	
	}

	private void setTime(String time) {
		this.time = time;
	}

	private void setDate(String date2) {
		this.date = date2;		
	}

	private void setBaseTable(DonationResultTable tableOrigin) {
		this.baseTable= tableOrigin;
		tableOrigin.getServer();
	}

	private void setIconLocator(String iconLocator) {
		this.icon = iconLocator;		
	}	
	
	public String getStatus() {
		return status;
	}
	
	public String getDate() {
		return date;
	}

	public String getTime() {
		return time;
	}

	public String getDropDown() {
		return this.dropDown;		
	}


	public DonationInfo getInfoDetails() {
		return new DonationInfo(navigator, info);
	}

	public String getDonor() {
		return donor;
	}

	public String getDonorCode() {
		return donor_codeId;
	}
	
	public OrgInfoDialog getDonorLinkDetails() {
		return new OrgInfoDialog(navigator, donorLinkLocator);
	}

	public String getCharity() {
		return charity;
	}

	public String getCollectionWindow() {
		return collectionWindow;
	}

	//click and get list of actions  
	public DropDownDialog getDropDownActions() {
		return new DropDownDialog(navigator, this);
	}
	

//	public OrgInfoDialog getDonorLinkDetails() {
//		return new OrgInfoDialog(navigator, donorLinkLocator);
//	}

	
	public DonationResultTable getBaseTable() {
		return baseTable;
	}

	public String getIconLocator() {
		return this.icon;		
	}

	public FCTestNavigator getNavigator() {
		return this.navigator;
	}

	/**
	 * opens Timeline 
	 * 
	 * @return true if it is open, false otherwise
	 */
	public boolean clickTimeline(boolean toOpen){
		boolean isOpened = false;

		boolean isAlreadyOpen = navigator.getServer().isElementPresent(	this.openPanelLocator);
		

		if(toOpen && !isAlreadyOpen){
			navigator.getServer().click(this.getIconLocator());

			navigator.getServer().waitForActionToComplete();

			isOpened = true;
		}
		return isOpened;
	}
	
	
	/**
	 * returns all the TimelineRows.
	 * Needs the timeline element to be already visible
	 *  
	 * @return
	 */
	public List<TimelineRow> getTimelineRows() {
						
		int size = navigator.getDriver().findElements(navigator.getServer().getLocatorType(this.timeLinesLocator))
				.size();

		if (size > 0) {
			ArrayList<TimelineRow> timelineRows = new ArrayList<TimelineRow>();
			for (int i = 0; i < size; i++) {
				timelineRows.add(new TimelineRow(this, this.timeLinesLocator + "[" + i + 1 + "]"));
			}
			return timelineRows;
		} else {
			return Collections.emptyList();
		}
	}

	
// get the id ? 	
//	public Map<String,TimelineRow> getTimelineMap() {
//		
//		int size = navigator.getDriver().findElements(navigator.getServer().getLocatorType(this.timeLinesLocator))
//				.size();
//
//		if (size > 0) {
//			ArrayList<TimelineRow> timelineRows = new ArrayList<TimelineRow>();
//			for (int i = 0; i < size; i++) {
//				timelineRows.add(new TimelineRow(this, this.timeLinesLocator + "[" + i + 1 + "]"));
//			}
//			return timelineRows;
//		} else {
//			return Collections.emptyList();
//		}
//	}

	
	/**
	 * Assumes that the Donation objects have same locale / format  
		 if date < dateo = current smaller -1 
		 if date = dateo = equal  0 
				 if time < timeo = smaller 				
				 if date > dateo = current bigger +1 
		 if date > dateo = current bigger +1 


	move the comparison 
	 */
	@Override
	public int compareTo(Object other) {
		int comparationResult = 0;

		DonationRow otherRow = (DonationRow) other;

		LocalDate.now(FCTestUtils.getZoneId(FCTestUtils.UTC));
		LocalDate currentDate = LocalDate.parse(this.getDate(), FCTestUtils.getCopiaUIDateFormatter());
		LocalDate otherDate = LocalDate.parse(otherRow.getDate(), FCTestUtils.getCopiaUIDateFormatter());

		if (currentDate.isBefore(otherDate)) {
			comparationResult = -1;
		}

		if (currentDate.isEqual(otherDate)) {

			LocalTime.now(FCTestUtils.getZoneId(FCTestUtils.UTC));
			LocalTime currentTime = LocalTime.parse(this.getTime(), FCTestUtils.getCopiaUITimeFormatter());
			LocalTime otherTime = LocalTime.parse(otherRow.getTime(), FCTestUtils.getCopiaUITimeFormatter());

			if (currentTime.isBefore(otherTime)) {
				comparationResult = -1;
			}

			if (currentTime.equals(otherTime)) {
				String nameCurrent = this.getDonor();
				String nameOther = otherRow.getDonor();

				String statusCurrent = this.getStatus();
				String statusOther = otherRow.getStatus();

				if (nameCurrent.equalsIgnoreCase(nameOther)) {

					if (statusCurrent.equalsIgnoreCase(statusOther)) {
						comparationResult = 0;
					}					
					else{
						comparationResult = +1;
					}													
				} else {
					char currentChar = nameCurrent.toLowerCase().toCharArray()[0];
					char otherChar = nameOther.toLowerCase().toCharArray()[0];

					if (currentChar < otherChar) {
						System.out.println("char 1 : " + currentChar);
						System.out.println("char 2 : " + otherChar);

						comparationResult = -1;
					} else if (currentChar > otherChar) {
						System.out.println("char 1 : " + currentChar);
						System.out.println("char 2 : " + otherChar);

						comparationResult = +1;
					}

				}
			}
			if (currentTime.isAfter(otherTime)) {
				comparationResult = +1;
			}
		}

		if (currentDate.isAfter(otherDate)) {
			comparationResult = +1;
		}
		return comparationResult;
	}
}
