package com.foodcloud.model.tables;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Collections;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.foodcloud.model.pages.DonationsPage;
import com.foodcloud.server.FCTestNavigator;
import com.foodcloud.server.FCTestServer;

public class DonationResultTable {

	private String tableLocator;
	private FCTestNavigator nav;
	private FCTestServer server;
	
	private Map<Integer,DonationRow> rowsMapWithIndex;
	
	private List<DonationRow> rowElements;
	private String tableBodyLocator = "./tbody[@id='don-data']";
	private String tableRowLocator =	tableBodyLocator + "/tr[@ng-repeat-start='row in donations']";
	
	private DonationsPage currentPage;
	private WebDriver driver;
	
	/**
	 * creates a new resultTable object. Expecting Xpath expression as table locator 
	 * @param navigator FoodCloudTestNavigator instance
	 * @param locator String representing Xpath locator
	 * @param page current page (should be Result page but also some read only page have ResultTable components)
	 */
	public DonationResultTable(FCTestNavigator navigator, String locator, DonationsPage page) {
		this.tableLocator = locator;
		this.nav = navigator;	
		this.server = navigator.getServer();	
		this.rowElements= new ArrayList<DonationRow>();
		this.currentPage = page; 
		this.driver = navigator.getDriver();	

		this.rowsMapWithIndex= new HashMap<Integer,DonationRow>();

	}
	
	/**
	 * checks if the results table is empty or not.
	 * @return true if empty, false otherwise
	 */
	public boolean isEmpty() {
		//checking 
		if (server.isElementPresent(tableLocator,tableRowLocator)) {
			return false;
		} else {
			return true;
		}
	}

	public DonationResultTable load() {
		server.waitForElement(tableLocator, tableBodyLocator);
		return this;
	}
	
	
	public DonationResultTable read() {			

		if(currentPage.isSpinnerSpinning()){
			currentPage.waitAfterSpinning();
		}
		
		if(!isEmpty()) {

			WebElement parentTable = driver.findElement(server.getLocatorType(tableLocator));
			
			List<WebElement> elements = parentTable.findElements(server.getLocatorType(tableRowLocator)); 
			
			//Debug			
			// find visible elements instead?
			System.out.println("DEBUG ResultTable locator: " +tableLocator);
			System.out.println("DEBUG ResultTable Row locator: " +tableRowLocator);		
			System.out.println("DEBUG ResultTable elements#: " + elements.size() );
			
			int index=1;
			for (Iterator<WebElement> iterator = elements.iterator(); iterator.hasNext();) {
				WebElement webEl = (WebElement) iterator.next();
				
				String baseLocator =  tableRowLocator.replace('.', '/') + "[" + index + "]";

				System.out.println("DEBUG Base locator: " + baseLocator);		

				DonationRow tr = new DonationRow(this, baseLocator);
				
				rowElements.add(tr);				
				
				rowsMapWithIndex.put(index, tr);				
				
				index = index+1;				
				
			}
		} else 
			{
			rowElements = Collections.emptyList();
			}
		
		return this;		
	}

	public FCTestServer getServer() {
		return this.server;
		
	}

	//TODO: Refactoring avoiding returning null
	public DonationRow getRow(int i) {
		if(!rowElements.isEmpty()) {
			return this.rowElements.get(i-1);
		} else
			return null;		
	}
	
	public List<DonationRow> getRowList() {		
		return this.rowElements;
	}

	public Map<Integer, DonationRow> getMap() {		
		return this.rowsMapWithIndex;
	}

	
	public FCTestNavigator getNavigator() {
		return this.nav;
		
	}

	public DonationsPage getPage() {
		return this.currentPage;		
	}

	
}
