INSERT INTO public.region(
	parent_id, name, full_name, idc, status, version, locale, tz)
	VALUES (2, 'IS', 'Iceland', 354, 'Active', 0, 'en', 'Atlantic/Reykjavik');

SELECT * FROM organization WHERE type='CG';

INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES (
		(SELECT id FROM public.region WHERE name='IS' ),
		'#Test Iceland Charity Group','1','Charity', 'None', '','CG','2018-09-25 10:50:11.584','New', 0, true, 0);	
	
INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES 	((
		 SELECT id FROM public.region WHERE name='IS' ),	
		'#Test UTC Ts Charity 1',
		(SELECT id FROM public.organization WHERE name='#Test Iceland Charity Group' ),'Charity', 'charity', '3540003541','C','2018-09-25 11:43:11.584','New', 0, true, 0  );	

INSERT INTO public.location(org_id, lat, "long", address, addr_code, country, version, timezone)
VALUES (
		 (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 1' ),
		 '64.132729','-21.920445','Bústaðavegur Reykjavík, Iceland','43MJ+3G','IS', 0 , 'Atlantic/Reykjavik');
						
INSERT INTO public.charity(
					 org_id, travel_radius, has_vehicle, accept_ubd, accept_chill, version, multi_donation, external_id, accept_frozen, rules)
					VALUES (
					         (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 1' ),
							 '100', true, true, true, 0, true, 'TEST_IS:000354001', true, 'BD');	
					
				INSERT INTO public.contact(
					party_id, party_type, rank, type, data, status, label, version)
					VALUES (
				            (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 1' ),
				            'O', 1, 'phone', '35423456777777780', 'Active', '#Test UTC Ts Charity 1', 0),
				    		(
				             (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 1' ),
				             'O', 2, 'email', 'charityTsTest1@iceland.is', 'Active', '#Test UTC Ts Charity 1', 0),
							(
				             (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 1' ),   
				              'O', 3, 'sms', '+35423456777777780', 'Active', '#Test UTC Ts Charity 1', 0),
				            (
				             (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 1' ),   
				             'O', 1, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test UTC Ts Charity 1', 0);
		
		
INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES 	((
		 SELECT id FROM public.region WHERE name='IS' ),	
		'#Test UTC Ts Charity 2',
		(SELECT id FROM public.organization WHERE name='#Test Iceland Charity Group' ),'Charity', 'charity', '354000752','C','2018-09-25 11:43:11.584','New', 0, true, 0  );	

INSERT INTO public.location(org_id, lat, "long", address, addr_code, country, version, timezone)
VALUES (
		 (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 2' ),
		 '64.132337',' -21.903113','Grænahlíð 17-11 Reykjavík, Iceland','354001','IS', 0 , 'Atlantic/Reykjavik');
						
INSERT INTO public.charity(
					 org_id, travel_radius, has_vehicle, accept_ubd, accept_chill, version, multi_donation, external_id, accept_frozen, rules)
					VALUES (
					         (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 2' ),
							 '100', true, true, true, 0, true, 'TEST_IS:000354002', true, 'BD');	
					
				INSERT INTO public.contact(
					party_id, party_type, rank, type, data, status, label, version)
					VALUES (
				            (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 2' ),
				            'O', 1, 'phone', '+48123456777777779', 'Active', '#Test UTC Ts Charity 2', 0),
				    		(
				             (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 2' ),
				             'O', 2, 'email', 'charityTsTest2@iceland.is', 'Active', '#Test UTC Ts Charity 2', 0),
							(
				             (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 2' ),   
				              'O', 3, 'sms', '+48123456777777779', 'Active', '#Test UTC Ts Charity 2', 0),
				            (
				             (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 2' ),   
				             'O', 3, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test UTC Ts Charity 2', 0);
				 
				 
INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES 	((
		 SELECT id FROM public.region WHERE name='IS' ),	
		'#Test UTC Ts Charity 3',
		(SELECT id FROM public.organization WHERE name='#Test Iceland Charity Group' ),'Charity', 'charity', '340000753','C','2018-09-25 11:43:11.584','New', 0, true, 0  );	

INSERT INTO public.location(org_id, lat, "long", address, addr_code, country, version, timezone)
VALUES (
		 (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 3' ),
		 '64.134689','-21.902151','Skaftahlíð 26-38 Reykjavík, Iceland','28009','IS', 0 , 'Atlantic/Reykjavik');
						
INSERT INTO public.charity(
					 org_id, travel_radius, has_vehicle, accept_ubd, accept_chill, version, multi_donation, external_id, accept_frozen, rules)
					VALUES (
					         (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 3' ),
							 '100', true, true, true, 0, true, 'TEST_IS:000354003', true, 'BD');	
					
				INSERT INTO public.contact(
					party_id, party_type, rank, type, data, status, label, version)
					VALUES (
				            (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 3' ),
				            'O', 1, 'phone', '+48123456777777780', 'Active', '#Test UTC Ts Charity 3', 0),
				    		(
				             (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 3' ),
				             'O', 2, 'email', 'charityTsTest3@iceland.is', 'Active', '#Test UTC Ts Charity 3', 0),
							(
				             (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 3' ),   
				              'O', 3, 'sms', '+48123456777777780', 'Active', '#Test UTC Ts Charity 3', 0),
				            (
				             (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 3' ),   
				             'O', 3, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test UTC Ts Charity 3', 0);

				             
				             
				             
				UPDATE organization 
				SET  status='Active'
				WHERE type='CG' and id = (SELECT id FROM public.organization WHERE name='#Test Iceland Charity Group' );
				

				UPDATE organization 
				SET  status='Active'
				WHERE type='C' and id = (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 1' );

				
				UPDATE organization 
				SET  status='Active'
				WHERE type='C' and id = (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 2' );
							
				UPDATE organization 
				SET  status='Active'
				WHERE type='C' and id = (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 3' );
								

INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES (
		(SELECT id FROM public.region WHERE name='IS' ),
		'#Test Iceland Donor Group','1','Retail', 'None', '','DG','2018-09-25 10:50:11.584','New', 0, true, 0);	
	

INSERT INTO public.external_category( org_id, code, external_code, units)
	VALUES ( 
			(SELECT id FROM organization WHERE name='#Test Iceland Donor Group'),
		'BKR', 'Bakery', 'Tray'),
		( 
			(SELECT id FROM organization WHERE name='#Test Iceland Donor Group'),
		'MTP', 'Meat','Tray'),
		( 
			(SELECT id FROM organization WHERE name='#Test Iceland Donor Group'),
		'FSH', 'Fish','Tray'),
		( 
			(SELECT id FROM organization WHERE name='#Test Iceland Donor Group'),
		'EGG', 'Eggs','Tray');
		

	
INSERT INTO public.organization(
	 region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
	VALUES (
        	(SELECT id FROM public.region WHERE name='IS' ),
        	'#Test UTC Ts Donor 1',
            (SELECT id FROM public.organization WHERE name='#Test Iceland Donor Group' ),                          
        	'Retail', 'None', '','DS','2018-09-25 10:50:05.584','New', 0, true, 0  );

 INSERT INTO public.donor(
	 org_id, external_id, post_by, collect_start, collect_end, version, group_name, rules)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test UTC Ts Donor 1' ),                          
            'TEST_IS:35401', '01:30', '08:00', '11:00', 0, 'TEST.UOM', 'BD;AM;UWE;');

INSERT INTO public.location(
	org_id, lat, "long", address, addr_code, country, version, timezone)
	VALUES (
        (SELECT id FROM organization WHERE name='#Test UTC Ts Donor 1'), 
            '64.1394513', '-21.926433', 'Tryggvagata 1, 101 Reykjavík, Iceland', '43X61', 
         'Iceland', 1, 'Atlantic/Reykjavik');			
			
			
 INSERT INTO public.contact(
	 party_id, party_type, rank, type, data, status, label, version)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test UTC Ts Donor 1' ),                          
            'O', 1, 'phone', '+35812345678882', 'Active', '#Test UTC Ts Donor 1', 0),
    		(
 		    (SELECT id FROM public.organization WHERE name='#Test UTC Ts Donor 1' ),                          
            'O', 2, 'email', 'donorTsTest1@iceland.is', 'Active', '#Test UTC Ts Donor 1', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test UTC Ts Donor 1' ),                          
			 'O', 1, 'push', 'FC:77te7s77-7tes-7t7t-7est-7t5583723te8', 'Active', '#Test UTC Ts Donor 1', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test UTC Ts Donor 1'),                          
            'O', 3, 'sms', '+35812345678882', 'Active', '#Test UTC Ts Donor 1', 0);
			
			
INSERT INTO public.schedule(
	org_id, party_id, day, start_time, end_time, status, version)
	VALUES (
        (SELECT id FROM public.organization WHERE name='#Test UTC Ts Donor 1' ),
        (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 2' ),                        
        1, '07:00', '11:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test UTC Ts Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 2' ),                      
        2, '07:00', '11:00', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test UTC Ts Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 2' ),                      
		3, '08:00', '11:30', 'Assigned', 0),        
    (
        (SELECT id FROM public.organization WHERE name='#Test UTC Ts Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 2' ),                      
         4, '08:00', '11:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test UTC Ts Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 2' ),                      
         5, '08:00', '11:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test UTC Ts Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 2' ),                      
         6, '08:00', '11:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test UTC Ts Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 3' ),                      
         7, '08:00', '11:30', 'Assigned', 0);



		 
UPDATE organization
SET status = 'Active'
WHERE id = (SELECT id FROM organization WHERE type='DG' and name ='#Test Iceland Donor Group');

UPDATE organization
SET status = 'Active'
WHERE id = (SELECT id FROM organization WHERE type='DS' and name ='#Test UTC Ts Donor 1');



INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='IS'),	
  		   'charityTsTest1@iceland.is','#Test UTC Ts Charity 1','Charity','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 1' ),
			'2018-09-25 13:55:29.883138','en','Active',0,'#Test UTC Ts Charity 1', '+393472478613', '');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='IS'),	
  		   'charityTsTest2@iceland.is','#Test UTC Ts Charity 2','Charity','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 2' ),
			'2018-09-25 13:55:29.883138','en','Active',0,'#Test UTC Ts Charity 2', '+393472478613', '');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='IS'),	
  		   'charityTsTest3@iceland.is','#Test UTC Ts Charity 3','Charity','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test UTC Ts Charity 3' ),
			'2018-09-25 13:55:29.883138','en','Active',0,'#Test UTC Ts Charity 3', '+393472478613', '');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='IS'),	
  		   'donorTsTest1@iceland.is','#Test UTC Ts Donor 1','Store','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test UTC Ts Donor 1' ),
			'2018-09-25 13:55:29.883138','en','Active',0,'#Test UTC Ts Donor 1', '+393472478613', '');


			
INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='IS'),	
  		   'is.test.admin@foodcloud.ie','Iceland Test Admin','Admin','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  1,
			'2018-09-25 10:26:29.883138','es','Active',0,'Iceland', '+3481172478613', '');
			
