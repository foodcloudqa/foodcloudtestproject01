-- Canada region must not exist already 

SELECT * FROM organization WHERE type='CG';

INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES (
		(SELECT id FROM public.region WHERE name='CA' ),
		'#Test CA Roll Ch Group','1','Charity', 'None', '','CG','2018-10-10 13:00:11.584','New', 0, true, 0);	
	
--  charity n.1
		
INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES 	((
		 SELECT id FROM public.region WHERE name='CA' ),	
		'#Test Canada Rv Charity 1',
		(SELECT id FROM public.organization WHERE name='#Test CA Roll Ch Group' ),'Charity', 'CA_T_C_R', '100001001','C','2018-10-10 11:43:11.584','New', 0, true, 0  );	

		
INSERT INTO public.location(org_id, lat, "long", address, addr_code, country, version, timezone)
VALUES (
		 (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 1' ),
		 '43.683538', '-79.390026','84-10 Summerhill Ave Toronto, ON M4T 1A8, Canada','ON M4T 1A8','CA', 0 , 'America/Toronto');
						
INSERT INTO public.charity(
					 org_id, travel_radius, has_vehicle, accept_ubd, accept_chill, version, multi_donation, external_id, accept_frozen, rules)
					VALUES (
					         (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 1' ),
							 '100', true, true, true, 0, true, 'TEST_CA:100001001', true, 'BD');	

INSERT INTO public.contact(
			party_id, party_type, rank, type, data, status, label, version)
			VALUES (
				    (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 1' ),
				    'O', 1, 'phone', '+102345611111180', 'Active', '#Test Canada Rv Charity 1', 0),
				    (
				     (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 1' ),
				     'O', 2, 'email', 'charityRvTest1@canada.ca', 'Active', '#Test Canada Rv Charity 1', 0),
					(
		             (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 1' ),   
		             'O', 3, 'sms', '+102345611111180', 'Active', '#Test Canada Rv Charity 1', 0),
		            (
		            (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 1' ),   
				     'O', 1, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test Canada Rv Charity 1', 0);

--  charity n.2
INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES 	((
		 SELECT id FROM public.region WHERE name='CA' ),	
		'#Test Canada Rv Charity 2',
		(SELECT id FROM public.organization WHERE name='#Test CA Roll Ch Group' ),'Charity', 'CA_T_C_R', '100001002','C','2018-10-10 11:43:11.584','New', 0, true, 0  );	

		
INSERT INTO public.location(org_id, lat, "long", address, addr_code, country, version, timezone)
VALUES (
		 (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 2' ),
		 '43.681907', '-79.377698','66-2 Highland Ave Toronto, ON M4W 2A3, Canada','ON M4W 2A3','CA', 0 , 'America/Toronto');
						
INSERT INTO public.charity(
					 org_id, travel_radius, has_vehicle, accept_ubd, accept_chill, version, multi_donation, external_id, accept_frozen, rules)
					VALUES (
					         (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 2' ),
							 '100', true, true, true, 0, true, 'TEST_CA:100001002', true, 'BD');	

INSERT INTO public.contact(
			party_id, party_type, rank, type, data, status, label, version)
			VALUES (
				    (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 2' ),
				    'O', 1, 'phone', '+102345611111182', 'Active', '#Test Canada Rv Charity 2', 0),
				    (
				     (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 2' ),
				     'O', 2, 'email', 'charityRvTest2@canada.ca', 'Active', '#Test Canada Rv Charity 2', 0),
					(
		             (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 2' ),   
		             'O', 3, 'sms', '+102345611111182', 'Active', '#Test Canada Rv Charity 2', 0),
		            (
		            (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 2' ),   
				     'O', 1, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test Canada Rv Charity 2', 0);

				     
--  charity n.3
INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES 	((
		 SELECT id FROM public.region WHERE name='CA' ),	
		'#Test Canada Rv Charity 3',
		(SELECT id FROM public.organization WHERE name='#Test CA Roll Ch Group' ),'Charity', 'CA_T_C_R', '100001003','C','2018-10-10 11:43:11.584','New', 0, true, 0  );	

		
INSERT INTO public.location(org_id, lat, "long", address, addr_code, country, version, timezone)
VALUES (
		 (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 3' ),
		 '43.689760', '-79.271837','69-107 Dunington Dr Scarborough, ON M1N 3E4, Canada','ON M1N 3E4','CA', 0 , 'America/Toronto');
						
INSERT INTO public.charity(
					 org_id, travel_radius, has_vehicle, accept_ubd, accept_chill, version, multi_donation, external_id, accept_frozen, rules)
					VALUES (
					         (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 3' ),
							 '100', true, true, true, 0, true, 'TEST_CA:100001003', true, 'BD');	

INSERT INTO public.contact(
			party_id, party_type, rank, type, data, status, label, version)
			VALUES (
				    (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 3' ),
				    'O', 1, 'phone', '+102345611111183', 'Active', '#Test Canada Rv Charity 3', 0),
				    (
				     (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 3' ),
				     'O', 2, 'email', 'charityRvTest3@canada.ca', 'Active', '#Test Canada Rv Charity 3', 0),
					(
		             (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 3' ),   
		             'O', 3, 'sms', '+102345611111183', 'Active', '#Test Canada Rv Charity 3', 0),
		            (
		            (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 3' ),   
				     'O', 1, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test Canada Rv Charity 3', 0);
				     				     
--  Activation
UPDATE organization 
SET  status='Active'
WHERE type='CG' and id = (SELECT id FROM public.organization WHERE name='#Test CA Roll Ch Group' );

UPDATE organization 
SET  status='Active'
WHERE type='C' and id = (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 1' );

UPDATE organization 
SET  status='Active'
WHERE type='C' and id = (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 2' );

UPDATE organization 
SET  status='Active'
WHERE type='C' and id = (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 3' );

     				     
--  Donor Group
INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version, rollover)
VALUES (
		(SELECT id FROM public.region WHERE name='CA' ),
		'#Test CA Roll Do Group','1','Retail', 'None', '','DG','2018-10-10 13:00:11.584','New', 0, true, 0, '14:00:00');	
	

INSERT INTO public.external_category( org_id, code, external_code, units)
	VALUES ( 
			(SELECT id FROM organization WHERE name='#Test CA Roll Do Group'),
		'BKR', 'Bakery', 'Tray'),
		( 
			(SELECT id FROM organization WHERE name='#Test CA Roll Do Group'),
		'MTP', 'Meat','Tray'),
		( 
			(SELECT id FROM organization WHERE name='#Test CA Roll Do Group'),
		'FSH', 'Fish','Tray'),
		( 
			(SELECT id FROM organization WHERE name='#Test CA Roll Do Group'),
		'EGG', 'Eggs','Tray');
		

--  donor n.1

INSERT INTO public.organization(
	 region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
	VALUES (
        	(SELECT id FROM public.region WHERE name='CA' ),
        	'#Test Canada Rv Donor 1',
            (SELECT id FROM public.organization WHERE name='#Test CA Roll Do Group' ),                          
        	'Retail', 'None', '','DS','2018-10-10 13:00:05.584','New', 0, true, 0  );

 INSERT INTO public.donor(
	 org_id, external_id, post_by, collect_start, collect_end, version, group_name, rules)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Canada Rv Donor 1' ),                          
            'TEST_CA:110001', '01:00', '08:00', '13:59', 0, 'TEST.CA', 'BD;AM;UWE;');

INSERT INTO public.location(
	org_id, lat, "long", address, addr_code, country, version, timezone)
	VALUES (
        (SELECT id FROM organization WHERE name='#Test Canada Rv Donor 1'), 
            '43.6876912','-79.5218168', '140 La Rose Ave, Etobicoke, ON M9P 1B2, Canada', 'ON M9P 1B2', 
         'CA', 0 , 'America/Toronto');
							
			
			
 INSERT INTO public.contact(
	 party_id, party_type, rank, type, data, status, label, version)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Canada Rv Donor 1' ),                          
            'O', 1, 'phone', '+18123456781110', 'Active', '#Test Canada Rv Donor 1', 0),
    		(
 		    (SELECT id FROM public.organization WHERE name='#Test Canada Rv Donor 1' ),                          
            'O', 2, 'email', 'donorRvTest1@canada.ca', 'Active', '#Test Canada Rv Donor 1', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Canada Rv Donor 1' ),                          
			 'O', 1, 'push', 'FC:77te7s77-7tes-7t7t-7est-7t5583723te8', 'Active', '#Test Canada Rv Donor 1', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Canada Rv Donor 1'),                          
            'O', 3, 'sms', '+35812345678882', 'Active', '#Test Canada Rv Donor 1', 0);
			

--  donor n.1 schedule
            
INSERT INTO public.schedule(
	org_id, party_id, day, start_time, end_time, status, version, cancel_after)
	VALUES (
        (SELECT id FROM public.organization WHERE name='#Test Canada Rv Donor 1' ),
        (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 2' ),                        
        1, '08:00', '13:59:00', 'Assigned', 0, '21:00:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Canada Rv Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 2' ),                      
        2, '08:00', '13:59:00', 'Assigned', 0, '21:00:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Canada Rv Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 2' ),                      
		3, '08:00', '13:59:00', 'Assigned', 0, '21:00:00'),        
    (
        (SELECT id FROM public.organization WHERE name='#Test Canada Rv Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 2' ),                      
         4, '08:00', '13:59:00', 'Assigned', 0, '21:00:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Canada Rv Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 2' ),                      
         5, '08:00', '13:59:00', 'Assigned', 0, '21:00:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Canada Rv Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 2' ),                      
         6, '08:00', '13:59:00', 'Assigned', 0, '21:00:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Canada Rv Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 3' ),                      
         7, '08:00', '13:59:00', 'Assigned', 0, '21:00:00');


--  donor Activation
		 
UPDATE organization
SET status = 'Active'
WHERE id = (SELECT id FROM organization WHERE type='DG' and name ='#Test CA Roll Do Group');

UPDATE organization
SET status = 'Active'
WHERE id = (SELECT id FROM organization WHERE type='DS' and name ='#Test Canada Rv Donor 1');



INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='CA'),	
  		   'charityRvTest1@canada.ca','#Test Canada Rv Charity 1','Charity','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 1' ),
			'2018-10-10 13:55:29.883138','en','Active',0,'#Test Canada Rv Charity 1', '+393472478613', '');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='CA'),	
  		   'charityRvTest2@canada.ca','#Test Canada Rv Charity 2','Charity','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 2' ),
			'2018-10-10 13:55:29.883138','en','Active',0,'#Test Canada Rv Charity 2', '+393472478613', '');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='CA'),	
  		   'charityRvTest3@canada.ca','#Test Canada Rv Charity 3','Charity','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Canada Rv Charity 3' ),
			'2018-10-10 13:55:29.883138','en','Active',0,'#Test Canada Rv Charity 3', '+393472478613', '');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='CA'),	
  		   'donorRvTest1@canada.ca','#Test Canada Rv Donor 1','Store','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Canada Rv Donor 1' ),
			'2018-10-10 13:00:29.883138','en','Active',0,'#Test Canada Rv Donor 1', '+393472478613', '');


			
INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='CA'),	
  		   'ca.test.admin@foodcloud.ie','Canada Test Admin','Admin','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  1,
			'2018-10-10 13:00:29.883138','en','Active',0,'Canada Admin', '+3481172478613', '');




				     