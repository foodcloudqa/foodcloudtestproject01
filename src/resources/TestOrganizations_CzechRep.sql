INSERT INTO public.region(
	parent_id, name, full_name, idc, status, version, locale, tz)
	VALUES (2, 'CZ', 'Czech Republic', 420, 'Active', 0, 'en', 'Europe/Prague');

SELECT * FROM organization WHERE type='CG';

--  charity Group

INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version, rollover)
VALUES (
		(SELECT id FROM public.region WHERE name='CZ' ),
		'#Test Czech Charity Group','1','Charity', 'None', '','CG','2018-09-26 10:50:11.584','New', 0, true, 0,'14:00:00');	


--  charity n.1

INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES 	((
		 SELECT id FROM public.region WHERE name='CZ' ),	
		'#Test Czech Ts Charity 1',
		(SELECT id FROM public.organization WHERE name='#Test Czech Charity Group' ),'Charity', 'charity', '4200004201','C','2018-09-26 11:43:11.584','New', 0, true, 0  );	

INSERT INTO public.location(org_id, lat, "long", address, addr_code, country, version, timezone)
VALUES (
		 (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 1' ),
		 '50.073724','14.464047','Vlašimská 7-3 101 00 Praha 10, Czechia','10100','CZ', 0 , 'Europe/Prague');
						
INSERT INTO public.charity(
					 org_id, travel_radius, has_vehicle, accept_ubd, accept_chill, version, multi_donation, external_id, accept_frozen, rules)
					VALUES (
					         (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 1' ),
							 '100', true, true, true, 0, true, 'TEST_CZ:000420001', true, 'BD');	
					
				INSERT INTO public.contact(
					party_id, party_type, rank, type, data, status, label, version)
					VALUES (
				            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 1' ),
				            'O', 1, 'phone', '42023456777777780', 'Active', '#Test Czech Ts Charity 1', 0),
				    		(
				             (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 1' ),
				             'O', 2, 'email', 'charityTsTest1@czech.cz', 'Active', '#Test Czech Ts Charity 1', 0),
							(
				             (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 1' ),   
				              'O', 3, 'sms', '+42023456777777780', 'Active', '#Test Czech Ts Charity 1', 0),
				            (
				             (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 1' ),   
				             'O', 1, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test Czech Ts Charity 1', 0);

--  charity n.2

INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES 	((
		 SELECT id FROM public.region WHERE name='CZ' ),	
		'#Test Czech Ts Charity 2',
		(SELECT id FROM public.organization WHERE name='#Test Czech Charity Group' ),'Charity', 'charity', '4200004202','C','2018-09-26 11:43:11.584','New', 0, true, 0  );	

INSERT INTO public.location(org_id, lat, "long", address, addr_code, country, version, timezone)
VALUES (
		 (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),
		 '50.073607','14.486609','Vilová 11-1 100 00 Strašnice, Czechia','10000','CZ', 0 , 'Europe/Prague');
						
INSERT INTO public.charity(
					 org_id, travel_radius, has_vehicle, accept_ubd, accept_chill, version, multi_donation, external_id, accept_frozen, rules)
					VALUES (
					         (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),
							 '100', true, true, true, 0, true, 'TEST_CZ:000420002', true, 'BD');	
					
				INSERT INTO public.contact(
					party_id, party_type, rank, type, data, status, label, version)
					VALUES (
				            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),
				            'O', 1, 'phone', '42023456777777782', 'Active', '#Test Czech Ts Charity 2', 0),
				    		(
				             (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),
				             'O', 2, 'email', 'charityTsTest2@czech.cz', 'Active', '#Test Czech Ts Charity 2', 0),
							(
				             (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),   
				              'O', 3, 'sms', '+42023456777777782', 'Active', '#Test Czech Ts Charity 2', 0),
				            (
				             (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),   
				             'O', 1, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test Czech Ts Charity 2', 0);


--  charity n.3

INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES 	((
		 SELECT id FROM public.region WHERE name='CZ' ),	
		'#Test Czech Ts Charity 3',
		(SELECT id FROM public.organization WHERE name='#Test Czech Charity Group' ),'Charity', 'charity', '4200004203','C','2018-09-26 11:43:11.584','New', 0, true, 0  );	

INSERT INTO public.location(org_id, lat, "long", address, addr_code, country, version, timezone)
VALUES (
		 (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),
		 '50.069937','14.492500','Svojetická 2-4 100 00 Strašnice, Czechia','10000','CZ', 0 , 'Europe/Prague');
						
INSERT INTO public.charity(
					 org_id, travel_radius, has_vehicle, accept_ubd, accept_chill, version, multi_donation, external_id, accept_frozen, rules)
					VALUES (
					         (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),
							 '100', true, true, true, 0, true, 'TEST_CZ:000420003', true, 'BD');	
					
				INSERT INTO public.contact(
					party_id, party_type, rank, type, data, status, label, version)
					VALUES (
				            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),
				            'O', 1, 'phone', '42023456777777783', 'Active', '#Test Czech Ts Charity 3', 0),
				    		(
				             (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),
				             'O', 2, 'email', 'charityTsTest3@czech.cz', 'Active', '#Test Czech Ts Charity 3', 0),
							(
				             (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),   
				              'O', 3, 'sms', '+42023456777777783', 'Active', '#Test Czech Ts Charity 3', 0),
				            (
				             (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),   
				             'O', 1, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test Czech Ts Charity 3', 0);

--  Charities Activation
				   
UPDATE organization 
SET  status='Active'
WHERE type='CG' and id = (SELECT id FROM public.organization WHERE name='#Test Czech Charity Group' );

UPDATE organization 
SET  status='Active'
WHERE type='C' and id = (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 1' );

				
UPDATE organization 
SET  status='Active'
WHERE type='C' and id = (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' );
							
UPDATE organization 
SET  status='Active'WHERE type='C' and id = (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' );
								

--  Donor Group

INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version, rollover)
VALUES (
		(SELECT id FROM public.region WHERE name='CZ' ),
		'#Test Czech Donor Group','1','Retail', 'None', '','DG','2018-09-26 10:50:11.584','New', 0, true, 0, '14:00:00');	
	

INSERT INTO public.external_category( org_id, code, external_code, units)
	VALUES ( 
			(SELECT id FROM organization WHERE name='#Test Czech Donor Group'),
		'BKR', 'Bakery', 'Tray'),
		( 
			(SELECT id FROM organization WHERE name='#Test Czech Donor Group'),
		'MTP', 'Meat','Tray'),
		( 
			(SELECT id FROM organization WHERE name='#Test Czech Donor Group'),
		'FSH', 'Fish','Tray'),
		( 
			(SELECT id FROM organization WHERE name='#Test Czech Donor Group'),
		'EGG', 'Eggs','Tray');
		

--  donor n.1

INSERT INTO public.organization(
	 region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
	VALUES (
        	(SELECT id FROM public.region WHERE name='CZ' ),
        	'#Test Czech Ts Donor 1',
            (SELECT id FROM public.organization WHERE name='#Test Czech Donor Group' ),                          
        	'Retail', 'None', '','DS','2018-09-26 10:50:05.584','New', 0, true, 0  );

 INSERT INTO public.donor(
	 org_id, external_id, post_by, collect_start, collect_end, version, group_name, rules)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 1' ),                          
            'TEST_CZ:420001', '01:00', '08:00', '13:59', 0, 'TEST.CZ', 'BD;AM;UWE;');

INSERT INTO public.location(
	org_id, lat, "long", address, addr_code, country, version, timezone)
	VALUES (
        (SELECT id FROM organization WHERE name='#Test Czech Ts Donor 1'), 
            '50.0701436','14.4851288', 'V Olšinách 1022/42, 100 00 Strašnice, Czechia', '10000', 
         'Czech', 1, 'Europe/Prague');			
			
			
 INSERT INTO public.contact(
	 party_id, party_type, rank, type, data, status, label, version)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 1' ),                          
            'O', 1, 'phone', '+35812345678882', 'Active', '#Test Czech Ts Donor 1', 0),
    		(
 		    (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 1' ),                          
            'O', 2, 'email', 'donorTsTest1@czech.cz', 'Active', '#Test Czech Ts Donor 1', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 1' ),                          
			 'O', 1, 'push', 'FC:77te7s77-7tes-7t7t-7est-7t5583723te8', 'Active', '#Test Czech Ts Donor 1', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 1'),                          
            'O', 3, 'sms', '+35812345678882', 'Active', '#Test Czech Ts Donor 1', 0);
			
			
INSERT INTO public.schedule(
	org_id, party_id, day, start_time, end_time, status, version, cancel_after)
	VALUES (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 1' ),
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),                        
        1, '08:00', '13:59:00', 'Assigned', 0, '21:00:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),                      
        2, '08:00', '13:59:00', 'Assigned', 0, '21:00:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),                      
		3, '08:00', '13:59:00', 'Assigned', 0, '21:00:00'),        
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),                      
         4, '08:00', '13:59:00', 'Assigned', 0, '21:00:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),                      
         5, '08:00', '13:59:00', 'Assigned', 0, '21:00:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),                      
         6, '08:00', '13:59:00', 'Assigned', 0, '21:00:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),                      
         7, '08:00', '13:59:00', 'Assigned', 0, '21:00:00');



		 
UPDATE organization
SET status = 'Active'
WHERE id = (SELECT id FROM organization WHERE type='DG' and name ='#Test Czech Donor Group');

UPDATE organization
SET status = 'Active'
WHERE id = (SELECT id FROM organization WHERE type='DS' and name ='#Test Czech Ts Donor 1');



INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='CZ'),	
  		   'charityTsTest1@czech.cz','#Test Czech Ts Charity 1','Charity','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 1' ),
			'2018-09-26 13:55:29.883138','en','Active',0,'#Test Czech Ts Charity 1', '+393472478613', '');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='CZ'),	
  		   'charityTsTest2@czech.cz','#Test Czech Ts Charity 2','Charity','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),
			'2018-09-26 13:55:29.883138','en','Active',0,'#Test Czech Ts Charity 2', '+393472478613', '');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='CZ'),	
  		   'charityTsTest3@czech.cz','#Test Czech Ts Charity 3','Charity','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),
			'2018-09-26 13:55:29.883138','en','Active',0,'#Test Czech Ts Charity 3', '+393472478613', '');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='CZ'),	
  		   'donorTsTest1@czech.cz','#Test Czech Ts Donor 1','Store','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 1' ),
			'2018-09-26 13:55:29.883138','en','Active',0,'#Test Czech Ts Donor 1', '+393472478613', '');


			
INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='CZ'),	
  		   'cz.test.admin@foodcloud.ie','Czech Test Admin','Admin','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  1,
			'2018-09-26 10:26:29.883138','en','Active',0,'Czech', '+3481172478613', '');
			
				             
			
			
INSERT INTO public.organization(
	 region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
	VALUES (
        	(SELECT id FROM public.region WHERE name='CZ' ),
        	'#Test Czech Ts Donor 2',
            (SELECT id FROM public.organization WHERE name='#Test Czech Donor Group' ),                          
        	'Retail', 'None', '','DS','2018-09-26 10:50:05.584','New', 0, true, 0  );

 INSERT INTO public.donor(
	 org_id, external_id, post_by, collect_start, collect_end, version, group_name, rules)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 2' ),                          
            'TEST_CZ:4200002', '01:30', '07:00', '12:30', 0, 'TEST.CZ', 'BD;');

INSERT INTO public.location(
	org_id, lat, "long", address, addr_code, country, version, timezone)
	VALUES (
        (SELECT id FROM organization WHERE name='#Test Czech Ts Donor 2'), 
            '50.0490085','14.4539256', 'Michelská 1006/59, 141 00 Praha 4, Czechia', '14100', 
         'CZ', 1, 'Europe/Prague');			
	
		
 INSERT INTO public.contact(
	 party_id, party_type, rank, type, data, status, label, version)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 2' ),                          
            'O', 1, 'phone', '+35812345678882', 'Active', '#Test Czech Ts Donor 2', 0),
    		(
 		    (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 2' ),                          
            'O', 2, 'email', 'donorTsTest2@czech.cz', 'Active', '#Test Czech Ts Donor 2', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 2' ),                          
			 'O', 1, 'push', 'FC:77te7s77-7tes-7t7t-7est-7t5583723te8', 'Active', '#Test Czech Ts Donor 2', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 2'),                          
            'O', 3, 'sms', '+35812345678882', 'Active', '#Test Czech Ts Donor 2', 0);
			

            
INSERT INTO public.schedule(
	org_id, party_id, day, start_time, end_time, status, version)
	VALUES (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 2' ),
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 1' ),                        
        1, '07:00', '13:45:00', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 2' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),                      
        2, '07:00', '13:45:00', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 2' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),                      
		3, '08:00', '13:45:00', 'Assigned', 0),        
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 2' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),                      
         4, '08:00', '13:45:00', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 2' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),                      
         5, '08:00', '13:45:00', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 2' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 1' ),                      
         6, '08:00', '13:45:00', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 2' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),                      
         7, '08:00', '13:45:00', 'Assigned', 0);


UPDATE organization
SET status = 'Active'
WHERE id = (SELECT id FROM organization WHERE type='DS' and name ='#Test Czech Ts Donor 2');
         
         
INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='CZ'),	
  		   'donorTsTest2@czech.cz','#Test Czech Ts Donor 2','Store','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 2' ),
			'2018-09-26 13:55:29.883138','en','Active',0,'#Test Czech Ts Donor 2', '+393472478613', '');

			
INSERT INTO public.organization(
	 region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
	VALUES (
        	(SELECT id FROM public.region WHERE name='CZ' ),
        	'#Test Czech Ts Donor 3',
            (SELECT id FROM public.organization WHERE name='#Test Czech Donor Group' ),                          
        	'Retail', '', '','DS','2018-09-26 10:50:05.584','New', 0, true, 0  );

 INSERT INTO public.donor(
	 org_id, external_id, post_by, collect_start, collect_end, version, group_name, rules)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 3' ),                          
            'TEST_CZ:4200003', '01:30', '20:00', '21:00', 0, 'TEST.CZ', 'BD;');

INSERT INTO public.location(
	org_id, lat, "long", address, addr_code, country, version, timezone)
	VALUES (
        (SELECT id FROM organization WHERE name='#Test Czech Ts Donor 3'), 
            '50.1374611','14.5135193', 'Beranových 97, 199 00 Praha 9 - Letňany, 199 00 Praha 18, Czechia', '19900', 
         'CZ', 1, 'Europe/Prague');			
	
		
 INSERT INTO public.contact(
	 party_id, party_type, rank, type, data, status, label, version)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 3' ),                          
            'O', 1, 'phone', '+35812345678883', 'Active', '#Test Czech Ts Donor 3', 0),
    		(
 		    (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 3' ),                          
            'O', 2, 'email', 'donorTsTest3@czech.cz', 'Active', '#Test Czech Ts Donor 3', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 3' ),                          
			 'O', 1, 'push', 'FC:77te7s77-7tes-7t7t-7est-7t5583723te8', 'Active', '#Test Czech Ts Donor 3', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 3'),                          
            'O', 3, 'sms', '+35812345678883', 'Active', '#Test Czech Ts Donor 3', 0);
			

            
 INSERT INTO public.schedule(
	org_id, party_id, day, start_time, end_time, status, version)
	VALUES (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 3' ),
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),                        
        1, '19:00', '20:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 3' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),                      
        2, '19:00', '21:00', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 3' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),                      
		3, '19:00', '20:30', 'Assigned', 0),        
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 3' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),                      
         4, '19:00', '20:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 3' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 2' ),                      
         5, '19:00', '20:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 3' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 1' ),                      
         6, '19:00', '20:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 3' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 3' ),                      
         7, '19:00', '20:30', 'Assigned', 0);
		 
UPDATE organization
SET status = 'Active'
WHERE id = (SELECT id FROM organization WHERE type='DS' and name ='#Test Czech Ts Donor 3');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='CZ'),	
  		   'donorTsTest3@czech.cz','#Test Czech Ts Donor 3','Store','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 3' ),
			'2018-09-26 13:55:29.883138','en','Active',0,'#Test Czech Ts Donor 3', '+393472478613', '');

		 
INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES 	((
		 SELECT id FROM public.region WHERE name='CZ' ),	
		'#Test Czech Ts Charity 4',
		(SELECT id FROM public.organization WHERE name='#Test Czech Charity Group' ),'Charity', 'ch_cz', '4200004204','C','2018-09-27 11:43:11.584','New', 0, true, 0  );	

INSERT INTO public.location(org_id, lat, "long", address, addr_code, country, version, timezone)
VALUES (
		 (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 4' ),
		 '50.127218','14.491185','Jablonecká 74 190 00 Praha 9, Czechia','19000','CZ', 0 , 'Europe/Prague');
						
INSERT INTO public.charity(
					 org_id, travel_radius, has_vehicle, accept_ubd, accept_chill, version, multi_donation, external_id, accept_frozen, rules)
					VALUES (
					         (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 4' ),
							 '100', true, true, true, 0, true, 'TEST_CZ:000420004', true, 'BD');	
					
				INSERT INTO public.contact(
					party_id, party_type, rank, type, data, status, label, version)
					VALUES (
				            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 4' ),
				            'O', 1, 'phone', '42023456777777784', 'Active', '#Test Czech Ts Charity 4', 0),
				    		(
				             (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 4' ),
				             'O', 2, 'email', 'charityTsTest4@czech.cz', 'Active', '#Test Czech Ts Charity 4', 0),
							(
				             (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 4' ),   
				              'O', 3, 'sms', '+42023456777777784', 'Active', '#Test Czech Ts Charity 4', 0),
				            (
				             (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 4' ),   
				             'O', 1, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test Czech Ts Charity 4', 0);



	UPDATE organization 
				SET  status='Active'
				WHERE type='C' and id = (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 4' );
				
				
	INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='CZ'),	
  		   'charityTsTest4@czech.cz','#Test Czech Ts Charity 4','Charity','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 4' ),
			'2018-09-27 13:55:29.883138','en','Active',0,'#Test Czech Ts Charity 4', '+393472478613', '');
			
			
INSERT INTO public.organization(
	 region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
	VALUES (
        	(SELECT id FROM public.region WHERE name='CZ' ),
        	'#Test Czech Ts Donor 4',
            (SELECT id FROM public.organization WHERE name='#Test Czech Donor Group' ),                          
        	'Retail', '', '','DS','2018-09-27 10:50:05.584','New', 0, true, 0  );

 INSERT INTO public.donor(
	 org_id, external_id, post_by, collect_start, collect_end, version, group_name, rules)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 4' ),                          
            'TEST_CZ:4200004', '01:30', '20:00', '21:00', 0, 'TEST.CZ', 'BD;PM;');

INSERT INTO public.location(
	org_id, lat, "long", address, addr_code, country, version, timezone)
	VALUES (
        (SELECT id FROM organization WHERE name='#Test Czech Ts Donor 4'), 
            '50.1244786','14.4804003', 'Střelničná 2270/46, 180 00 Praha 8-Libeň, Czechia', '18000', 
         'CZ', 1, 'Europe/Prague');			
	
		
 INSERT INTO public.contact(
	 party_id, party_type, rank, type, data, status, label, version)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 4' ),                          
            'O', 1, 'phone', '+35812345678884', 'Active', '#Test Czech Ts Donor 4', 0),
    		(
 		    (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 4' ),                          
            'O', 2, 'email', 'donorTsTest4@czech.cz', 'Active', '#Test Czech Ts Donor 4', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 4' ),                          
			 'O', 1, 'push', 'FC:77te7s77-7tes-7t7t-7est-7t5583723te8', 'Active', '#Test Czech Ts Donor 4', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 4'),                          
            'O', 3, 'sms', '+35812345678884', 'Active', '#Test Czech Ts Donor 4', 0);
			

            
 INSERT INTO public.schedule(
	org_id, party_id, day, start_time, end_time, status, version)
	VALUES (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 4' ),
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 4' ),                        
        1, '19:00', '20:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 4' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 4' ),                      
        2, '19:00', '21:00', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 4' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 4' ),                      
		3, '19:00', '20:30', 'Assigned', 0),        
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 4' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 4' ),                      
         4, '19:00', '20:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 4' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 4' ),                      
         5, '19:00', '20:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 4' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Charity 4' ),                      
         6, '19:00', '20:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 4' ),                        
        null,                      
         7, '19:00', '20:30', 'Available', 0);
		 
UPDATE organization
SET status = 'Active'
WHERE id = (SELECT id FROM organization WHERE type='DS' and name ='#Test Czech Ts Donor 4');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='CZ'),	
  		   'donorTsTest4@czech.cz','#Test Czech Ts Donor 4','Store','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Czech Ts Donor 4' ),
			'2018-09-27 13:55:29.883138','en','Active',0,'#Test Czech Ts Donor 4', '+393472478613', '');
			
