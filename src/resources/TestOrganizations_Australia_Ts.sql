
INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version, rollover)
VALUES (
		(SELECT id FROM public.region WHERE name='AU_NSW' ),
		'#Test Au Midnight Charity Group','1','Charity', 'None', '','CG','2018-10-04 10:50:11.584','New', 0, true, 0,'00:00:00');	


INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES 	((
		 SELECT id FROM public.region WHERE name='AU_NSW' ),	
		'#Test Au Midnight Charity 1',
		(SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity Group' ),'Charity', 'AU_S_C_T', '061010001','C','2018-10-04 11:43:11.584','New', 0, true, 0  );	

INSERT INTO public.location(org_id, lat, "long", address, addr_code, country, version, timezone)
VALUES (
		 (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 1' ),
		 '-32.931091', '151.779622','10-2 Barker St The Hill NSW 2300, Australia','2300','AU_NSW', 0 , 'Australia/NSW');


		 
INSERT INTO public.charity(
					 org_id, travel_radius, has_vehicle, accept_ubd, accept_chill, version, multi_donation, external_id, accept_frozen, rules)
					VALUES (
					         (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 1' ),
							 '100', true, true, true, 0, true, 'TEST_AU_C:061010001', true, 'BD');	
					
				INSERT INTO public.contact(
					party_id, party_type, rank, type, data, status, label, version)
					VALUES (
				            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 1' ),
				            'O', 1, 'phone', '+42023456777777780', 'Active', '#Test Au Midnight Charity 1', 0),
				    		(
				             (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 1' ),
				             'O', 2, 'email', 'charityKgTest1@australia.au', 'Active', '#Test Au Midnight Charity 1', 0),
							(
				             (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 1' ),   
				              'O', 3, 'sms', '+42023456777777780', 'Active', '#Test Au Midnight Charity 1', 0),
				            (
				             (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 1' ),   
				             'O', 1, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test Au Midnight Charity 1', 0);



INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES 	((
		 SELECT id FROM public.region WHERE name='AU_NSW' ),	
		'#Test Au Midnight Charity 2',
		(SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity Group' ),'Charity', 'AU_S_C_T', '061010002','C','2018-10-04 11:43:11.584','New', 0, true, 0  );	

INSERT INTO public.location(org_id, lat, "long", address, addr_code, country, version, timezone)
VALUES (
		 (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 2' ),
		 '-32.932133', '151.769447','57-99 Dawson St Cooks Hill NSW 2300, Australia','2300','AU_NSW', 0 , 'Australia/NSW');

						
		 
		 
INSERT INTO public.charity(
					 org_id, travel_radius, has_vehicle, accept_ubd, accept_chill, version, multi_donation, external_id, accept_frozen, rules)
					VALUES (
					         (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 2' ),
							 '100', true, true, true, 0, true, 'TEST_AU_C:061010002', true, 'BD');	
					
				INSERT INTO public.contact(
					party_id, party_type, rank, type, data, status, label, version)
					VALUES (
				            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 2' ),
				            'O', 1, 'phone', '42023456777777782', 'Active', '#Test Au Midnight Charity 2', 0),
				    		(
				             (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 2' ),
				             'O', 2, 'email', 'charityKgTest2@australia.au', 'Active', '#Test Au Midnight Charity 2', 0),
							(
				             (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 2' ),   
				              'O', 3, 'sms', '+42023456777777722', 'Active', '#Test Au Midnight Charity 2', 0),
				            (
				             (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 2' ),   
				             'O', 1, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test Au Midnight Charity 2', 0);


INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
VALUES 	((
		 SELECT id FROM public.region WHERE name='AU_NSW' ),	
		'#Test Au Midnight Charity 3',
		(SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity Group' ),'Charity', 'AU_S_C_T', '061010003','C','2018-10-04 11:43:11.584','New', 0, true, 0  );	

INSERT INTO public.location(org_id, lat, "long", address, addr_code, country, version, timezone)
VALUES (
		 (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' ),
		 '-32.936941', '151.752313','1-29 Cram St Merewether NSW 2291, Australia','2291','AU_NSW', 0 , 'Australia/NSW');
						
INSERT INTO public.charity(
					 org_id, travel_radius, has_vehicle, accept_ubd, accept_chill, version, multi_donation, external_id, accept_frozen, rules)
					VALUES (
					         (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' ),
							 '100', true, true, true, 0, true, 'TEST_AU_C:061010003', true, 'BD');	
					
				INSERT INTO public.contact(
					party_id, party_type, rank, type, data, status, label, version)
					VALUES (
				            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' ),
				            'O', 1, 'phone', '42023456777777783', 'Active', '#Test Au Midnight Charity 3', 0),
				    		(
				             (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' ),
				             'O', 2, 'email', 'charityKgTest3@australia.au', 'Active', '#Test Au Midnight Charity 3', 0),
							(
				             (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' ),   
				              'O', 3, 'sms', '+42023456777777783', 'Active', '#Test Au Midnight Charity 3', 0),
				            (
				             (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' ),   
				             'O', 1, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test Au Midnight Charity 3', 0);

				UPDATE organization 
				SET  status='Active'
				WHERE type='CG' and id = (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity Group' );
				

				UPDATE organization 
				SET  status='Active'
				WHERE type='C' and id = (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 1' );

				
				UPDATE organization 
				SET  status='Active'
				WHERE type='C' and id = (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 2' );
							
				UPDATE organization 
				SET  status='Active'
				WHERE type='C' and id = (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' );
								
-- Donor Group and all categories with  Kg as Unit Of Measure  AND the roll over at Midnight
				
INSERT INTO public.organization(region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version, rollover)
VALUES (
		(SELECT id FROM public.region WHERE name='AU_NSW' ),
		'#Test Au Midnight Donor Group','1','Retail', 'None', '','DG','2018-10-04 10:50:11.584','New', 0, true, 0, '00:00:00');	
	

INSERT INTO public.external_category( org_id, code, external_code, units)
	VALUES ( 
			(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
		'BKR', 'Bakery', 'Kg'),
		( 
			(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
		'MTP', 'Meat','Kg'),
		( 
			(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
		'FSH', 'Fish','Kg'),
		( 
			(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
		'EGG', 'Eggs','Kg'),
		(
			(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
			'GRY', 'Grocery', 'Kg'),
		(
			(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
		'RDY', 'Ready meals', 'Kg'),
		(
					(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
		'NOF', 'Non food Prod', 'Kg'),
		(
			(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
		'CHL', 'Chilled', 'Kg'),
		(
			(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
		'AMB', 'Canned and ambient food', 'Kg'),
		(
			(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
			'FFV', 'Fruit and veg', 'Kg'),
		( 
			(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
		'CNF', 'Sweets', 'Kg'),	
		( 
			(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
		'CRL', 'Cereals', 'Kg'),
		( 
			(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
		'FRT', 'Just fruit', 'Kg'),
	    ( 
			(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
		'VEG', 'Just vegetables', 'Kg'),
		( 
			(SELECT id FROM organization WHERE name='#Test Au Midnight Donor Group'),
		'OTH', 'Other items', 'Kg');

		

--  Donor 1
	
INSERT INTO public.organization(
	 region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
	VALUES (
        	(SELECT id FROM public.region WHERE name='AU_NSW' ),
        	'#Test Au Midnight Donor 1',
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor Group' ),                          
        	'Retail', '', '','DS','2018-10-04 10:50:05.584','New', 0, true, 0  );

 INSERT INTO public.donor(
	 org_id, external_id, post_by, collect_start, collect_end, version, group_name, rules)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 1' ),                          
            'TEST_AU_NSW:61010001', '01:30', '08:00', '11:00', 0, 'TEST.AU_NSW', 'BD;AM;UWE;');

INSERT INTO public.location(
	org_id, lat, "long", address, addr_code, country, version, timezone)
	VALUES (
        (SELECT id FROM organization WHERE name='#Test Au Midnight Donor 1'), 
            '32.9302786','151.775534', '131 Darby St, Cooks Hill NSW 2300, Australia', '2300', 
         'AU_NSW', 1, 'Australia/Sydney');			
			
			
 INSERT INTO public.contact(
	 party_id, party_type, rank, type, data, status, label, version)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 1' ),                          
            'O', 1, 'phone', '+35812345677771', 'Active', '#Test Au Midnight Donor 1', 0),
    		(
 		    (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 1' ),                          
            'O', 2, 'email', 'donorKgTest1@australia.au', 'Active', '#Test Au Midnight Donor 1', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 1' ),                          
			 'O', 1, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', 'T#Test Au Midnight Donor 1', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 1'),                          
            'O', 3, 'sms', '+35812345678882', 'Active', '#Test Au Midnight Donor 1', 0);
			
			
INSERT INTO public.schedule(
	org_id, party_id, day, start_time, end_time, status, version, cancel_after)
	VALUES (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 1' ),
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 1' ),                        
        1, '07:00', '11:59:00', 'Assigned', 0, '20:45:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 1' ),                        
        null,                      
        2, '07:00', '11:59:00', 'None', 0, '20:45:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 1' ),                        
        null,                      
		3, '08:00', '11:59:00', 'Available', 0, '20:45:00'),        
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 1' ),                      
         4, '08:00', '11:59:00', 'Assigned', 0, '20:45:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 1' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 1' ),                      
         5, '08:00', '11:59:00', 'Assigned', 0, '20:45:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 1' ),                        
       null,                      
         6, '08:00', '11:59:00', 'None', 0, '20:45:00'),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 1' ),                        
		null,                      
         7, '08:00', '11:59:00', 'None', 0, '20:45:00');



		 
UPDATE organization
SET status = 'Active'
WHERE id = (SELECT id FROM organization WHERE type='DG' and name ='#Test Au Midnight Donor Group');

UPDATE organization
SET status = 'Active'
WHERE id = (SELECT id FROM organization WHERE type='DS' and name ='#Test Au Midnight Donor 1');



INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='AU_NSW'),	
  		   'charityKgTest1@australia.auv','#Test Au Midnight Charity 1','Charity','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 1' ),
			'2018-10-04 13:55:29.883138','en','Active',0,'#Test Au Midnight Charity 1', '+393472478613', '');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='AU_NSW'),	
  		   'charityKgTest2@australia.au','#Test Au Midnight Charity 2','Charity','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 2' ),
			'2018-10-04 13:55:29.883138','en','Active',0,'#Test Au Midnight Charity 2', '+393472478613', '');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='AU_NSW'),	
  		   'charityKgTest3@australia.au','#Test Au Midnight Charity 3','Charity','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' ),
			'2018-10-04 13:55:29.883138','en','Active',0,'#Test Au Midnight Charity 3', '+393472478613', '');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='AU_NSW'),	
  		   'donorKgTest1@australia.au','#Test Au Midnight Donor 1','Store','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 1' ),
			'2018-10-04 13:55:29.883138','en','Active',0,'#Test Au Midnight Donor 1', '+393472478613', '');

				             
			
INSERT INTO public.organization(
	 region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
	VALUES (
        	(SELECT id FROM public.region WHERE name='AU_NSW' ),
        	'#Test Au Midnight Donor 2',
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor Group' ),                          
        	'Retail', 'None', '','DS','2018-10-01 10:50:05.584','New', 0, true, 0  );

 INSERT INTO public.donor(
	 org_id, external_id, post_by, collect_start, collect_end, version, group_name, rules)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 2' ),                          
            'TEST_AU_NSW:61010002', '01:30', '07:00', '12:30', 0, 'TEST.AU_NSW', 'BD;');

INSERT INTO public.location(
	org_id, lat, "long", address, addr_code, country, version, timezone)
	VALUES (
        (SELECT id FROM organization WHERE name='#Test Au Midnight Donor 2'), 
            '50.0705043','14.436651', 'Šmilovského 1427/12, 120 00 Praha 2, AU_NSW', '12000', 
         'AU_NSW', 1, 'Australia/NSW');			
	
		
 INSERT INTO public.contact(
	 party_id, party_type, rank, type, data, status, label, version)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 2' ),                          
            'O', 1, 'phone', '+35812345678882', 'Active', '#Test Au Midnight Donor 2', 0),
    		(
 		    (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 2' ),                          
            'O', 2, 'email', 'donorKgTest2@australia.au', 'Active', '#Test Au Midnight Donor 2', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 2' ),                          
			 'O', 1, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test Au Midnight Donor 2', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 2'),                          
            'O', 3, 'sms', '+35812345678882', 'Active', '#Test Au Midnight Donor 2', 0);
			

            
INSERT INTO public.schedule(
	org_id, party_id, day, start_time, end_time, status, version)
	VALUES (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 2' ),
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 1' ),                        
        1, '07:00', '11:59:00', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 2' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' ),                      
        2, '07:00', '11:59:00', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 2' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 2' ),                      
		3, '08:00', '11:59:00', 'Assigned', 0),        
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 2' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' ),                      
         4, '08:00', '11:59:00', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 2' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 2' ),                      
         5, '08:00', '11:59:00', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 2' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 1' ),                      
         6, '08:00', '11:59:00', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 2' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' ),                      
         7, '08:00', '11:59:00', 'Assigned', 0);


UPDATE organization
SET status = 'Active'
WHERE id = (SELECT id FROM organization WHERE type='DS' and name ='#Test Au Midnight Donor 2');
         
         
INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='AU_NSW'),	
  		   'donorKgTest2@australia.au','#Test Au Midnight Donor 2','Store','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 2' ),
			'2018-10-04 13:55:29.883138','en','Active',0,'#Test Au Midnight Donor 2', '+393472478613', '');

			
INSERT INTO public.organization(
	 region_id, name, parent_id, sector, reg_type, reg_id, type, created, status, rating, support, version)
	VALUES (
        	(SELECT id FROM public.region WHERE name='AU_NSW' ),
        	'#Test Au Midnight Donor 3',
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor Group' ),                          
        	'Retail', '', '','DS','2018-10-04 10:50:05.584','New', 0, true, 0  );

 INSERT INTO public.donor(
	 org_id, external_id, post_by, collect_start, collect_end, version, group_name, rules)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 3' ),                          
            'TEST_AU_NSW:4420003', '01:30', '20:00', '21:00', 0, 'TEST.AU_NSW', 'BD;');

INSERT INTO public.location(
	org_id, lat, "long", address, addr_code, country, version, timezone)
	VALUES (
        (SELECT id FROM organization WHERE name='#Test Au Midnight Donor 3'), 
            '32.9349875','151.7567443', '188 Union St, The Junction NSW 2291, Australia', '2291', 
         'AU_NSW', 1, 'Australia/NSW');			
	
		
 INSERT INTO public.contact(
	 party_id, party_type, rank, type, data, status, label, version)
	VALUES (
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 3' ),                          
            'O', 1, 'phone', '+35812345678883', 'Active', '#Test Au Midnight Donor 3', 0),
    		(
 		    (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 3' ),                          
            'O', 2, 'email', 'donorKgTest3@australia.au', 'Active', '#Test Au Midnight Donor 3', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 3' ),                          
			 'O', 1, 'push', 'FC:120a66be-f04e-4bfe-b828-aa6981a55595', 'Active', '#Test Au Midnight Donor 3', 0),
			(
            (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 3'),                          
            'O', 3, 'sms', '+35812345678883', 'Active', '#Test Au Midnight Donor 3', 0);
			

            
 INSERT INTO public.schedule(
	org_id, party_id, day, start_time, end_time, status, version)
	VALUES (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 3' ),
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' ),                        
        1, '19:00', '20:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 3' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' ),                      
        2, '19:00', '20:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 3' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 2' ),                      
		3, '19:00', '20:30', 'Assigned', 0),        
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 3' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' ),                      
         4, '19:00', '20:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 3' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 2' ),                      
         5, '19:00', '20:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 3' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 1' ),                      
         6, '19:00', '20:30', 'Assigned', 0),
    (
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 3' ),                        
        (SELECT id FROM public.organization WHERE name='#Test Au Midnight Charity 3' ),                      
         7, '19:00', '20:30', 'Assigned', 0);
		 
UPDATE organization
SET status = 'Active'
WHERE id = (SELECT id FROM organization WHERE type='DS' and name ='#Test Au Midnight Donor 3');


INSERT INTO public."user"(
	 region_id, email, name, role, hashword, org_id, created, locale, status, version, comment, phone, rules)
	VALUES (
    		(SELECT id FROM public.region WHERE name='AU_NSW'),	sssssss
  		   'donorKgTest3@australia.au','#Test Au Midnight Donor 3','Store','764McwioK8d0pVf1HTUA1yoHQR1bwS03ggYj2u5CjYJB/IpiN6G0RqURH3yhdhPJ',
	    	  (SELECT id FROM public.organization WHERE name='#Test Au Midnight Donor 3' ),
			'2018-10-04 13:55:29.883138','en','Active',0,'#Test Au Midnight Donor 3', '+393472478613', '');

		 
